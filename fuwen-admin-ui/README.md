## 开发



# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```
## 错误解决
 Error: Cannot find module 'webpack/package.json'
npm install webpack --save-dev

最终解决方案
重新获取到 package.json ,npm install
浏览器访问 http://localhost:80

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## nginx 页面放刷新404 问题
try_files $uri $uri/ /index.html;

location / {
            root   html;
			try_files $uri $uri/ /index.html; # 这行必须加
            index  index.html index.htm;
        }