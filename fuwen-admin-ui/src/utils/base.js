// 服务器地址
// const BASE = 'http://192.168.1.7' // 本地连接
//const BASE = 'http://116.63.223.64' // test 环境
//const BASE = 'http://127.0.0.1' // 测试连接
const BASE = 'http://122.112.192.38'  // env 环境
// 配置域名
export const BASE_URL = BASE + ":15611/"

// 配置图片上传地址
export const UPLOAD_BASE_URL = BASE + ":15610/web/core/uploadImage"
