import request from '@/utils/request'

// 查询商品评价列表
export function listEvaluation(query) {
  return request({
    url: '/admin/evaluation/list',
    method: 'get',
    params: query
  })
}

// 查询商品评价详细
export function getEvaluation(id) {
  return request({
    url: '/admin/evaluation/' + id,
    method: 'get'
  })
}

// 新增商品评价
export function addEvaluation(data) {
  return request({
    url: '/admin/evaluation',
    method: 'post',
    data: data
  })
}

// 修改商品评价
export function updateEvaluation(data) {
  return request({
    url: '/admin/evaluation',
    method: 'put',
    data: data
  })
}

// 删除商品评价
export function delEvaluation(id) {
  return request({
    url: '/admin/evaluation/' + id,
    method: 'delete'
  })
}

// 导出商品评价
export function exportEvaluation(query) {
  return request({
    url: '/admin/evaluation/export',
    method: 'get',
    params: query
  })
}