import request from '@/utils/request'

// 查询商甲池列表
export function listShangjipool(query) {
  return request({
    url: '/admin/shangjipool/list',
    method: 'get',
    params: query
  })
}

// 查询商甲池详细
export function getShangjipool(id) {
  return request({
    url: '/admin/shangjipool/' + id,
    method: 'get'
  })
}

// 新增商甲池
export function addShangjipool(data) {
  return request({
    url: '/admin/shangjipool',
    method: 'post',
    data: data
  })
}

// 修改商甲池
export function updateShangjipool(data) {
  return request({
    url: '/admin/shangjipool',
    method: 'put',
    data: data
  })
}

// 删除商甲池
export function delShangjipool(id) {
  return request({
    url: '/admin/shangjipool/' + id,
    method: 'delete'
  })
}

// 导出商甲池
export function exportShangjipool(query) {
  return request({
    url: '/admin/shangjipool/export',
    method: 'get',
    params: query
  })
}