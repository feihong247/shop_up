import request from '@/utils/request'

// 查询订单与spu中间列表
export function listOsjoin(query) {
  return request({
    url: '/admin/osjoin/list',
    method: 'get',
    params: query
  })
}

// 查询订单与spu中间详细
export function getOsjoin(id) {
  return request({
    url: '/admin/osjoin/' + id,
    method: 'get'
  })
}

// 新增订单与spu中间
export function addOsjoin(data) {
  return request({
    url: '/admin/osjoin',
    method: 'post',
    data: data
  })
}

// 修改订单与spu中间
export function updateOsjoin(data) {
  return request({
    url: '/admin/osjoin',
    method: 'put',
    data: data
  })
}

// 删除订单与spu中间
export function delOsjoin(id) {
  return request({
    url: '/admin/osjoin/' + id,
    method: 'delete'
  })
}

// 导出订单与spu中间
export function exportOsjoin(query) {
  return request({
    url: '/admin/osjoin/export',
    method: 'get',
    params: query
  })
}