import request from '@/utils/request'

// 查询商铺环境展示列表
export function listShop_keyword(query) {
  return request({
    url: '/admin/shop_keyword/list',
    method: 'get',
    params: query
  })
}

// 查询商铺环境展示详细
export function getShop_keyword(id) {
  return request({
    url: '/admin/shop_keyword/' + id,
    method: 'get'
  })
}

// 新增商铺环境展示
export function addShop_keyword(data) {
  return request({
    url: '/admin/shop_keyword',
    method: 'post',
    data: data
  })
}

// 修改商铺环境展示
export function updateShop_keyword(data) {
  return request({
    url: '/admin/shop_keyword',
    method: 'put',
    data: data
  })
}

// 删除商铺环境展示
export function delShop_keyword(id) {
  return request({
    url: '/admin/shop_keyword/' + id,
    method: 'delete'
  })
}

// 导出商铺环境展示
export function exportShop_keyword(query) {
  return request({
    url: '/admin/shop_keyword/export',
    method: 'get',
    params: query
  })
}