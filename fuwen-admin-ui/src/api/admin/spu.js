import request from '@/utils/request'

// 查询商品列表
export function listSpu(query) {
  return request({
    url: '/admin/spu/list',
    method: 'get',
    params: query
  })
}

// 查询商品详细
export function getSpu(id) {
  return request({
    url: '/admin/spu/' + id,
    method: 'get'
  })
}

// 新增商品
export function addSpu(data) {
  return request({
    url: '/admin/spu',
    method: 'post',
    data: data
  })
}

// 修改商品
export function updateSpu(data) {
  return request({
    url: '/admin/spu',
    method: 'put',
    data: data
  })
}

// 删除商品
export function delSpu(id) {
  return request({
    url: '/admin/spu/' + id,
    method: 'delete'
  })
}

// 导出商品
export function exportSpu(query) {
  return request({
    url: '/admin/spu/export',
    method: 'get',
    params: query
  })
}