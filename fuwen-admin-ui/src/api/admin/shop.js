import request from '@/utils/request'

// 查询商铺列表
export function listShop(query) {
  return request({
    url: '/admin/shop/list',
    method: 'get',
    params: query
  })
}

// 查询商铺列表
export function listShopPolList(query) {
  return request({
    url: '/admin/pol/listShopPolList',
    method: 'get',
    params: query
  })
}

// 查询商铺详细
export function getShop(id) {
  return request({
    url: '/admin/shop/' + id,
    method: 'get'
  })
}

// 新增商铺
export function addShop(data) {
  return request({
    url: '/admin/shop',
    method: 'post',
    data: data
  })
}

// 修改商铺
export function updateShop(data) {
  return request({
    url: '/admin/shop',
    method: 'put',
    data: data
  })
}

// 删除商铺
export function delShop(id) {
  return request({
    url: '/admin/shop/' + id,
    method: 'delete'
  })
}

// 导出商铺
export function exportShop(query) {
  return request({
    url: '/admin/shop/export',
    method: 'get',
    params: query
  })
}
