import request from '@/utils/request'

// 查询业务日志列表
export function listLogs(query) {
  return request({
    url: '/admin/logs/list',
    method: 'get',
    params: query
  })
}

// 查询消证日志列表
export function listDisappear_logs(query) {
  return request({
    url: '/admin/disappear_logs/list',
    method: 'get',
    params: query
  })
}

// 查询积分日志列表
export function listIntegral_logs(query) {
  return request({
    url: '/admin/integral_logs/list',
    method: 'get',
    params: query
  })
}

// 用户释放总量
export function userLogsDisappearCount(query) {
  return request({
    url: '/admin/logs/userLogsDisappearCount',
    method: 'get',
    params: query
  })
}

// 用户 单次释放量
export function userLogsDisappearInfos(query) {
  return request({
    url: '/admin/logs/userLogsDisappearInfos',
    method: 'get',
    params: query
  })
}

// 用户总提现金额
export function userLogsIntegralCount(query) {
  return request({
    url: '/admin/logs/userLogsIntegralCount',
    method: 'get',
    params: query
  })
}

// 用户 总提现金额 infos
export function userLogsIntegralInfos(query) {
  return request({
    url: '/admin/logs/userLogsIntegralInfos',
    method: 'get',
    params: query
  })
}

// 查询业务日志详细
export function getLogs(id) {
  return request({
    url: '/admin/logs/' + id,
    method: 'get'
  })
}

// 新增业务日志
export function addLogs(data) {
  return request({
    url: '/admin/logs',
    method: 'post',
    data: data
  })
}

// 修改业务日志
export function updateLogs(data) {
  return request({
    url: '/admin/logs',
    method: 'put',
    data: data
  })
}

// 删除业务日志
export function delLogs(id) {
  return request({
    url: '/admin/logs/' + id,
    method: 'delete'
  })
}

// 导出业务日志
export function exportLogs(query) {
  return request({
    url: '/admin/logs/export',
    method: 'get',
    params: query
  })
}
