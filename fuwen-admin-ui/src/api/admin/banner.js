import request from '@/utils/request'

// 查询首页轮播列表
export function listBanner(query) {
  return request({
    url: '/admin/banner/list',
    method: 'get',
    params: query
  })
}

// 查询首页轮播详细
export function getBanner(id) {
  return request({
    url: '/admin/banner/' + id,
    method: 'get'
  })
}

// 新增首页轮播
export function addBanner(data) {
  return request({
    url: '/admin/banner',
    method: 'post',
    data: data
  })
}

// 修改首页轮播
export function updateBanner(data) {
  return request({
    url: '/admin/banner',
    method: 'put',
    data: data
  })
}

// 删除首页轮播
export function delBanner(id) {
  return request({
    url: '/admin/banner/' + id,
    method: 'delete'
  })
}

// 导出首页轮播
export function exportBanner(query) {
  return request({
    url: '/admin/banner/export',
    method: 'get',
    params: query
  })
}