import request from '@/utils/request'

// 查询常见问题列表
export function listProblem(query) {
  return request({
    url: '/admin/problem/list',
    method: 'get',
    params: query
  })
}

// 查询常见问题详细
export function getProblem(id) {
  return request({
    url: '/admin/problem/' + id,
    method: 'get'
  })
}

// 新增常见问题
export function addProblem(data) {
  return request({
    url: '/admin/problem',
    method: 'post',
    data: data
  })
}

// 修改常见问题
export function updateProblem(data) {
  return request({
    url: '/admin/problem',
    method: 'put',
    data: data
  })
}

// 删除常见问题
export function delProblem(id) {
  return request({
    url: '/admin/problem/' + id,
    method: 'delete'
  })
}

// 导出常见问题
export function exportProblem(query) {
  return request({
    url: '/admin/problem/export',
    method: 'get',
    params: query
  })
}

// 查询常见问题类目列表
export function listProblemcate(query) {
  return request({
    url: '/admin/problemcate/list',
    method: 'get',
    params: query
  })
}
