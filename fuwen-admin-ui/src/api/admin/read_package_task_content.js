import request from '@/utils/request'

// 查询红包关联任务用户完成情况列表
export function listRead_package_task_content(query) {
  return request({
    url: '/admin/read_package_task_content/list',
    method: 'get',
    params: query
  })
}

// 查询红包关联任务用户完成情况详细
export function getRead_package_task_content(id) {
  return request({
    url: '/admin/read_package_task_content/' + id,
    method: 'get'
  })
}

// 新增红包关联任务用户完成情况
export function addRead_package_task_content(data) {
  return request({
    url: '/admin/read_package_task_content',
    method: 'post',
    data: data
  })
}

// 修改红包关联任务用户完成情况
export function updateRead_package_task_content(data) {
  return request({
    url: '/admin/read_package_task_content',
    method: 'put',
    data: data
  })
}

// 删除红包关联任务用户完成情况
export function delRead_package_task_content(id) {
  return request({
    url: '/admin/read_package_task_content/' + id,
    method: 'delete'
  })
}

// 导出红包关联任务用户完成情况
export function exportRead_package_task_content(query) {
  return request({
    url: '/admin/read_package_task_content/export',
    method: 'get',
    params: query
  })
}