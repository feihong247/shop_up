import request from '@/utils/request'

// 查询足迹列表
export function listFootprint(query) {
  return request({
    url: '/admin/footprint/list',
    method: 'get',
    params: query
  })
}

// 查询足迹详细
export function getFootprint(id) {
  return request({
    url: '/admin/footprint/' + id,
    method: 'get'
  })
}

// 新增足迹
export function addFootprint(data) {
  return request({
    url: '/admin/footprint',
    method: 'post',
    data: data
  })
}

// 修改足迹
export function updateFootprint(data) {
  return request({
    url: '/admin/footprint',
    method: 'put',
    data: data
  })
}

// 删除足迹
export function delFootprint(id) {
  return request({
    url: '/admin/footprint/' + id,
    method: 'delete'
  })
}

// 导出足迹
export function exportFootprint(query) {
  return request({
    url: '/admin/footprint/export',
    method: 'get',
    params: query
  })
}