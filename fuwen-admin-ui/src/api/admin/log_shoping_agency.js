import request from '@/utils/request'

// 查询用户购买代理日志列表
export function listLog_shoping_agency(query) {
  return request({
    url: '/admin/log_shoping_agency/list',
    method: 'get',
    params: query
  })
}

// 查询用户购买代理日志详细
export function getLog_shoping_agency(id) {
  return request({
    url: '/admin/log_shoping_agency/' + id,
    method: 'get'
  })
}

// 新增用户购买代理日志
export function addLog_shoping_agency(data) {
  return request({
    url: '/admin/log_shoping_agency',
    method: 'post',
    data: data
  })
}

// 修改用户购买代理日志
export function updateLog_shoping_agency(data) {
  return request({
    url: '/admin/log_shoping_agency',
    method: 'put',
    data: data
  })
}

// 删除用户购买代理日志
export function delLog_shoping_agency(id) {
  return request({
    url: '/admin/log_shoping_agency/' + id,
    method: 'delete'
  })
}

// 导出用户购买代理日志
export function exportLog_shoping_agency(query) {
  return request({
    url: '/admin/log_shoping_agency/export',
    method: 'get',
    params: query
  })
}
