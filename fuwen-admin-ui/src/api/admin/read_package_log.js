import request from '@/utils/request'

// 查询红包抽取记录列表
export function listRead_package_log(query) {
  return request({
    url: '/admin/read_package_log/list',
    method: 'get',
    params: query
  })
}

// 查询红包抽取记录详细
export function getRead_package_log(id) {
  return request({
    url: '/admin/read_package_log/' + id,
    method: 'get'
  })
}

// 新增红包抽取记录
export function addRead_package_log(data) {
  return request({
    url: '/admin/read_package_log',
    method: 'post',
    data: data
  })
}

// 修改红包抽取记录
export function updateRead_package_log(data) {
  return request({
    url: '/admin/read_package_log',
    method: 'put',
    data: data
  })
}

// 删除红包抽取记录
export function delRead_package_log(id) {
  return request({
    url: '/admin/read_package_log/' + id,
    method: 'delete'
  })
}

// 导出红包抽取记录
export function exportRead_package_log(query) {
  return request({
    url: '/admin/read_package_log/export',
    method: 'get',
    params: query
  })
}