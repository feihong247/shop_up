import request from '@/utils/request'

// 查询竞猜列表
export function listGuess(query) {
  return request({
    url: '/admin/guess/list',
    method: 'get',
    params: query
  })
}

// 查询竞猜详细
export function getGuess(guessId) {
  return request({
    url: '/admin/guess/' + guessId,
    method: 'get'
  })
}

// 新增竞猜
export function addGuess(data) {
  return request({
    url: '/admin/guess',
    method: 'post',
    data: data
  })
}

// 修改竞猜
export function updateGuess(data) {
  return request({
    url: '/admin/guess',
    method: 'put',
    data: data
  })
}

// 删除竞猜
export function delGuess(guessId) {
  return request({
    url: '/admin/guess/' + guessId,
    method: 'delete'
  })
}

// 导出竞猜
export function exportGuess(query) {
  return request({
    url: '/admin/guess/export',
    method: 'get',
    params: query
  })
}