import request from '@/utils/request'

// 查询我的商铺收藏中间列表
export function listUsjoin(query) {
  return request({
    url: '/admin/usjoin/list',
    method: 'get',
    params: query
  })
}

// 查询我的商铺收藏中间详细
export function getUsjoin(id) {
  return request({
    url: '/admin/usjoin/' + id,
    method: 'get'
  })
}

// 新增我的商铺收藏中间
export function addUsjoin(data) {
  return request({
    url: '/admin/usjoin',
    method: 'post',
    data: data
  })
}

// 修改我的商铺收藏中间
export function updateUsjoin(data) {
  return request({
    url: '/admin/usjoin',
    method: 'put',
    data: data
  })
}

// 删除我的商铺收藏中间
export function delUsjoin(id) {
  return request({
    url: '/admin/usjoin/' + id,
    method: 'delete'
  })
}

// 导出我的商铺收藏中间
export function exportUsjoin(query) {
  return request({
    url: '/admin/usjoin/export',
    method: 'get',
    params: query
  })
}