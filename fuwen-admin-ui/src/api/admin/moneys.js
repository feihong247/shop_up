import request from '@/utils/request'

// 查询用户钱包列表
export function listMoneys(query) {
  return request({
    url: '/admin/moneys/list',
    method: 'get',
    params: query
  })
}

// 查询用户钱包详细
export function getMoneys(id) {
  return request({
    url: '/admin/moneys/' + id,
    method: 'get'
  })
}

// 新增用户钱包
export function addMoneys(data) {
  return request({
    url: '/admin/moneys',
    method: 'post',
    data: data
  })
}

// 修改用户钱包
export function updateMoneys(data) {
  return request({
    url: '/admin/moneys',
    method: 'put',
    data: data
  })
}

// 删除用户钱包
export function delMoneys(id) {
  return request({
    url: '/admin/moneys/' + id,
    method: 'delete'
  })
}

// 导出用户钱包
export function exportMoneys(query) {
  return request({
    url: '/admin/moneys/export',
    method: 'get',
    params: query
  })
}