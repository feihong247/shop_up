import request from '@/utils/request'

// 查询商品图片列列表
export function listImages(query) {
  return request({
    url: '/admin/images/list',
    method: 'get',
    params: query
  })
}

// 查询商品图片列详细
export function getImages(id) {
  return request({
    url: '/admin/images/' + id,
    method: 'get'
  })
}

// 新增商品图片列
export function addImages(data) {
  return request({
    url: '/admin/images',
    method: 'post',
    data: data
  })
}

// 修改商品图片列
export function updateImages(data) {
  return request({
    url: '/admin/images',
    method: 'put',
    data: data
  })
}

// 删除商品图片列
export function delImages(id) {
  return request({
    url: '/admin/images/' + id,
    method: 'delete'
  })
}

// 导出商品图片列
export function exportImages(query) {
  return request({
    url: '/admin/images/export',
    method: 'get',
    params: query
  })
}