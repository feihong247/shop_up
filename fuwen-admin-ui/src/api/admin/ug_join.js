import request from '@/utils/request'

// 查询用户竞猜中间列表
export function listUg_join(query) {
  return request({
    url: '/admin/ug_join/list',
    method: 'get',
    params: query
  })
}

// 查询用户竞猜中间详细
export function getUg_join(ugId) {
  return request({
    url: '/admin/ug_join/' + ugId,
    method: 'get'
  })
}

// 新增用户竞猜中间
export function addUg_join(data) {
  return request({
    url: '/admin/ug_join',
    method: 'post',
    data: data
  })
}

// 修改用户竞猜中间
export function updateUg_join(data) {
  return request({
    url: '/admin/ug_join',
    method: 'put',
    data: data
  })
}

// 删除用户竞猜中间
export function delUg_join(ugId) {
  return request({
    url: '/admin/ug_join/' + ugId,
    method: 'delete'
  })
}

// 导出用户竞猜中间
export function exportUg_join(query) {
  return request({
    url: '/admin/ug_join/export',
    method: 'get',
    params: query
  })
}