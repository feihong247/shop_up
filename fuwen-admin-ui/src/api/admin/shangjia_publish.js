import request from '@/utils/request'

// 分配技术商甲
export function dealTecShangJia(shangjia,userId) {
  return request({
    url: `/admin/shangjia/dealTecShangJia/${shangjia}/${userId}`,
    method: 'post'
  })
}

// 回收技术商甲
export function recycleTecShangJia(shangjia,userId) {
  return request({
    url: `/admin/shangjia/recycleTecShangJia/${shangjia}/${userId}`,
    method: 'post'
  })
}

// 分配管理商甲
export function dealManShangJia(shangjia,userId) {
  return request({
    url: `/admin/shangjia/dealManShangJia/${shangjia}/${userId}`,
    method: 'post'
  })
}

// 回收管理商甲
export function recycleManShangJia(shangjia,userId) {
  return request({
    url: `/admin/shangjia/recycleManShangJia/${shangjia}/${userId}`,
    method: 'post'
  })
}



// 分配原始商甲
export function dealOrgShangJia(shangjia,userId) {
  return request({
    url: `/admin/shangjia/dealOrgShangJia/${shangjia}/${userId}`,
    method: 'post'
  })
}

// 统一查询记录
export function dealShangJiaList(isAccount,params) {
  return request({
    url: `/admin/shangjia/dealShangJiaList/${isAccount}`,
    method: 'get',
    params:params
  })
}
