import request from '@/utils/request'

// 查询版本列表
export function listVersion(query) {
  return request({
    url: '/admin/version/list',
    method: 'get',
    params: query
  })
}

// 查询版本详细
export function getVersion(id) {
  return request({
    url: '/admin/version/' + id,
    method: 'get'
  })
}

// 新增版本
export function addVersion(data) {
  return request({
    url: '/admin/version',
    method: 'post',
    data: data
  })
}

// 修改版本
export function updateVersion(data) {
  return request({
    url: '/admin/version',
    method: 'put',
    data: data
  })
}

// 删除版本
export function delVersion(id) {
  return request({
    url: '/admin/version/' + id,
    method: 'delete'
  })
}

// 导出版本
export function exportVersion(query) {
  return request({
    url: '/admin/version/export',
    method: 'get',
    params: query
  })
}