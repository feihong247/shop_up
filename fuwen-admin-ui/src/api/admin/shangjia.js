import request from '@/utils/request'

// 查询商甲发行列表
export function listShangjia(query) {
  return request({
    url: '/admin/shangjia/list',
    method: 'get',
    params: query
  })
}

// 查询商甲发行详细
export function getShangjia(id) {
  return request({
    url: '/admin/shangjia/' + id,
    method: 'get'
  })
}

// 新增商甲发行
export function addShangjia(data) {
  return request({
    url: '/admin/shangjia',
    method: 'post',
    data: data
  })
}

// 修改商甲发行
export function updateShangjia(data) {
  return request({
    url: '/admin/shangjia',
    method: 'put',
    data: data
  })
}

// 删除商甲发行
export function delShangjia(id) {
  return request({
    url: '/admin/shangjia/' + id,
    method: 'delete'
  })
}

// 导出商甲发行
export function exportShangjia(query) {
  return request({
    url: '/admin/shangjia/export',
    method: 'get',
    params: query
  })
}