import request from '@/utils/request'

// 查询我的收藏中间列表
export function listUcjoin(query) {
  return request({
    url: '/admin/ucjoin/list',
    method: 'get',
    params: query
  })
}

// 查询我的收藏中间详细
export function getUcjoin(id) {
  return request({
    url: '/admin/ucjoin/' + id,
    method: 'get'
  })
}

// 新增我的收藏中间
export function addUcjoin(data) {
  return request({
    url: '/admin/ucjoin',
    method: 'post',
    data: data
  })
}

// 修改我的收藏中间
export function updateUcjoin(data) {
  return request({
    url: '/admin/ucjoin',
    method: 'put',
    data: data
  })
}

// 删除我的收藏中间
export function delUcjoin(id) {
  return request({
    url: '/admin/ucjoin/' + id,
    method: 'delete'
  })
}

// 导出我的收藏中间
export function exportUcjoin(query) {
  return request({
    url: '/admin/ucjoin/export',
    method: 'get',
    params: query
  })
}