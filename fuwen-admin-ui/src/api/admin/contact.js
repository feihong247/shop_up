import request from '@/utils/request'

// 查询关于我们列表
export function listContact(query) {
  return request({
    url: '/admin/contact/list',
    method: 'get',
    params: query
  })
}

// 查询关于我们详细
export function getContact(id) {
  return request({
    url: '/admin/contact/' + id,
    method: 'get'
  })
}

// 新增关于我们
export function addContact(data) {
  return request({
    url: '/admin/contact',
    method: 'post',
    data: data
  })
}

// 修改关于我们
export function updateContact(data) {
  return request({
    url: '/admin/contact',
    method: 'put',
    data: data
  })
}

// 删除关于我们
export function delContact(id) {
  return request({
    url: '/admin/contact/' + id,
    method: 'delete'
  })
}

// 导出关于我们
export function exportContact(query) {
  return request({
    url: '/admin/contact/export',
    method: 'get',
    params: query
  })
}