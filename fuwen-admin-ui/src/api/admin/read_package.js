import request from '@/utils/request'

// 查询红包类型列表
export function listRead_package(query) {
  return request({
    url: '/admin/read_package/list',
    method: 'get',
    params: query
  })
}

// 查询红包类型详细
export function getRead_package(id) {
  return request({
    url: '/admin/read_package/' + id,
    method: 'get'
  })
}

// 新增红包类型
export function addRead_package(data) {
  return request({
    url: '/admin/read_package',
    method: 'post',
    data: data
  })
}

// 修改红包类型
export function updateRead_package(data) {
  return request({
    url: '/admin/read_package',
    method: 'put',
    data: data
  })
}

// 删除红包类型
export function delRead_package(id) {
  return request({
    url: '/admin/read_package/' + id,
    method: 'delete'
  })
}

// 导出红包类型
export function exportRead_package(query) {
  return request({
    url: '/admin/read_package/export',
    method: 'get',
    params: query
  })
}