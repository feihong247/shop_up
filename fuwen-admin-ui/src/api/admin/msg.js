import request from '@/utils/request'

// 查询用户消息列表
export function listMsg(query) {
  return request({
    url: '/admin/msg/list',
    method: 'get',
    params: query
  })
}

// 查询用户消息详细
export function getMsg(id) {
  return request({
    url: '/admin/msg/' + id,
    method: 'get'
  })
}

// 新增用户消息
export function addMsg(data) {
  return request({
    url: '/admin/msg',
    method: 'post',
    data: data
  })
}

// 修改用户消息
export function updateMsg(data) {
  return request({
    url: '/admin/msg',
    method: 'put',
    data: data
  })
}

// 删除用户消息
export function delMsg(id) {
  return request({
    url: '/admin/msg/' + id,
    method: 'delete'
  })
}

// 导出用户消息
export function exportMsg(query) {
  return request({
    url: '/admin/msg/export',
    method: 'get',
    params: query
  })
}