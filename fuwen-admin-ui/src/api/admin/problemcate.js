import request from '@/utils/request'

// 查询常见问题类目列表
export function listProblemcate(query) {
  return request({
    url: '/admin/problemcate/list',
    method: 'get',
    params: query
  })
}

// 查询常见问题类目详细
export function getProblemcate(id) {
  return request({
    url: '/admin/problemcate/' + id,
    method: 'get'
  })
}

// 新增常见问题类目
export function addProblemcate(data) {
  return request({
    url: '/admin/problemcate',
    method: 'post',
    data: data
  })
}

// 修改常见问题类目
export function updateProblemcate(data) {
  return request({
    url: '/admin/problemcate',
    method: 'put',
    data: data
  })
}

// 删除常见问题类目
export function delProblemcate(id) {
  return request({
    url: '/admin/problemcate/' + id,
    method: 'delete'
  })
}

// 导出常见问题类目
export function exportProblemcate(query) {
  return request({
    url: '/admin/problemcate/export',
    method: 'get',
    params: query
  })
}