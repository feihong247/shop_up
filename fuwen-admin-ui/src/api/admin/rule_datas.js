import request from '@/utils/request'

// 查询数据规则列表
export function listRule_datas(query) {
  return request({
    url: '/admin/rule_datas/list',
    method: 'get',
    params: query
  })
}

// 查询数据规则详细
export function getRule_datas(id) {
  return request({
    url: '/admin/rule_datas/' + id,
    method: 'get'
  })
}

// 新增数据规则
export function addRule_datas(data) {
  return request({
    url: '/admin/rule_datas',
    method: 'post',
    data: data
  })
}

// 修改数据规则
export function updateRule_datas(data) {
  return request({
    url: '/admin/rule_datas',
    method: 'put',
    data: data
  })
}

// 删除数据规则
export function delRule_datas(id) {
  return request({
    url: '/admin/rule_datas/' + id,
    method: 'delete'
  })
}

// 导出数据规则
export function exportRule_datas(query) {
  return request({
    url: '/admin/rule_datas/export',
    method: 'get',
    params: query
  })
}
