import request from '@/utils/request'

// 查询代理区域列表
export function listAgent_region(query) {
  return request({
    url: '/admin/agent_region/list',
    method: 'get',
    params: query
  })
}


// 查询代理区域详细
export function getAgent_region(id) {
  return request({
    url: '/admin/agent_region/' + id,
    method: 'get'
  })
}

// 新增代理区域
export function addAgent_region(data) {
  return request({
    url: '/admin/agent_region',
    method: 'post',
    data: data
  })
}

// 修改代理区域
export function updateAgent_region(data) {
  return request({
    url: '/admin/agent_region',
    method: 'put',
    data: data
  })
}

// 删除代理区域
export function delAgent_region(id) {
  return request({
    url: '/admin/agent_region/' + id,
    method: 'delete'
  })
}

// 导出代理区域
export function exportAgent_region(query) {
  return request({
    url: '/admin/agent_region/export',
    method: 'get',
    params: query
  })
}
