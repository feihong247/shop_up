import request from '@/utils/request'

// 查询意见反馈列表
export function listFreed(query) {
  return request({
    url: '/admin/freed/list',
    method: 'get',
    params: query
  })
}

// 查询意见反馈详细
export function getFreed(id) {
  return request({
    url: '/admin/freed/' + id,
    method: 'get'
  })
}

// 新增意见反馈
export function addFreed(data) {
  return request({
    url: '/admin/freed',
    method: 'post',
    data: data
  })
}

// 修改意见反馈
export function updateFreed(data) {
  return request({
    url: '/admin/freed',
    method: 'put',
    data: data
  })
}

// 删除意见反馈
export function delFreed(id) {
  return request({
    url: '/admin/freed/' + id,
    method: 'delete'
  })
}

// 导出意见反馈
export function exportFreed(query) {
  return request({
    url: '/admin/freed/export',
    method: 'get',
    params: query
  })
}