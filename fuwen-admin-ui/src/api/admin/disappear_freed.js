import request from '@/utils/request'

// 查询消证释放列表
export function listDisappear_freed(query) {
  return request({
    url: '/admin/disappear_freed/list',
    method: 'get',
    params: query
  })
}

// 查询消证释放详细
export function getDisappear_freed(id) {
  return request({
    url: '/admin/disappear_freed/' + id,
    method: 'get'
  })
}

// 新增消证释放
export function addDisappear_freed(data) {
  return request({
    url: '/admin/disappear_freed',
    method: 'post',
    data: data
  })
}

// 修改消证释放
export function updateDisappear_freed(data) {
  return request({
    url: '/admin/disappear_freed',
    method: 'put',
    data: data
  })
}

// 删除消证释放
export function delDisappear_freed(id) {
  return request({
    url: '/admin/disappear_freed/' + id,
    method: 'delete'
  })
}

// 导出消证释放
export function exportDisappear_freed(query) {
  return request({
    url: '/admin/disappear_freed/export',
    method: 'get',
    params: query
  })
}