import request from '@/utils/request'

// 查询线下消费记录列表
export function listOffline(query) {
  return request({
    url: '/admin/offline/list',
    method: 'get',
    params: query
  })
}

// 查询线下消费记录详细
export function getOffline(id) {
  return request({
    url: '/admin/offline/' + id,
    method: 'get'
  })
}

// 新增线下消费记录
export function addOffline(data) {
  return request({
    url: '/admin/offline',
    method: 'post',
    data: data
  })
}

// 修改线下消费记录
export function updateOffline(data) {
  return request({
    url: '/admin/offline',
    method: 'put',
    data: data
  })
}

// 删除线下消费记录
export function delOffline(id) {
  return request({
    url: '/admin/offline/' + id,
    method: 'delete'
  })
}

// 导出线下消费记录
export function exportOffline(query) {
  return request({
    url: '/admin/offline/export',
    method: 'get',
    params: query
  })
}