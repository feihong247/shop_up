import request from '@/utils/request'

// 查询身份列表
export function listIdentity(query) {
  return request({
    url: '/admin/Identity/list',
    method: 'get',
    params: query
  })
}

// 查询身份详细
export function getIdentity(id) {
  return request({
    url: '/admin/Identity/' + id,
    method: 'get'
  })
}

// 新增身份
export function addIdentity(data) {
  return request({
    url: '/admin/Identity',
    method: 'post',
    data: data
  })
}

// 修改身份
export function updateIdentity(data) {
  return request({
    url: '/admin/Identity',
    method: 'put',
    data: data
  })
}

// 删除身份
export function delIdentity(id) {
  return request({
    url: '/admin/Identity/' + id,
    method: 'delete'
  })
}

// 导出身份
export function exportIdentity(query) {
  return request({
    url: '/admin/Identity/export',
    method: 'get',
    params: query
  })
}