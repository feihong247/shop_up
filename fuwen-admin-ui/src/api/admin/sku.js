import request from '@/utils/request'

// 查询商品sku规格列表
export function listSku(query) {
  return request({
    url: '/admin/sku/list',
    method: 'get',
    params: query
  })
}

// 查询商品sku规格详细
export function getSku(id) {
  return request({
    url: '/admin/sku/' + id,
    method: 'get'
  })
}

// 新增商品sku规格
export function addSku(data) {
  return request({
    url: '/admin/sku',
    method: 'post',
    data: data
  })
}

// 修改商品sku规格
export function updateSku(data) {
  return request({
    url: '/admin/sku',
    method: 'put',
    data: data
  })
}

// 删除商品sku规格
export function delSku(id) {
  return request({
    url: '/admin/sku/' + id,
    method: 'delete'
  })
}

// 导出商品sku规格
export function exportSku(query) {
  return request({
    url: '/admin/sku/export',
    method: 'get',
    params: query
  })
}