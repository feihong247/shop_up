import request from '@/utils/request'

// 查询用户收货地址列表
export function listAddrs() {
  return request({
    url: '/admin/showAll/showIndex',
    method: 'get'
  })
}



