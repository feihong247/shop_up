import request from '@/utils/request'

// 查询商品类目列表
export function listSpucate(query) {
  return request({
    url: '/admin/spucate/list',
    method: 'get',
    params: query
  })
}

// 查询商品类目详细
export function getSpucate(id) {
  return request({
    url: '/admin/spucate/' + id,
    method: 'get'
  })
}

// 新增商品类目
export function addSpucate(data) {
  return request({
    url: '/admin/spucate',
    method: 'post',
    data: data
  })
}

// 修改商品类目
export function updateSpucate(data) {
  return request({
    url: '/admin/spucate',
    method: 'put',
    data: data
  })
}

// 删除商品类目
export function delSpucate(id) {
  return request({
    url: '/admin/spucate/' + id,
    method: 'delete'
  })
}

// 导出商品类目
export function exportSpucate(query) {
  return request({
    url: '/admin/spucate/export',
    method: 'get',
    params: query
  })
}

// 获取类目树
export function getCateTree() {
  return request({
    url: '/admin/spucate/getCateTree',
    method: 'get'
  })
}