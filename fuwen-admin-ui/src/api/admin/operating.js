import request from '@/utils/request'

// 查询商家经营平台分类列表
export function listOperating(query) {
  return request({
    url: '/admin/operating/list',
    method: 'get',
    params: query
  })
}

// 查询商家经营平台分类详细
export function getOperating(id) {
  return request({
    url: '/admin/operating/' + id,
    method: 'get'
  })
}

// 新增商家经营平台分类
export function addOperating(data) {
  return request({
    url: '/admin/operating',
    method: 'post',
    data: data
  })
}

// 修改商家经营平台分类
export function updateOperating(data) {
  return request({
    url: '/admin/operating',
    method: 'put',
    data: data
  })
}

// 删除商家经营平台分类
export function delOperating(id) {
  return request({
    url: '/admin/operating/' + id,
    method: 'delete'
  })
}

// 导出商家经营平台分类
export function exportOperating(query) {
  return request({
    url: '/admin/operating/export',
    method: 'get',
    params: query
  })
}