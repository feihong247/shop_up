import request from '@/utils/request'

// 查询用户和身份中间列表
export function listUljoin(query) {
  return request({
    url: '/admin/uljoin/list',
    method: 'get',
    params: query
  })
}

// 查询用户和身份中间详细
export function getUljoin(id) {
  return request({
    url: '/admin/uljoin/' + id,
    method: 'get'
  })
}

// 新增用户和身份中间
export function addUljoin(data) {
  return request({
    url: '/admin/uljoin',
    method: 'post',
    data: data
  })
}

// 修改用户和身份中间
export function updateUljoin(data) {
  return request({
    url: '/admin/uljoin',
    method: 'put',
    data: data
  })
}

// 删除用户和身份中间
export function delUljoin(id) {
  return request({
    url: '/admin/uljoin/' + id,
    method: 'delete'
  })
}

// 导出用户和身份中间
export function exportUljoin(query) {
  return request({
    url: '/admin/uljoin/export',
    method: 'get',
    params: query
  })
}