import request from '@/utils/request'

// 查询一县一品列表
export function listOne_area_item(query) {
  return request({
    url: '/admin/one_area_item/list',
    method: 'get',
    params: query
  })
}


// 查询一县一品商品列表
export function listOne_area_item_spu() {
  return request({
    url: '/admin/spu/areaList',
    method: 'get'
  })
}

// 查询一县一品详细
export function getOne_area_item(id) {
  return request({
    url: '/admin/one_area_item/' + id,
    method: 'get'
  })
}

// 新增一县一品
export function addOne_area_item(data) {
  return request({
    url: '/admin/one_area_item',
    method: 'post',
    data: data
  })
}

// 修改一县一品
export function updateOne_area_item(data) {
  return request({
    url: '/admin/one_area_item',
    method: 'put',
    data: data
  })
}

// 删除一县一品
export function delOne_area_item(id) {
  return request({
    url: '/admin/one_area_item/' + id,
    method: 'delete'
  })
}

// 导出一县一品
export function exportOne_area_item(query) {
  return request({
    url: '/admin/one_area_item/export',
    method: 'get',
    params: query
  })
}
