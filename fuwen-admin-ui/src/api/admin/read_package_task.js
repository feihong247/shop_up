import request from '@/utils/request'

// 查询红包任务类型列列表
export function listRead_package_task(query) {
  return request({
    url: '/admin/read_package_task/list',
    method: 'get',
    params: query
  })
}

// 查询红包任务类型列详细
export function getRead_package_task(id) {
  return request({
    url: '/admin/read_package_task/' + id,
    method: 'get'
  })
}

// 新增红包任务类型列
export function addRead_package_task(data) {
  return request({
    url: '/admin/read_package_task',
    method: 'post',
    data: data
  })
}

// 修改红包任务类型列
export function updateRead_package_task(data) {
  return request({
    url: '/admin/read_package_task',
    method: 'put',
    data: data
  })
}

// 删除红包任务类型列
export function delRead_package_task(id) {
  return request({
    url: '/admin/read_package_task/' + id,
    method: 'delete'
  })
}

// 导出红包任务类型列
export function exportRead_package_task(query) {
  return request({
    url: '/admin/read_package_task/export',
    method: 'get',
    params: query
  })
}