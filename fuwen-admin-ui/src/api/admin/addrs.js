import request from '@/utils/request'

// 查询用户收货地址列表
export function listAddrs(query) {
  return request({
    url: '/admin/addrs/list',
    method: 'get',
    params: query
  })
}

// 查询用户收货地址详细
export function getAddrs(id) {
  return request({
    url: '/admin/addrs/' + id,
    method: 'get'
  })
}

// 新增用户收货地址
export function addAddrs(data) {
  return request({
    url: '/admin/addrs',
    method: 'post',
    data: data
  })
}

// 修改用户收货地址
export function updateAddrs(data) {
  return request({
    url: '/admin/addrs',
    method: 'put',
    data: data
  })
}

// 删除用户收货地址
export function delAddrs(id) {
  return request({
    url: '/admin/addrs/' + id,
    method: 'delete'
  })
}

// 导出用户收货地址
export function exportAddrs(query) {
  return request({
    url: '/admin/addrs/export',
    method: 'get',
    params: query
  })
}