import request from '@/utils/request'

// 查询消证获取列表
export function listDisappear_get(query) {
  return request({
    url: '/admin/disappear_get/list',
    method: 'get',
    params: query
  })
}

// 查询消证获取详细
export function getDisappear_get(id) {
  return request({
    url: '/admin/disappear_get/' + id,
    method: 'get'
  })
}

// 新增消证获取
export function addDisappear_get(data) {
  return request({
    url: '/admin/disappear_get',
    method: 'post',
    data: data
  })
}

// 修改消证获取
export function updateDisappear_get(data) {
  return request({
    url: '/admin/disappear_get',
    method: 'put',
    data: data
  })
}

// 删除消证获取
export function delDisappear_get(id) {
  return request({
    url: '/admin/disappear_get/' + id,
    method: 'delete'
  })
}

// 导出消证获取
export function exportDisappear_get(query) {
  return request({
    url: '/admin/disappear_get/export',
    method: 'get',
    params: query
  })
}