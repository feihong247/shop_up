import request from '@/utils/request'

// 查询地图列表
export function listPol(query) {
  return request({
    url: '/admin/pol/list',
    method: 'get',
    params: query
  })
}

// 查询地图详细
export function getPol(id) {
  return request({
    url: '/admin/pol/' + id,
    method: 'get'
  })
}


// 根据业务编号查询地图详细
export function getByBusId(busId) {
  return request({
    url: '/admin/pol/getByBusId/' + busId,
    method: 'get'
  })
}
// 新增地图
export function addPol(data) {
  return request({
    url: '/admin/pol',
    method: 'post',
    data: data
  })
}

// 修改地图
export function updatePol(data) {
  return request({
    url: '/admin/pol',
    method: 'put',
    data: data
  })
}

// 删除地图
export function delPol(id) {
  return request({
    url: '/admin/pol/' + id,
    method: 'delete'
  })
}

// 导出地图
export function exportPol(query) {
  return request({
    url: '/admin/pol/export',
    method: 'get',
    params: query
  })
}