import request from '@/utils/request'

// 查询锁仓池列表
export function listLock_storage(query) {
  return request({
    url: '/admin/lock_storage/list',
    method: 'get',
    params: query
  })
}

// 查询锁仓池详细
export function getLock_storage(id) {
  return request({
    url: '/admin/lock_storage/' + id,
    method: 'get'
  })
}

// 新增锁仓池
export function addLock_storage(data) {
  return request({
    url: '/admin/lock_storage',
    method: 'post',
    data: data
  })
}

// 修改锁仓池
export function updateLock_storage(data) {
  return request({
    url: '/admin/lock_storage',
    method: 'put',
    data: data
  })
}

// 删除锁仓池
export function delLock_storage(id) {
  return request({
    url: '/admin/lock_storage/' + id,
    method: 'delete'
  })
}

// 导出锁仓池
export function exportLock_storage(query) {
  return request({
    url: '/admin/lock_storage/export',
    method: 'get',
    params: query
  })
}