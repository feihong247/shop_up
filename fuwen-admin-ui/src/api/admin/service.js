import request from '@/utils/request'

// 查询在线客服列表
export function listService(query) {
  return request({
    url: '/admin/service/list',
    method: 'get',
    params: query
  })
}

// 查询在线客服详细
export function getService(id) {
  return request({
    url: '/admin/service/' + id,
    method: 'get'
  })
}

// 新增在线客服
export function addService(data) {
  return request({
    url: '/admin/service',
    method: 'post',
    data: data
  })
}

// 修改在线客服
export function updateService(data) {
  return request({
    url: '/admin/service',
    method: 'put',
    data: data
  })
}

// 删除在线客服
export function delService(id) {
  return request({
    url: '/admin/service/' + id,
    method: 'delete'
  })
}

// 导出在线客服
export function exportService(query) {
  return request({
    url: '/admin/service/export',
    method: 'get',
    params: query
  })
}