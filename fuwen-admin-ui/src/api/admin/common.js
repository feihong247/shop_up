import request from '@/utils/request'
import {codeCvr} from '@/constant/constant'
// 查询省｜市｜区｜街道 联动接口,0是总节点数据
export function listDistrict(pid) {
  return request({
    url: `district/findPid/${pid}`,
    method: 'get'
    })
}

//根据code转移名称
export function codeConversionName(code) {
  return request({
    url: `/district/codeConversionName/${code}`,
    method: 'get'
    })
}
