package com.fw.common;
import sun.misc.Unsafe;
import java.lang.reflect.Field;
import java.util.concurrent.atomic.AtomicLong;


//@State(Scope.Benchmark)
public class NoLockUtils {

    private static Cell cell = new Cell();
    private static  volatile Boolean flag = Boolean.TRUE;


    /**
     * true ： 可以执行
     * false ： 其他用户在占用
     * @return
     */
    public static boolean tryLock(){
      boolean isTry =  cell.casLock() && flag;
      flag = Boolean.FALSE;
      return isTry;
    }

    public static  void tryBool(){
        flag = Boolean.TRUE;
    }




    @sun.misc.Contended
   static final  class Cell{
        volatile  long quickOrderCount = 0L;
        private  AtomicLong countAto = new AtomicLong(0L);
        static final long offset;
        static final Unsafe UNSAFE;
        static {
            try {
                Field theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
                theUnsafe.setAccessible(true);
                UNSAFE = (Unsafe) theUnsafe.get(null);
                //获取偏移量
                offset =  UNSAFE.objectFieldOffset(Cell.class.getDeclaredField("quickOrderCount"));
            } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }

       private   final  boolean cas(long prev,long next){
            return UNSAFE.compareAndSwapLong(this, offset, prev, next);
        }

        public boolean casLock(){
            long prev = countAto.getAndIncrement();
            long next = countAto.get();
            return cas(prev,next);
        }
    }
/*
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger();
        LongAdder longAdder = new LongAdder();
        LongAdder longAdder1 = new LongAdder();

        ThreadUtil.concurrencyTest(1000, () -> {
            // 测试的逻辑内容
            try {
                if (NoLockUtils.tryLock()){
                    // 安全区
                    Console.log("{}",atomicInteger.incrementAndGet());
                    longAdder1.increment();
                }else{
                    longAdder.increment();
                }
            }finally {
                // 归还
                NoLockUtils.tryBool();
            }
        });


    }*/


   /* @Benchmark
    @Warmup(iterations = 1,time = 2)
    @Threads(1000)
    @Fork(6)
    @BenchmarkMode(Mode.Throughput)
    @Measurement(iterations = 2,time = 2)
    public  void asd() {
        NoLockUtils noLockComm = new NoLockUtils();
        noLockComm.mainroot();
    }*/


/*    public  void mainroot() {
        AtomicInteger count = new AtomicInteger();
        LongAdder longAdder = new LongAdder();
            // 测试的逻辑内容
         *//*   if (isPass()){
                longAdder.increment();
                Console.log( count.getAndIncrement());
            }else{
                Console.log("去除");
                longAdder.increment();
            }  *//*
        try {
            if (tryLock()) {
                longAdder.increment();
            }
        }finally {
              tryBool();
        }
        }*/




// 获取总的执行时间，单位毫秒
      //  Console.log("测试啊:{}",longAdder.sum());

        /**
         *  对象头锁
         * Benchmark        Mode  Cnt          Score           Error  Units
         * NoLockComm.asd  thrpt   12  467610962.946 ± 299421348.590  ops/s
         *
         *  重入锁
         * Benchmark        Mode  Cnt          Score           Error  Units
         * NoLockComm.asd  thrpt   12  238968238.275 ± 129038271.672  ops/s
         *
         *  cas
         * Benchmark        Mode  Cnt         Score           Error  Units
         * NoLockComm.asd  thrpt   12  45599920.685 ± 112647569.647  ops/s
         *
         *  cas + 块区
         *
         */




}
