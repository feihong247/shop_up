package com.fw.common;

import com.fw.config.AliConfig;
import com.ijpay.alipay.AliPayApiConfig;
import com.ijpay.alipay.AliPayApiConfigKit;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

public abstract class AliCommAbs {


    /**
     * 初始化支付宝信息
     */
    @SneakyThrows
    public void initAliConfig(){
        AliConfig aliConfig = getAliConfig();
        AliPayApiConfig aliPayApiConfig = AliPayApiConfig.builder()
                .setAppId(aliConfig.getAppId())
                .setAppCertPath(aliConfig.getAppCertPath())
                .setAliPayCertPath(aliConfig.getAliPayCertPath())
                .setAliPayRootCertPath(aliConfig.getAliPayRootCertPath())
                .setCharset("UTF-8")
                .setPrivateKey(aliConfig.getPrivateKey())
                .setAliPayPublicKey(aliConfig.getAliPayPublicKey())
                .setServiceUrl(aliConfig.getServiceUrl())
                .setSignType("RSA2")
                // 普通公钥方式
                //.build();
                // 证书模式
                .buildByCert();
        AliPayApiConfigKit.setThreadLocalAppId(aliConfig.getAppId()); // 2.1.2 之后的版本，可以不用单独设置
        AliPayApiConfigKit.setThreadLocalAliPayApiConfig(aliPayApiConfig);

    }

    public abstract AliConfig getAliConfig();

}
