package com.fw.utils;

/**
 * @Author GHB
 * @Description 微信配置
 */
public class WechatConfig {
    private String openid;
    private String access_token;

    public String getOpenid() {
        return openid;
    }


    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public WechatConfig(String openid, String access_token) {
        this.openid = openid;
        this.access_token = access_token;
    }

    public WechatConfig() {
    }
}
