package com.fw.utils;

import cn.hutool.core.util.NumberUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class RandomUtils {

    private static final String charlist = "0123456789";

    public static String createRandomString(int len) {
        String str = new String();
        for (int i = 0; i < len; i++) {
            str += charlist.charAt(getRandom(charlist.length()));
        }
        return str;
    }

    public static int getRandom(int mod) {
        if (mod < 1) {
            return 0;
        }
        int ret = getInt() % mod;
        return ret;
    }

    private static int getInt() {
        int ret = Math.abs(Long.valueOf(getRandomNumString()).intValue());
        return ret;
    }

    private static String getRandomNumString() {
        double d = Math.random();
        String dStr = String.valueOf(d).replaceAll("[^\\d]", "");
        if (dStr.length() > 1) {
            dStr = dStr.substring(0, dStr.length() - 1);
        }
        return dStr;
    }

    public static int getBilityRomdom(int bility){
        return (int) (Math.random() * bility);
    }

    public static int getMaxMin(){
        int i = (int)(10+Math.random()*(20-10+1));
        return i;
    }

    public static int getMoney(){
        int i = (int)(5+Math.random()*(10-5+1));
        return i;
    }

    public static double getLevelMoney(double i,double j){
        double h = j+Math.random()*(i-j);
        return new BigDecimal(h).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

/**
 2      * 求a+b平均值
 3      * @param a
 4      * @param b
 5      * @return a+b的平均值
 6      */
  public  static int avg(int a ,int b){
               double random = Math.random();
                if (random >= 0 && random <= 0.25){
                        //正确写法1
                        return ((a&b) + ((a^b) >> 1));
                    }else if (random > 0.25 && random <= 0.5){
                        //正确写法2
                        return b+(a-b)/2;
                    }else if (random > 0.5 && random <= 0.75){
                        //正确写法3
                        return b+((a-b)>>1);
                    }else if (random > 0.75 && random <= 1.0){
                        //正确写法4
                        return (a+b)>>>1;
                    }else {
                        return -1;
                    }
            }

    /**
     * 数量 / 总数 * 100
     * @param number     数量
     * @param count      总数
     * @return
     * 投机取巧法，遇小数点必死无疑
     */
    @Deprecated
    public static BigDecimal mathNum(BigDecimal number,BigDecimal count){
       return  count.multiply(new BigDecimal("0.".concat(number.toString())));
    }

    /**
     * 百分比
     * @return
     */
    public static BigDecimal mathNumber(BigDecimal proportion,BigDecimal number ){
       return   proportion.multiply(number).divide(new BigDecimal(100D),3,BigDecimal.ROUND_DOWN);
    }

    /**
     * 千分比
     */
    public static BigDecimal mathNumberThousand(BigDecimal proportion,BigDecimal number ){
        return   proportion.multiply(number).divide(new BigDecimal(1000D),3,BigDecimal.ROUND_DOWN);
    }

    /**
     * 万分比
     */
    public static BigDecimal mathNumberTen(BigDecimal proportion,BigDecimal number ){
        return   proportion.multiply(number).divide(new BigDecimal(10000D),3,BigDecimal.ROUND_DOWN);
    }



    /**
     * 格式化保留后两位
     * @param process
     * @return
     */
    public static BigDecimal runodMath(BigDecimal process){
       return   process.setScale(2, RoundingMode.HALF_UP);
    }

}