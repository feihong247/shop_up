package com.fw.utils;

/**
 * @Author:yanwei
 * @Date: 2020/8/5 - 12:07
 *
 * 系统安全检查机制
 */

public class SystemSecurityUtil {
    public static boolean pageSize(Integer pageSize) {
        return pageSize < 100;
    }
}
