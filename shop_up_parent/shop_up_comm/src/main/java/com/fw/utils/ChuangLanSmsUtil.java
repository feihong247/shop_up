package com.fw.utils;


import com.alibaba.fastjson.JSONObject;
import com.fw.constant.Constant;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
@Slf4j
public class ChuangLanSmsUtil {

    /**
     * 手机号
     * @param phone
     * @param code 验证码
     * @param time 过期时间
     */
/*    public static void sendSms(String phone,String code,String time) {
        //短信下发
        String sendUrl = "http://smssh1.253.com/msg/variable/json";
        Map map = new HashMap();
        map.put("account","YZM0154272");//API账号
        map.put("password","MYSskmgxei27a9");//API密码
        map.put("msg","【商甲尚品】尊敬的用户，您本次的验证码为 {$var}有效期{$var}分钟。");//短信内容
        map.put("params",""+phone+","+code+","+time+"");//手机号
        JSONObject js = (JSONObject) JSONObject.toJSON(map);
      //  System.out.println();
        //查询余额
        log.info(sendSmsByPost(sendUrl,js.toString()));
    }*/
    /**
     * 手机号
     * @param phone
     * @param code 验证码
     * @param time 过期时间
     */
    public static void sendSms(String phone,String code,String time) {
        //短信下发
        String sendUrl = "http://smssh1.253.com/msg/variable/json";
        Map map = new HashMap();
        map.put("account","N3210616");//API账号
        map.put("password","N6Qo458Gztab11");//API密码
        map.put("msg","【商甲尚品】您的验证码为{$var}请在{$var}分钟内输入。感谢您对{$var}的支持，祝您生活愉快!");//短信内容
        map.put("params",""+phone+","+code+","+time+""+","+"商甲尚品");//手机号
        JSONObject js = (JSONObject) JSONObject.toJSON(map);
        //  System.out.println();
        //查询余额
        log.info(sendSmsByPost(sendUrl,js.toString()));
    }
    /**
     * 手机号
     * @param phone
     */
    public static void sendNotice(String adminUrl,String phone,String passWord) {
        //短信下发
        String sendUrl = "http://smssh1.253.com/msg/variable/json";
        Map map = new HashMap();
        map.put("account","N3210616");//API账号
        map.put("password","N6Qo458Gztab11");//API密码
        map.put("msg","【商甲尚品】用户您好,您申请商户已经通过审批了,请登录 {$var}  ,默认登陆账号{$var},默认登陆密码{$var}");//短信内容
        map.put("params",""+phone+","+adminUrl+","+phone+","+passWord+"");//手机号
        JSONObject js = (JSONObject) JSONObject.toJSON(map);
        //  System.out.println();
        //查询余额
        log.info(sendSmsByPost(sendUrl,js.toString()));
    }


    /**
     * 手机号
     * @param phone
     */
    public static void sendProxyNotice(String adminUrl,String phone,String userName,String passWord) {
        //短信下发
        String sendUrl = "http://smssh1.253.com/msg/variable/json";
        Map map = new HashMap();
        map.put("account","N3210616");//API账号
        map.put("password","N6Qo458Gztab11");//API密码
        map.put("msg","【商甲尚品】用户您好,您申请代理已经通过审批了,请登录 {$var}  ,默认登陆账号{$var},默认登陆密码{$var}");//短信内容
        map.put("params",""+phone+","+adminUrl+","+userName+","+passWord+"");//手机号
        JSONObject js = (JSONObject) JSONObject.toJSON(map);
        //  System.out.println();
        //查询余额
        log.info(sendSmsByPost(sendUrl,js.toString()));
    }


    public static String sendSmsByPost(String path, String postContent) {
        URL url = null;
        try {
            url = new URL(path);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setConnectTimeout(10000);
            httpURLConnection.setReadTimeout(10000);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setRequestProperty("Charset", "UTF-8");
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.connect();
            OutputStream os=httpURLConnection.getOutputStream();
            os.write(postContent.getBytes("UTF-8"));
            os.flush();
            StringBuilder sb = new StringBuilder();
            int httpRspCode = httpURLConnection.getResponseCode();
            if (httpRspCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(httpURLConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                br.close();
                return sb.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

/*    public static void main(String[] args) {
        sendNotice(Constant.ShopInitLogin.SHOP_LOGIN_URL,"18539499449","134567");
    }*/
}
