package com.fw.utils;

import com.sun.mail.util.MailSSLSocketFactory;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.security.GeneralSecurityException;
import java.util.Properties;

/**
 * @Author:yanwei
 * @Date: 2020/10/17 - 18:39
 */

public class MailUtils {

    // 发件人邮箱地址
    private static String from = "762659436@qq.com";
    // 发件人称号，同邮箱地址
    private static String user = "762659436@qq.com";
    // 发件人邮箱客户端授权码
    private static String password = "zbqumoxbzbiibdjd";
    //发件人的邮箱服务器
    private static String mailHost = "smtp.qq.com";

    /**
     *
     * 456 smtp ssl安全协议发送
     * @param to
     * @param html
     * @param code
     */
    /* 发送验证信息的邮件 */
    public static boolean sendMailSSl(String to, String code,String html) {
        Properties props = new Properties();
// 设置发送邮件的邮件服务器的属性（这里使用qq的smtp服务器）
        props.put("mail.smtp.host", mailHost);
// 需要经过授权，也就是有户名和密码的校验，这样才能通过验证（一定要有这一条）
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable","true");
        //-------当需使用SSL验证时添加，邮箱不需SSL验证时删除即可（测试SSL验证使用QQ企业邮箱）
        String SSL_FACTORY="javax.net.ssl.SSLSocketFactory";
        props.put("mail.smtp.socketFactory.class", SSL_FACTORY);
        props.put("mail.smtp.socketFactory.fallback", "false");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.port", "465"); //google使用465或587端口
        MailSSLSocketFactory sf = null;
        try {
            sf = new MailSSLSocketFactory();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        sf.setTrustAllHosts(true);
        props.put("mail.smtp.ssl.socketFactory", sf);
// 用刚刚设置好的props对象构建一个session
        Session session = Session.getDefaultInstance(props);
// 有了这句便可以在发送邮件的过程中在console处显示过程信息，供调试使用（你可以在控制台（console)上看到发送邮件的过程）
// session.setDebug(true);
// 用session为参数定义消息对象
        MimeMessage message = new MimeMessage(session);
        try {
// 加载发件人地址
            message.setFrom(new InternetAddress(from));
// 加载收件人地址
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
// 加载标题
            message.setSubject("直播邮箱".intern());
// 向multipart对象中添加邮件的各个部分内容，包括文本内容和附件
            Multipart multipart = new MimeMultipart();
// 设置邮件的文本内容
            BodyPart contentPart = new MimeBodyPart();
            contentPart.setContent(html, "text/html;charset=utf-8");
            multipart.addBodyPart(contentPart);
            message.setContent(multipart);
            message.saveChanges(); // 保存变化
// 连接服务器的邮箱
            Transport transport = session.getTransport("smtp");
// 把邮件发送出去
            transport.connect(mailHost, user, password);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            //System.out.println("邮件发送成功");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
