package com.fw.utils;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author guohaibin
 * @Description 阿里云短信
 * @see <a href="https://help.aliyun.com/document_detail/101414.html?spm=a2c4g.11174283.6.616.4ede2c421FAhPH"/>
 */
@Slf4j
public class AliSmsUtil {


    private static final String accessKeyId = "LTAIoJ9QOQK24bbm";
    private static final String secret = "s2rb2ovh2hzTyBBaiX4GBiKJHWKCCx";


    /**
     * 发送验证码
     *
     * @param phoneNumber
     * @param code
     * @return
     */
    public static boolean send(String phoneNumber, String code) {
        return send(phoneNumber, code, "SMS_139910267");
    }

    private static boolean send(String phoneNumber, String code, String templateCode) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, secret);
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", phoneNumber);
        request.putQueryParameter("SignName", "喵趣");
        request.putQueryParameter("TemplateCode", templateCode);
        JSONObject templateParam = new JSONObject();
        templateParam.put("code", code);
        request.putQueryParameter("TemplateParam", templateParam.toJSONString());
        try {
            CommonResponse response = client.getCommonResponse(request);
            JSONObject responseJSON = JSONObject.parseObject(response.getData());
            if ("OK".equals(responseJSON.getString("Message"))) {
                return true;
            }
            log.error("【阿里云短信】:发送短信失败，手机号：{},验证码：{},错误信息：{}", phoneNumber, code, responseJSON.getString("Message"));
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return false;
    }

/*    public static void main(String[] args) {
        boolean send = AliSmsUtil.send("110", "234s23");
        System.out.println("send = " + send);
    }*/
}
