package com.fw.config;

import com.fw.utils.spring.SpringUtils;
import lombok.SneakyThrows;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "ali")
public class AliConfig {

    // 应用编号
    private String appId;
    // 应用证书路径
    private String appCertPath;
    // 支付宝证书路径
    private String aliPayCertPath;
    // 支付宝颁发根路径
    private String aliPayRootCertPath;
    // 应用私钥
    private String privateKey;
    //支付宝公钥
    private String aliPayPublicKey;
    // 支付宝网管
    private  String serviceUrl;
    // 外网访问域名
    private String domain;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppCertPath() {
        return appCertPath;
    }

    public void setAppCertPath(String appCertPath) {
        // 得到路径
        this.appCertPath = getPath(appCertPath);
    }

    public String getAliPayCertPath() {
        return aliPayCertPath;
    }

    public void setAliPayCertPath(String aliPayCertPath) {
        // 得到路径
        this.aliPayCertPath = getPath(aliPayCertPath);
    }

    public String getAliPayRootCertPath() {
        return aliPayRootCertPath;
    }

    public void setAliPayRootCertPath(String aliPayRootCertPath) {
        // 得到路径
        this.aliPayRootCertPath = getPath(aliPayRootCertPath);
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getAliPayPublicKey() {
        return aliPayPublicKey;
    }

    public void setAliPayPublicKey(String aliPayPublicKey) {
        this.aliPayPublicKey = aliPayPublicKey;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    @SneakyThrows
    public String getPath(String path){
        return "dev".equals(SpringUtils.getActiveProfile()) ?
             new ClassPathResource(path).getFile().getAbsolutePath() : path;



    }
}
