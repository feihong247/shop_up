package com.fw.annotation;


import java.lang.annotation.*;

/**
 * @author: yanwei
 * @create: 2020-08-05
 * @description:打印方法入参
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface PrintParam {

    /**
     * 返回值打印
     * @return
     */
    boolean output() default true;

    /**
     * 参数打印
     * @return
     */
    boolean input() default true;

}
