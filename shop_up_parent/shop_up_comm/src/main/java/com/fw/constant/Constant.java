package com.fw.constant;

import cn.hutool.core.util.NumberUtil;
import com.fw.utils.RandomUtils;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;

/**
 * yanwei
 * <p>
 * 该类用来装载魔法值,
 */
public interface Constant {


    interface RequestConstant {
        String HEAD_FLAG = "Authorization";
        String BEARER = "Bearer ";
        String SECURITY_ROLE = "ROLE_";
    }

    interface DictTypes {
        String RECHARGE_VIP_TYPE = "recharge_type";
    }

    interface RedisDistrict {
        // redis 存储省市区 所有大项 值
        String DISTRICT_DATA = "district";
        String DISTRICT_CODE_DATA = "district_code";
        String SHOP_HOST = "shop_host";
    }

    interface AddrIsDefault {
        Integer IS_DEFAULT_YES = 1;//默认收货地址
        Integer IS_DEFAULT_NO = 0;//非默认收货地址
    }
    //是否正在使用此身份
    interface IsUsr {
        Integer IS_USR = 1;//是
        Integer IS_USR_NO = 0;//否
    }

    /**
     * 身份表内数据id
     */
    interface Identity {
        String VIP_ID = "1";//vipID
        String ONE_VIP_ID = "2";//1星ID
        String TWO_VIP_ID = "3";//2星ID
        String THREE_VIP_ID = "4";//3星ID
        String AREA_VIP_ID = "5";//区县代理
        String CITY_VIP_ID = "6";//市级代理
        String PROVINCE_VIP_ID = "7";//省级代理
        Set<String> PROXY_AGENTS = Sets.newHashSet(AREA_VIP_ID,CITY_VIP_ID,PROVINCE_VIP_ID);
    }

    /**
     * 数据规则表内数据id
     */
    interface IsRuleData{
        String VIP_MONEY_ID = "1";//vipID
        String ONE_VIP_MONEY_ID = "2";//1星ID
        String TWO_VIP_MONEY_ID = "3";//2星ID
        String THREE_VIP_MONEY_ID = "4";//3星ID
        String AREA_VIP_MONEY_ID = "5";//区县代理
        String CITY_VIP_MONEY_ID = "6";//市级代理
        String PROVINCE_VIP_MONEY_ID = "7";//省级代理
        String ONE_STAR_SHANG_JIA_ID = "8";//成为1星时额外获得商甲
        String TWO_STAR_SHANG_JIA_ID = "9";//成为2星时额外获得商甲
        String THREE_STAR_SHANG_JIA_ID = "10";//成为3星时额外获得商甲
        String INTEGRAL_RELEASE = "11";//用户每天兑换积分比例id
        // 商户规则固定编号
        // 商户线上质押商甲规则编号
        String SHOP_PUBLISH_SPU = "12";
        // 商户线下质押商甲规则编号
        String SHOP_OFF_PUBLISH_SPU = "34";

        String WITHDRAW_INTEGRAL_ID = "13";//积分提现手续费比例
        String BECOME_VIP_MONEY_ID = "14";//成为vip时所需金额的增长比例id
        String BECOME_PAY_MONEY_ID = "15";//商甲交易,用户买卖商甲的手续费比例id
        String SHOP_OFFLINE_ID = "16"; // 商户线下消费用户反消证乘基
        String CONSUMPTION_OUTPUT = "17";//消费产出商甲所需金额

        String VIP_INTEGRAL_RELEASE = "18";//vip用户每天兑换积分比例id
        String ONE_VIP_ONE_INTEGRAL_RELEASE = "19";//一星用户每天兑换积分比例id
        String TWO_VIP_INTEGRAL_RELEASE = "20";//二星用户每天兑换积分比例id
        String THREE_VIP_INTEGRAL_RELEASE = "21";//三星用户每天兑换积分比例id
        String AREA_VIP_INTEGRAL_RELEASE = "22";//区县代理用户每天兑换积分比例id
        String CITY_VIP_INTEGRAL_RELEASE = "23";//市级代理用户每天兑换积分比例id
        String PROVINCE_INTEGRAL_RELEASE = "24";//省级代理用户每天兑换积分比例id
        String BONUS = "25";

        String ONLINE_DISAPPEAR_MAGNIFICATION = "26";//线上消证计算倍率
        String GUESS_ROOM_MINUTE = "27";//竞猜房间存在时间（分钟）
        String GUESS_PK_SECOND = "28";//竞猜出拳时间（秒）
        String MATCH_AUTO_SECOND = "29";//竞猜匹配多久给机器人（秒）
        String NORMAL_USER_SHANGJIA = "30";//普通用户赠送单个商甲需消费金额
        String NO_NORMAL_USER_SHANGJIA = "31";//非普通用户赠送商家消费金额
        String OVER_THREE_STARS_SHANGJIA = "32";//三星后每推广一个vip额外获得商甲
        String SHANGJIA_LOCK_NUMBER="33";//商甲锁仓单位数量
        String GO_PROXY_LEVEL_VIP = "35";// 代理购买级差返利比
        String GO_PROXY_LEVEL_ONE_START = "36";// 一星购买级差返利比
        String GO_PROXY_LEVEL_TOW_START = "37";// 二星购买级差返利比
        String GO_PROXY_LEVEL_THREE_START = "38";// 三星购买级差返利比
        String GO_PROXY_LEVEL_AREA_START = "39";// 区域购买级差返利比
        String GO_PROXY_LEVEL_CITY_START = "40";//市购买级差返利比
        String GO_PROXY_LEVEL_PROVINCE_START = "41";//省购买级差返利比
        String SHOP_REGIS = "42";//线上，线下商户质押商甲起步

    }

    /**
     * 商甲发行表数据id
     */
    interface ShangJia{
        String CONSUME_ID = "1";//消费商甲数据id
        String ORIGINAL_ID = "3";//原始商甲
        String BAZAAR_ID = "2";//市场
        String TECHNOLOGY_ID = "4";//技术
        String MANAGEMENT_ID = "5";//管理
    }

    /**
     * 星级身份所需直推人数
     */
    interface Recommend{
        String ONE_VIP_COUNT="2";//1星所需人数
        String TWO_VIP_COUNT="3";//2
        String THREE_VIP_COUNT="4";//3
    }
    /**
     * 审核状态:0已购买待审核,1审核通过,赋予身份,2驳回
     */
    interface ShopIngLogType{
        Integer IS_TYPE_ING=0;//已购买待审核
        Integer IS_TYPE_SUCCESS=1;//审核通过,赋予身份
        Integer IS_TYPE_FAIL=2;//驳回
    }

    /**
     * 锁仓状态
     */
    interface LockStorage{
        // 分红
        String LOCK_ING = "0";
        // 不分红
        String RELEASE = "1";
    }
    /**
     * 商甲交易状态:0售卖中,1已卖出
     */
    interface ShopIngShangJiaType{
        Integer IS_TYPE_ING=0;//已购买待审核
        Integer IS_TYPE_SUCCESS=1;//审核通过,赋予身份
    }

    /**
     * 商户相关规则验证
     */
    interface ShopRuleData{

        // 商户商甲线上 质押 字符
        String SHOP_SHANGJIA_ONLINE = "onLine";
        // 商户商甲线下 质押 字符
        String SHOP_SHANGJIA_OFFLINE = "offLine";

        // 商户消证除外统计项
        String SHOP_COUNT_DIS = "shop_count_dis";

        // 得到 线下商户反证量
        static Map<String,BigDecimal> getOffShopRule(){
            Map<String, BigDecimal> shopOffline = Maps.newHashMap();
            shopOffline.put("1", NumberUtil.add("1"));
            shopOffline.put("2",NumberUtil.add("2"));
            shopOffline.put("3",NumberUtil.add("3"));
            shopOffline.put("4",NumberUtil.add("4"));
            shopOffline.put("5",NumberUtil.add("5"));
            // 代理 处理
            shopOffline.put("proxy",NumberUtil.add("2"));
            return shopOffline;
        }
    }

    /**
     * 商户下发后台管理角色标识编码
     */
    interface ShopSysUserRole{
        Long[] ROLE_IDS = {101L};
        Long[] POST_IDS = {5L};
    }
    /**
     * 商户后台管理初始化登陆密码
     */
    interface ShopInitLogin{
        @Deprecated
        String SHOP_PASS_WORD = RandomUtils.createRandomString(6);

        String SHOP_LOGIN_URL = "http://admin.kuangshikeji.com";
    }

    /**
     * 商户线下消费鉴权per
     */
    interface ShopAuthPer{
        String SHOP_AUTH_PER = "shop_auth_per";
    }

    /** 分享地址 */
    interface ShareUrl{
        /** 首页分享 */
        String APP_DESC_URL = "http://sure.kuangshikeji.com/#/subPackages/share/appDesc";
        /** 商品分享 */
        String GOOD_URL = "http://sure.kuangshikeji.com/#/subPackages/share/good";
        /** 竞猜分享 */
        String GUESS_URL = "http://sure.kuangshikeji.com/#/subPackages/share/guess";
    }



    /** 审批结果 */
    interface approvalResult{

    }

    //代理身份
    interface IdentityCode{
        String PROVINCE = "省级代理";
        String CITY = "市级代理";
        String AREA = "区级代理";
        String NO_AREA = "未开通代理";

    }

}
