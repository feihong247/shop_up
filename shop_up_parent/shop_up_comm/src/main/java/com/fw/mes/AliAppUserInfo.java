package com.fw.mes;

import lombok.Data;

import java.io.Serializable;

@Data
public class AliAppUserInfo implements Serializable {

    // 支付宝 用户编号
    private String userId;
    // 用户头像
    private String avatar;
    // 用户省份
    private String province;
    // 用户城市
    private String city;
    //  支付宝昵称
    private String nickName;

    // 性别  F：女性；
    //      M：男性。
    private String gender;
}
