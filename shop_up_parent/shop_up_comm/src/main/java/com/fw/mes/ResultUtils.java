package com.fw.mes;

import com.fw.enums.CodeStatus;

/**
 * @Author: yanwei
 * @Date: 2018/5/16 16:41
 * @Description:
 */
public final class ResultUtils {

    public static <T> T getResultData(Result<T> result) {
        if (result == null) {
            return null;
        }
        T data = result.getData();
        if (data == null) {
            return null;
        }
        return data;
    }



    /**
     * 无分页
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> Result<T> success(T data) {
        return new Result<T>(Boolean.TRUE, CodeStatus.SUCCESS,data);
    }

    public static <T> Result<T> success() {
        return new Result<T>().success();
    }

    /*public static <T> Result<T> getFault(Integer code,String message) {
        return new Result<T>(code,message);
    }*/



    /*public static Result<BooleanResult> getBooleanResult(boolean result,String msg) {
        int code = result?Result.RESULT_SUCCESS:Result.RESULT_FAULT;
        BooleanResult br = new BooleanResult(result,msg);
        return new Result<BooleanResult>(br,code,msg,null,false);
    }*/





}
