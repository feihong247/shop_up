package com.fw.mes;

import lombok.Data;

import java.io.Serializable;

@Data
public class AliAppUserAccToken  implements Serializable {

    // 支付宝 用户编号
    private String userId;
    // 支付宝 回话标识
    private String accessToken;
    // 回话时间
    private String expiresIn;
    // 刷新token
    private String refreshToken;
    // 刷新token 时间
    private String reExpiresIn;
}
