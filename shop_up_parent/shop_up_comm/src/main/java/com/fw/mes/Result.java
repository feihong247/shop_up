package com.fw.mes;

import com.fw.enums.CodeStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author: tianchong
 * @Date: 2018/5/16 16:38
 * @Description:
 */
@ApiModel("通用返回属性")
@Getter
@Setter
@SuppressWarnings("unchecked")
public class Result<T> extends IResult<T> {

    protected static final Boolean RESULT_SUCCESS = Boolean.TRUE;
    protected static final Boolean RESULT_FAIL = Boolean.FALSE;

    @ApiModelProperty(value = "状态码，200 就是成功", example = "状态码")
    private Integer code;
    @ApiModelProperty(value = "返回信息，一般状态码不是200,前台就可以展示", example = "返回信息")
    private String msg;
    @ApiModelProperty(value = "是否成功, 成功为true, 失败为false", dataType = "boolean")
    private Boolean success = true;


    public Result(T data, Integer errorCode, String errorMsg, Boolean success) {
        super(data);
        this.code = errorCode;
        this.msg = errorMsg;
        this.success = success;

    }

    public Result(Boolean success, T data) {
        this(data, (Integer) null, (String) null, success);
    }


    public Result(Boolean success, T data, String errorMsg) {
        this(data, (Integer) null, errorMsg, success);
    }

    public Result(Boolean success) {
        this((T) null, (Integer) null, (String) null, success);
    }

    public Result(Boolean success, Integer errorCode, String errorMsg) {
        this(null, errorCode, errorMsg, success);
    }

    public Result() {
        this(true);
    }

    public Result(CodeStatus illegalParameter) {
        super(null);
        this.success = RESULT_SUCCESS;
        this.code = illegalParameter.getCode();
        this.msg = illegalParameter.getMessage();
    }

    public Result(Boolean aTrue, CodeStatus success, T data) {
        super(data);
        this.success = aTrue;
        this.code = success.getCode();
        this.msg = success.getMessage();
    }

    /* 成功返回,无返回数据 */
    public Result<T> success() {
        return new Result(CodeStatus.SUCCESS);
    }


    /* 成功返回,有返回数据,有分页 */
    public Result<T> success(Object data) {
        return new Result(RESULT_SUCCESS, data, null);
    }

    /* 成功返回,有返回数据,有分页 */
    public Result<T> success(Object data, String errorMsg) {
        return new Result(RESULT_SUCCESS, data, errorMsg);
    }

    /* 失败返回 */
    public Result<T> fail(int errorCode, String errorMsg) {
        return new Result(RESULT_FAIL, errorCode, errorMsg);
    }
}
