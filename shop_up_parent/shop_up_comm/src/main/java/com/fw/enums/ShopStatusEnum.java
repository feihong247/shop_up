package com.fw.enums;
import lombok.Getter;

/**
 * 商户的审核状态
 */
@Getter
public enum ShopStatusEnum {

    SHOP_STATUS_WAIT(0,"待审核"),
    SHOP_STATUS_PASS(1,"审核成功"),
    SHOP_STATUS_FAIL(2,"审核失败");

    private Integer code;
    private String info;

    ShopStatusEnum(Integer code, String info) {
        this.code = code;
        this.info = info;
    }



}
