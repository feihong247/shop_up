package com.fw.enums;

/**
 * 商户状态
 * 
 * @author ruoyi
 */
public enum ShopStatus
{
    WAIT_PASS(0, "待审核"), PASS_SUCCESS(1, "审核通过"), PASS_FAIL(2, "审核失败");

    private final Integer code;
    private final String info;

    ShopStatus(Integer code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public Integer getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }
}
