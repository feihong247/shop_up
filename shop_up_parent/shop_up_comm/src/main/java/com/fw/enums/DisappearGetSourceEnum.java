package com.fw.enums;

import lombok.Getter;

/**
 * 消证来源枚举
 *
 * @author ruoyi
 */
@Getter
public enum DisappearGetSourceEnum
{
    SHOP_OFFLINE("线下扫码消费产生消证");

    private final String sourceInfo;

    DisappearGetSourceEnum(String sourceInfo)
    {
        this.sourceInfo = sourceInfo;
    }


}
