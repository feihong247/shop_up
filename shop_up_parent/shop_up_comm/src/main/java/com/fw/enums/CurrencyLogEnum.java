package com.fw.enums;

import lombok.Getter;

/**
 * 平台支付标识
 *
 * @author ruoyi
 */
@Getter
public enum CurrencyLogEnum
{
    // 1， 支出，2 收入
    INCOME(1,"商户质押商甲扣除"),
    EXPAND(2,"商户质押商甲退回");
    private final Integer infoForm;
    private final String logName;

    CurrencyLogEnum(Integer infoForm, String logName)
    {
        this.infoForm = infoForm;
        this.logName = logName;
    }

    /**
     * 1商甲 2积分 3消证
     * @return
     */
    public static Integer getShangjiaType(){
        return Integer.valueOf(1);
    }

    public static Integer getIntegralType(){
        return Integer.valueOf(2);
    }

    public static Integer getDisappearType(){
        return Integer.valueOf(3);
    }


}
