package com.fw.enums;

import lombok.Getter;

/**
 * 平台支付标识
 *
 * @author ruoyi
 */
@Getter
public enum ReleaseLogEnum
{
    // 可变type 1，1= 技术，2= 管理,3=原始 4推广
    PUBLISH_POWER(1,"发放技术商甲"),
    PUBLISH_MAN(2,"发放管理商甲"),
    PUBLISH_SOURCE(3,"发放原始商甲"),
    PUBLISH(4,"发放推广商甲"),
    PUBLISH_POWER_BACK(1,"收回技术商甲"),
    PUBLISH_MAN_BACK(2,"收回管理商甲"),
    PUBLISH_SOURCE_BACK(3,"收回原始商甲"),
    PUBLISH_BACK(4,"收回推广商甲");

    private final Integer logType;
    private final String logName;

    ReleaseLogEnum(Integer logType, String logName)
    {
        this.logType = logType;
        this.logName = logName;
    }

    /**
     * 1商甲 2积分 3消证
     * @return
     */
    public static Integer getShangjiaType(){
        return Integer.valueOf(1);
    }

    public static Integer getIntegralType(){
        return Integer.valueOf(2);
    }

    public static Integer getDisappearType(){
        return Integer.valueOf(3);
    }


}
