package com.fw.enums;

import lombok.Getter;

/**
 * 类型枚举
 */
@Getter
public enum LogsTypeEnum {

    USER_MODEL("user_type"),
    PART_MODEL("part_type"),

    // 审核通过
    PART_TIME_IS_TRUE("part_is_true"),
    // 审核未通过
    PART_TIME_IS_FALSE("part_is_false"),
    // 审核 中
    PART_TIME_IS_WAIT("part_is_wait"),
    // 钱包类型
    MONEYS_TYPE("moneys_type"),
    // 提现类型
    WITHDRAW_TYPE("LogsTypeEnum_type"),
    // 订单提交
    ORDER_COMMIT("order_commit"),
    // 订单支付成功
    ORDER_SUCCESS("order_success"),
    // 订单 取消
    ORDER_CANCEL("order_cancel"),
    //收支类型 收入/消证相关类型 获取
    MONEY_INCOME("收入"),
    //收支类型 支出/消证相关类型 兑换为积分
    MONEY_CONSUMPTION("支出"),
    //商甲交易类型  买入
    MONEY_BUY("买入"),
    //商甲交易类型 卖出
    MONEY_SELL("卖出"),
    MONEY_SELL_RETURN("撤回"),
    MONEY_DESTORY("销毁"),

    //商甲相关类型 锁仓
    MONEY_LOCK("锁仓"),
    //商甲相关类型 兑换
    MONEY_RELEASE("兑换"),
    //积分相关类型 兑换
    INTEGRAL_RELEASE("兑换"),
    //积分相关类型 加速
    INTEGRAL_SPEED("加速"),
    //积分相关类型 分红
    INTEGRAL_BONUS("分红"),
    SYS_BONUS("平台分红"),
    //积分相关类型 提现
    INTEGRAL_WITHDRAWAL("提现"),
    //商甲相关类型 销毁
    INTEGRAL_DESTRUCTION("销毁"),
    //商甲相关类型 销毁
    INTEGRAL_DESTRUCTION_PRE("预计销毁"),
    //商甲相关类型 平台卖原始
    MONEY_PLATFORM_ORI("原始卖出"),
    //商甲相关类型 技术分配
    MONEY_PLATFORM_TEC("技术分配"),
    //商甲相关类型 管理分配
    MONEY_PLATFORM_MAN("管理分配"),
    //商甲相关类型 技术分配
    MONEY_RECYCLE_TEC("技术回收"),
    //商甲相关类型 管理分配
    MONEY_RECYCLE_MAN("管理回收"),
    //商甲相关类型 市场推广赠送
    MONEY_PLATFORM_BAZ("推广赠送"),
    //商甲相关类型 兑换
    MONEY_RELEASE_ORI("原始兑换"),
    //商甲相关类型 兑换
    MONEY_RELEASE_TEC("技术兑换"),
    //商甲相关类型 兑换
    MONEY_RELEASE_MAN("管理兑换");


    LogsTypeEnum(String type_name) {
        this.typeName = type_name;
    }
    public String getTypeName(){
        return typeName;
    }
    private String typeName;
}
