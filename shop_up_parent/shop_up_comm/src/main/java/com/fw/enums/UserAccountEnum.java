package com.fw.enums;

/**
 * 用户状态
 *
 * @author ruoyi
 */
public enum UserAccountEnum
{
    USER(0, "普通用户"), DISABLE(1, "技术"), MANAGER(2, "管理"), SOURCE(3, "原始");

    private final Integer code;
    private final String info;

    UserAccountEnum(Integer code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public Integer getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }
}
