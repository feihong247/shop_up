package com.fw.enums;

import lombok.Getter;

/**
 * 平台支付标识
 *
 * @author ruoyi
 */
@Getter
public enum PayTypeEnum
{
    PAY_ALI(0, "支付宝"),
    PAY_WHCHAT(1,"微信"),
    PAY_BANK(2,"银联");

    private final Integer type;
    private final String msg;

    PayTypeEnum(Integer type, String msg)
    {
        this.type = type;
        this.msg = msg;
    }


}
