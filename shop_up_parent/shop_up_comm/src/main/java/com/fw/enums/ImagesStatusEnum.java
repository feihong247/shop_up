package com.fw.enums;
import lombok.Getter;

/**
 * 商户的审核状态
 */
@Getter
public enum ImagesStatusEnum {

    /**\
     * 0=禁用，1=启用
     */
    IMAGES_STATUS_DISABLE(0,"禁用"),
    IMAGES_STATUS_PASS(1,"启用");

    private Integer code;
    private String info;

    ImagesStatusEnum(Integer code, String info) {
        this.code = code;
        this.info = info;
    }



}
