package com.fw.enums;

import lombok.Getter;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 回调信息
 *
 * @author ruoyi
 */
@Getter
public enum AliPayEnum
{
    ALI_ORDER_TEST("/web/aliPay/test_url", "商品标题"),
    SPU_ORDER_PAY("/web/aliPay/spuOrderPay","商品订单支付"),
    order_pay("/order/test_url","订单支付"),
    agency_pay("/web/aliPay/agency_url","代理支付"),
    shop_Ing_Trading("/web/aliPay/shop_Ing_Trading_url","交易大厅买支付"),
    shop_Pay("/web/aliPay/shop_Pay_url","交易大厅商家支付"),
    shopIng_shangJia_pay("/web/aliPay/shopIng_shangJia_pay_url","认购商甲支付"),
    shop_offline_pay("/web/aliPay/shop_offline_pay_url","用户线下消费");

    private final String notifyUrl;
    private final String body;

    AliPayEnum(String notifyUrl, String body)
    {
        this.notifyUrl = notifyUrl;
        this.body = body;
    }


}
