package com.fw.enums;
import lombok.Getter;

/**
 * 关于商品状态的
 * 1 = 下架
 * 0 = 上架
 *
 */
@Getter
public enum CommodityStatusEnum {

     COMMODITY_STATUS_YES(1,"下架"),
     COMMODITY_STATUS_NO(0,"上架");


    private Integer code;
    private String info;

    CommodityStatusEnum(Integer code, String info) {
        this.code = code;
        this.info = info;
    }



}
