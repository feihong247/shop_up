package com.fw.enums;

import lombok.Getter;

@Getter
public enum OpenAuthEnum {

    SHOP_AUTH_KEYS("shop_open_pay");

    private final String body;

    OpenAuthEnum(String body)
    {
        this.body = body;
    }
}
