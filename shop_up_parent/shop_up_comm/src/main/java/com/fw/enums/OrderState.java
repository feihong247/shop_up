package com.fw.enums;

/**
 * @Effect 订单状态
 * @Author 姚
 * @Date 2021/6/10
 **/
public enum OrderState {
    //  `status` tinyint(1) NOT NULL COMMENT '订单状态,-1全部，0=待付款，1=待发货，2=待收货，3=待评价，4=待退货,5=退货/退款，6确认收货，7申请售后',
    ALL_ORDER(-1,"全部"),
    PENDING_PAYMENT (0,"待付款"),
    TO_BE_DELIVERED (1,"待发货"),
    TO_BE_RECEIVED (2,"待收货"),
//    TO_BE_COMMENT (3,"待评价"),
//    TO_BE_REFUND (4,"待退货"),
//    REFUND (5,"退货/退款"),
    RECEIVED(6,"确认收货"),
    AFTER_SALE(7,"申请售后"),


    //售后状态，申请，同意，拒绝
    AFTER_SALE_APPLY(1,"申请"),
    AFTER_SALE_CONSENT(2,"同意"),
    AFTER_SALE_REJECT(3,"拒绝"),


    PAY_TYPE_ALI(0,"支付宝"),
    PAY_TYPE_WECHAT(1,"微信");

    private final Integer code;
    private final String info;

    OrderState(Integer code,String info){
        this.code = code;
        this.info = info;
    }

    public Integer getCode(){return code;}
    public String getInfo(){return info;}
    public static String getName(int index) {
        for (OrderState c : OrderState.values()) {
            if (c.getCode() == index) {
                return c.info;
            }
        }
        return null;
    }
    public static String getName(int index,int count) {
        int countFlag = 0;
        for (OrderState c : OrderState.values()) {
            if (c.getCode() == index) {
                countFlag++;
                if (countFlag == count)
                    return c.info;
            }
        }
        return null;
    }
}
