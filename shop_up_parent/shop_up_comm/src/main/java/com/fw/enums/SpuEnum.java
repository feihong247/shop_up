package com.fw.enums;

import lombok.Getter;

/**
 * 商户状态
 * 
 * @author ruoyi
 */
@Getter
public enum SpuEnum
{   // 线下             //线上
    SPU_OFF("1"), SPU_ON("0");

    private final String code;

    SpuEnum(String code)
    {
        this.code = code;
    }



}
