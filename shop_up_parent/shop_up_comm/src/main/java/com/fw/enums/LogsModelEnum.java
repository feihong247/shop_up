package com.fw.enums;

import lombok.Getter;

/**
 * 模块枚举
 */
@Getter
public enum LogsModelEnum {

    USER_MODEL("用户模块"),
    SYS_BONUS("分红模块"),
    WITHDRAW_MODEL("提现模块"),
    ORDER_MODEL("订单模块"),
    SHANG_JIA_MODEL("商甲模块"),
    MONEYS_MODEL("钱包模块"),
    INTEGRAL_MODEL("积分模块"),
    DISAPPEAR_MODEL("消证模块"),
    DISAPPEAR_VIP_MODEL("返消证送会员模块"),//标识用户获得VIP身份和后边每满1000消证送商甲的日志使用
    ALI_PAY("支付宝支付模块");


    LogsModelEnum(String modelName) {
      this.modelName = modelName;
    }
    public String getModelName(){
        return modelName;
    }
    private String modelName;
}
