package com.fw.enums;

import lombok.Getter;

/**
 * 商户状态
 * 
 * @author ruoyi
 */
@Getter
public enum SpuStatusEnum
{   // 下架             //正常
    DOWN_SPU(1), UP_SPU(0);

    private final Integer code;

    SpuStatusEnum(Integer code)
    {
        this.code = code;
    }



}
