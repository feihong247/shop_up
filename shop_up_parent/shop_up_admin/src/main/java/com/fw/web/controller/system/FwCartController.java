package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwCart;
import com.fw.system.admin.service.IFwCartService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 购物车Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/cart")
public class FwCartController extends BaseController
{
    @Autowired
    private IFwCartService fwCartService;

    /**
     * 查询购物车列表
     */
    @PreAuthorize("@ss.hasPermi('admin:cart:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwCart fwCart)
    {
        startPage();
        List<FwCart> list = fwCartService.selectFwCartList(fwCart);
        return getDataTable(list);
    }

    /**
     * 导出购物车列表
     */
    @PreAuthorize("@ss.hasPermi('admin:cart:export')")
    @Log(title = "购物车", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwCart fwCart)
    {
        List<FwCart> list = fwCartService.selectFwCartList(fwCart);
        ExcelUtil<FwCart> util = new ExcelUtil<FwCart>(FwCart.class);
        return util.exportExcel(list, "cart");
    }

    /**
     * 获取购物车详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:cart:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwCartService.selectFwCartById(id));
    }

    /**
     * 新增购物车
     */
    @PreAuthorize("@ss.hasPermi('admin:cart:add')")
    @Log(title = "购物车", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwCart fwCart)
    {
        return toAjax(fwCartService.insertFwCart(fwCart));
    }

    /**
     * 修改购物车
     */
    @PreAuthorize("@ss.hasPermi('admin:cart:edit')")
    @Log(title = "购物车", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwCart fwCart)
    {
        return toAjax(fwCartService.updateFwCart(fwCart));
    }

    /**
     * 删除购物车
     */
    @PreAuthorize("@ss.hasPermi('admin:cart:remove')")
    @Log(title = "购物车", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwCartService.deleteFwCartByIds(ids));
    }
}
