package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwProblemcate;
import com.fw.system.admin.service.IFwProblemcateService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 常见问题类目Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/problemcate")
public class FwProblemcateController extends BaseController
{
    @Autowired
    private IFwProblemcateService fwProblemcateService;

    /**
     * 查询常见问题类目列表
     */
    @PreAuthorize("@ss.hasPermi('admin:problemcate:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwProblemcate fwProblemcate)
    {
        startPage();
        List<FwProblemcate> list = fwProblemcateService.selectFwProblemcateList(fwProblemcate);
        return getDataTable(list);
    }

    /**
     * 导出常见问题类目列表
     */
    @PreAuthorize("@ss.hasPermi('admin:problemcate:export')")
    @Log(title = "常见问题类目", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwProblemcate fwProblemcate)
    {
        List<FwProblemcate> list = fwProblemcateService.selectFwProblemcateList(fwProblemcate);
        ExcelUtil<FwProblemcate> util = new ExcelUtil<FwProblemcate>(FwProblemcate.class);
        return util.exportExcel(list, "problemcate");
    }

    /**
     * 获取常见问题类目详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:problemcate:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwProblemcateService.selectFwProblemcateById(id));
    }

    /**
     * 新增常见问题类目
     */
    @PreAuthorize("@ss.hasPermi('admin:problemcate:add')")
    @Log(title = "常见问题类目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwProblemcate fwProblemcate)
    {
        return toAjax(fwProblemcateService.insertFwProblemcate(fwProblemcate));
    }

    /**
     * 修改常见问题类目
     */
    @PreAuthorize("@ss.hasPermi('admin:problemcate:edit')")
    @Log(title = "常见问题类目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwProblemcate fwProblemcate)
    {
        return toAjax(fwProblemcateService.updateFwProblemcate(fwProblemcate));
    }

    /**
     * 删除常见问题类目
     */
    @PreAuthorize("@ss.hasPermi('admin:problemcate:remove')")
    @Log(title = "常见问题类目", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwProblemcateService.deleteFwProblemcateByIds(ids));
    }
}
