package com.fw.web.controller.system;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwRuleDatas;
import com.fw.system.admin.service.IFwRuleDatasService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 数据规则Controller
 *
 * @author yanwei
 * @date 2021-06-02
 */
@RestController
@RequestMapping("/admin/rule_datas")
public class FwRuleDatasController extends BaseController {
    @Autowired
    private IFwRuleDatasService fwRuleDatasService;

    /**
     * 查询数据规则列表
     */
    @PreAuthorize("@ss.hasPermi('admin:rule_datas:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwRuleDatas fwRuleDatas) {
        startPage();
        List<FwRuleDatas> list = fwRuleDatasService.selectFwRuleDatasList(fwRuleDatas);
        return getDataTable(list);
    }

    /**
     * 导出数据规则列表
     */
    @PreAuthorize("@ss.hasPermi('admin:rule_datas:export')")
    @Log(title = "数据规则", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwRuleDatas fwRuleDatas) {
        List<FwRuleDatas> list = fwRuleDatasService.selectFwRuleDatasList(fwRuleDatas);
        ExcelUtil<FwRuleDatas> util = new ExcelUtil<FwRuleDatas>(FwRuleDatas.class);
        return util.exportExcel(list, "rule_datas");
    }

    /**
     * 获取数据规则详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:rule_datas:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(fwRuleDatasService.selectFwRuleDatasById(id));
    }

    /**
     * 新增数据规则
     */
    @PreAuthorize("@ss.hasPermi('admin:rule_datas:add')")
    @Log(title = "数据规则", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwRuleDatas fwRuleDatas) {
        return toAjax(fwRuleDatasService.insertFwRuleDatas(fwRuleDatas));
    }

    /**
     * 修改数据规则
     */
    @PreAuthorize("@ss.hasPermi('admin:rule_datas:edit')")
    @Log(title = "数据规则", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwRuleDatas fwRuleDatas) {
        return toAjax(fwRuleDatasService.updateFwRuleDatas(fwRuleDatas));
    }

    /**
     * 删除数据规则
     */
    @PreAuthorize("@ss.hasPermi('admin:rule_datas:remove')")
    @Log(title = "数据规则", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(fwRuleDatasService.deleteFwRuleDatasByIds(ids));
    }
}
