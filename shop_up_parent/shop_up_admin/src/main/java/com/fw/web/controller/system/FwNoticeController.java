package com.fw.web.controller.system;

import java.util.List;

import com.fw.core.domain.entity.SysUser;
import com.fw.core.domain.model.LoginUser;
import com.fw.utils.DateUtils;
import com.fw.utils.ServletUtils;
import com.fw.web.web.service.TokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwNotice;
import com.fw.system.admin.service.IFwNoticeService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 系统公告&通知Controller
 *
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/notice")
@RequiredArgsConstructor
public class FwNoticeController extends BaseController {

    private final IFwNoticeService fwNoticeService;


    private final TokenService tokenService;

    /**
     * 查询系统公告&通知列表
     */
    @PreAuthorize("@ss.hasPermi('admin:notice:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwNotice fwNotice) {
        startPage();
        List<FwNotice> list = fwNoticeService.selectFwNoticeList(fwNotice);
        return getDataTable(list);
    }

    /**
     * 导出系统公告&通知列表
     */
    @PreAuthorize("@ss.hasPermi('admin:notice:export')")
    @Log(title = "系统公告&通知", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwNotice fwNotice) {
        List<FwNotice> list = fwNoticeService.selectFwNoticeList(fwNotice);
        ExcelUtil<FwNotice> util = new ExcelUtil<FwNotice>(FwNotice.class);
        return util.exportExcel(list, "notice");
    }

    /**
     * 获取系统公告&通知详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:notice:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(fwNoticeService.selectFwNoticeById(id));
    }

    /**
     * 新增系统公告&通知
     */
    @PreAuthorize("@ss.hasPermi('admin:notice:add')")
    @Log(title = "系统公告&通知", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwNotice fwNotice) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        SysUser user = loginUser.getUser();
        fwNotice.setCreateBy(user.getUserName());
        return toAjax(fwNoticeService.insertFwNotice(fwNotice));
    }

    /**
     * 修改系统公告&通知
     */
    @PreAuthorize("@ss.hasPermi('admin:notice:edit')")
    @Log(title = "系统公告&通知", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwNotice fwNotice) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        SysUser user = loginUser.getUser();
        fwNotice.setUpdateBy(user.getUserName());
        fwNotice.setUpdateTime(DateUtils.getNowDate());
        return toAjax(fwNoticeService.updateFwNotice(fwNotice));
    }

    /**
     * 删除系统公告&通知
     */
    @PreAuthorize("@ss.hasPermi('admin:notice:remove')")
    @Log(title = "系统公告&通知", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(fwNoticeService.deleteFwNoticeByIds(ids));
    }
}
