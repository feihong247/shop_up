package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwReadPackageLog;
import com.fw.system.admin.service.IFwReadPackageLogService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 红包抽取记录Controller
 * 
 * @author yanwei
 * @date 2021-10-12
 */
@RestController
@RequestMapping("/admin/read_package_log")
public class FwReadPackageLogController extends BaseController
{
    @Autowired
    private IFwReadPackageLogService fwReadPackageLogService;

    /**
     * 查询红包抽取记录列表
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package_log:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwReadPackageLog fwReadPackageLog)
    {
        startPage();
        List<FwReadPackageLog> list = fwReadPackageLogService.selectFwReadPackageLogList(fwReadPackageLog);
        return getDataTable(list);
    }

    /**
     * 导出红包抽取记录列表
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package_log:export')")
    @Log(title = "红包抽取记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwReadPackageLog fwReadPackageLog)
    {
        List<FwReadPackageLog> list = fwReadPackageLogService.selectFwReadPackageLogList(fwReadPackageLog);
        ExcelUtil<FwReadPackageLog> util = new ExcelUtil<FwReadPackageLog>(FwReadPackageLog.class);
        return util.exportExcel(list, "read_package_log");
    }

    /**
     * 获取红包抽取记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package_log:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwReadPackageLogService.selectFwReadPackageLogById(id));
    }

    /**
     * 新增红包抽取记录
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package_log:add')")
    @Log(title = "红包抽取记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwReadPackageLog fwReadPackageLog)
    {
        return toAjax(fwReadPackageLogService.insertFwReadPackageLog(fwReadPackageLog));
    }

    /**
     * 修改红包抽取记录
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package_log:edit')")
    @Log(title = "红包抽取记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwReadPackageLog fwReadPackageLog)
    {
        return toAjax(fwReadPackageLogService.updateFwReadPackageLog(fwReadPackageLog));
    }

    /**
     * 删除红包抽取记录
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package_log:remove')")
    @Log(title = "红包抽取记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwReadPackageLogService.deleteFwReadPackageLogByIds(ids));
    }
}
