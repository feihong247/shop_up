package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwLogShopingAgency;
import com.fw.system.admin.service.IFwLogShopingAgencyService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 用户购买代理日志Controller
 *
 * @author yanwei
 * @date 2021-06-05
 */
@RestController
@RequestMapping("/admin/log_shoping_agency")
public class FwLogShopingAgencyController extends BaseController
{
    @Autowired
    private IFwLogShopingAgencyService fwLogShopingAgencyService;

    /**
     * 查询用户购买代理日志列表
     */
    @PreAuthorize("@ss.hasPermi('admin:log_shoping_agency:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwLogShopingAgency fwLogShopingAgency)
    {
        startPage();
        List<FwLogShopingAgency> list = fwLogShopingAgencyService.selectFwLogShopingAgencyList(fwLogShopingAgency);
        return getDataTable(list);
    }

    /**
     * 导出用户购买代理日志列表
     */
    @PreAuthorize("@ss.hasPermi('admin:log_shoping_agency:export')")
    @Log(title = "用户购买代理日志", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwLogShopingAgency fwLogShopingAgency)
    {
        List<FwLogShopingAgency> list = fwLogShopingAgencyService.selectFwLogShopingAgencyList(fwLogShopingAgency);
        ExcelUtil<FwLogShopingAgency> util = new ExcelUtil<FwLogShopingAgency>(FwLogShopingAgency.class);
        return util.exportExcel(list, "log_shoping_agency");
    }

    /**
     * 获取用户购买代理日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:log_shoping_agency:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwLogShopingAgencyService.selectFwLogShopingAgencyById(id));
    }

    /**
     * 新增用户购买代理日志
     */
    @PreAuthorize("@ss.hasPermi('admin:log_shoping_agency:add')")
    @Log(title = "用户购买代理日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwLogShopingAgency fwLogShopingAgency)
    {
        return toAjax(fwLogShopingAgencyService.insertFwLogShopingAgency(fwLogShopingAgency));
    }

    /**
     * 修改用户购买代理日志
     */
    @PreAuthorize("@ss.hasPermi('admin:log_shoping_agency:edit')")
    @Log(title = "用户购买代理日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwLogShopingAgency fwLogShopingAgency)
    {
        return toAjax(fwLogShopingAgencyService.updateFwLogShopingAgency(fwLogShopingAgency));
    }

    /**
     * 删除用户购买代理日志
     */
    @PreAuthorize("@ss.hasPermi('admin:log_shoping_agency:remove')")
    @Log(title = "用户购买代理日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwLogShopingAgencyService.deleteFwLogShopingAgencyByIds(ids));
    }
}
