package com.fw.web.controller.system;

import java.util.List;

import com.fw.system.admin.service.IFwContactService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwContact;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 关于我们Controller
 *
 * @author yanwei
 * @date 2021-05-24
 */
@RestController
@RequestMapping("/admin/contact")
public class FwContactController extends BaseController
{
    @Autowired
    private IFwContactService fwContactService;

    /**
     * 查询关于我们列表
     */
    @PreAuthorize("@ss.hasPermi('admin:contact:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwContact fwContact)
    {
        startPage();
        List<FwContact> list = fwContactService.selectFwContactList(fwContact);
        return getDataTable(list);
    }

    /**
     * 导出关于我们列表
     */
    @PreAuthorize("@ss.hasPermi('admin:contact:export')")
    @Log(title = "关于我们", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwContact fwContact)
    {
        List<FwContact> list = fwContactService.selectFwContactList(fwContact);
        ExcelUtil<FwContact> util = new ExcelUtil<FwContact>(FwContact.class);
        return util.exportExcel(list, "contact");
    }

    /**
     * 获取关于我们详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:contact:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwContactService.selectFwContactById(id));
    }

    /**
     * 新增关于我们
     */
    @PreAuthorize("@ss.hasPermi('admin:contact:add')")
    @Log(title = "关于我们", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwContact fwContact)
    {
        return toAjax(fwContactService.insertFwContact(fwContact));
    }

    /**
     * 修改关于我们
     */
    @PreAuthorize("@ss.hasPermi('admin:contact:edit')")
    @Log(title = "关于我们", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwContact fwContact)
    {
        return toAjax(fwContactService.updateFwContact(fwContact));
    }

    /**
     * 删除关于我们
     */
    @PreAuthorize("@ss.hasPermi('admin:contact:remove')")
    @Log(title = "关于我们", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwContactService.deleteFwContactByIds(ids));
    }
}
