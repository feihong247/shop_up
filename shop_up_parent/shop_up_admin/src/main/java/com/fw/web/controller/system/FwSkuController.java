package com.fw.web.controller.system;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.system.admin.domain.FwOrder;
import com.fw.system.admin.service.IFwOrderService;
import com.fw.utils.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwSku;
import com.fw.system.admin.service.IFwSkuService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 商品sku规格Controller
 *
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/sku")
public class FwSkuController extends BaseController {
    @Autowired
    private IFwSkuService fwSkuService;
    @Autowired
    private IFwOrderService orderService;

    /**
     * 查询商品sku规格列表
     */
    @PreAuthorize("@ss.hasPermi('admin:sku:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwSku fwSku) {
        startPage();
        List<FwSku> list = fwSkuService.selectFwSkuList(fwSku);
        return getDataTable(list);
    }

    /**
     * 导出商品sku规格列表
     */
    @PreAuthorize("@ss.hasPermi('admin:sku:export')")
    @Log(title = "商品sku规格", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwSku fwSku) {
        List<FwSku> list = fwSkuService.selectFwSkuList(fwSku);
        ExcelUtil<FwSku> util = new ExcelUtil<FwSku>(FwSku.class);
        return util.exportExcel(list, "sku");
    }

    /**
     * 获取商品sku规格详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:sku:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(fwSkuService.selectFwSkuById(id));
    }

    /**
     * 新增商品sku规格
     */
    @PreAuthorize("@ss.hasPermi('admin:sku:add')")
    @Log(title = "商品sku规格", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwSku fwSku) {
        return toAjax(fwSkuService.insertFwSku(fwSku));
    }

    /**
     * 修改商品sku规格
     */
    @PreAuthorize("@ss.hasPermi('admin:sku:edit')")
    @Log(title = "商品sku规格", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwSku fwSku) {
        return toAjax(fwSkuService.updateFwSku(fwSku));
    }

    /**
     * 删除商品sku规格
     */
    @PreAuthorize("@ss.hasPermi('admin:sku:remove')")
    @Log(title = "商品sku规格", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        for (String spuId : ids) {
            //是否能在订单中找到这个商品
            FwOrder order = orderService.getOne(Wrappers.<FwOrder>lambdaQuery()
                    .eq(FwOrder::getSpuId, spuId));
            if (order != null && order.getId() != null) {
                return AjaxResult.error(1,"已出售商品不可删除，可进行下架或修改");
            }
            fwSkuService.removeById(spuId);
        }
        return toAjax(ids.length);
    }
}
