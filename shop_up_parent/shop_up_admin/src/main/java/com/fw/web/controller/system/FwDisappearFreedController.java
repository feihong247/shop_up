package com.fw.web.controller.system;

import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.core.page.TableDataInfo;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwDisappearFreed;
import com.fw.system.admin.service.IFwDisappearFreedService;
import com.fw.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 消证兑换Controller
 *
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/disappear_freed")
public class FwDisappearFreedController extends BaseController
{
    @Autowired
    private IFwDisappearFreedService fwDisappearFreedService;

    /**
     * 查询消证兑换列表
     */
    @PreAuthorize("@ss.hasPermi('admin:disappear_freed:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwDisappearFreed fwDisappearFreed)
    {
        startPage();
        List<FwDisappearFreed> list = fwDisappearFreedService.selectFwDisappearFreedList(fwDisappearFreed);
        return getDataTable(list);
    }

    /**
     * 导出消证兑换列表
     */
    @PreAuthorize("@ss.hasPermi('admin:disappear_freed:export')")
    @Log(title = "消证兑换", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwDisappearFreed fwDisappearFreed)
    {
        List<FwDisappearFreed> list = fwDisappearFreedService.selectFwDisappearFreedList(fwDisappearFreed);
        ExcelUtil<FwDisappearFreed> util = new ExcelUtil<FwDisappearFreed>(FwDisappearFreed.class);
        return util.exportExcel(list, "disappear_freed");
    }

    /**
     * 获取消证兑换详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:disappear_freed:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwDisappearFreedService.selectFwDisappearFreedById(id));
    }

    /**
     * 新增消证兑换
     */
    @PreAuthorize("@ss.hasPermi('admin:disappear_freed:add')")
    @Log(title = "消证兑换", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwDisappearFreed fwDisappearFreed)
    {
        return toAjax(fwDisappearFreedService.insertFwDisappearFreed(fwDisappearFreed));
    }

    /**
     * 修改消证兑换
     */
    @PreAuthorize("@ss.hasPermi('admin:disappear_freed:edit')")
    @Log(title = "消证兑换", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwDisappearFreed fwDisappearFreed)
    {
        return toAjax(fwDisappearFreedService.updateFwDisappearFreed(fwDisappearFreed));
    }

    /**
     * 删除消证兑换
     */
    @PreAuthorize("@ss.hasPermi('admin:disappear_freed:remove')")
    @Log(title = "消证兑换", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwDisappearFreedService.deleteFwDisappearFreedByIds(ids));
    }
}
