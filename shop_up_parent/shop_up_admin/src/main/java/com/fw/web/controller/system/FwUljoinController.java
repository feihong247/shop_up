package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwUljoin;
import com.fw.system.admin.service.IFwUljoinService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 用户和身份中间Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/uljoin")
public class FwUljoinController extends BaseController
{
    @Autowired
    private IFwUljoinService fwUljoinService;

    /**
     * 查询用户和身份中间列表
     */
    @PreAuthorize("@ss.hasPermi('admin:uljoin:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwUljoin fwUljoin)
    {
        startPage();
        List<FwUljoin> list = fwUljoinService.selectFwUljoinList(fwUljoin);
        return getDataTable(list);
    }

    /**
     * 导出用户和身份中间列表
     */
    @PreAuthorize("@ss.hasPermi('admin:uljoin:export')")
    @Log(title = "用户和身份中间", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwUljoin fwUljoin)
    {
        List<FwUljoin> list = fwUljoinService.selectFwUljoinList(fwUljoin);
        ExcelUtil<FwUljoin> util = new ExcelUtil<FwUljoin>(FwUljoin.class);
        return util.exportExcel(list, "uljoin");
    }

    /**
     * 获取用户和身份中间详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:uljoin:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwUljoinService.selectFwUljoinById(id));
    }

    /**
     * 新增用户和身份中间
     */
    @PreAuthorize("@ss.hasPermi('admin:uljoin:add')")
    @Log(title = "用户和身份中间", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwUljoin fwUljoin)
    {
        return toAjax(fwUljoinService.insertFwUljoin(fwUljoin));
    }

    /**
     * 修改用户和身份中间
     */
    @PreAuthorize("@ss.hasPermi('admin:uljoin:edit')")
    @Log(title = "用户和身份中间", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwUljoin fwUljoin)
    {
        return toAjax(fwUljoinService.updateFwUljoin(fwUljoin));
    }

    /**
     * 删除用户和身份中间
     */
    @PreAuthorize("@ss.hasPermi('admin:uljoin:remove')")
    @Log(title = "用户和身份中间", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwUljoinService.deleteFwUljoinByIds(ids));
    }
}
