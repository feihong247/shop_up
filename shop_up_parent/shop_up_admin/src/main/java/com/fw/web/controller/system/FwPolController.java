package com.fw.web.controller.system;

import java.util.List;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.system.admin.domain.FwShop;
import com.fw.system.admin.service.IFwShopService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwPol;
import com.fw.system.admin.service.IFwPolService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 地图Controller
 *
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/pol")
public class FwPolController extends BaseController
{
    @Autowired
    private IFwPolService fwPolService;

    @Autowired
    private IFwShopService shopService;

    /**
     * 查询地图列表
     */
    @PreAuthorize("@ss.hasPermi('admin:pol:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwPol fwPol)
    {
        startPage();
        List<FwPol> list = fwPolService.selectFwPolList(fwPol);
        return getDataTable(list);
    }

    /**
     * 查询
     */
    @PreAuthorize("@ss.hasPermi('admin:pol:list')")
    @GetMapping("/listShopPolList")
    public TableDataInfo listShopPolList(FwPol fwPol)
    {
        startPage();
        List<FwPol> list = fwPolService.selectFwPolList(fwPol);
        if (list.isEmpty())
            return getDataTable(list);
        return getDataTable(shopService.list(Wrappers.<FwShop>lambdaQuery().in(FwShop::getId,list.parallelStream().map(item -> item.getBusId()).collect(Collectors.toList()))));
    }

    /**
     * 导出地图列表
     */
    @PreAuthorize("@ss.hasPermi('admin:pol:export')")
    @Log(title = "地图", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwPol fwPol)
    {
        List<FwPol> list = fwPolService.selectFwPolList(fwPol);
        ExcelUtil<FwPol> util = new ExcelUtil<FwPol>(FwPol.class);
        return util.exportExcel(list, "pol");
    }

    /**
     * 获取地图详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:pol:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwPolService.selectFwPolById(id));
    }

    /**
     * 获取地图详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:pol:query')")
    @GetMapping(value = "/getByBusId/{busId}")
    public AjaxResult getByBusId(@PathVariable("busId") String busId)
    {
        return AjaxResult.success(fwPolService.getOne(Wrappers.<FwPol>lambdaQuery().eq(FwPol::getBusId,busId)));
    }

    /**
     * 新增地图
     */
    @PreAuthorize("@ss.hasPermi('admin:pol:add')")
    @Log(title = "地图", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwPol fwPol)
    {
        return toAjax(fwPolService.insertFwPol(fwPol));
    }

    /**
     * 修改地图
     */
    @PreAuthorize("@ss.hasPermi('admin:pol:edit')")
    @Log(title = "地图", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwPol fwPol)
    {
        return toAjax(fwPolService.updateFwPol(fwPol));
    }

    /**
     * 删除地图
     */
    @PreAuthorize("@ss.hasPermi('admin:pol:remove')")
    @Log(title = "地图", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwPolService.deleteFwPolByIds(ids));
    }
}
