package com.fw.web.controller.system;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwFootprint;
import com.fw.system.admin.service.IFwFootprintService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 足迹Controller
 *
 * @author yanwei
 * @date 2021-06-23
 */
@RestController
@RequestMapping("/admin/footprint")
public class FwFootprintController extends BaseController {
    @Autowired
    private IFwFootprintService fwFootprintService;

    /**
     * 查询足迹列表
     */
    @PreAuthorize("@ss.hasPermi('admin:footprint:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwFootprint fwFootprint) {
        startPage();
        List<FwFootprint> list = fwFootprintService.selectFwFootprintList(fwFootprint);
        return getDataTable(list);
    }

    /**
     * 导出足迹列表
     */
    @PreAuthorize("@ss.hasPermi('admin:footprint:export')")
    @Log(title = "足迹", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwFootprint fwFootprint) {
        List<FwFootprint> list = fwFootprintService.selectFwFootprintList(fwFootprint);
        ExcelUtil<FwFootprint> util = new ExcelUtil<FwFootprint>(FwFootprint.class);
        return util.exportExcel(list, "footprint");
    }

    /**
     * 获取足迹详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:footprint:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(fwFootprintService.selectFwFootprintById(id));
    }

    /**
     * 新增足迹
     */
    @PreAuthorize("@ss.hasPermi('admin:footprint:add')")
    @Log(title = "足迹", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwFootprint fwFootprint) {
        return toAjax(fwFootprintService.insertFwFootprint(fwFootprint));
    }

    /**
     * 修改足迹
     */
    @PreAuthorize("@ss.hasPermi('admin:footprint:edit')")
    @Log(title = "足迹", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwFootprint fwFootprint) {
        return toAjax(fwFootprintService.updateFwFootprint(fwFootprint));
    }

    /**
     * 删除足迹
     */
    @PreAuthorize("@ss.hasPermi('admin:footprint:remove')")
    @Log(title = "足迹", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(fwFootprintService.deleteFwFootprintByIds(ids));
    }
}
