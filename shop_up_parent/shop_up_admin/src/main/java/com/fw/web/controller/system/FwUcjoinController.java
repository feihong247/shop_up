package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwUcjoin;
import com.fw.system.admin.service.IFwUcjoinService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 我的收藏中间Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/ucjoin")
public class FwUcjoinController extends BaseController
{
    @Autowired
    private IFwUcjoinService fwUcjoinService;

    /**
     * 查询我的收藏中间列表
     */
    @PreAuthorize("@ss.hasPermi('admin:ucjoin:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwUcjoin fwUcjoin)
    {
        startPage();
        List<FwUcjoin> list = fwUcjoinService.selectFwUcjoinList(fwUcjoin);
        return getDataTable(list);
    }

    /**
     * 导出我的收藏中间列表
     */
    @PreAuthorize("@ss.hasPermi('admin:ucjoin:export')")
    @Log(title = "我的收藏中间", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwUcjoin fwUcjoin)
    {
        List<FwUcjoin> list = fwUcjoinService.selectFwUcjoinList(fwUcjoin);
        ExcelUtil<FwUcjoin> util = new ExcelUtil<FwUcjoin>(FwUcjoin.class);
        return util.exportExcel(list, "ucjoin");
    }

    /**
     * 获取我的收藏中间详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:ucjoin:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwUcjoinService.selectFwUcjoinById(id));
    }

    /**
     * 新增我的收藏中间
     */
    @PreAuthorize("@ss.hasPermi('admin:ucjoin:add')")
    @Log(title = "我的收藏中间", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwUcjoin fwUcjoin)
    {
        return toAjax(fwUcjoinService.insertFwUcjoin(fwUcjoin));
    }

    /**
     * 修改我的收藏中间
     */
    @PreAuthorize("@ss.hasPermi('admin:ucjoin:edit')")
    @Log(title = "我的收藏中间", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwUcjoin fwUcjoin)
    {
        return toAjax(fwUcjoinService.updateFwUcjoin(fwUcjoin));
    }

    /**
     * 删除我的收藏中间
     */
    @PreAuthorize("@ss.hasPermi('admin:ucjoin:remove')")
    @Log(title = "我的收藏中间", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwUcjoinService.deleteFwUcjoinByIds(ids));
    }
}
