package com.fw.web.controller.system;

import java.util.List;

import cn.hutool.core.lang.Assert;
import com.fw.core.domain.model.LoginUser;
import com.fw.system.admin.domain.FwAgentRegion;
import com.fw.system.admin.service.ISysUserService;
import com.fw.utils.ServletUtils;
import com.fw.web.web.service.TokenService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwOneAreaItem;
import com.fw.system.admin.service.IFwOneAreaItemService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 一县一品Controller
 * 
 * @author yanwei
 * @date 2021-11-09
 */
@RestController
@RequestMapping("/admin/one_area_item")
public class FwOneAreaItemController extends BaseController
{
    @Autowired
    private IFwOneAreaItemService fwOneAreaItemService;

    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysUserService sysUserService;

    /**
     * 查询一县一品列表
     */
    @PreAuthorize("@ss.hasPermi('admin:one_area_item:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwOneAreaItem fwOneAreaItem)
    {
        // 获取省市区
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 如果是超级管理员就不在管理
        if (!"admin".equals(loginUser.getUsername())) {
            FwAgentRegion agentRegion = sysUserService.getAgentRegion(loginUser);
            fwOneAreaItem.setArea(agentRegion.getArea());
            fwOneAreaItem.setCity(agentRegion.getCity());
            fwOneAreaItem.setProvince(agentRegion.getProvince());
        }
        startPage();
        List<FwOneAreaItem> list = fwOneAreaItemService.selectFwOneAreaItemList(fwOneAreaItem);
        return getDataTable(list);
    }

    /**
     * 导出一县一品列表
     */
    @PreAuthorize("@ss.hasPermi('admin:one_area_item:export')")
    @Log(title = "一县一品", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwOneAreaItem fwOneAreaItem)
    {

        List<FwOneAreaItem> list = fwOneAreaItemService.selectFwOneAreaItemList(fwOneAreaItem);
        ExcelUtil<FwOneAreaItem> util = new ExcelUtil<FwOneAreaItem>(FwOneAreaItem.class);
        return util.exportExcel(list, "one_area_item");
    }

    /**
     * 获取一县一品详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:one_area_item:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwOneAreaItemService.selectFwOneAreaItemById(id));
    }

    /**
     * 新增一县一品
     */
    @PreAuthorize("@ss.hasPermi('admin:one_area_item:add')")
    @Log(title = "一县一品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwOneAreaItem fwOneAreaItem)
    {
        // 获取省市区
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        FwAgentRegion agentRegion = sysUserService.getAgentRegion(loginUser);
        fwOneAreaItem.setArea(agentRegion.getArea());
        fwOneAreaItem.setCity(agentRegion.getCity());
        fwOneAreaItem.setProvince(agentRegion.getProvince());

        return toAjax(fwOneAreaItemService.insertFwOneAreaItem(fwOneAreaItem));
    }

    /**
     * 修改一县一品
     */
    @PreAuthorize("@ss.hasPermi('admin:one_area_item:edit')")
    @Log(title = "一县一品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwOneAreaItem fwOneAreaItem)
    {
        // 获取省市区
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        FwAgentRegion agentRegion = sysUserService.getAgentRegion(loginUser);
        fwOneAreaItem.setArea(agentRegion.getArea());
        fwOneAreaItem.setCity(agentRegion.getCity());
        fwOneAreaItem.setProvince(agentRegion.getProvince());
        return toAjax(fwOneAreaItemService.updateFwOneAreaItem(fwOneAreaItem));
    }

    /**
     * 删除一县一品
     */
    @PreAuthorize("@ss.hasPermi('admin:one_area_item:remove')")
    @Log(title = "一县一品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwOneAreaItemService.deleteFwOneAreaItemByIds(ids));
    }
}
