package com.fw.web.controller.system;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwService;
import com.fw.system.admin.service.IFwServiceService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 在线客服Controller
 *
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/service")
public class FwServiceController extends BaseController {
    @Autowired
    private IFwServiceService fwServiceService;

    /**
     * 查询在线客服列表
     */
    @PreAuthorize("@ss.hasPermi('admin:service:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwService fwService) {
        startPage();
        List<FwService> list = fwServiceService.selectFwServiceList(fwService);
        return getDataTable(list);
    }

    /**
     * 导出在线客服列表
     */
    @PreAuthorize("@ss.hasPermi('admin:service:export')")
    @Log(title = "在线客服", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwService fwService) {
        List<FwService> list = fwServiceService.selectFwServiceList(fwService);
        ExcelUtil<FwService> util = new ExcelUtil<FwService>(FwService.class);
        return util.exportExcel(list, "service");
    }

    /**
     * 获取在线客服详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:service:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(fwServiceService.selectFwServiceById(id));
    }

    /**
     * 新增在线客服
     */
    @PreAuthorize("@ss.hasPermi('admin:service:add')")
    @Log(title = "在线客服", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwService fwService) {
        List<FwService> list = fwServiceService.list();
        if (list.size()!=0){
            return AjaxResult.error("该数据仅可添加一条");
        }
        return toAjax(fwServiceService.insertFwService(fwService));
    }

    /**
     * 修改在线客服
     */
    @PreAuthorize("@ss.hasPermi('admin:service:edit')")
    @Log(title = "在线客服", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwService fwService) {
        return toAjax(fwServiceService.updateFwService(fwService));
    }

    /**
     * 删除在线客服
     */
    @PreAuthorize("@ss.hasPermi('admin:service:remove')")
    @Log(title = "在线客服", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(fwServiceService.deleteFwServiceByIds(ids));
    }
}
