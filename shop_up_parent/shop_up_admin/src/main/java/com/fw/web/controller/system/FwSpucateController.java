package com.fw.web.controller.system;

import java.util.List;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwSpucate;
import com.fw.system.admin.service.IFwSpucateService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 商品类目Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/spucate")
public class FwSpucateController extends BaseController
{
    @Autowired
    private IFwSpucateService fwSpucateService;

    /**
     * 查询商品类目列表
     */
    @PreAuthorize("@ss.hasPermi('admin:spucate:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwSpucate fwSpucate)
    {
        startPage();
        List<FwSpucate> list = fwSpucateService.selectFwSpucateList(fwSpucate);
        return getDataTable(list);
    }

    /**
     * 导出商品类目列表
     */
    @PreAuthorize("@ss.hasPermi('admin:spucate:export')")
    @Log(title = "商品类目", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwSpucate fwSpucate)
    {
        List<FwSpucate> list = fwSpucateService.selectFwSpucateList(fwSpucate);
        ExcelUtil<FwSpucate> util = new ExcelUtil<FwSpucate>(FwSpucate.class);
        return util.exportExcel(list, "spucate");
    }

    /**
     * 获取商品类目详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:spucate:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwSpucateService.selectFwSpucateById(id));
    }

    /**
     * 新增商品类目
     */
    @PreAuthorize("@ss.hasPermi('admin:spucate:add')")
    @Log(title = "商品类目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwSpucate fwSpucate)
    {
        return toAjax(fwSpucateService.insertFwSpucate(fwSpucate));
    }

    /**
     * 修改商品类目
     */
    @PreAuthorize("@ss.hasPermi('admin:spucate:edit')")
    @Log(title = "商品类目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwSpucate fwSpucate)
    {
        return toAjax(fwSpucateService.updateFwSpucate(fwSpucate));
    }

    /**
     * 删除商品类目
     */
    @PreAuthorize("@ss.hasPermi('admin:spucate:remove')")
    @Log(title = "商品类目", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwSpucateService.deleteFwSpucateByIds(ids));
    }

    /** 获取类目树 */
    @GetMapping("/getCateTree")
    public AjaxResult getCateTree(){
        List<FwSpucate> firstList = fwSpucateService.list(Wrappers.<FwSpucate>lambdaQuery()
                .eq(FwSpucate::getParentId, "-1")
                .or()
                .isNull(FwSpucate::getParentId));
        for (FwSpucate spucate : firstList) {
            spucate.setSpucateList(fwSpucateService.list(Wrappers.<FwSpucate>lambdaQuery()
                    .eq(FwSpucate::getParentId, spucate.getId())));
        }
        return AjaxResult.success(firstList);
    }
}
