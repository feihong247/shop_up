package com.fw.web.controller.system;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwBanner;
import com.fw.system.admin.service.IFwBannerService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 首页轮播Controller
 *
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/banner")
public class FwBannerController extends BaseController {
    @Autowired
    private IFwBannerService fwBannerService;

    /**
     * 查询首页轮播列表
     */
    @PreAuthorize("@ss.hasPermi('admin:banner:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwBanner fwBanner) {
        startPage();
        List<FwBanner> list = fwBannerService.selectFwBannerList(fwBanner);
        return getDataTable(list);
    }

    /**
     * 导出首页轮播列表
     */
    @PreAuthorize("@ss.hasPermi('admin:banner:export')")
    @Log(title = "首页轮播", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwBanner fwBanner) {
        List<FwBanner> list = fwBannerService.selectFwBannerList(fwBanner);
        ExcelUtil<FwBanner> util = new ExcelUtil<FwBanner>(FwBanner.class);
        return util.exportExcel(list, "banner");
    }

    /**
     * 获取首页轮播详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:banner:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(fwBannerService.selectFwBannerById(id));
    }

    /**
     * 新增首页轮播
     */
    @PreAuthorize("@ss.hasPermi('admin:banner:add')")
    @Log(title = "首页轮播", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwBanner fwBanner) {
        List<FwBanner> list = fwBannerService.list();
        if (list.size()>=6){
            return AjaxResult.error("轮播图最多可添加六张");
        }
        return toAjax(fwBannerService.insertFwBanner(fwBanner));
    }

    /**
     * 修改首页轮播
     */
    @PreAuthorize("@ss.hasPermi('admin:banner:edit')")
    @Log(title = "首页轮播", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwBanner fwBanner) {
        return toAjax(fwBannerService.updateFwBanner(fwBanner));
    }

    /**
     * 删除首页轮播
     */
    @PreAuthorize("@ss.hasPermi('admin:banner:remove')")
    @Log(title = "首页轮播", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(fwBannerService.deleteFwBannerByIds(ids));
    }
}
