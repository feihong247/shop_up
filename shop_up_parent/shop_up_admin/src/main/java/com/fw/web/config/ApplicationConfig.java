package com.fw.web.config;

import com.fw.common.AliComm;
import com.fw.common.IdXD;
/*
import com.fw.filter.TraceIdFilterImpl;
*/
import com.fw.system.config.GenConfig;
import com.fw.system.config.ScheduleConfig;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;

import javax.servlet.Filter;
import java.util.TimeZone;

/**
 * 程序注解配置
 *
 * @author ruoyi
 */
@Configuration
// 表示通过aop框架暴露该代理对象,AopContext能够访问
@EnableAspectJAutoProxy(exposeProxy = true)
// 指定要扫描的Mapper类的包的路径
@Import({ScheduleConfig.class, GenConfig.class})
public class ApplicationConfig
{
    /**
     * 时区配置
     */
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jacksonObjectMapperCustomization()
    {
        return jacksonObjectMapperBuilder -> jacksonObjectMapperBuilder.timeZone(TimeZone.getDefault());
    }

    @Bean
    public IdXD idXD() {
        return new IdXD();
    }

    @Bean(initMethod = "initAliConfig")
    public AliComm aliComm(){
        return new AliComm();
    }


    //注解配置filter示例
/*    @Bean
    public FilterRegistrationBean filterTraceIdRegistrationBean() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(initCustomFilter());
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.setOrder(Integer.MIN_VALUE);
        return filterRegistrationBean;
    }*/

/*
    public Filter initCustomFilter() {
        return new TraceIdFilterImpl();
    }
*/
}
