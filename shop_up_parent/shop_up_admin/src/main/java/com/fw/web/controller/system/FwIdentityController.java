package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwIdentity;
import com.fw.system.admin.service.IFwIdentityService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 身份Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/Identity")
public class FwIdentityController extends BaseController
{
    @Autowired
    private IFwIdentityService fwIdentityService;

    /**
     * 查询身份列表
     */
    @PreAuthorize("@ss.hasPermi('admin:Identity:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwIdentity fwIdentity)
    {
        startPage();
        List<FwIdentity> list = fwIdentityService.selectFwIdentityList(fwIdentity);
        return getDataTable(list);
    }

    /**
     * 导出身份列表
     */
    @PreAuthorize("@ss.hasPermi('admin:Identity:export')")
    @Log(title = "身份", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwIdentity fwIdentity)
    {
        List<FwIdentity> list = fwIdentityService.selectFwIdentityList(fwIdentity);
        ExcelUtil<FwIdentity> util = new ExcelUtil<FwIdentity>(FwIdentity.class);
        return util.exportExcel(list, "Identity");
    }

    /**
     * 获取身份详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:Identity:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwIdentityService.selectFwIdentityById(id));
    }

    /**
     * 新增身份
     */
    @PreAuthorize("@ss.hasPermi('admin:Identity:add')")
    @Log(title = "身份", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwIdentity fwIdentity)
    {
        return toAjax(fwIdentityService.insertFwIdentity(fwIdentity));
    }

    /**
     * 修改身份
     */
    @PreAuthorize("@ss.hasPermi('admin:Identity:edit')")
    @Log(title = "身份", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwIdentity fwIdentity)
    {
        return toAjax(fwIdentityService.updateFwIdentity(fwIdentity));
    }

    /**
     * 删除身份
     */
    @PreAuthorize("@ss.hasPermi('admin:Identity:remove')")
    @Log(title = "身份", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwIdentityService.deleteFwIdentityByIds(ids));
    }
}
