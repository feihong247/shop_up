package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwReadPackage;
import com.fw.system.admin.service.IFwReadPackageService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 红包类型Controller
 * 
 * @author yanwei
 * @date 2021-10-12
 */
@RestController
@RequestMapping("/admin/read_package")
public class FwReadPackageController extends BaseController
{
    @Autowired
    private IFwReadPackageService fwReadPackageService;

    /**
     * 查询红包类型列表
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwReadPackage fwReadPackage)
    {
        startPage();
        List<FwReadPackage> list = fwReadPackageService.selectFwReadPackageList(fwReadPackage);
        return getDataTable(list);
    }

    /**
     * 导出红包类型列表
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package:export')")
    @Log(title = "红包类型", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwReadPackage fwReadPackage)
    {
        List<FwReadPackage> list = fwReadPackageService.selectFwReadPackageList(fwReadPackage);
        ExcelUtil<FwReadPackage> util = new ExcelUtil<FwReadPackage>(FwReadPackage.class);
        return util.exportExcel(list, "read_package");
    }

    /**
     * 获取红包类型详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwReadPackageService.selectFwReadPackageById(id));
    }

    /**
     * 新增红包类型
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package:add')")
    @Log(title = "红包类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwReadPackage fwReadPackage)
    {
        return toAjax(fwReadPackageService.insertFwReadPackage(fwReadPackage));
    }

    /**
     * 修改红包类型
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package:edit')")
    @Log(title = "红包类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwReadPackage fwReadPackage)
    {
        return toAjax(fwReadPackageService.updateFwReadPackage(fwReadPackage));
    }

    /**
     * 删除红包类型
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package:remove')")
    @Log(title = "红包类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwReadPackageService.deleteFwReadPackageByIds(ids));
    }
}
