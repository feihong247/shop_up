package com.fw.web.controller.system;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwUsjoin;
import com.fw.system.admin.service.IFwUsjoinService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 我的商铺收藏中间Controller
 *
 * @author yanwei
 * @date 2021-05-24
 */
@RestController
@RequestMapping("/admin/usjoin")
public class FwUsjoinController extends BaseController {
    @Autowired
    private IFwUsjoinService fwUsjoinService;

    /**
     * 查询我的商铺收藏中间列表
     */
    @PreAuthorize("@ss.hasPermi('admin:usjoin:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwUsjoin fwUsjoin) {
        startPage();
        List<FwUsjoin> list = fwUsjoinService.selectFwUsjoinList(fwUsjoin);
        return getDataTable(list);
    }

    /**
     * 导出我的商铺收藏中间列表
     */
    @PreAuthorize("@ss.hasPermi('admin:usjoin:export')")
    @Log(title = "我的商铺收藏中间", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwUsjoin fwUsjoin) {
        List<FwUsjoin> list = fwUsjoinService.selectFwUsjoinList(fwUsjoin);
        ExcelUtil<FwUsjoin> util = new ExcelUtil<FwUsjoin>(FwUsjoin.class);
        return util.exportExcel(list, "usjoin");
    }

    /**
     * 获取我的商铺收藏中间详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:usjoin:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(fwUsjoinService.selectFwUsjoinById(id));
    }

    /**
     * 新增我的商铺收藏中间
     */
    @PreAuthorize("@ss.hasPermi('admin:usjoin:add')")
    @Log(title = "我的商铺收藏中间", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwUsjoin fwUsjoin) {
        return toAjax(fwUsjoinService.insertFwUsjoin(fwUsjoin));
    }

    /**
     * 修改我的商铺收藏中间
     */
    @PreAuthorize("@ss.hasPermi('admin:usjoin:edit')")
    @Log(title = "我的商铺收藏中间", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwUsjoin fwUsjoin) {
        return toAjax(fwUsjoinService.updateFwUsjoin(fwUsjoin));
    }

    /**
     * 删除我的商铺收藏中间
     */
    @PreAuthorize("@ss.hasPermi('admin:usjoin:remove')")
    @Log(title = "我的商铺收藏中间", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(fwUsjoinService.deleteFwUsjoinByIds(ids));
    }
}
