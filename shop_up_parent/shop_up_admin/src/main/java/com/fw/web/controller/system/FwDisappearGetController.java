package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwDisappearGet;
import com.fw.system.admin.service.IFwDisappearGetService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 消证获取Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/disappear_get")
public class FwDisappearGetController extends BaseController
{
    @Autowired
    private IFwDisappearGetService fwDisappearGetService;

    /**
     * 查询消证获取列表
     */
    @PreAuthorize("@ss.hasPermi('admin:disappear_get:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwDisappearGet fwDisappearGet)
    {
        startPage();
        List<FwDisappearGet> list = fwDisappearGetService.selectFwDisappearGetList(fwDisappearGet);
        return getDataTable(list);
    }

    /**
     * 导出消证获取列表
     */
    @PreAuthorize("@ss.hasPermi('admin:disappear_get:export')")
    @Log(title = "消证获取", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwDisappearGet fwDisappearGet)
    {
        List<FwDisappearGet> list = fwDisappearGetService.selectFwDisappearGetList(fwDisappearGet);
        ExcelUtil<FwDisappearGet> util = new ExcelUtil<FwDisappearGet>(FwDisappearGet.class);
        return util.exportExcel(list, "disappear_get");
    }

    /**
     * 获取消证获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:disappear_get:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwDisappearGetService.selectFwDisappearGetById(id));
    }

    /**
     * 新增消证获取
     */
    @PreAuthorize("@ss.hasPermi('admin:disappear_get:add')")
    @Log(title = "消证获取", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwDisappearGet fwDisappearGet)
    {
        return toAjax(fwDisappearGetService.insertFwDisappearGet(fwDisappearGet));
    }

    /**
     * 修改消证获取
     */
    @PreAuthorize("@ss.hasPermi('admin:disappear_get:edit')")
    @Log(title = "消证获取", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwDisappearGet fwDisappearGet)
    {
        return toAjax(fwDisappearGetService.updateFwDisappearGet(fwDisappearGet));
    }

    /**
     * 删除消证获取
     */
    @PreAuthorize("@ss.hasPermi('admin:disappear_get:remove')")
    @Log(title = "消证获取", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwDisappearGetService.deleteFwDisappearGetByIds(ids));
    }
}
