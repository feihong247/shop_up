package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwMsg;
import com.fw.system.admin.service.IFwMsgService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 用户消息Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/msg")
public class FwMsgController extends BaseController
{
    @Autowired
    private IFwMsgService fwMsgService;

    /**
     * 查询用户消息列表
     */
    @PreAuthorize("@ss.hasPermi('admin:msg:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwMsg fwMsg)
    {
        startPage();
        List<FwMsg> list = fwMsgService.selectFwMsgList(fwMsg);
        return getDataTable(list);
    }

    /**
     * 导出用户消息列表
     */
    @PreAuthorize("@ss.hasPermi('admin:msg:export')")
    @Log(title = "用户消息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwMsg fwMsg)
    {
        List<FwMsg> list = fwMsgService.selectFwMsgList(fwMsg);
        ExcelUtil<FwMsg> util = new ExcelUtil<FwMsg>(FwMsg.class);
        return util.exportExcel(list, "msg");
    }

    /**
     * 获取用户消息详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:msg:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwMsgService.selectFwMsgById(id));
    }

    /**
     * 新增用户消息
     */
    @PreAuthorize("@ss.hasPermi('admin:msg:add')")
    @Log(title = "用户消息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwMsg fwMsg)
    {
        return toAjax(fwMsgService.insertFwMsg(fwMsg));
    }

    /**
     * 修改用户消息
     */
    @PreAuthorize("@ss.hasPermi('admin:msg:edit')")
    @Log(title = "用户消息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwMsg fwMsg)
    {
        return toAjax(fwMsgService.updateFwMsg(fwMsg));
    }

    /**
     * 删除用户消息
     */
    @PreAuthorize("@ss.hasPermi('admin:msg:remove')")
    @Log(title = "用户消息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwMsgService.deleteFwMsgByIds(ids));
    }
}
