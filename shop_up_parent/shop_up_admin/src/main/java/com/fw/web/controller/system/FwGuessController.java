package com.fw.web.controller.system;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwGuess;
import com.fw.system.admin.service.IFwGuessService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 竞猜Controller
 *
 * @author yanwei
 * @date 2021-06-23
 */
@RestController
@RequestMapping("/admin/guess")
public class FwGuessController extends BaseController {
    private final IFwGuessService fwGuessService;

    public FwGuessController(IFwGuessService fwGuessService) {
        this.fwGuessService = fwGuessService;
    }

    /**
     * 查询竞猜列表
     */
    @PreAuthorize("@ss.hasPermi('admin:guess:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwGuess fwGuess) {
        startPage();
        List<FwGuess> list = fwGuessService.selectFwGuessList(fwGuess);
        return getDataTable(list);
    }

    /**
     * 导出竞猜列表
     */
    @PreAuthorize("@ss.hasPermi('admin:guess:export')")
    @Log(title = "竞猜", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwGuess fwGuess) {
        List<FwGuess> list = fwGuessService.selectFwGuessList(fwGuess);
        ExcelUtil<FwGuess> util = new ExcelUtil<>(FwGuess.class);
        return util.exportExcel(list, "guess");
    }

    /**
     * 获取竞猜详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:guess:query')")
    @GetMapping(value = "/{guessId}")
    public AjaxResult getInfo(@PathVariable("guessId") String guessId) {
        return AjaxResult.success(fwGuessService.selectFwGuessById(guessId));
    }

    /**
     * 新增竞猜
     */
    @PreAuthorize("@ss.hasPermi('admin:guess:add')")
    @Log(title = "竞猜", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwGuess fwGuess) {
        return toAjax(fwGuessService.insertFwGuess(fwGuess));
    }

    /**
     * 修改竞猜
     */
    @PreAuthorize("@ss.hasPermi('admin:guess:edit')")
    @Log(title = "竞猜", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwGuess fwGuess) {
        return toAjax(fwGuessService.updateFwGuess(fwGuess));
    }

    /**
     * 删除竞猜
     */
    @PreAuthorize("@ss.hasPermi('admin:guess:remove')")
    @Log(title = "竞猜", businessType = BusinessType.DELETE)
    @DeleteMapping("/{guessIds}")
    public AjaxResult remove(@PathVariable String[] guessIds) {
        return toAjax(fwGuessService.deleteFwGuessByIds(guessIds));
    }
}
