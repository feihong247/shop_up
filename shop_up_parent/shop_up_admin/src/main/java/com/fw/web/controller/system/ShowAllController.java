package com.fw.web.controller.system;

import com.fw.constant.Constants;
import com.fw.core.domain.AjaxResult;
import com.fw.core.domain.model.LoginUser;
import com.fw.core.redis.RedisCache;
import com.fw.mes.Result;
import com.fw.system.admin.domain.vo.ShopScreenDataVo;
import com.fw.system.admin.service.ISysUserService;
import com.fw.utils.ServletUtils;
import com.fw.web.web.service.TokenService;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static com.fw.constant.Constants.ADMIN_LOOK;
import static com.fw.mes.ResultUtils.success;

@RestController
@RequestMapping("/admin/showAll")
public class ShowAllController {
    @Autowired
    private RedisCache redisCache;

    @Autowired
    private TokenService tokenServicel;

    @Autowired
    private ISysUserService sysUserService;

    @GetMapping("/showIndex")
    @ApiOperation("首页查看大屏")
    public Result<List<Object>> showAll(){

        LoginUser loginUser = tokenServicel.getLoginUser(ServletUtils.getRequest());
        String shopId = sysUserService.isShop(loginUser);
        if (StringUtils.isNotBlank(shopId))
            return success(redisCache.getCacheList(Constants.SHOP_REDIS_KEY.concat(shopId)));

        return success(redisCache.getCacheList(ADMIN_LOOK));
     }
}
