package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwShangjipool;
import com.fw.system.admin.service.IFwShangjipoolService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 商甲池Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/shangjipool")
public class FwShangjipoolController extends BaseController
{
    @Autowired
    private IFwShangjipoolService fwShangjipoolService;

    /**
     * 查询商甲池列表
     */
    @PreAuthorize("@ss.hasPermi('admin:shangjipool:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwShangjipool fwShangjipool)
    {
        startPage();
        List<FwShangjipool> list = fwShangjipoolService.selectFwShangjipoolList(fwShangjipool);
        return getDataTable(list);
    }

    /**
     * 导出商甲池列表
     */
    @PreAuthorize("@ss.hasPermi('admin:shangjipool:export')")
    @Log(title = "商甲池", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwShangjipool fwShangjipool)
    {
        List<FwShangjipool> list = fwShangjipoolService.selectFwShangjipoolList(fwShangjipool);
        ExcelUtil<FwShangjipool> util = new ExcelUtil<FwShangjipool>(FwShangjipool.class);
        return util.exportExcel(list, "shangjipool");
    }

    /**
     * 获取商甲池详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:shangjipool:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwShangjipoolService.selectFwShangjipoolById(id));
    }

    /**
     * 新增商甲池
     */
    @PreAuthorize("@ss.hasPermi('admin:shangjipool:add')")
    @Log(title = "商甲池", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwShangjipool fwShangjipool)
    {
        return toAjax(fwShangjipoolService.insertFwShangjipool(fwShangjipool));
    }

    /**
     * 修改商甲池
     */
    @PreAuthorize("@ss.hasPermi('admin:shangjipool:edit')")
    @Log(title = "商甲池", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwShangjipool fwShangjipool)
    {
        return toAjax(fwShangjipoolService.updateFwShangjipool(fwShangjipool));
    }

    /**
     * 删除商甲池
     */
    @PreAuthorize("@ss.hasPermi('admin:shangjipool:remove')")
    @Log(title = "商甲池", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwShangjipoolService.deleteFwShangjipoolByIds(ids));
    }
}
