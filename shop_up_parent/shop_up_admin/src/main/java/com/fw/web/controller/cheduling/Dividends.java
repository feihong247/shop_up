package com.fw.web.controller.cheduling;

import com.fw.utils.http.HttpUtils;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 定时任务
 * @Author 姚自强
 * @Date 2021/7/17
 **/
@Slf4j
@Component
public class Dividends {

    /** 分红， 调用web方法 */
    @Async
    public void bonus(){
        log.info("\r\n分红， 调用web方法\r\n");
        long l = System.currentTimeMillis() ^ 3;
//        String url = "http://192.168.1.8:15610/deal/bonus/";
        String url = "https://kuangshikeji.com/deal/bonus/";
        String res = HttpUtils.sendGet(url + l, null);
        log.info(res);
    }
}
