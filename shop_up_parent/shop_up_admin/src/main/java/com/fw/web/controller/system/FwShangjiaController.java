package com.fw.web.controller.system;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.core.page.TableDataInfo;
import com.fw.enums.BusinessType;
import com.fw.enums.LogsModelEnum;
import com.fw.enums.LogsTypeEnum;
import com.fw.enums.ReleaseLogEnum;
import com.fw.mes.Result;
import com.fw.mes.ResultUtils;
import com.fw.system.admin.domain.FwLogs;
import com.fw.system.admin.domain.FwShangjia;
import com.fw.system.admin.domain.FwShangjiaReleaseLog;
import com.fw.system.admin.service.*;
import com.fw.utils.StringUtils;
import com.fw.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 商甲发行Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/shangjia")
public class FwShangjiaController extends BaseController
{
    @Autowired
    private IFwShangjiaService fwShangjiaService;
    @Autowired
    private IFwMoneysService moneysService;
    @Autowired
    private IFwLogsService logsService;
    @Autowired
    private IFwUserService fwUserService;

    @Autowired
    private IFwShangjiaReleaseLogService releaseLogService;

    /**
     * 查询商甲发行列表
     */
    @PreAuthorize("@ss.hasPermi('admin:shangjia:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwShangjia fwShangjia)
    {
        startPage();
        List<FwShangjia> list = fwShangjiaService.selectFwShangjiaList(fwShangjia);
        return getDataTable(list);
    }

    /**
     * 导出商甲发行列表
     */
    @PreAuthorize("@ss.hasPermi('admin:shangjia:export')")
    @Log(title = "商甲发行", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwShangjia fwShangjia)
    {
        List<FwShangjia> list = fwShangjiaService.selectFwShangjiaList(fwShangjia);
        ExcelUtil<FwShangjia> util = new ExcelUtil<FwShangjia>(FwShangjia.class);
        return util.exportExcel(list, "shangjia");
    }

    /**
     * 获取商甲发行详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:shangjia:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwShangjiaService.selectFwShangjiaById(id));
    }

    /**
     * 新增商甲发行
     */
    @PreAuthorize("@ss.hasPermi('admin:shangjia:add')")
    @Log(title = "商甲发行", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwShangjia fwShangjia)
    {
        return toAjax(fwShangjiaService.insertFwShangjia(fwShangjia));
    }

    /**
     * 修改商甲发行
     */
    @PreAuthorize("@ss.hasPermi('admin:shangjia:edit')")
    @Log(title = "商甲发行", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwShangjia fwShangjia)
    {
        return toAjax(fwShangjiaService.updateFwShangjia(fwShangjia));
    }

    /**
     * 删除商甲发行
     */
    @PreAuthorize("@ss.hasPermi('admin:shangjia:remove')")
    @Log(title = "商甲发行", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwShangjiaService.deleteFwShangjiaByIds(ids));
    }

    /**
     * 获取分配原始、技术、管理列表
     * @return
     */
    @GetMapping("/dealShangJiaList/{isAccount}")
   // @Log(title = "商甲发行", businessType = BusinessType.OTHER)
    @Transactional
    public TableDataInfo dealShangJiaList(@PathVariable int isAccount,@RequestParam(required = false) String userId) {
        List<FwShangjiaReleaseLog> list=new ArrayList<>();
        startPage();
        switch (isAccount){
            case 1:
              list =     releaseLogService.list(Wrappers.<FwShangjiaReleaseLog>lambdaQuery().eq(StringUtils.isNotBlank(userId),FwShangjiaReleaseLog::getUserId, userId).eq(FwShangjiaReleaseLog::getLogType, ReleaseLogEnum.PUBLISH_POWER.getLogType()));
                break;
            case 2:
                list =     releaseLogService.list(Wrappers.<FwShangjiaReleaseLog>lambdaQuery().eq(StringUtils.isNotBlank(userId),FwShangjiaReleaseLog::getUserId, userId).eq(FwShangjiaReleaseLog::getLogType, ReleaseLogEnum.PUBLISH_MAN.getLogType()));
                break;
            case 3:
                list =     releaseLogService.list(Wrappers.<FwShangjiaReleaseLog>lambdaQuery().eq(StringUtils.isNotBlank(userId),FwShangjiaReleaseLog::getUserId, userId).eq(FwShangjiaReleaseLog::getLogType, ReleaseLogEnum.PUBLISH_SOURCE.getLogType()));
        }
        return getDataTable(list);
    }
    /**
     * 交易原始商甲
     */
    @PreAuthorize("@ss.hasPermi('admin:shangjia:dealOrgShangJia')")
    @PostMapping("/dealOrgShangJia/{shangjia}/{userId}")
    @Log(title = "商甲发行", businessType = BusinessType.INSERT)
    @Transactional
    public Result dealOrgShangJia(@PathVariable BigDecimal shangjia,
                                  @PathVariable String userId) {
        /*BigDecimal divide = shangjia.divide(new BigDecimal(1000), 0, BigDecimal.ROUND_DOWN);
        if(divide.multiply(new BigDecimal(1000)).compareTo(shangjia)<0){
            return new Result().fail(0, "分配额度不是1000的整数倍");
        }*/
        Integer isAccount = 3;
        if(shangjia.compareTo(new BigDecimal(0))>=0){
            int i = moneysService.updateMoneysShangJia(shangjia, userId,isAccount);
            if(i>0){
                return ResultUtils.success();
            }
            return new Result().fail(0, "分配额度错误");
        }
        return new Result().fail(0, "分配额度错误");
    }
    /**
     * 交易技术商甲
     */
    @PreAuthorize("@ss.hasPermi('admin:shangjia:dealTecShangJia')")
    @PostMapping("/dealTecShangJia/{shangjia}/{userId}")
    @Log(title = "商甲发行", businessType = BusinessType.INSERT)
    @Transactional
    public Result dealTecShangJia(@PathVariable BigDecimal shangjia,
                                  @PathVariable String userId) {
        Integer isAccount = 1;
        BigDecimal divide = shangjia.divide(new BigDecimal(1000), 0, BigDecimal.ROUND_DOWN);
        if(divide.multiply(new BigDecimal(1000)).compareTo(shangjia)<0){
            return new Result().fail(0, "分配额度不是1000的整数倍");
        }
        if(shangjia.compareTo(new BigDecimal(0))>1){
            int i = moneysService.updateMoneysTecShangJia(shangjia, userId,isAccount);
            if(i>0){
                return ResultUtils.success();
            }
            return new Result().fail(0, "分配额度错误");
        }
        return new Result().fail(0, "分配额度错误");
    }
    /**
     * 交易管理商甲
     */
    @PreAuthorize("@ss.hasPermi('admin:shangjia:dealManShangJia')")
    @PostMapping("/dealManShangJia/{shangjia}/{userId}")
    @Log(title = "商甲发行", businessType = BusinessType.INSERT)
    @Transactional
    public Result dealManShangJia(@PathVariable BigDecimal shangjia,
                                  @PathVariable String userId) {
        Integer isAccount = 2;

        BigDecimal divide = shangjia.divide(new BigDecimal(1000), 0, BigDecimal.ROUND_DOWN);
        if(divide.multiply(new BigDecimal(1000)).compareTo(shangjia)<0){
            return new Result().fail(0, "分配额度不是1000的整数倍");
        }
        if(shangjia.compareTo(new BigDecimal(0))>1){
            int i = moneysService.updateMoneysManShangJia(shangjia, userId,isAccount);
            if(i>0){
                return ResultUtils.success();
            }
            return new Result().fail(0, "分配额度错误");
        }
        return new Result().fail(0, "分配额度错误");
    }
    /**
     * 回收管理商甲
     */
    @PreAuthorize("@ss.hasPermi('admin:shangjia:recycleManShangJia')")
    @PostMapping("/recycleManShangJia/{shangjia}/{userId}")
    @Log(title = "商甲发行", businessType = BusinessType.DELETE)
    @Transactional
    public Result recycleManShangJia(@PathVariable BigDecimal shangjia,
                                  @PathVariable String userId) {
        Integer isAccount = 2;

        BigDecimal divide = shangjia.divide(new BigDecimal(1000), 0, BigDecimal.ROUND_DOWN);
        if(divide.multiply(new BigDecimal(1000)).compareTo(shangjia)<0){
            return new Result().fail(0, "回收额度不是1000的整数倍");
        }
        if(shangjia.compareTo(new BigDecimal(0))>1){
            int i = moneysService.recycleManShangJia(shangjia, userId,isAccount);
            if(i>0){
                return ResultUtils.success();
            }
            return new Result().fail(0, "分配额度错误");
        }
        return new Result().fail(0, "分配额度错误");
    }
    /**
     * 回收技术商甲
     */
    @PreAuthorize("@ss.hasPermi('admin:shangjia:recycleTecShangJia')")
    @PostMapping("/recycleTecShangJia/{shangjia}/{userId}")
    @Log(title = "商甲发行", businessType = BusinessType.DELETE)
    @Transactional
    public Result recycleTecShangJia(@PathVariable BigDecimal shangjia,
                                  @PathVariable String userId) {
        Integer isAccount = 1;
        BigDecimal divide = shangjia.divide(new BigDecimal(1000), 0, BigDecimal.ROUND_DOWN);
        if(divide.multiply(new BigDecimal(1000)).compareTo(shangjia)<0){
            return new Result().fail(0, "回收额度不是1000的整数倍");
        }
        if(shangjia.compareTo(new BigDecimal(0))>1){
            int i = moneysService.recycleTecShangJia(shangjia, userId,isAccount);
            if(i>0){
                return ResultUtils.success();
            }
            return new Result().fail(0, "分配额度错误");
        }
        return new Result().fail(0, "分配额度错误");
    }
}
