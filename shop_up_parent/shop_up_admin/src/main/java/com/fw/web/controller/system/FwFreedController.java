package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwFreed;
import com.fw.system.admin.service.IFwFreedService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 意见反馈Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/freed")
public class FwFreedController extends BaseController
{
    @Autowired
    private IFwFreedService fwFreedService;

    /**
     * 查询意见反馈列表
     */
    @PreAuthorize("@ss.hasPermi('admin:freed:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwFreed fwFreed)
    {
        startPage();
        List<FwFreed> list = fwFreedService.selectFwFreedList(fwFreed);
        return getDataTable(list);
    }

    /**
     * 导出意见反馈列表
     */
    @PreAuthorize("@ss.hasPermi('admin:freed:export')")
    @Log(title = "意见反馈", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwFreed fwFreed)
    {
        List<FwFreed> list = fwFreedService.selectFwFreedList(fwFreed);
        ExcelUtil<FwFreed> util = new ExcelUtil<FwFreed>(FwFreed.class);
        return util.exportExcel(list, "freed");
    }

    /**
     * 获取意见反馈详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:freed:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwFreedService.selectFwFreedById(id));
    }

    /**
     * 新增意见反馈
     */
    @PreAuthorize("@ss.hasPermi('admin:freed:add')")
    @Log(title = "意见反馈", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwFreed fwFreed)
    {
        return toAjax(fwFreedService.insertFwFreed(fwFreed));
    }

    /**
     * 修改意见反馈
     */
    @PreAuthorize("@ss.hasPermi('admin:freed:edit')")
    @Log(title = "意见反馈", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwFreed fwFreed)
    {
        return toAjax(fwFreedService.updateFwFreed(fwFreed));
    }

    /**
     * 删除意见反馈
     */
    @PreAuthorize("@ss.hasPermi('admin:freed:remove')")
    @Log(title = "意见反馈", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwFreedService.deleteFwFreedByIds(ids));
    }
}
