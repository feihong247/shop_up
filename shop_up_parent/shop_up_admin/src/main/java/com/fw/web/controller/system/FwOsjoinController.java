package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwOsjoin;
import com.fw.system.admin.service.IFwOsjoinService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 订单与spu中间Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/osjoin")
public class FwOsjoinController extends BaseController
{
    @Autowired
    private IFwOsjoinService fwOsjoinService;

    /**
     * 查询订单与spu中间列表
     */
    @PreAuthorize("@ss.hasPermi('admin:osjoin:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwOsjoin fwOsjoin)
    {
        startPage();
        List<FwOsjoin> list = fwOsjoinService.selectFwOsjoinList(fwOsjoin);
        return getDataTable(list);
    }

    /**
     * 导出订单与spu中间列表
     */
    @PreAuthorize("@ss.hasPermi('admin:osjoin:export')")
    @Log(title = "订单与spu中间", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwOsjoin fwOsjoin)
    {
        List<FwOsjoin> list = fwOsjoinService.selectFwOsjoinList(fwOsjoin);
        ExcelUtil<FwOsjoin> util = new ExcelUtil<FwOsjoin>(FwOsjoin.class);
        return util.exportExcel(list, "osjoin");
    }

    /**
     * 获取订单与spu中间详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:osjoin:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwOsjoinService.selectFwOsjoinById(id));
    }

    /**
     * 新增订单与spu中间
     */
    @PreAuthorize("@ss.hasPermi('admin:osjoin:add')")
    @Log(title = "订单与spu中间", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwOsjoin fwOsjoin)
    {
        return toAjax(fwOsjoinService.insertFwOsjoin(fwOsjoin));
    }

    /**
     * 修改订单与spu中间
     */
    @PreAuthorize("@ss.hasPermi('admin:osjoin:edit')")
    @Log(title = "订单与spu中间", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwOsjoin fwOsjoin)
    {
        return toAjax(fwOsjoinService.updateFwOsjoin(fwOsjoin));
    }

    /**
     * 删除订单与spu中间
     */
    @PreAuthorize("@ss.hasPermi('admin:osjoin:remove')")
    @Log(title = "订单与spu中间", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwOsjoinService.deleteFwOsjoinByIds(ids));
    }
}
