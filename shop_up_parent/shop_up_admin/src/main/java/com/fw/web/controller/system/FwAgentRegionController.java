package com.fw.web.controller.system;

import java.util.List;
import java.util.stream.Collectors;

import cn.hutool.core.bean.BeanUtil;
import com.fw.system.admin.domain.vo.AgentRegionVo;
import com.fw.system.admin.service.IFwUserService;
import com.fw.system.comm.service.DistrictService;
import com.github.pagehelper.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwAgentRegion;
import com.fw.system.admin.service.IFwAgentRegionService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 代理区域Controller
 * 
 * @author yanwei
 * @date 2021-07-23
 */
@RestController
@RequestMapping("/admin/agent_region")
public class FwAgentRegionController extends BaseController
{
    @Autowired
    private IFwAgentRegionService fwAgentRegionService;


    @Autowired
    private DistrictService districtService;

    @Autowired
    private IFwUserService userService;

    /**
     * 查询代理区域列表
     */
    @PreAuthorize("@ss.hasPermi('admin:agent_region:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwAgentRegion fwAgentRegion)
    {
        startPage();
        Page<FwAgentRegion> list = (Page<FwAgentRegion>) fwAgentRegionService.selectFwAgentRegionList(fwAgentRegion);
        // 查看区域商铺详细信息
        List<AgentRegionVo> agentRegionVos = list.parallelStream().map(item -> {
            AgentRegionVo agentRegionVo = new AgentRegionVo();
            BeanUtil.copyProperties(item, agentRegionVo);
            // 补充code信息
            agentRegionVo.setProvinceName(districtService.codeConversionName(item.getProvince()));
            agentRegionVo.setAreaName(districtService.codeConversionName(item.getArea()));
            agentRegionVo.setCityName(districtService.codeConversionName(item.getCity()));
            // 补充用户信息
            agentRegionVo.setUserAgentVo(userService.getUserAgentVo(item.getUserId()));
            return agentRegionVo;
        }).collect(Collectors.toList());
        Page pageAgentRegionVos = new Page();
        BeanUtil.copyProperties(list,pageAgentRegionVos,"elementData");
        pageAgentRegionVos.addAll(agentRegionVos);
        return getDataTable(pageAgentRegionVos);
    }

    /**
     * 导出代理区域列表
     */
    @PreAuthorize("@ss.hasPermi('admin:agent_region:export')")
    @Log(title = "代理区域", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwAgentRegion fwAgentRegion)
    {
        List<FwAgentRegion> list = fwAgentRegionService.selectFwAgentRegionList(fwAgentRegion);
        ExcelUtil<FwAgentRegion> util = new ExcelUtil<FwAgentRegion>(FwAgentRegion.class);
        return util.exportExcel(list, "agent_region");
    }

    /**
     * 获取代理区域详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:agent_region:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwAgentRegionService.selectFwAgentRegionById(id));
    }

    /**
     * 新增代理区域
     */
    @PreAuthorize("@ss.hasPermi('admin:agent_region:add')")
    @Log(title = "代理区域", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwAgentRegion fwAgentRegion)
    {
        return toAjax(fwAgentRegionService.insertFwAgentRegion(fwAgentRegion));
    }

    /**
     * 修改代理区域
     */
    @PreAuthorize("@ss.hasPermi('admin:agent_region:edit')")
    @Log(title = "代理区域", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwAgentRegion fwAgentRegion)
    {
        return toAjax(fwAgentRegionService.updateFwAgentRegion(fwAgentRegion));
    }

    /**
     * 删除代理区域
     */
    @PreAuthorize("@ss.hasPermi('admin:agent_region:remove')")
    @Log(title = "代理区域", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwAgentRegionService.deleteFwAgentRegionByIds(ids));
    }
}
