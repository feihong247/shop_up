package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwVersion;
import com.fw.system.admin.service.IFwVersionService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 版本Controller
 * 
 * @author yanwei
 * @date 2021-10-12
 */
@RestController
@RequestMapping("/admin/version")
public class FwVersionController extends BaseController
{
    @Autowired
    private IFwVersionService fwVersionService;

    /**
     * 查询版本列表
     */
    @PreAuthorize("@ss.hasPermi('admin:version:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwVersion fwVersion)
    {
        startPage();
        List<FwVersion> list = fwVersionService.selectFwVersionList(fwVersion);
        return getDataTable(list);
    }

    /**
     * 导出版本列表
     */
    @PreAuthorize("@ss.hasPermi('admin:version:export')")
    @Log(title = "版本", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwVersion fwVersion)
    {
        List<FwVersion> list = fwVersionService.selectFwVersionList(fwVersion);
        ExcelUtil<FwVersion> util = new ExcelUtil<FwVersion>(FwVersion.class);
        return util.exportExcel(list, "version");
    }

    /**
     * 获取版本详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:version:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwVersionService.selectFwVersionById(id));
    }

    /**
     * 新增版本
     */
    @PreAuthorize("@ss.hasPermi('admin:version:add')")
    @Log(title = "版本", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwVersion fwVersion)
    {
        return toAjax(fwVersionService.insertFwVersion(fwVersion));
    }

    /**
     * 修改版本
     */
    @PreAuthorize("@ss.hasPermi('admin:version:edit')")
    @Log(title = "版本", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwVersion fwVersion)
    {
        return toAjax(fwVersionService.updateFwVersion(fwVersion));
    }

    /**
     * 删除版本
     */
    @PreAuthorize("@ss.hasPermi('admin:version:remove')")
    @Log(title = "版本", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwVersionService.deleteFwVersionByIds(ids));
    }
}
