package com.fw.web.controller.system;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwEvaluation;
import com.fw.system.admin.service.IFwEvaluationService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 商品评价Controller
 *
 * @author yanwei
 * @date 2021-05-24
 */
@RestController
@RequestMapping("/admin/evaluation")
public class FwEvaluationController extends BaseController {
    @Autowired
    private IFwEvaluationService fwEvaluationService;

    /**
     * 查询商品评价列表
     */
    @PreAuthorize("@ss.hasPermi('admin:evaluation:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwEvaluation fwEvaluation) {
        startPage();
        List<FwEvaluation> list = fwEvaluationService.selectFwEvaluationList(fwEvaluation);
        return getDataTable(list);
    }

    /**
     * 导出商品评价列表
     */
    @PreAuthorize("@ss.hasPermi('admin:evaluation:export')")
    @Log(title = "商品评价", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwEvaluation fwEvaluation) {
        List<FwEvaluation> list = fwEvaluationService.selectFwEvaluationList(fwEvaluation);
        ExcelUtil<FwEvaluation> util = new ExcelUtil<FwEvaluation>(FwEvaluation.class);
        return util.exportExcel(list, "evaluation");
    }

    /**
     * 获取商品评价详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:evaluation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(fwEvaluationService.selectFwEvaluationById(id));
    }

    /**
     * 新增商品评价
     */
    @PreAuthorize("@ss.hasPermi('admin:evaluation:add')")
    @Log(title = "商品评价", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwEvaluation fwEvaluation) {
        return toAjax(fwEvaluationService.insertFwEvaluation(fwEvaluation));
    }

    /**
     * 修改商品评价
     */
    @PreAuthorize("@ss.hasPermi('admin:evaluation:edit')")
    @Log(title = "商品评价", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwEvaluation fwEvaluation) {
        return toAjax(fwEvaluationService.updateFwEvaluation(fwEvaluation));
    }

    /**
     * 删除商品评价
     */
    @PreAuthorize("@ss.hasPermi('admin:evaluation:remove')")
    @Log(title = "商品评价", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(fwEvaluationService.deleteFwEvaluationByIds(ids));
    }
}
