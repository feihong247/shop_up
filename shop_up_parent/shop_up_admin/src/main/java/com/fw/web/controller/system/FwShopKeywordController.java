package com.fw.web.controller.system;

import java.util.List;

import com.fw.core.domain.model.LoginUser;
import com.fw.system.admin.service.ISysUserService;
import com.fw.utils.ServletUtils;
import com.fw.web.web.service.TokenService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwShopKeyword;
import com.fw.system.admin.service.IFwShopKeywordService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 商铺环境展示Controller
 * 
 * @author yanwei
 * @date 2021-07-16
 */
@RestController
@RequestMapping("/admin/shop_keyword")
public class FwShopKeywordController extends BaseController
{
    @Autowired
    private IFwShopKeywordService fwShopKeywordService;
    @Autowired
    private  TokenService tokenService;
    @Autowired
    private  ISysUserService sysUserService;

    /**
     * 查询商铺环境展示列表
     */
    @PreAuthorize("@ss.hasPermi('admin:shop_keyword:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwShopKeyword fwShopKeyword)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String shopId = sysUserService.isShop(loginUser);
        fwShopKeyword.setShopId(shopId);
        startPage();
        List<FwShopKeyword> list = fwShopKeywordService.selectFwShopKeywordList(fwShopKeyword);
        return getDataTable(list);
    }

    /**
     * 导出商铺环境展示列表
     */
    @PreAuthorize("@ss.hasPermi('admin:shop_keyword:export')")
    @Log(title = "商铺环境展示", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwShopKeyword fwShopKeyword)
    {
        List<FwShopKeyword> list = fwShopKeywordService.selectFwShopKeywordList(fwShopKeyword);
        ExcelUtil<FwShopKeyword> util = new ExcelUtil<FwShopKeyword>(FwShopKeyword.class);
        return util.exportExcel(list, "shop_keyword");
    }

    /**
     * 获取商铺环境展示详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:shop_keyword:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwShopKeywordService.selectFwShopKeywordById(id));
    }

    /**
     * 新增商铺环境展示
     */
    @PreAuthorize("@ss.hasPermi('admin:shop_keyword:add')")
    @Log(title = "商铺环境展示", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwShopKeyword fwShopKeyword)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String shopId = sysUserService.isShop(loginUser);
        fwShopKeyword.setShopId(shopId);
        return toAjax(fwShopKeywordService.insertFwShopKeyword(fwShopKeyword));
    }

    /**
     * 修改商铺环境展示
     */
    @PreAuthorize("@ss.hasPermi('admin:shop_keyword:edit')")
    @Log(title = "商铺环境展示", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwShopKeyword fwShopKeyword)
    {
        return toAjax(fwShopKeywordService.updateFwShopKeyword(fwShopKeyword));
    }

    /**
     * 删除商铺环境展示
     */
    @PreAuthorize("@ss.hasPermi('admin:shop_keyword:remove')")
    @Log(title = "商铺环境展示", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwShopKeywordService.deleteFwShopKeywordByIds(ids));
    }
}
