package com.fw.web.controller.system;

import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.core.page.TableDataInfo;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwLogs;
import com.fw.system.admin.domain.vo.DisappearLogsVo;
import com.fw.system.admin.domain.vo.UserLogsDisappearCountVo;
import com.fw.system.admin.service.IFwLogsService;
import com.fw.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 业务日志Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/disappear_logs")
public class FwDisappearLogsController extends BaseController
{
    @Autowired
    private IFwLogsService fwLogsService;

    /**
     * 查询业务日志列表
     */
    @PreAuthorize("@ss.hasPermi('admin:disappear_logs:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwLogs fwLogs)
    {
        startPage();
        fwLogs.setModelName("消证模块");
        List<DisappearLogsVo> list = fwLogsService.selectCommLogsList(fwLogs);
        return getDataTable(list);
    }

}
