package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwProblem;
import com.fw.system.admin.service.IFwProblemService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 常见问题Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/problem")
public class FwProblemController extends BaseController
{
    @Autowired
    private IFwProblemService fwProblemService;

    /**
     * 查询常见问题列表
     */
    @PreAuthorize("@ss.hasPermi('admin:problem:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwProblem fwProblem)
    {
        startPage();
        List<FwProblem> list = fwProblemService.selectFwProblemList(fwProblem);
        return getDataTable(list);
    }

    /**
     * 导出常见问题列表
     */
    @PreAuthorize("@ss.hasPermi('admin:problem:export')")
    @Log(title = "常见问题", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwProblem fwProblem)
    {
        List<FwProblem> list = fwProblemService.selectFwProblemList(fwProblem);
        ExcelUtil<FwProblem> util = new ExcelUtil<FwProblem>(FwProblem.class);
        return util.exportExcel(list, "problem");
    }

    /**
     * 获取常见问题详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:problem:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwProblemService.selectFwProblemById(id));
    }

    /**
     * 新增常见问题
     */
    @PreAuthorize("@ss.hasPermi('admin:problem:add')")
    @Log(title = "常见问题", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwProblem fwProblem)
    {
        return toAjax(fwProblemService.insertFwProblem(fwProblem));
    }

    /**
     * 修改常见问题
     */
    @PreAuthorize("@ss.hasPermi('admin:problem:edit')")
    @Log(title = "常见问题", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwProblem fwProblem)
    {
        return toAjax(fwProblemService.updateFwProblem(fwProblem));
    }

    /**
     * 删除常见问题
     */
    @PreAuthorize("@ss.hasPermi('admin:problem:remove')")
    @Log(title = "常见问题", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwProblemService.deleteFwProblemByIds(ids));
    }
}
