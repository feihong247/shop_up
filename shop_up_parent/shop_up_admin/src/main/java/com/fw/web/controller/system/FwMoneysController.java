package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwMoneys;
import com.fw.system.admin.service.IFwMoneysService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 用户钱包Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/moneys")
public class FwMoneysController extends BaseController
{
    @Autowired
    private IFwMoneysService fwMoneysService;

    /**
     * 查询用户钱包列表
     */
    @PreAuthorize("@ss.hasPermi('admin:moneys:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwMoneys fwMoneys)
    {
        startPage();
        List<FwMoneys> list = fwMoneysService.selectFwMoneysList(fwMoneys);
        return getDataTable(list);
    }

    /**
     * 导出用户钱包列表
     */
    @PreAuthorize("@ss.hasPermi('admin:moneys:export')")
    @Log(title = "用户钱包", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwMoneys fwMoneys)
    {
        List<FwMoneys> list = fwMoneysService.selectFwMoneysList(fwMoneys);
        ExcelUtil<FwMoneys> util = new ExcelUtil<FwMoneys>(FwMoneys.class);
        return util.exportExcel(list, "moneys");
    }

    /**
     * 获取用户钱包详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:moneys:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwMoneysService.selectFwMoneysById(id));
    }

    /**
     * 新增用户钱包
     */
    @PreAuthorize("@ss.hasPermi('admin:moneys:add')")
    @Log(title = "用户钱包", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwMoneys fwMoneys)
    {
        return toAjax(fwMoneysService.insertFwMoneys(fwMoneys));
    }

    /**
     * 修改用户钱包
     */
    @PreAuthorize("@ss.hasPermi('admin:moneys:edit')")
    @Log(title = "用户钱包", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwMoneys fwMoneys)
    {
        return toAjax(fwMoneysService.updateFwMoneys(fwMoneys));
    }

    /**
     * 删除用户钱包
     */
    @PreAuthorize("@ss.hasPermi('admin:moneys:remove')")
    @Log(title = "用户钱包", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwMoneysService.deleteFwMoneysByIds(ids));
    }
}
