package com.fw.web.controller.system;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwAddrs;
import com.fw.system.admin.service.IFwAddrsService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 用户收货地址Controller
 *
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/addrs")
public class FwAddrsController extends BaseController {
    @Autowired
    private IFwAddrsService fwAddrsService;

    /**
     * 查询用户收货地址列表
     */
    @PreAuthorize("@ss.hasPermi('admin:addrs:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwAddrs fwAddrs) {
        startPage();
        List<FwAddrs> list = fwAddrsService.selectFwAddrsList(fwAddrs);
        return getDataTable(list);
    }

    /**
     * 导出用户收货地址列表
     */
    @PreAuthorize("@ss.hasPermi('admin:addrs:export')")
    @Log(title = "用户收货地址", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwAddrs fwAddrs) {
        List<FwAddrs> list = fwAddrsService.selectFwAddrsList(fwAddrs);
        ExcelUtil<FwAddrs> util = new ExcelUtil<FwAddrs>(FwAddrs.class);
        return util.exportExcel(list, "addrs");
    }

    /**
     * 获取用户收货地址详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:addrs:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(fwAddrsService.selectFwAddrsById(id));
    }

    /**
     * 新增用户收货地址
     */
    @PreAuthorize("@ss.hasPermi('admin:addrs:add')")
    @Log(title = "用户收货地址", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwAddrs fwAddrs) {
        return toAjax(fwAddrsService.insertFwAddrs(fwAddrs));
    }

    /**
     * 修改用户收货地址
     */
    @PreAuthorize("@ss.hasPermi('admin:addrs:edit')")
    @Log(title = "用户收货地址", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwAddrs fwAddrs) {
        return toAjax(fwAddrsService.updateFwAddrs(fwAddrs));
    }

    /**
     * 删除用户收货地址
     */
    @PreAuthorize("@ss.hasPermi('admin:addrs:remove')")
    @Log(title = "用户收货地址", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(fwAddrsService.deleteFwAddrsByIds(ids));
    }
}
