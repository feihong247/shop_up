package com.fw.web.controller.system;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwUgJoin;
import com.fw.system.admin.service.IFwUgJoinService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 用户竞猜中间Controller
 *
 * @author yanwei
 * @date 2021-07-03
 */
@RestController
@RequestMapping("/admin/ug_join")
public class FwUgJoinController extends BaseController {
    private final IFwUgJoinService fwUgJoinService;

    public FwUgJoinController(IFwUgJoinService fwUgJoinService) {
        this.fwUgJoinService = fwUgJoinService;
    }

    /**
     * 查询用户竞猜中间列表
     */
    @PreAuthorize("@ss.hasPermi('admin:ug_join:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwUgJoin fwUgJoin) {
        startPage();
        List<FwUgJoin> list = fwUgJoinService.selectFwUgJoinList(fwUgJoin);
        return getDataTable(list);
    }

    /**
     * 导出用户竞猜中间列表
     */
    @PreAuthorize("@ss.hasPermi('admin:ug_join:export')")
    @Log(title = "用户竞猜中间", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwUgJoin fwUgJoin) {
        List<FwUgJoin> list = fwUgJoinService.selectFwUgJoinList(fwUgJoin);
        ExcelUtil<FwUgJoin> util = new ExcelUtil<>(FwUgJoin.class);
        return util.exportExcel(list, "ug_join");
    }

    /**
     * 获取用户竞猜中间详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:ug_join:query')")
    @GetMapping(value = "/{ugId}")
    public AjaxResult getInfo(@PathVariable("ugId") String ugId) {
        return AjaxResult.success(fwUgJoinService.selectFwUgJoinById(ugId));
    }

    /**
     * 新增用户竞猜中间
     */
    @PreAuthorize("@ss.hasPermi('admin:ug_join:add')")
    @Log(title = "用户竞猜中间", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwUgJoin fwUgJoin) {
        return toAjax(fwUgJoinService.insertFwUgJoin(fwUgJoin));
    }

    /**
     * 修改用户竞猜中间
     */
    @PreAuthorize("@ss.hasPermi('admin:ug_join:edit')")
    @Log(title = "用户竞猜中间", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwUgJoin fwUgJoin) {
        return toAjax(fwUgJoinService.updateFwUgJoin(fwUgJoin));
    }

    /**
     * 删除用户竞猜中间
     */
    @PreAuthorize("@ss.hasPermi('admin:ug_join:remove')")
    @Log(title = "用户竞猜中间", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ugIds}")
    public AjaxResult remove(@PathVariable String[] ugIds) {
        return toAjax(fwUgJoinService.deleteFwUgJoinByIds(ugIds));
    }
}
