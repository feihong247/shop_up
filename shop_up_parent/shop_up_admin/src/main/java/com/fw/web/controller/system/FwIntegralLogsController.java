package com.fw.web.controller.system;

import com.fw.core.controller.BaseController;
import com.fw.core.page.TableDataInfo;
import com.fw.system.admin.domain.FwLogs;
import com.fw.system.admin.domain.vo.DisappearLogsVo;
import com.fw.system.admin.service.IFwLogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 业务日志Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/integral_logs")
public class FwIntegralLogsController extends BaseController
{
    @Autowired
    private IFwLogsService fwLogsService;

    /**
     * 查询业务日志列表
     */
    @PreAuthorize("@ss.hasPermi('admin:integral_logs:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwLogs fwLogs)
    {
        startPage();
        fwLogs.setModelName("积分模块");
        List<DisappearLogsVo> list = fwLogsService.selectCommLogsInList(fwLogs);
        return getDataTable(list);
    }

}
