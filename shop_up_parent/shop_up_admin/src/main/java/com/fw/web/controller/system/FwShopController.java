package com.fw.web.controller.system;

import java.util.List;

import com.fw.core.domain.model.LoginUser;
import com.fw.system.admin.service.ISysUserService;
import com.fw.utils.ServletUtils;
import com.fw.web.web.service.TokenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwShop;
import com.fw.system.admin.service.IFwShopService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 商铺Controller
 *
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/shop")
@Api(tags = "商户相关")
public class FwShopController extends BaseController
{
    @Autowired
    private IFwShopService fwShopService;

    @Autowired
    private TokenService tokenServicel;

    @Autowired
    private ISysUserService sysUserService;

    /**
     * 查询商铺列表
     */
    @PreAuthorize("@ss.hasAnyPermi('admin:shop:list,admin:one_area_item:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwShop fwShop)
    {
        LoginUser loginUser = tokenServicel.getLoginUser(ServletUtils.getRequest());
        String shopId = sysUserService.isShop(loginUser);
        fwShop.setId(shopId);

        startPage();
        List<FwShop> list = fwShopService.selectFwShopList(fwShop);
        return getDataTable(list);
    }

    /**
     * 导出商铺列表
     */
    @PreAuthorize("@ss.hasPermi('admin:shop:export')")
    @Log(title = "商铺", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwShop fwShop)
    {
        List<FwShop> list = fwShopService.selectFwShopList(fwShop);
        ExcelUtil<FwShop> util = new ExcelUtil<FwShop>(FwShop.class);
        return util.exportExcel(list, "shop");
    }

    /**
     * 获取商铺详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:shop:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwShopService.selectFwShopById(id));
    }

    /**
     * 新增商铺
     */
    @PreAuthorize("@ss.hasPermi('admin:shop:add')")
    @Log(title = "商铺", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwShop fwShop)
    {
        return toAjax(fwShopService.insertFwShop(fwShop));
    }

    /**
     * 修改商铺
     */
    @PreAuthorize("@ss.hasPermi('admin:shop:edit')")
    @Log(title = "商铺", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwShop fwShop)
    {
        return toAjax(fwShopService.updateFwShop(fwShop));
    }

    /**
     * 修改商铺
     */
    @PostMapping("/openEdit")
    @ApiOperation("web端调用商户审核")
    public AjaxResult openEdit(@RequestBody FwShop fwShop)
    {
        return toAjax(fwShopService.updateFwShop(fwShop));
    }

    /**
     * 删除商铺
     */
    @PreAuthorize("@ss.hasPermi('admin:shop:remove')")
    @Log(title = "商铺", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwShopService.deleteFwShopByIds(ids));
    }
}
