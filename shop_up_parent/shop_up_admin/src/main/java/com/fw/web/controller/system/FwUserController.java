package com.fw.web.controller.system;

import java.util.List;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.system.admin.domain.vo.UserLevelVo;
import com.fw.system.admin.domain.vo.UserVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwUser;
import com.fw.system.admin.service.IFwUserService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 用户Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/user")
public class FwUserController extends BaseController
{
    @Autowired
    private IFwUserService fwUserService;

    /**
     * 查询用户列表
     */
    @PreAuthorize("@ss.hasPermi('admin:user:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwUser fwUser)
    {
        startPage();
        List<UserVo> list = fwUserService.selectFwUserList(fwUser);
        return getDataTable(list);
    }

    /**
     * 查询用户列表
     */
    @PreAuthorize("@ss.hasPermi('admin:user:list')")
    @GetMapping("/all")
    public AjaxResult all()
    {
        List<FwUser> list = fwUserService.list(Wrappers.<FwUser>lambdaQuery().last( "order by create_time desc "));
       return   AjaxResult.success(list);
    }

    /**
     * 导出用户列表
     */
    @PreAuthorize("@ss.hasPermi('admin:user:export')")
    @Log(title = "用户", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwUser fwUser)
    {
        List<UserVo> list = fwUserService.selectFwUserList(fwUser);
        ExcelUtil<UserVo> util = new ExcelUtil<UserVo>(UserVo.class);
        return util.exportExcel(list, "user");
    }

    /**
     * 获取用户详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:user:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwUserService.selectFwUserById(id));
    }

    /**
     * 新增用户
     */
    @PreAuthorize("@ss.hasPermi('admin:user:add')")
    @Log(title = "用户", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwUser fwUser)
    {
        return toAjax(fwUserService.insertFwUser(fwUser));
    }

    /**
     * 修改用户
     */
    @PreAuthorize("@ss.hasPermi('admin:user:edit')")
    @Log(title = "用户", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwUser fwUser)
    {
        return toAjax(fwUserService.updateFwUser(fwUser));
    }

    /**
     * 删除用户
     */
    @PreAuthorize("@ss.hasPermi('admin:user:remove')")
    @Log(title = "用户", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwUserService.deleteFwUserByIds(ids));
    }

    /**
     * 查看用户关系图谱
     */
    @PreAuthorize("@ss.hasPermi('admin:user:level')")
    @Log(title = "用户级别",businessType = BusinessType.OTHER)
    @GetMapping("/userLevel/{parentId}")
    public AjaxResult userLevel(@PathVariable String parentId){
        // 查看当前用户上下级关系。
      List<UserLevelVo> userLevelVoList = fwUserService.userLevel(parentId);
        return AjaxResult.success(userLevelVoList);
    }



}
