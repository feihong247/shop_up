package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwImages;
import com.fw.system.admin.service.IFwImagesService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 商品图片列Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/images")
public class FwImagesController extends BaseController
{
    @Autowired
    private IFwImagesService fwImagesService;

    /**
     * 查询商品图片列列表
     */
    @PreAuthorize("@ss.hasPermi('admin:images:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwImages fwImages)
    {
        startPage();
        List<FwImages> list = fwImagesService.selectFwImagesList(fwImages);
        return getDataTable(list);
    }

    /**
     * 导出商品图片列列表
     */
    @PreAuthorize("@ss.hasPermi('admin:images:export')")
    @Log(title = "商品图片列", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwImages fwImages)
    {
        List<FwImages> list = fwImagesService.selectFwImagesList(fwImages);
        ExcelUtil<FwImages> util = new ExcelUtil<FwImages>(FwImages.class);
        return util.exportExcel(list, "images");
    }

    /**
     * 获取商品图片列详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:images:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwImagesService.selectFwImagesById(id));
    }

    /**
     * 新增商品图片列
     */
    @PreAuthorize("@ss.hasPermi('admin:images:add')")
    @Log(title = "商品图片列", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwImages fwImages)
    {
        return toAjax(fwImagesService.insertFwImages(fwImages));
    }

    /**
     * 修改商品图片列
     */
    @PreAuthorize("@ss.hasPermi('admin:images:edit')")
    @Log(title = "商品图片列", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwImages fwImages)
    {
        return toAjax(fwImagesService.updateFwImages(fwImages));
    }

    /**
     * 删除商品图片列
     */
    @PreAuthorize("@ss.hasPermi('admin:images:remove')")
    @Log(title = "商品图片列", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwImagesService.deleteFwImagesByIds(ids));
    }
}
