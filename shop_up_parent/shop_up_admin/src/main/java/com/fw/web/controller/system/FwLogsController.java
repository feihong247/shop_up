package com.fw.web.controller.system;

import java.util.List;

import com.fw.mes.Result;
import com.fw.system.admin.domain.vo.UserLogsDisappearCountVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwLogs;
import com.fw.system.admin.service.IFwLogsService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

import static com.fw.mes.ResultUtils.success;

/**
 * 业务日志Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/logs")
public class FwLogsController extends BaseController
{
    @Autowired
    private IFwLogsService fwLogsService;

    /**
     * 查询业务日志列表
     */
    @PreAuthorize("@ss.hasPermi('admin:logs:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwLogs fwLogs)
    {
        startPage();
        List<FwLogs> list = fwLogsService.selectFwLogsList(fwLogs);
        return getDataTable(list);
    }

    /**
     * 新增统计 列表 细分
     *
     * 用户消证
     *  用户释放总量 -》 用户单次释放量
     *
     * 用户积分
     *   用户总提现金额 -》 用户单次提现量
     *
     *
     *
     */

    /**
     * 用户释放总量
     */
    @PreAuthorize("@ss.hasPermi('admin:logs:list')")
    @GetMapping("/userLogsDisappearCount")
    public TableDataInfo userLogsDisappearCount(FwLogs fwLogs){
        startPage();
       List<UserLogsDisappearCountVo> userLogsDisappearCountVos =   fwLogsService.userLogsDisappearCount(fwLogs);
        fwLogs.setIsType("支出");
        userLogsDisappearCountVos.parallelStream().forEach(item ->{
            fwLogs.setPhone(item.getPhone());
            item.setToDayDisappearCount(fwLogsService.toDayUserLogsDisappearCount(fwLogs));
        });
        return getDataTable(userLogsDisappearCountVos);
    }

    /**
     * 用户 单次释放量
     */
    @PreAuthorize("@ss.hasPermi('admin:logs:list')")
    @GetMapping("/userLogsDisappearInfos")
    public TableDataInfo userLogsDisappearInfos(FwLogs fwLogs){
        startPage();
        List<FwLogs> fwLogsList =   fwLogsService.userLogsDisappearInfos(fwLogs);
        return getDataTable(fwLogsList);
    }

    /**
     * 用户总提现金额
     */
    @PreAuthorize("@ss.hasPermi('admin:logs:list')")
    @GetMapping("/userLogsIntegralCount")
    public TableDataInfo userLogsIntegralCount(FwLogs fwLogs){
        startPage();
        List<UserLogsDisappearCountVo> userLogsDisappearCountVos =   fwLogsService.userLogsIntegralCount(fwLogs);
        // 找到该信息今日 积分 总提现得量
        fwLogs.setIsType("提现");
        userLogsDisappearCountVos.parallelStream().forEach(item ->{
            fwLogs.setPhone(item.getPhone());
            Double aDouble = fwLogsService.toDayUserLogsIntegralCount(fwLogs);
            item.setToDayIntegralCount(aDouble == null ? 0D : aDouble);
        });
        return getDataTable(userLogsDisappearCountVos);
    }

    /**
     * 用户 总提现金额 infos
     */
    @PreAuthorize("@ss.hasPermi('admin:logs:list')")
    @GetMapping("/userLogsIntegralInfos")
    public TableDataInfo userLogsIntegralInfos(FwLogs fwLogs){
        startPage();
        List<FwLogs> fwLogsList =   fwLogsService.userLogsIntegralInfos(fwLogs);
        return getDataTable(fwLogsList);
    }


    /**
     * 导出业务日志列表
     */
    @PreAuthorize("@ss.hasPermi('admin:logs:export')")
    @Log(title = "业务日志", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwLogs fwLogs)
    {
        List<FwLogs> list = fwLogsService.selectFwLogsList(fwLogs);
        ExcelUtil<FwLogs> util = new ExcelUtil<FwLogs>(FwLogs.class);
        return util.exportExcel(list, "logs");
    }

    /**
     * 获取业务日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:logs:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwLogsService.selectFwLogsById(id));
    }

    /**
     * 新增业务日志
     */
    @PreAuthorize("@ss.hasPermi('admin:logs:add')")
    @Log(title = "业务日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwLogs fwLogs)
    {
        return toAjax(fwLogsService.insertFwLogs(fwLogs));
    }

    /**
     * 修改业务日志
     */
    @PreAuthorize("@ss.hasPermi('admin:logs:edit')")
    @Log(title = "业务日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwLogs fwLogs)
    {
        return toAjax(fwLogsService.updateFwLogs(fwLogs));
    }

    /**
     * 删除业务日志
     */
    @PreAuthorize("@ss.hasPermi('admin:logs:remove')")
    @Log(title = "业务日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwLogsService.deleteFwLogsByIds(ids));
    }
}
