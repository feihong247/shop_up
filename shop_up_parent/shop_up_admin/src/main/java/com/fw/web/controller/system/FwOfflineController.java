package com.fw.web.controller.system;

import java.util.List;

import com.fw.core.domain.model.LoginUser;
import com.fw.system.admin.service.ISysUserService;
import com.fw.utils.ServletUtils;
import com.fw.web.web.service.TokenService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwOffline;
import com.fw.system.admin.service.IFwOfflineService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 线下消费记录Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/offline")
public class FwOfflineController extends BaseController
{
    @Autowired
    private IFwOfflineService fwOfflineService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private ISysUserService sysUserService;

    /**
     * 查询线下消费记录列表
     */
    @PreAuthorize("@ss.hasPermi('admin:offline:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwOffline fwOffline)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String shopId = sysUserService.isShop(loginUser);
        fwOffline.setShopId(shopId);
        startPage();
        List<FwOffline> list = fwOfflineService.selectFwOfflineList(fwOffline);
        return getDataTable(list);
    }

    /**
     * 导出线下消费记录列表
     */
    @PreAuthorize("@ss.hasPermi('admin:offline:export')")
    @Log(title = "线下消费记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwOffline fwOffline)
    {
        List<FwOffline> list = fwOfflineService.selectFwOfflineList(fwOffline);
        ExcelUtil<FwOffline> util = new ExcelUtil<FwOffline>(FwOffline.class);
        return util.exportExcel(list, "offline");
    }

    /**
     * 获取线下消费记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:offline:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwOfflineService.selectFwOfflineById(id));
    }

    /**
     * 新增线下消费记录
     */
    @PreAuthorize("@ss.hasPermi('admin:offline:add')")
    @Log(title = "线下消费记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwOffline fwOffline)
    {
        return toAjax(fwOfflineService.insertFwOffline(fwOffline));
    }

    /**
     * 修改线下消费记录
     */
    @PreAuthorize("@ss.hasPermi('admin:offline:edit')")
    @Log(title = "线下消费记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwOffline fwOffline)
    {
        return toAjax(fwOfflineService.updateFwOffline(fwOffline));
    }

    /**
     * 删除线下消费记录
     */
    @PreAuthorize("@ss.hasPermi('admin:offline:remove')")
    @Log(title = "线下消费记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwOfflineService.deleteFwOfflineByIds(ids));
    }
}
