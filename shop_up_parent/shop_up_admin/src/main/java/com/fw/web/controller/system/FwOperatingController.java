package com.fw.web.controller.system;

import java.util.List;

import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.system.admin.domain.FwShop;
import com.fw.system.admin.service.IFwShopService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwOperating;
import com.fw.system.admin.service.IFwOperatingService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 商家经营平台分类Controller
 * 
 * @author yanwei
 * @date 2021-05-19
 */
@RestController
@RequestMapping("/admin/operating")
public class FwOperatingController extends BaseController
{
    @Autowired
    private IFwOperatingService fwOperatingService;

    @Autowired
    private IFwShopService shopService;

    /**
     * 查询商家经营平台分类列表
     */
    @PreAuthorize("@ss.hasPermi('admin:operating:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwOperating fwOperating)
    {
        startPage();
        List<FwOperating> list = fwOperatingService.selectFwOperatingList(fwOperating);
        return getDataTable(list);
    }

    /**
     * 导出商家经营平台分类列表
     */
    @PreAuthorize("@ss.hasPermi('admin:operating:export')")
    @Log(title = "商家经营平台分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwOperating fwOperating)
    {
        List<FwOperating> list = fwOperatingService.selectFwOperatingList(fwOperating);
        ExcelUtil<FwOperating> util = new ExcelUtil<FwOperating>(FwOperating.class);
        return util.exportExcel(list, "operating");
    }

    /**
     * 获取商家经营平台分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:operating:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwOperatingService.selectFwOperatingById(id));
    }

    /**
     * 新增商家经营平台分类
     */
    @PreAuthorize("@ss.hasPermi('admin:operating:add')")
    @Log(title = "商家经营平台分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwOperating fwOperating)
    {
        return toAjax(fwOperatingService.insertFwOperating(fwOperating));
    }

    /**
     * 修改商家经营平台分类
     */
    @PreAuthorize("@ss.hasPermi('admin:operating:edit')")
    @Log(title = "商家经营平台分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwOperating fwOperating)
    {
        return toAjax(fwOperatingService.updateFwOperating(fwOperating));
    }

    /**
     * 删除商家经营平台分类
     */
    @PreAuthorize("@ss.hasPermi('admin:operating:remove')")
    @Log(title = "商家经营平台分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        List<FwShop> fwShops = shopService.list(Wrappers.<FwShop>lambdaQuery().in(FwShop::getOperatingName));
        Assert.isTrue(fwShops.isEmpty(),"该类目已绑定,不可随意删除!");
        return toAjax(fwOperatingService.deleteFwOperatingByIds(ids));
    }
}
