package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwWithdraw;
import com.fw.system.admin.service.IFwWithdrawService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 用户提现Controller
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/admin/withdraw")
public class FwWithdrawController extends BaseController
{
    @Autowired
    private IFwWithdrawService fwWithdrawService;

    /**
     * 查询用户提现列表
     */
    @PreAuthorize("@ss.hasPermi('admin:withdraw:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwWithdraw fwWithdraw)
    {
        startPage();
        List<FwWithdraw> list = fwWithdrawService.selectFwWithdrawList(fwWithdraw);
        return getDataTable(list);
    }

    /**
     * 导出用户提现列表
     */
    @PreAuthorize("@ss.hasPermi('admin:withdraw:export')")
    @Log(title = "用户提现", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwWithdraw fwWithdraw)
    {
        List<FwWithdraw> list = fwWithdrawService.selectFwWithdrawList(fwWithdraw);
        ExcelUtil<FwWithdraw> util = new ExcelUtil<FwWithdraw>(FwWithdraw.class);
        return util.exportExcel(list, "withdraw");
    }

    /**
     * 获取用户提现详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:withdraw:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwWithdrawService.selectFwWithdrawById(id));
    }

    /**
     * 新增用户提现
     */
    @PreAuthorize("@ss.hasPermi('admin:withdraw:add')")
    @Log(title = "用户提现", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwWithdraw fwWithdraw)
    {
        return toAjax(fwWithdrawService.insertFwWithdraw(fwWithdraw));
    }

    /**
     * 修改用户提现
     */
    @PreAuthorize("@ss.hasPermi('admin:withdraw:edit')")
    @Log(title = "用户提现", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwWithdraw fwWithdraw)
    {
        return toAjax(fwWithdrawService.updateFwWithdraw(fwWithdraw));
    }

    /**
     * 删除用户提现
     */
    @PreAuthorize("@ss.hasPermi('admin:withdraw:remove')")
    @Log(title = "用户提现", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwWithdrawService.deleteFwWithdrawByIds(ids));
    }
}
