package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwReadPackageTask;
import com.fw.system.admin.service.IFwReadPackageTaskService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 红包任务类型列Controller
 * 
 * @author yanwei
 * @date 2021-10-12
 */
@RestController
@RequestMapping("/admin/read_package_task")
public class FwReadPackageTaskController extends BaseController
{
    @Autowired
    private IFwReadPackageTaskService fwReadPackageTaskService;

    /**
     * 查询红包任务类型列列表
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package_task:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwReadPackageTask fwReadPackageTask)
    {
        startPage();
        List<FwReadPackageTask> list = fwReadPackageTaskService.selectFwReadPackageTaskList(fwReadPackageTask);
        return getDataTable(list);
    }

    /**
     * 导出红包任务类型列列表
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package_task:export')")
    @Log(title = "红包任务类型列", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwReadPackageTask fwReadPackageTask)
    {
        List<FwReadPackageTask> list = fwReadPackageTaskService.selectFwReadPackageTaskList(fwReadPackageTask);
        ExcelUtil<FwReadPackageTask> util = new ExcelUtil<FwReadPackageTask>(FwReadPackageTask.class);
        return util.exportExcel(list, "read_package_task");
    }

    /**
     * 获取红包任务类型列详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package_task:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwReadPackageTaskService.selectFwReadPackageTaskById(id));
    }

    /**
     * 新增红包任务类型列
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package_task:add')")
    @Log(title = "红包任务类型列", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwReadPackageTask fwReadPackageTask)
    {
        return toAjax(fwReadPackageTaskService.insertFwReadPackageTask(fwReadPackageTask));
    }

    /**
     * 修改红包任务类型列
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package_task:edit')")
    @Log(title = "红包任务类型列", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwReadPackageTask fwReadPackageTask)
    {
        return toAjax(fwReadPackageTaskService.updateFwReadPackageTask(fwReadPackageTask));
    }

    /**
     * 删除红包任务类型列
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package_task:remove')")
    @Log(title = "红包任务类型列", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwReadPackageTaskService.deleteFwReadPackageTaskByIds(ids));
    }
}
