package com.fw.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwReadPackageTaskContent;
import com.fw.system.admin.service.IFwReadPackageTaskContentService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 红包关联任务用户完成情况Controller
 * 
 * @author yanwei
 * @date 2021-10-12
 */
@RestController
@RequestMapping("/admin/read_package_task_content")
public class FwReadPackageTaskContentController extends BaseController
{
    @Autowired
    private IFwReadPackageTaskContentService fwReadPackageTaskContentService;

    /**
     * 查询红包关联任务用户完成情况列表
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package_task_content:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwReadPackageTaskContent fwReadPackageTaskContent)
    {
        startPage();
        List<FwReadPackageTaskContent> list = fwReadPackageTaskContentService.selectFwReadPackageTaskContentList(fwReadPackageTaskContent);
        return getDataTable(list);
    }

    /**
     * 导出红包关联任务用户完成情况列表
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package_task_content:export')")
    @Log(title = "红包关联任务用户完成情况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwReadPackageTaskContent fwReadPackageTaskContent)
    {
        List<FwReadPackageTaskContent> list = fwReadPackageTaskContentService.selectFwReadPackageTaskContentList(fwReadPackageTaskContent);
        ExcelUtil<FwReadPackageTaskContent> util = new ExcelUtil<FwReadPackageTaskContent>(FwReadPackageTaskContent.class);
        return util.exportExcel(list, "read_package_task_content");
    }

    /**
     * 获取红包关联任务用户完成情况详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package_task_content:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fwReadPackageTaskContentService.selectFwReadPackageTaskContentById(id));
    }

    /**
     * 新增红包关联任务用户完成情况
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package_task_content:add')")
    @Log(title = "红包关联任务用户完成情况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwReadPackageTaskContent fwReadPackageTaskContent)
    {
        return toAjax(fwReadPackageTaskContentService.insertFwReadPackageTaskContent(fwReadPackageTaskContent));
    }

    /**
     * 修改红包关联任务用户完成情况
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package_task_content:edit')")
    @Log(title = "红包关联任务用户完成情况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwReadPackageTaskContent fwReadPackageTaskContent)
    {
        return toAjax(fwReadPackageTaskContentService.updateFwReadPackageTaskContent(fwReadPackageTaskContent));
    }

    /**
     * 删除红包关联任务用户完成情况
     */
    @PreAuthorize("@ss.hasPermi('admin:read_package_task_content:remove')")
    @Log(title = "红包关联任务用户完成情况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fwReadPackageTaskContentService.deleteFwReadPackageTaskContentByIds(ids));
    }
}
