package com.fw.web.controller.system;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.core.controller.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.system.admin.domain.FwLockStorage;
import com.fw.system.admin.service.IFwLockStorageService;
import com.fw.utils.poi.ExcelUtil;
import com.fw.core.page.TableDataInfo;

/**
 * 锁仓池Controller
 *
 * @author yanwei
 * @date 2021-06-28
 */
@RestController
@RequestMapping("/admin/lock_storage")
public class FwLockStorageController extends BaseController {
    @Autowired
    private IFwLockStorageService fwLockStorageService;

    /**
     * 查询锁仓池列表
     */
    @PreAuthorize("@ss.hasPermi('admin:lock_storage:list')")
    @GetMapping("/list")
    public TableDataInfo list(FwLockStorage fwLockStorage) {
        startPage();
        List<FwLockStorage> list = fwLockStorageService.selectFwLockStorageList(fwLockStorage);
        return getDataTable(list);
    }

    /**
     * 导出锁仓池列表
     */
    @PreAuthorize("@ss.hasPermi('admin:lock_storage:export')")
    @Log(title = "锁仓池", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FwLockStorage fwLockStorage) {
        List<FwLockStorage> list = fwLockStorageService.selectFwLockStorageList(fwLockStorage);
        ExcelUtil<FwLockStorage> util = new ExcelUtil<FwLockStorage>(FwLockStorage.class);
        return util.exportExcel(list, "lock_storage");
    }

    /**
     * 获取锁仓池详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:lock_storage:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(fwLockStorageService.selectFwLockStorageById(id));
    }

    /**
     * 新增锁仓池
     */
    @PreAuthorize("@ss.hasPermi('admin:lock_storage:add')")
    @Log(title = "锁仓池", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FwLockStorage fwLockStorage) {
        return toAjax(fwLockStorageService.insertFwLockStorage(fwLockStorage));
    }

    /**
     * 修改锁仓池
     */
    @PreAuthorize("@ss.hasPermi('admin:lock_storage:edit')")
    @Log(title = "锁仓池", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FwLockStorage fwLockStorage) {
        return toAjax(fwLockStorageService.updateFwLockStorage(fwLockStorage));
    }

    /**
     * 删除锁仓池
     */
    @PreAuthorize("@ss.hasPermi('admin:lock_storage:remove')")
    @Log(title = "锁仓池", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(fwLockStorageService.deleteFwLockStorageByIds(ids));
    }
}
