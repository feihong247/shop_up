package com.fw.web.controller.quartz.task;

import com.fw.system.admin.service.IFwShangjiaService;
import com.fw.system.admin.service.IFwSpuService;
import com.fw.utils.StringUtils;
import com.fw.web.controller.cheduling.Dividends;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 定时任务调度测试
 *
 * @author ruoyi
 */
@Component("ryTask")
@Slf4j
public class RyTask {

    @Autowired
    private IFwShangjiaService fwShangjiaService;

    @Autowired
    private Dividends dividends;

    @Autowired
    private IFwSpuService spuService;

/*
    @Autowired
    private com.fw.system.web.service.IFwShangjiaService shangjiaService;*/

    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i) {
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void ryParams(String params) {
        System.out.println("执行有参方法：" + params);
    }

    public void ryNoParams() {
        System.out.println("执行无参方法");
    }

    public void ryShopScreenData(){
        fwShangjiaService.shopScreenData();
        log.info("定时任务查询商户大屏数据执行了");
    }
    public void ryScreenData(){
        fwShangjiaService.ScreenData();
        log.info("定时任务查询管理员大屏数据执行了");
    }

    public void ryBonus(){
        log.info("分红方法开始执行了!");
        dividends.bonus();
    }

    public void checkTops(){
        spuService.checkTops();
    }
    /*
    public void ryOutput(){
        shangjiaService.output();
        log.info("定时任务查询商甲存储数据执行了");
    }
*/
}
