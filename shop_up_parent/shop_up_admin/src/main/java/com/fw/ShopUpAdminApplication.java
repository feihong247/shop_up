package com.fw;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

/**
 * 启动程序
 * 
 * @author yanwei
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
@MapperScan({"com.fw.system.admin.mapper","com.fw.system.comm.mapper"})
@ComponentScan(basePackages = "com.fw.**",excludeFilters = {@ComponentScan.Filter(type=FilterType.CUSTOM,classes = {ExcludeClass.class})})
public class ShopUpAdminApplication
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(ShopUpAdminApplication.class, args);
    }
}
