package com.fw.application.controller.v2.shop.analyze;

import com.fw.application.controller.base.BaseController;
import com.fw.mes.Result;
import com.fw.system.web.model.dto.UserDto;
import com.fw.system.web.model.form.v2.OrderManageQuery;
import com.fw.system.web.model.form.v2.ShopAnalyzeQuery;
import com.fw.system.web.model.form.v2.SpuAnalyzeQuery;
import com.fw.system.web.model.vo.v2.OrderAnalyzeVo;
import com.fw.system.web.model.vo.v2.OrderMoneyVo;
import com.fw.system.web.model.vo.v2.ShopAnalyzeVo;
import com.fw.system.web.model.vo.v2.SpuAnalyzeVo;
import com.fw.system.web.service.v2.V2ShopAnalyzeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.fw.mes.ResultUtils.success;

/**
 * 商户端相关API
 */
@RestController
@RequestMapping("/v2/analyze")
@Api(tags = "商户端智能化分析")
public class V2ShopAnalyzeController implements BaseController {

    @Autowired
    private V2ShopAnalyzeService analyzeService;




    @PostMapping("/shopAnalyze")
    @ApiOperation("商铺分析")
    public Result<ShopAnalyzeVo> shopAnalyze(@Validated @RequestBody ShopAnalyzeQuery shopAnalyzeQuery){
        UserDto shopDto = getShopDto();
        return success(analyzeService.shopAnalyze(shopDto.getShopId(),shopAnalyzeQuery));
    }

    @PostMapping("/spuAnalyze")
    @ApiOperation("商品分析")
    public Result<SpuAnalyzeVo> spuAnalyze(@Validated @RequestBody SpuAnalyzeQuery spuAnalyzeQuery){
        UserDto shopDto = getShopDto();
        return success(analyzeService.spuAnalyze(shopDto.getShopId(),spuAnalyzeQuery));
    }


    @PostMapping("/orderAnalyze")
    @ApiOperation("订单分析")
    public Result<OrderAnalyzeVo> orderAnalyze(@Validated @RequestBody ShopAnalyzeQuery shopAnalyzeQuery){
        UserDto shopDto = getShopDto();
        return success(analyzeService.orderAnalyze(shopDto,shopAnalyzeQuery));
    }

    @PostMapping("/manageOnlineOrder")
    @ApiOperation("财务管理，线上分析 日，月，年,总数 把 集合中 所有的值加起来即可")
    public Result<List<OrderMoneyVo>> manageOnlineOrder(@Validated @RequestBody OrderManageQuery orderManageQuery){
        UserDto shopDto = getShopDto();
        return success(analyzeService.manageOnlineOrder(shopDto.getShopId(),orderManageQuery));
    }

    @PostMapping("/manageOffOrder")
    @ApiOperation("财务管理，线下分析 日，月，年,总数 把 集合中 所有的值加起来即可")
    public Result<List<OrderMoneyVo>> manageOffOrder(@Validated @RequestBody OrderManageQuery orderManageQuery){
        UserDto shopDto = getShopDto();
        return success(analyzeService.manageOffOrder(shopDto.getShopId(),orderManageQuery));
    }




}
