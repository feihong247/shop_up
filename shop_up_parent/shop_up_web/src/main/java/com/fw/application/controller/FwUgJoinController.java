package com.fw.application.controller;

import java.util.List;

import com.fw.application.controller.base.BaseController;
import com.fw.mes.Result;
import com.fw.system.web.model.entity.FwUgJoin;
import com.fw.system.web.service.IFwUgJoinService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.fw.annotation.Log;
import com.fw.enums.BusinessType;

import static com.fw.mes.ResultUtils.success;

/**
 * 用户竞猜中间Controller
 * 
 * @author yanwei
 * @date 2021-06-23
 */
@RestController
@RequestMapping("/admin/ug_join")
public class FwUgJoinController implements BaseController {
    @Autowired
    private IFwUgJoinService fwUgJoinService;

    /**
     * 查询用户竞猜中间列表
     */
//    @PreAuthorize("@ss.hasPermi('admin:ug_join:list')")
//    @GetMapping("/list")
//    public TableDataInfo list(FwUgJoin fwUgJoin) {
//        startPage();
//        List<FwUgJoin> list = fwUgJoinService.selectFwUgJoinList(fwUgJoin);
//        return getDataTable(list);
//    }

    /**
     * 获取用户竞猜中间详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:ug_join:query')")
    @GetMapping(value = "/{ugId}")
    public Result getInfo(@PathVariable("ugId") String ugId)
    {
        return success(fwUgJoinService.selectFwUgJoinById(ugId));
    }

    /**
     * 新增用户竞猜中间
     */
    @PreAuthorize("@ss.hasPermi('admin:ug_join:add')")
    @Log(title = "用户竞猜中间", businessType = BusinessType.INSERT)
    @PostMapping
    public Result add(@RequestBody FwUgJoin fwUgJoin)
    {
        return success(fwUgJoinService.insertFwUgJoin(fwUgJoin));
    }

    /**
     * 修改用户竞猜中间
     */
    @PreAuthorize("@ss.hasPermi('admin:ug_join:edit')")
    @Log(title = "用户竞猜中间", businessType = BusinessType.UPDATE)
    @PutMapping
    public Result edit(@RequestBody FwUgJoin fwUgJoin)
    {
        return success(fwUgJoinService.updateFwUgJoin(fwUgJoin));
    }

    /**
     * 删除用户竞猜中间
     */
    @PreAuthorize("@ss.hasPermi('admin:ug_join:remove')")
    @Log(title = "用户竞猜中间", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ugIds}")
    public Result remove(@PathVariable String[] ugIds)
    {
        return success(fwUgJoinService.deleteFwUgJoinByIds(ugIds));
    }
}
