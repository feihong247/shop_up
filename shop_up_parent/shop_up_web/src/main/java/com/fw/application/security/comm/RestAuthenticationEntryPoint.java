package com.fw.application.security.comm;

import cn.hutool.json.JSONUtil;
import com.fw.enums.CodeStatus;
import com.fw.mes.Result;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 当未登录或者token失效访问接口时，自定义的送回结果
 * @Author:yanwei
 * @Date: 2020/10/14 - 17:09
 */
@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().println(JSONUtil.parse(new Result<>(CodeStatus.NOT_LOGIN)));
        response.getWriter().flush();
    }
}
