package com.fw.application.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 积分销毁商甲表 前端控制器
 * </p>
 *
 * @author  
 * @since 2021-07-08
 */
@RestController
@RequestMapping("/fw.system.web/fw-withdraw-shangjia")
public class FwWithdrawShangjiaController {

}

