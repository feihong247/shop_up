package com.fw.application.controller.open;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.RandomUtil;
import com.fw.common.AliComm;
import com.fw.enums.AliPayEnum;
import com.fw.enums.CodeStatus;
import com.fw.mes.Result;
import com.fw.utils.AliUtil;
import com.fw.utils.ChuangLanSmsUtil;
import com.fw.utils.HuaweiUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.fw.mes.ResultUtils.success;

/**
 * @Author:yanwei
 * @Date: 2020/10/19 - 14:18
 */
@CrossOrigin
@RestController
@RequestMapping("/web/core")
@Api(tags = "公众接口- 例如文件上传")
public class CoreController {

    @Autowired
    private RedisTemplate redisTemplate;



    @SneakyThrows
    @PostMapping("/uploadImage")
    @ApiOperation(value = "文件上传:data = imageUrl")
    public Result<String> uploadImage(@RequestPart("file") MultipartFile multipartFile){
        return success(HuaweiUtil.uploadFile(multipartFile.getInputStream(), multipartFile.getOriginalFilename()));
    }

    @GetMapping("/sendPhone/{phone}")
    @ApiOperation(value = "发送验证码",hidden = true)
    public Result sendPhone(@PathVariable String phone){
        if (!Validator.isMobile(phone))
         return new Result(CodeStatus.ACCOUNT_FORMAT_ERROR);
        String code =  RandomUtil.randomInt(6)+ "" + RandomUtil.randomInt(6) + "" +RandomUtil.randomInt(6) + "" +RandomUtil.randomInt(6)+ "" +RandomUtil.randomInt(6)+ "" +RandomUtil.randomInt(6);
        Assert.isTrue(Objects.isNull(redisTemplate.opsForValue().get(phone)),"5分钟内不可在发送.");
        redisTemplate.opsForValue().set(phone,code,5, TimeUnit.MINUTES);
        ChuangLanSmsUtil.sendSms(phone,code,"5");
        return success();
    }







}
