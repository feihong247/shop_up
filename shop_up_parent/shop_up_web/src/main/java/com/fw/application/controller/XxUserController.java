package com.fw.application.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.application.controller.base.BaseController;
import com.fw.common.IdXD;
import com.fw.mes.Result;
import com.fw.system.web.model.entity.FwMoneys;
import com.fw.system.web.model.entity.FwUser;
import com.fw.system.web.service.IFwLogsService;
import com.fw.system.web.service.IFwMoneysService;
import com.fw.system.web.service.IFwUserService;
import com.fw.system.web.service.IXxUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

import static com.fw.mes.ResultUtils.success;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author
 * @since 2021-07-31
 */
@RestController
@RequestMapping("/xxuser")
public class XxUserController implements BaseController {


    @Autowired
    private IXxUserService xxUserService;
    @Autowired
    private IFwUserService userService;
    @Autowired
    private IFwMoneysService moneysService;
    @Autowired
    private IdXD idXD;
    @Autowired
    private IFwLogsService logsService;


  /*  //查询十月28日多提现积分的人
    @GetMapping("/getTiXianList")
    @ApiOperation("查询十月28日多提现积分的人")
    public Result getTiXianList() {
        LocalDate dateTime = LocalDate.parse("2021-10-28");
        //肖证兑换记录
        List<FwLogs> list = logsService.list(Wrappers.<FwLogs>lambdaQuery().eq(FwLogs::getIsType, "兑换")
                .last("AND create_time  between '2021-10-28 00:00:00' and '2021-10-28 01:00:00' "));
        //查出这个人的提现记录
        list.forEach(item -> {
            BigDecimal b = new BigDecimal("0");
            List<FwLogs> logs = logsService.list(Wrappers.<FwLogs>lambdaQuery().eq(FwLogs::getIsType, "提现").eq(FwLogs::getBuiId, item.getBuiId())
                    .last("AND create_time  between '2021-10-28 00:00:00' and '2021-10-28 23:00:00' "));
            for (FwLogs log : logs) {
                b = b.add(new BigDecimal(log.getFormInfo()));
                //如果这个人的提现记录金额大于积分释放金额，直接扣钱包积分没有扣成负数
            }
            if (b.compareTo(new BigDecimal(item.getFormInfo())) > 0){
                BigDecimal result = b.subtract(new BigDecimal(item.getFormInfo()));
                FwMoneys moneys = moneysService.getOne(Wrappers.<FwMoneys>lambdaQuery().eq(FwMoneys::getUserId, item.getBuiId()));
                moneys.setIntegral(moneys.getIntegral().add(result)).updateById();
            }

        });
        return success(list);
    }*/

    //补肖证
    @GetMapping("/xiaozheng/{phone}/{num}")
    @ApiOperation("补肖证")
    public Result xiaozheng(@PathVariable String phone, @PathVariable BigDecimal num) {

        FwUser one = new FwUser();
        one = userService.getOne(Wrappers.<FwUser>lambdaQuery().eq(FwUser::getPhone, phone));
        if (one == null) {
            List<FwUser> list = userService.list(Wrappers.<FwUser>lambdaQuery().eq(FwUser::getAliPhone, phone));
            if (list.size() > 0) {
                one = list.get(0);
            }
        }
        if (one == null) {
            return new Result().fail(0, "通过注册手机号和支付宝号都找不到用户");
        }
        moneysService.disappearReturn(one.getId(), num, "线下消费送消证(补)");

        return success();
    }

    //补积分
    @PostMapping("/addIntegral/{phone}/{num}")
    @ApiOperation("不计分")
    public Result addIntegral(@PathVariable String phone, @PathVariable BigDecimal num) {

        FwUser one = new FwUser();
        one = userService.getOne(Wrappers.<FwUser>lambdaQuery().eq(FwUser::getPhone, phone));
        if (one == null) {
            List<FwUser> list = userService.list(Wrappers.<FwUser>lambdaQuery().eq(FwUser::getAliPhone, phone));
            if (list.size() > 0) {
                one = list.get(0);
            }
        }
        if (one == null) {
            return new Result().fail(0, "通过注册手机号和支付宝号都找不到用户");
        }
        FwMoneys moneys = moneysService.getOne(Wrappers.<FwMoneys>lambdaQuery().eq(FwMoneys::getUserId, one.getId()));
        moneys.setIntegral(moneys.getIntegral().add(num));
        moneysService.updateById(moneys);
        logsService.saveLogs(idXD.nextId(), one.getId(), "兑换", "积分模块", "系统增补积分", num.toString());
        return success();
    }


}

