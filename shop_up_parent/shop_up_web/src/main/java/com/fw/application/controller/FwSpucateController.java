package com.fw.application.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.mes.Result;
import com.fw.system.web.model.entity.FwSpucate;
import com.fw.system.web.service.IFwSpucateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.fw.mes.ResultUtils.success;

/**
 * <p>
 * 商品类目 前端控制器
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@RestController
@RequestMapping("/spucate")
@Api(tags = "商品类目")
public class FwSpucateController {

    @Autowired
    private IFwSpucateService spucateService;

    /** 商品类目列表 */
    @ApiOperation("商品类目列表")
    @GetMapping("/list")
    public Result<List<FwSpucate>> list (){
        List<FwSpucate> firstList = spucateService.list(Wrappers.<FwSpucate>lambdaQuery()
                .eq(FwSpucate::getParentId, "-1")
                .or()
                .isNull(FwSpucate::getParentId));
        for (FwSpucate spucate : firstList) {
            spucate.setSpucateList(spucateService.list(Wrappers.<FwSpucate>lambdaQuery().eq(FwSpucate::getParentId, spucate.getId())));
        }
        return success(firstList);
    }

}

