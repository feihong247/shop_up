package com.fw.application.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.application.controller.base.BaseController;
import com.fw.common.Builder;
import com.fw.constant.Constant;
import com.fw.mes.Result;
import com.fw.system.web.model.entity.FwIdentity;
import com.fw.system.web.model.entity.FwUljoin;
import com.fw.system.web.model.entity.FwUser;
import com.fw.system.web.model.vo.ShareUserCountVo;
import com.fw.system.web.model.vo.ShareUserListVo;
import com.fw.system.web.service.IFwIdentityService;
import com.fw.system.web.service.IFwUljoinService;
import com.fw.system.web.service.IFwUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static com.fw.mes.ResultUtils.success;

@RestController
@RequestMapping("/share")
@CrossOrigin
@Api(tags = "区县分享统计")
public class ShareController implements BaseController {


    @Autowired
    private IFwUserService userService;
    @Autowired
    private IFwUljoinService joinService;
    @Autowired
    private IFwIdentityService iFwIdentityService;

    //查看我的分享人数及下级列表信息和下级分享人数
    @PostMapping("/shareList")
    @ApiOperation("查看我的分享人数及下级列表信息和下级分享人数")
    public Result<ShareUserCountVo> shareList() {

        String userId = this.getUser().getId();
        ShareUserCountVo build = Builder.of(ShareUserCountVo::new).with(ShareUserCountVo::setCountNum, userService.
                list(Wrappers.<FwUser>lambdaQuery().eq(FwUser::getParentId, userId)).size()).build();
        //填充用户信息和下级用户分享的人数
        List<ShareUserListVo> userList = new ArrayList<>();
        userService.list(Wrappers.<FwUser>lambdaQuery().eq(FwUser::getParentId, userId)).forEach(item -> {
            ShareUserListVo vo = Builder.of(ShareUserListVo::new).with(ShareUserListVo::setCountNum, userService.
                    list(Wrappers.<FwUser>lambdaQuery().eq(FwUser::getParentId, item.getId())).size()).
                    with(ShareUserListVo::setCreateTime, item.getCreateTime()).
                    with(ShareUserListVo::setHeadImage, item.getHeadImage())
                    .with(ShareUserListVo::setPhone, item.getPhone()).
                            with(ShareUserListVo::setUserName, item.getUserName()).with(ShareUserListVo::setIdentity, "普通用户").build();
            FwUljoin uljoin = joinService.getOne(Wrappers.<FwUljoin>lambdaQuery().eq(FwUljoin::getUserId, item.getId()).
                    eq(FwUljoin::getIsUse, Constant.IsUsr.IS_USR));
            if (uljoin != null) {
                FwIdentity identity = iFwIdentityService.getById(uljoin.getIdentityId());
                vo.setIdentity(identity.getIdentityName());
            }
            userList.add(vo);
            build.setUserList(userList);

        });
        return success(build);
    }
}
