package com.fw.application.controller.v2.shop.evaluation;

import com.fw.application.controller.base.BaseController;
import com.fw.mes.Result;
import com.fw.system.web.model.dto.UserDto;
import com.fw.system.web.model.form.v2.EvaluationQuery;
import com.fw.system.web.model.vo.v2.EvaluationCountVo;
import com.fw.system.web.model.vo.v2.EvaluationV2Vo;
import com.fw.system.web.service.IFwEvaluationService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static com.fw.mes.ResultUtils.success;

@RestController
@RequestMapping("/v2/evaluation")
@Api(tags = "商户端 商品评价相关api")
public class V2EvaluationController implements BaseController {

    @Autowired
    private IFwEvaluationService evaluationService;


    @PostMapping("/pageList")
    @ApiOperation("列表查询评价列表——数据透传")
    public Result<PageInfo<EvaluationV2Vo>> pageList(@RequestBody @Validated EvaluationQuery evaluationQuery){
        UserDto shopDto = getShopDto();
        return success(evaluationService.pageList(evaluationQuery,shopDto.getShopId()));
    }

    @GetMapping("/countEva")
    @ApiOperation("统计 有图（0）,最新（0）")
    public Result<EvaluationCountVo> countEva(){
        UserDto shopDto = getShopDto();
        EvaluationCountVo evaluationCountVo =  evaluationService.countEva(shopDto.getShopId());
        return success(evaluationCountVo);
    }

}
