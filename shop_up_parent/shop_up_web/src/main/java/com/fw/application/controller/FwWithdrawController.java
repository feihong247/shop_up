package com.fw.application.controller;


import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.fw.application.controller.base.BaseController;
import com.fw.common.AliComm;
import com.fw.common.IdXD;
import com.fw.enums.AliPayEnum;
import com.fw.enums.LogsModelEnum;
import com.fw.enums.LogsTypeEnum;
import com.fw.mes.Result;
import com.fw.system.web.model.entity.FwUser;
import com.fw.system.web.model.entity.FwWithdraw;
import com.fw.system.web.model.vo.DealVo;
import com.fw.system.web.service.*;
import com.fw.utils.RandomUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.UUID;

import static com.fw.mes.ResultUtils.success;

/**
 * <p>
 * 用户提现表 前端控制器
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@RestController
@RequestMapping("/withdraw")
@Api(tags = "积分交易")
@RequiredArgsConstructor
public class FwWithdrawController implements BaseController {

    private final IFwWithdrawService fwWithdrawService;
    private final IdXD idXD;
    private final AliComm aliComm;
    private final IFwUserService userService;
    private final IFwShangjipoolService fwShangjipoolService;
    private final IFwLogsService logsService;
    private final IFwWithdrawShangjiaService fwWithdrawShangjiaService;
    private final ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @PostMapping("/firstWithdraw")
    @ApiOperation("shlo:积分交易:积分提现")
    public Result firstWithdraw(@PathVariable FwWithdraw fwWithdraw) {
        FwUser user = getUser();
        fwWithdraw.setId(idXD.nextId());
        fwWithdraw.setUserId(user.getId());
        fwWithdraw.setStatus(0);
        fwWithdraw.setCreateBy(user.getId());
        fwWithdraw.setCreateTime(LocalDateTime.now());
        fwWithdraw.setUpdateBy(user.getId());
        fwWithdraw.setUpdateTime(LocalDateTime.now());
        fwWithdrawService.save(fwWithdraw);
        //添加积分提现日志
        logsService.saveLogs(fwWithdraw.getUserId(),fwWithdraw.getUserId(), LogsTypeEnum.INTEGRAL_WITHDRAWAL.getTypeName(), LogsModelEnum.INTEGRAL_MODEL.getModelName(),
                "积分提现申请" , JSON.toJSONString(fwWithdraw));
        return success();
    }
    @PostMapping("/withdrawConfirm")
    @ApiOperation("shlo:积分交易:确认积分提现")
    public Result withdrawConfirm(@PathVariable FwWithdraw fwWithdraw){
        FwWithdraw withdraw = fwWithdrawService.getById(fwWithdraw.getId());
        if(withdraw.getStatus()==1){
            /**
             * todo 调支付
             */
            try {
                FwUser fwUser = userService.selectFwUserById(withdraw.getUserId());
                //实际提现金额按照积分一比一的比例
                aliComm.aliTransfer(fwUser.getAliPhone(), withdraw.getIntegral(),fwUser.getAliUserName());
                fwWithdraw.setMoney(fwWithdraw.getIntegral());
                fwWithdrawService.saveOrUpdate(fwWithdraw);
                threadPoolTaskExecutor.execute(new Runnable(){
                    @Override
                    public void run() {
                        //将体现积分更新到积分销毁商甲表
                        fwWithdrawShangjiaService.updateIntegral(fwWithdraw.getIntegral(),fwWithdraw.getUserId());
                        //调用商甲销毁逻辑
                        fwShangjipoolService.destoryShangjia(fwWithdraw.getUserId());
                    }
                });
                //添加积分提现日志
                logsService.saveLogs(fwWithdraw.getUserId(),fwWithdraw.getUserId(), LogsTypeEnum.INTEGRAL_WITHDRAWAL.getTypeName(), LogsModelEnum.INTEGRAL_MODEL.getModelName(),
                        "积分提现成功" , JSON.toJSONString(fwWithdraw));
                return success();
            } catch (AlipayApiException e) {
                //添加积分提现日志
                logsService.saveLogs(fwWithdraw.getUserId(),fwWithdraw.getUserId(), LogsTypeEnum.INTEGRAL_WITHDRAWAL.getTypeName(), LogsModelEnum.INTEGRAL_MODEL.getModelName(),
                        "积分提现支付失败" , JSON.toJSONString(fwWithdraw));
                return new Result().fail(-1, "支付失败");
            }
        }
        //添加积分提现日志
        logsService.saveLogs(fwWithdraw.getUserId(),fwWithdraw.getUserId(), LogsTypeEnum.INTEGRAL_WITHDRAWAL.getTypeName(), LogsModelEnum.INTEGRAL_MODEL.getModelName(),
                "未审核或审核失败的积分提现操作" , JSON.toJSONString(fwWithdraw));
        return new Result().fail(-1, withdraw.getStatus()==0?"未审核":"审核失败");
    }
    }

