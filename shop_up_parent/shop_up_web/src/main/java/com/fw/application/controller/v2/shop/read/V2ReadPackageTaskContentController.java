package com.fw.application.controller.v2.shop.read;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 红包关联任务用户完成情况表 前端控制器
 * </p>
 *
 * @author  
 * @since 2021-09-03
 */
@RestController
@RequestMapping("/fw.system.web/fw-read-package-task-content")
public class V2ReadPackageTaskContentController {

}

