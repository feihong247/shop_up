package com.fw.application.controller.v2.shop.offine;

import com.fw.application.controller.base.BaseController;
import com.fw.mes.Result;
import com.fw.system.web.model.dto.UserDto;
import com.fw.system.web.model.form.v2.OfflineQuery;
import com.fw.system.web.model.vo.v2.OfflineVo;
import com.fw.system.web.service.IFwOfflineService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.fw.mes.ResultUtils.success;

/**
 * 商户端线下消费记录相关API
 */
@RestController
@RequestMapping("/v2/offline_log")
@Api(tags = "商户端线下消费相关api")
public class V2OffLineController implements BaseController {

    @Autowired
    private IFwOfflineService offlineService;

    @PostMapping("/pageList")
    @ApiOperation("分页查询线下消费记录")
    public Result<PageInfo<OfflineVo>> pageList(@Validated @RequestBody OfflineQuery offlineQuery){
        UserDto shopDto = getShopDto();
        return success(offlineService.pageList(offlineQuery,shopDto.getShopId()));
    }









}
