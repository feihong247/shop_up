package com.fw.application.config;

import com.fw.common.AliComm;
import com.fw.common.IdXD;
import com.fw.config.SwaggerConfig;
import com.fw.utils.AuthUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import java.util.concurrent.TimeUnit;

/**
 * @Author:yanwei
 * @Date: 2020/8/5 - 16:26
 */
@Configuration
@Import({SwaggerConfig.class})
public class GlaConfig {

    @Value("spring.application.name:example")
    // 加密KEY
    private String key;


    @Bean
    public AuthUtils authUtils() {
        return new AuthUtils(key, TimeUnit.DAYS.toMillis(24));
    }

    @Bean
    public IdXD idXD() {
        return new IdXD();
    }

    @Bean(initMethod = "initAliConfig")
    public AliComm aliComm(){
        return new AliComm();
    }



    /**
     * 跨域配置
     */
    @Bean
    public CorsFilter corsFilter()
    {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        // 设置访问源地址
        config.addAllowedOrigin("*");
        // 设置访问源请求头
        config.addAllowedHeader("*");
        // 设置访问源请求方法
        config.addAllowedMethod("*");
        // 对接口配置跨域设置
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }


    //注解配置filter示例
/*    @Bean
    public FilterRegistrationBean filterTraceIdRegistrationBean() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(initCustomFilter());
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.setOrder(Integer.MIN_VALUE);
        return filterRegistrationBean;
    }*/

/*    public Filter initCustomFilter() {
        return new TraceIdFilterImpl();
    }*/

}
