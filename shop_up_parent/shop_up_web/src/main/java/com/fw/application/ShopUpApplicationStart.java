package com.fw.application;
import org.jasypt.encryption.StringEncryptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.FilterType;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @Author:yanwei
 * @Date: 2020/8/3 - 18:35
 */
@SpringBootApplication
@EnableScheduling
@EnableAsync
@EnableTransactionManagement
@EnableAspectJAutoProxy
@MapperScan(basePackages={"com.fw.system.web.dao","com.fw.system.comm.mapper"})
@ComponentScan(basePackages = "com.fw.**",excludeFilters = {@ComponentScan.Filter(type = FilterType.CUSTOM,classes = {ExcludeClass.class})})
public class ShopUpApplicationStart {
    public static void main(String[] args) {
         SpringApplication.run(ShopUpApplicationStart.class, args);
    }

}
