package com.fw.application.controller;


import com.fw.mes.Result;
import com.fw.system.web.model.vo.OperatingVo;
import com.fw.system.web.service.IFwOperatingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.fw.mes.ResultUtils.success;

/**
 * <p>
 * 商家经营平台分类 前端控制器
 * </p>
 *
 * @author  
 * @since 2021-05-19
 */
@RestController
@RequestMapping("/operating")
@Api(tags = "商家经营类型相关API,例如 美食，健身 等")
public class FwOperatingController {

    @Autowired
    private IFwOperatingService operatingService;


    @GetMapping("/findAll")
    @ApiOperation("查询所有商家经营类型")
    public Result<List<OperatingVo>> findAll(){
      List<OperatingVo> operatingVos = operatingService.findAll();
        return success(operatingVos);
    }





}

