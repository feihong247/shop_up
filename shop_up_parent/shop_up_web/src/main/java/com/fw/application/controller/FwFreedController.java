package com.fw.application.controller;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.application.controller.base.BaseController;
import com.fw.mes.Result;
import com.fw.system.web.model.entity.FwFreed;
import com.fw.system.web.model.entity.FwUser;
import com.fw.system.web.model.form.FreedQuery;
import com.fw.system.web.model.form.NoticeQuery;
import com.fw.system.web.model.vo.FreedVo;
import com.fw.system.web.service.IFwFreedService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static com.fw.mes.ResultUtils.success;

/**
 * <p>
 * 意见反馈 前端控制器
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@RestController
@RequestMapping("/freed")
@Api(tags = "意见反馈")
@RequiredArgsConstructor
public class FwFreedController implements BaseController {
    private final IFwFreedService freedService;

    /**
     * shlo:新增意见反馈
     * @param fwFreed
     * @return
     */
    @PostMapping("/addFreed")
    @ApiOperation("shlo:用户新增意见反馈")
    public Result addFreed(@RequestBody FwFreed fwFreed) {
        FwUser user = getUser();
        fwFreed.setUserId(user.getId());
        return success(freedService.insertFwFreed(fwFreed));
    }

    /**
     * shlo:查询意见反馈列表
     * @return
     */
    @PostMapping("/findFreedList")
    @ApiOperation("shlo:用户查询意见反馈列表")
    public Result<PageInfo<FwFreed>> findFreedList(@RequestBody FreedQuery freedQuery){
        FwUser user = getUser();
        PageHelper.startPage(freedQuery.getPageNum(),freedQuery.getPageSize());
        List<FwFreed> freeds = freedService.list(Wrappers.<FwFreed>lambdaQuery().eq(FwFreed::getUserId, user.getId()));
        PageInfo<FwFreed> fwFreedPageInfo = new PageInfo<>(freeds);

        return success(fwFreedPageInfo);
    }

    /**
     * shlo:根据id查询意见反馈
     * @param id
     * @return
     */
    @GetMapping("/findFreedById/{id}")
    @ApiOperation("shlo:根据id查询意见反馈详情")
    public Result<FreedVo> findFreedById(@PathVariable String id) {
        FwFreed fwFreed = freedService.selectFwFreedById(id);
        FreedVo freedVo = new FreedVo();
        BeanUtil.copyProperties(fwFreed, freedVo);
        return success(freedVo);
    }
}
