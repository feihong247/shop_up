package com.fw.application.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 线下消费记录表 前端控制器
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@RestController
@RequestMapping("/offline")
public class FwOfflineController {

}

