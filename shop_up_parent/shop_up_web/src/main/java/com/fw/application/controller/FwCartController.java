package com.fw.application.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.application.controller.base.BaseController;
import com.fw.common.IdXD;
import com.fw.mes.Result;
import com.fw.system.web.model.entity.FwCart;
import com.fw.system.web.model.entity.FwShop;
import com.fw.system.web.model.entity.FwSku;
import com.fw.system.web.model.entity.FwSpu;
import com.fw.system.web.model.vo.CartVo;
import com.fw.system.web.service.IFwCartService;
import com.fw.system.web.service.IFwShopService;
import com.fw.system.web.service.IFwSkuService;
import com.fw.system.web.service.IFwSpuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.fw.mes.ResultUtils.success;

/**
 * <p>
 * 购物车 前端控制器
 * </p>
 *
 * @author  yao
 * @since 2021-05-10
 */
@RestController
@RequestMapping("/cart/")
@Api(tags = "购物车")
public class FwCartController implements BaseController {

    @Autowired
    private IFwCartService cartService;
    @Autowired
    private IdXD idXD;
    @Autowired
    private IFwSkuService skuService;
    @Autowired
    private IFwSpuService spuService;
    @Autowired
    private IFwShopService shopService;

    /** 我的购物车列表 */
    @GetMapping("list")
    @ApiOperation("我的购物车列表")
    public Result<List<CartVo>> list(){
        String userId = getUser().getId();
        List<FwCart> cartList = cartService.list(Wrappers.<FwCart>lambdaQuery()
                .eq(FwCart::getUserId, userId)
                .orderByAsc(FwCart::getShopId));
        //添加一个空的，让循环多走一次
        cartList.add(new FwCart());
        List<CartVo> cartVoList = new ArrayList<>();
        String shopId = "";
        List<FwCart> carts = new ArrayList<>();
        CartVo cartVo = null;
        for (FwCart cart : cartList) {
            cart.setFwSku(skuService.getById(cart.getSkuId()));
            cart.setFwSpu(spuService.getById(cart.getSpuId()));

            //一个新的店铺
            if (!shopId.equals(cart.getShopId())){
                //不是第一个元素
                if (cartVo != null){
                    cartVo.setCartList(carts);
                    carts = new ArrayList<>();
                    cartVoList.add(cartVo);
                    if (cart.getShopId() == null) break;
                }
                cartVo = new CartVo();
                shopId = cart.getShopId();
                cartVo.setShop(shopService.getById(shopId));
            }
            carts.add(cart);
        }
        return success(cartVoList);
    }

    /** 添加到购物车 */
    @GetMapping("putCar/{skuId}/{num}")
    @ApiOperation("添加到购物车")
    public Result<Object> putCar(@PathVariable String skuId,@PathVariable Integer num){
        FwCart cart = cartService.getOne(Wrappers.<FwCart>lambdaQuery()
                .eq(FwCart::getUserId, getUser().getId())
                .eq(FwCart::getSkuId, skuId));
        if (cart != null){
            cart.setSpuNumber(cart.getSpuNumber()+num);
            cartService.updateById(cart);
            return success();
        }
        FwSku fwSku = skuService.getById(skuId);
        if (fwSku == null)  return new Result<>().fail(1,"错误的规格");
        FwSpu fwSpu = spuService.getById(fwSku.getSpuId());
        cart = new FwCart()
                .setUserId(getUser().getId())
                .setSpuId(fwSku.getSpuId())
                .setSkuId(skuId)
                .setShopId(fwSpu.getShopId())
                .setSpuTitle(fwSpu.getTitle())
                .setSkuMoneys(fwSku.getSkuMoney())
                .setSpuNumber(num)
                .setCreateTime(LocalDateTime.now())
                .setUpdateTime(LocalDateTime.now())
                .setId(idXD.nextId());
        cartService.save(cart);
        return success();
    }

    @PostMapping("removeCar")
    @ApiOperation("从购物车中删除")
    public Result<Object> removeCar(String skuIdS){
        List<String> skuIdList = Arrays.asList(skuIdS.split(","));
        cartService.remove(Wrappers.<FwCart>lambdaQuery().in(FwCart::getSkuId, skuIdList));
        return success();
    }

    /** 修改购物车 */
    @GetMapping("updateCar/{skuId}/{num}")
    @ApiOperation("修改购物车")
    public Result<Object> updateCar(@PathVariable String skuId,@PathVariable Integer num){
        cartService.update(Wrappers.<FwCart>lambdaUpdate()
                .eq(FwCart::getUserId, getUser().getId())
                .eq(FwCart::getSkuId, skuId)
                .set(FwCart::getSpuNumber, num));
        return success();
    }

}

