package com.fw.application.controller.v2.shop.keyword;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Assert;
import com.fw.annotation.Log;
import com.fw.application.controller.base.BaseController;
import com.fw.common.IdXD;
import com.fw.core.domain.AjaxResult;
import com.fw.core.domain.model.LoginUser;
import com.fw.core.page.TableDataInfo;
import com.fw.enums.BusinessType;
import com.fw.mes.Result;
import com.fw.system.web.model.dto.UserDto;
import com.fw.system.web.model.entity.FwShopKeyword;
import com.fw.system.web.model.form.v2.ShopKeyWordForm;
import com.fw.system.web.model.vo.ShopKeywordVo;
import com.fw.system.web.service.IFwShopKeywordService;
import com.fw.system.web.service.IFwShopService;
import com.fw.utils.ServletUtils;
import com.fw.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.fw.mes.ResultUtils.success;

/**
 * 商户端相关API
 */
@RestController
@RequestMapping("/v2/keyword")
@Api(tags = "商户端 线下商户 环境相关api_ 线下商户才可以调用")
public class V2ShopKeywordController implements BaseController {

    @Autowired
    private IFwShopKeywordService shopKeywordService;

    @Autowired
    private IFwShopService shopService;

    @Autowired
    private IdXD idXD;

    /**
     * 查询商铺环境展示列表
     */
    @PostMapping("/list")
    @ApiOperation("查询商铺环境展示列表")
    public Result<List<ShopKeywordVo>> list()
    {
        UserDto shopDto = getShopDto();
        return success(shopService.queryKeyWord(shopDto.getShopId()));
    }



    /**
     * 获取商铺环境展示详细信息
     */
    @GetMapping(value = "/{id}")
    @ApiOperation("获取商铺环境展示详细信息")
    public Result<ShopKeywordVo> getInfo(@PathVariable("id") String id)
    {
        FwShopKeyword byId = shopKeywordService.getById(id);
        ShopKeywordVo shopKeywordVo = new ShopKeywordVo();
        BeanUtil.copyProperties(byId,shopKeywordVo);
        return success(shopKeywordVo);
    }

    /**
     * 新增商铺环境展示
     */
    @PostMapping("/add")
    @ApiOperation("新增商铺环境展示")
    public Result add(@RequestBody @Validated ShopKeyWordForm shopKeyWordForm)
    {
        UserDto shopDto = getShopDto();
        Assert.isTrue(shopService.shopOff(shopDto.getShopId()),"您不是线下商户,不可编辑");
        FwShopKeyword shopKeyword = new FwShopKeyword();
        BeanUtil.copyProperties(shopKeyWordForm,shopKeyword);
        shopKeyword.setShopId(shopDto.getShopId());
        shopKeyword.createD(shopDto.getId());
        shopKeyword.setId(idXD.nextId());
        shopKeywordService.save(shopKeyword);
        return success();
    }

    /**
     * 修改商铺环境展示
     */
    @PostMapping("/edit")
    @ApiOperation("修改商铺环境展示")
    public Result edit(@RequestBody @Validated ShopKeyWordForm shopKeyWordForm)
    {
        UserDto shopDto = getShopDto();
        FwShopKeyword shopKeyword = shopKeywordService.getById(shopKeyWordForm.getId());
        BeanUtil.copyProperties(shopKeyWordForm,shopKeyword);
        shopKeyword.setShopId(shopDto.getShopId());
        shopKeyword.updateD(shopDto.getId());
        shopKeywordService.updateById(shopKeyword);
        return success();

    }

    /**
     * 删除商铺环境展示
     */
    @DeleteMapping("/del/{id}")
    @ApiOperation("删除该商户环境")
    public Result del(@PathVariable String id)
    {
        shopKeywordService.removeById(id);
        return success();

    }





}
