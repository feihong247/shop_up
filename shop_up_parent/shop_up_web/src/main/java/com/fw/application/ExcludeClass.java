package com.fw.application;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.TypeFilter;

import java.io.IOException;

/**
 * @Author:yanwei
 * @Date: 2020/11/13 - 15:35
 */
@Slf4j
public class ExcludeClass implements TypeFilter {
    @Override
    public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) throws IOException {
        ClassMetadata classMetadata = metadataReader.getClassMetadata();
        String className = classMetadata.getClassName();
        if (className.contains("com.fw.system.admin")){
          log.info("忽略了该类注入{}",className);
            return true;
        }
        return false;
    }

}
