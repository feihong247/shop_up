package com.fw.application.controller;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.mes.Result;
import com.fw.system.web.model.entity.FwBanner;
import com.fw.system.web.model.vo.BannerVo;
import com.fw.system.web.model.vo.UcjoinVo;
import com.fw.system.web.service.IFwBannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static com.fw.mes.ResultUtils.success;

/**
 * <p>
 * 首页轮播 前端控制器
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@RestController
@RequestMapping("/web/banner")
@RequiredArgsConstructor
@Api(tags = "轮播图")
public class FwBannerController {

    private final IFwBannerService bannerService;

    /**
     * shlo:查询首页轮播列表
     *
     * @return
     */
    @GetMapping("/findBannerList")
    @ApiOperation("shlo:查询首页轮播列表")
    public Result<List<BannerVo>> findBannerList() {
        List<FwBanner> list = bannerService.list();
        ArrayList<BannerVo> bannerVos = new ArrayList<>();
        for (FwBanner fwBanner : list) {
            BannerVo bannerVo = new BannerVo();
            BeanUtil.copyProperties(fwBanner, bannerVo);
            bannerVos.add(bannerVo);
        }
        return success(bannerVos);
    }

    /**
     * shlo:根据id查询首页轮播详情
     *
     * @return
     */
    @GetMapping("/findBannerById/{id}")
    @ApiOperation("shlo:根据id查询首页轮播详情")
    public Result<BannerVo> findBannerById(@PathVariable String id) {
        FwBanner banner = bannerService.getOne(Wrappers.<FwBanner>lambdaQuery().eq(FwBanner::getId, id));
        BannerVo bannerVo = new BannerVo();
        BeanUtil.copyProperties(banner, bannerVo);
        return success(bannerVo);
    }
}

