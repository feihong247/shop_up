package com.fw.application.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户购买代理日志 前端控制器
 * </p>
 *
 * @author
 * @since 2021-06-07
 */
@RestController
@RequestMapping("/log-shoping-agency")
public class FwLogShopingAgencyController {

}

