package com.fw.application.controller;


import cn.hutool.core.thread.ThreadUtil;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.application.controller.base.BaseController;
import com.fw.constant.Constant;
import com.fw.mes.Result;
import com.fw.mes.ResultUtils;
import com.fw.system.web.model.entity.*;
import com.fw.system.web.service.*;
import com.sun.net.httpserver.Authenticator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 商甲池表 前端控制器
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@RestController
@RequestMapping("/shangjipool")
@Api(tags = "商甲池交易")
@RequiredArgsConstructor
public class FwShangjipoolController implements BaseController {
   // private static HashMap<String, Object> boardMap = new HashMap<String, Object>();
    private final IFwMoneysService fwMoneysService;
    private final IFwShangjiaService fwShangjiaService;
    private final IFwWithdrawShangjiaService fwWithdrawShangjiaService;
    private final IFwWithdrawService fwWithdrawService;
    private final IFwLockStorageService fwLockStorageService;
    private final IFwUserService fwUserService;
    private final IFwUljoinService fwUljoinService;
    private final IFwLogsService fwLogsService;
    private final IFwShangjipoolService fwShangjipoolService;
    @GetMapping("/getBoardDetail")
    @ApiOperation("shlo:商甲交易:统计看板展示")
    public Result getBoardDetail(){

            HashMap<String, Object> boardMap = new HashMap<String, Object>();
            List<FwShangjia> listShangJia = fwShangjiaService.list();
            boardMap.put("shangJia",listShangJia);
            List<FwShangjia> consumList = listShangJia.stream().filter(o -> o.getId().equals(Constant.ShangJia.CONSUME_ID)).collect(Collectors.toList());
            FwShangjia consum = consumList.get(0);
            boardMap.put("presentPrice",consum.getPresentPrice());
            return ResultUtils.success(boardMap);

    }


}

