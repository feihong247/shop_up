package com.fw.application.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 技术商甲管理商甲推广商甲释放明细表 前端控制器
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
@RestController
@RequestMapping("/fw.system.web/fw-shangjia-release-log")
public class FwShangjiaReleaseLogController {

}

