package com.fw.application.controller.v2.shop;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.application.controller.base.BaseController;
import com.fw.constant.Constant;
import com.fw.mes.Result;
import com.fw.system.web.model.dto.UserDto;
import com.fw.system.web.model.entity.FwPol;
import com.fw.system.web.model.entity.FwShop;
import com.fw.system.web.model.form.v2.ShopLoginForm;
import com.fw.system.web.model.vo.ShopVo;
import com.fw.system.web.model.vo.v2.ShopIsOffVo;
import com.fw.system.web.service.IFwShopService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;

import static com.fw.mes.ResultUtils.success;

/**
 * 商户端相关API
 */
@RestController
@RequestMapping("/v2/shop")
@Api(tags = "商户端相关api")
public class V2ShopController implements BaseController {

    @Autowired
    private IFwShopService shopService;

    @ApiOperation("商户统计_先废弃")
    @GetMapping("/shopOrder")
    public Result shopOrder(){
        // 获取到商铺用户
        UserDto shopDto = getShopDto();
        return success();
    }

    @ApiOperation("商户登录接口,返回 token 令牌")
    @PostMapping("/shopLogin")
    public Result shopLogin(@Validated @RequestBody ShopLoginForm shopLoginForm){
        return success(shopService.shopLogin(shopLoginForm));
    }

    @ApiOperation("查询商户详细信息")
    @GetMapping("/shopThisInfo")
    public Result<ShopVo> shopThisInfo(){
        UserDto shopDto = getShopDto();
        return success(shopService.shopDistance(shopDto.getShopId(),null,null,null));
    }

    @PostMapping("edit")
    @ApiOperation("修改商铺信息")
    public Result edit(@RequestBody ShopVo shopVo){
        UserDto shopDto = getShopDto();
        Assert.isTrue(StringUtils.equals(shopVo.getId(),shopDto.getShopId()),"商铺修改错误,只能自己修改自己!");
        shopVo.setOperatingName(shopVo.getOId());
        return success(shopService.edit(shopVo));
    }

    @GetMapping("/isOff")
    @ApiOperation("该商户是线上,还是线下")
    public Result<ShopIsOffVo> isOff(){
        return success(shopService.isOff(getShopDto().getShopId()));
    }






}
