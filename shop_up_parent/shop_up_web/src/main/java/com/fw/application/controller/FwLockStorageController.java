package com.fw.application.controller;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.application.controller.base.BaseController;
import com.fw.mes.Result;
import com.fw.mes.ResultUtils;
import com.fw.system.web.model.entity.FwLockStorage;
import com.fw.system.web.model.vo.LockStorageVo;
import com.fw.system.web.service.IFwLockStorageService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 锁仓池 前端控制器
 * </p>
 *
 * @author
 * @since 2021-06-28
 */
@RestController
@RequestMapping("/lock-storage")
@RequiredArgsConstructor
@Api(tags = "商甲锁仓相关")
public class FwLockStorageController implements BaseController {
    private final IFwLockStorageService fwLockStorageService;
    @ApiOperation("shlo:获取锁仓明细")
    @GetMapping("/getLockDetail/{pageNum}/{pageSize}")
    public Result getLockDetail(@PathVariable Integer pageNum, @PathVariable Integer pageSize){
        //对象里传什么条件查什么，如果需要防止信息越权查询，可加相关条件判断
        String id = getUser().getId();
        PageHelper.startPage(pageNum, pageSize);
        List<LockStorageVo> collect = fwLockStorageService.list(Wrappers.<FwLockStorage>lambdaQuery().eq(FwLockStorage::getUserId, id)).parallelStream()
                .map(item -> {
                    LockStorageVo vo = new LockStorageVo();
                    BeanUtil.copyProperties(item, vo);
                    Duration between = Duration.between(LocalDateTime.now(), item.getLockTime().plusDays(365));
                    long toDays = between.toDays();
                    if (toDays < 0) {
                        vo.setRemDay(0L);
                    } else {
                        vo.setRemDay(toDays);
                    }
                    return vo;
                }).collect(Collectors.toList());
        collect.stream().forEach(o->o.setPhone("*******"+getUser().getPhone().substring(7)));
        PageInfo<LockStorageVo> spuPageInfo = new PageInfo<>(collect);
       return ResultUtils.success(spuPageInfo);
    }

}

