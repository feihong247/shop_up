package com.fw.application.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.NumberUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.annotation.PrintParam;
import com.fw.application.controller.base.BaseController;
import com.fw.constant.Constant;
import com.fw.mes.Result;
import com.fw.system.web.model.entity.*;
import com.fw.system.web.model.form.ShopQuery;
import com.fw.system.web.model.form.ShopQuickQuery;
import com.fw.system.web.model.form.ShopRegisteredForm;
import com.fw.system.web.model.vo.ShopKeywordVo;
import com.fw.system.web.model.vo.ShopOnLineVo;
import com.fw.system.web.model.vo.ShopVo;
import com.fw.system.web.service.IFwAddrsService;
import com.fw.system.web.service.IFwPolService;
import com.fw.system.web.service.IFwRuleDatasService;
import com.fw.system.web.service.IFwShopService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

import static com.fw.mes.ResultUtils.success;

/**
 * <p>
 * 商铺表 前端控制器
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@RestController

@RequestMapping("/shop")
@Api(tags = "商户相关api")
public class FwShopController implements BaseController {

    @Autowired
    private IFwShopService shopService;

    @Autowired
    private IFwRuleDatasService ruleDatasService;
    @Autowired
    private IFwPolService polService;
    @Autowired
    private IFwAddrsService addrsService;


    /**
     * 商户申请
     */
    @PostMapping("/shopMerchant")
    @ApiOperation("商户申请")
    public Result shopMerchant(@RequestBody @Validated ShopRegisteredForm registeredForm) {
        FwUser user = getUser();
        if (null == registeredForm.getOfflineShangjia())
            registeredForm.setOfflineShangjia(new BigDecimal(0.00));
        if (null == registeredForm.getShangjia())
            registeredForm.setShangjia(new BigDecimal(0.00));
        shopService.shopMerchant(user, registeredForm);
        return success();
    }

    /**
     * 商户信息查询
     * 经纬度, + 类目 等条件
     */
    @PostMapping("/shopOnlineQuery")
    @ApiOperation("根据 范围大小 ，经营类目，商铺名称查询")
    @PrintParam
    public Result<List<ShopOnLineVo>> shopOnlineQuery(@Validated @RequestBody ShopQuery shopQuery) {
        // 商家个性化筛选用户条件
        FwUser user = getDefaultUser();
        return success(shopService.shopOnlineQuery(shopQuery, user));
    }

    /**
     * 点击商户 算出 距离 以及 步行多久才能到
     */
    @PostMapping("/shopDistance")
    @ApiOperation("点击商户 算出 距离 以及 步行多久才能到")
    public Result<ShopVo> shopDistance(@Validated @RequestBody ShopQuickQuery shopQuickQuery) {
        return success(shopService.shopDistance(shopQuickQuery.getShopId(), shopQuickQuery.getLatitude(), shopQuickQuery.getLongitude(), null));
    }

    /**
     * 查询商户所有信息
     */
    @GetMapping("queryId/{id}")
    @ApiOperation("查询商户所有信息")
    public Result<ShopVo> queryId(@PathVariable String id) {
        FwUser fwUser = getDefaultUser();
        return success(shopService.shopDistance(id, null, null, fwUser));
    }

    /**
     * 查询商户的的经营环境文案
     */
    @GetMapping("queryKeyWord/{shopId}")
    @ApiOperation("查询商户的的经营环境文案,传入商户编号")
    public Result<List<ShopKeywordVo>> queryKeyWord(@PathVariable String shopId) {

        return success(shopService.queryKeyWord(shopId));
    }

    /**
     * 商户质押 提示
     * 例如 当前商甲质押需要 xxx 个商甲起步.
     */
    @GetMapping("/prompt")
    @ApiOperation("商甲质押提示 当前商甲质押需要 xxx 个商甲起步。 ")
    public Result<String> prompt() {
        FwRuleDatas ruleDatas = ruleDatasService.getById(Constant.IsRuleData.SHOP_REGIS);
        return success(ruleDatas.getRuleCount().toString());
    }

    /**
     * 当前登陆用户是否是商铺？
     */
    @GetMapping("/useIsShop")
    @ApiOperation("查询当前登陆用户是否是商铺, data == null 不是，data != null 是商铺,如果审核失败，展示驳回原因，并且重新提交审核")
    public Result<ShopVo> useIsShop() {
        FwUser user = getUser();
        ShopVo shopVo = shopService.selectByUserId(user.getId());
        return success(shopVo);
    }

    @PostMapping("edit")
    @ApiOperation("修改商铺信息")
    @Transactional(rollbackFor = Exception.class)
    public Result<Object> edit(@RequestBody ShopVo shopVo){
        //更新经纬度，地址
        polService.update(Wrappers.<FwPol>lambdaUpdate()
                .set(FwPol::getArea, shopVo.getShopAddVo().getArea())
                .set(FwPol::getCity, shopVo.getShopAddVo().getCity())
                .set(FwPol::getProvince, shopVo.getShopAddVo().getProvince())
                .set(FwPol::getAddrInfo, shopVo.getShopAddVo().getAddrInfo())
                .set(FwPol::getLongitude, shopVo.getShopAddVo().getLongitude())
                .set(FwPol::getLatitude, shopVo.getShopAddVo().getLatitude())
                .eq(FwPol::getBusId, shopVo.getId()));


        FwShop shop = new FwShop();
        BeanUtil.copyProperties(shopVo, shop);
        FwShop oldShop = shopService.getById(shopVo.getId());
        // 商甲质押 - 线上
        shopService.shopShangjiaCustomer(oldShop,shop,Constant.ShopRuleData.SHOP_SHANGJIA_ONLINE);
        // 商甲质押 - 线下
        shopService.shopShangjiaCustomer(oldShop,shop,Constant.ShopRuleData.SHOP_SHANGJIA_OFFLINE);
        // 如果是线下 必须让利 超过百分之20 以及
        if (shop.getOfflineShangjia().compareTo(NumberUtil.add(0D)) > 0){
            // 是一个线下商户
           // Assert.isTrue(shop.getLineCustomer().compareTo(NumberUtil.add(20D)) >= 0,"线下商户必须让利 百分之20及以上！");
        }
        shopService.updateById(shop);
        return success();
    }
}

