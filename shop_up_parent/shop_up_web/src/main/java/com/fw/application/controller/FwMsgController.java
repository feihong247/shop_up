package com.fw.application.controller;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.application.controller.base.BaseController;
import com.fw.mes.Result;
import com.fw.system.web.model.entity.FwMsg;
import com.fw.system.web.model.entity.FwUser;
import com.fw.system.web.model.form.MsgQuery;
import com.fw.system.web.model.form.NoticeQuery;
import com.fw.system.web.model.vo.MsgVo;
import com.fw.system.web.service.IFwMsgService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static com.fw.mes.ResultUtils.success;

/**
 * <p>
 * 用户消息 前端控制器
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@RestController
@RequestMapping("/msg")
@Api(tags = "用户消息")
@RequiredArgsConstructor
public class FwMsgController implements BaseController {
    private final IFwMsgService msgService;

    /**
     * shlo
     * @return
     */
    @ApiOperation("shlo:查询用户消息列表")
    @PostMapping("/listMsg")
    public Result<PageInfo<FwMsg>> listMsg(@RequestBody MsgQuery msgQuery){
        FwUser user = getUser();
        PageHelper.startPage(msgQuery.getPageNum(),msgQuery.getPageSize());
        List<FwMsg> msgList = msgService.list(Wrappers.<FwMsg>lambdaQuery().eq(FwMsg::getUserId, user.getId()).orderByDesc(FwMsg::getCreateTime));
        PageInfo<FwMsg> fwMsgPageInfo = new PageInfo<>(msgList);
        return success(fwMsgPageInfo);
    }

    /**
     * shlo
     * @param id
     * @return
     */
    @ApiOperation("shlo:根据id查询用户消息")
    @GetMapping("/findMsgById/{id}")
    public Result<MsgVo> findMsgById(@PathVariable String id) {
        FwUser user = getUser();
        FwMsg fwMsg = msgService.getOne(Wrappers.<FwMsg>lambdaQuery().eq(FwMsg::getId, id).eq(FwMsg::getUserId,user.getId()));
        MsgVo msgVo = new MsgVo();
        BeanUtil.copyProperties(fwMsg, msgVo);
        return success(msgVo);
    }

}

