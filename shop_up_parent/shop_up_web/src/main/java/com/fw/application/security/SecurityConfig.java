package com.fw.application.security;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ArrayUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.application.security.comm.BaseUserDetails;
import com.fw.application.security.comm.JwtAuthenticationTokenFilter;
import com.fw.application.security.comm.RestAuthenticationEntryPoint;
import com.fw.application.security.comm.RestfulAccessDeniedHandler;
import com.fw.system.web.model.dto.UserDto;
import com.fw.system.web.model.entity.FwShop;
import com.fw.system.web.model.entity.FwUser;
import com.fw.system.web.service.IFwShopService;
import com.fw.system.web.service.IFwUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.CorsFilter;

import java.util.List;
import java.util.Objects;

/**
 * @Author:yanwei
 * @Date: 2020/10/14 - 13:37
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private RestfulAccessDeniedHandler restfulAccessDeniedHandler;

    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    /**
     * 跨域过滤器
     */
    @Autowired
    private CorsFilter corsFilter;

    @Autowired
    private IFwUserService userService;

    @Autowired
    private IFwShopService shopService;

    /**
     * 配置权限
     *
     * @param httpSecurity
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.csrf()// 由于使用的是JWT，我们这里不需要csrf
                .disable()
                .sessionManagement()// 基于token，所以不需要session
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                 // 允许对于网站静态资源的无授权访问
                .antMatchers(permitAll())// 对登录注册要允许匿名访问
                .permitAll()
                .antMatchers(HttpMethod.OPTIONS)//跨域请求会先进行一次options请求
                .permitAll()
                /*.antMatchers("/**")//测试时全部运行访问
                .permitAll()*/
                .anyRequest()// 除上面外的所有请求全部需要鉴权认证
                .authenticated();
        // 禁用缓存
        httpSecurity.headers().cacheControl();
        // 添加JWT filter
        httpSecurity.addFilterBefore(jwtAuthenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class);
        httpSecurity.addFilterBefore(corsFilter, JwtAuthenticationTokenFilter.class);
        //添加自定义未授权和未登录结果送回
        httpSecurity.exceptionHandling()
                .accessDeniedHandler(restfulAccessDeniedHandler)
                .authenticationEntryPoint(restAuthenticationEntryPoint);
    }




    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        //获取登录用户信息
        return userId -> {
            // 应该从数据库查出用户信息并附能
            FwUser user = null;
            // 如果是个 商铺号 , 也兼容
            FwShop shop = shopService.getOne(Wrappers.<FwShop>lambdaQuery().eq(FwShop::getUserId, userId));
            if ((user = userService.getById(userId)) != null){
                // 得到所有权限列表
                if (Objects.nonNull(shop)){
                    UserDto userDto = new UserDto();
                    BeanUtil.copyProperties(user,userDto);
                    userDto.setShopId(shop.getId());
                    return new BaseUserDetails(user,userDto);
                }
                return new BaseUserDetails(user);
            }


            throw new UsernameNotFoundException("用户名或密码错误");
        };
    }

    /**
     * 密码编码
     *
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    /**
     * token 解析过滤器
     *
     * @return
     */
    @Bean
    public JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter() {
        return new JwtAuthenticationTokenFilter();
    }


    /**
     * 认证管理容器
     *
     * @return
     * @throws Exception
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }



    /**
     * v1 接口 权限
     */
    private static String[] v1PermitOpen(){
        return new String[]{
                "/example/list",
                "/web/core/uploadImage",
                "/web/core/sendPhone/*",
                "/web/aliPay/**",
                "/spu/**/**",
                "/spu/**/**/**",
                "/xrebel/**",
                "/shop/shopOnlineQuery",
                "/shop/queryId/**",
                "/web/banner/**",
                "/spu/**",
                "/web/open/test",
                "/web/open/addXz",
                "/web/open/subNum",
                "/web/open/bugMethod",
                "/web/open/returnShangJiaById",
                "/web/open/releaseDisappearTasks",
                "/spucate/**",
                "web/evaluation/**",
                "user/pushSign",
                "/deal/bonus/**",//暴露给admin
                "/guess/guessCount",//竞猜统计
                "/guess/**",//竞猜详情，分享用
                "/user/Login/**/*",//放开登录
                "/web/contact/**/**",//关于我们
                "/user/loginPhone/**",
                "/user/userRegister",
                "/user/sendPhone/*",
                "/user/imeiNull/**",
                "/user/isCheckCode/*",
                "/user/**",
                "/**/xxuser/**",
                "/**/version/**",
                "/**/deleteDl/**",
                "/notice/**",
                "/district/**",
                "/test/**",
                "/example/getToken/*",
                "/",
                "/*.html",
                "/favicon.ico",
                "/**/*.html",
                "/**/*.css",
                "/**/*.js",
                "/swagger-resources/**",
                "/v2/api-docs/**"
        };
    }

    private static String[] v2PermitOpen() {
        return new String[]{
                "/v2/shop/shopLogin",
                "/read/package/checkReadPackage"
        };

    }
    public static String[] permitAll(){
        return ArrayUtil.addAll(v1PermitOpen(), v2PermitOpen());
    }
}
