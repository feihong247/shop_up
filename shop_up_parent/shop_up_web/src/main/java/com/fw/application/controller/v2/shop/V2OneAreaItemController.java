package com.fw.application.controller.v2.shop;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.application.controller.base.BaseController;
import com.fw.mes.Result;
import com.fw.system.web.model.entity.FwOneAreaItem;
import com.fw.system.web.model.entity.FwSpu;
import com.fw.system.web.service.IFwSpuService;
import com.fw.system.web.service.v2.IFwOneAreaItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.fw.mes.ResultUtils.success;

/**
 * <p>
 * 一县一品表 前端控制器
 * </p>
 *
 * @author  
 * @since 2021-09-29
 */
@RestController
@RequestMapping("/one/area")
@Api(tags = "一县一品")
@CrossOrigin
public class V2OneAreaItemController implements BaseController {


    @Autowired
    private IFwOneAreaItemService oneAreaItemService;
    @Autowired
    private IFwSpuService spuService;

    @GetMapping("/oneAreaList")
    @ApiOperation("根据省市区/县码查询一县一品列表 area的code")
    public Result  < List<FwOneAreaItem>> oneAreaList(@RequestParam String area){

        List<FwOneAreaItem> list = oneAreaItemService.list(Wrappers.<FwOneAreaItem>lambdaQuery().eq(FwOneAreaItem::getArea, area)
        .eq(FwOneAreaItem::getIsState,1));
        if (list.size()<=0){
            return new Result<List<FwOneAreaItem>>().fail(0,"好像比对不上,请手动选择地域!");
        }
        return success(list);
    }

    @GetMapping("/oneAreaItem")
    @ApiOperation("查询一线一品包含的商品 area的code")
    public Result <FwSpu>oneAreaItem(@RequestParam String area){

        FwSpu fwSpu = spuService.getOne(Wrappers.<FwSpu>lambdaQuery().eq(FwSpu::getArea, area));
        return success(fwSpu);
    }

}

