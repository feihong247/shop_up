package com.fw.application.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.NumberUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.annotation.Log;
import com.fw.application.controller.base.BaseController;
import com.fw.core.domain.AjaxResult;
import com.fw.enums.BusinessType;
import com.fw.mes.Result;
import com.fw.system.web.model.entity.FwEvaluation;
import com.fw.system.web.model.entity.FwOrder;
import com.fw.system.web.model.entity.FwSpu;
import com.fw.system.web.model.entity.FwUser;
import com.fw.system.web.model.form.ItemQuery;
import com.fw.system.web.model.vo.EvaluationVo;
import com.fw.system.web.service.IFwEvaluationService;
import com.fw.system.web.service.IFwOrderService;
import com.fw.system.web.service.IFwShopService;
import com.fw.utils.RandomUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static com.fw.mes.ResultUtils.success;

/**
 * <p>
 * 商品评价 前端控制器
 * </p>
 *
 * @author
 * @since 2021-05-24
 */
@RestController
@RequestMapping("/web/evaluation")
@RequiredArgsConstructor
@Api(tags = "商品评价")
public class FwEvaluationController implements BaseController {
    private final IFwEvaluationService evaluationService;
    private final IFwOrderService orderService;
    private final IFwShopService shopService;

    /**
     * shlo:查询商品评价列表
     *
     * @param id
     * @return
     */
    @GetMapping("/findEvaluationList/{id}/{pageNum}/{pageSize}")
    @ApiOperation("查询商品评价列表(传商品id)")
    public Result<PageInfo<EvaluationVo>> findEvaluationList(@PathVariable String id,
                                                             @PathVariable Integer pageNum,
                                                             @PathVariable Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<EvaluationVo> evaluationVos = evaluationService.selectFwEvaluationList(new FwEvaluation().setItemId(id));
        PageInfo<EvaluationVo> spuPageInfo = new PageInfo<>(evaluationVos);
        return success(spuPageInfo);
    }


    /**
     * shlo:根据id查询商品评价
     *
     * @param id
     * @return
     */
    @GetMapping("/findEvaluationList/{id}")
    @ApiOperation("查询商品评价列表(传商品id)")
    public Result<EvaluationVo> findEvaluationList(@PathVariable String id) {
        return success(evaluationService.selectFwEvaluationById(id));
    }


    /**
     * shlo:新增商品评价
     *
     * @param fwEvaluation
     * @return
     */
    @PostMapping("/addEvaluation")
    @ApiOperation("新增商品评价")
    public Result addEvaluation(@RequestBody FwEvaluation fwEvaluation) {
        FwUser user = getUser();
        fwEvaluation.setUserId(user.getId());
        //订单修改为已评价
        orderService.update(Wrappers.<FwOrder>lambdaUpdate()
                .set(FwOrder::getIsAppraise, 1)
                .eq(FwOrder::getId, fwEvaluation.getOrderId()));
        // 增加评价星级,给对应商铺打分
        shopService.svgStore(fwEvaluation.getItemId(),fwEvaluation.getStarNum());
        return success(evaluationService.insertFwEvaluation(fwEvaluation));
    }






    /**
     * shlo:删除商品评价
     *
     * @param id
     * @return
     */
    @DeleteMapping("/delEvaluation/{id}")
    @ApiOperation("删除商品评价(传商品评价id)")
    public Result delEvaluation(@PathVariable String id) {
        return success(evaluationService.deleteFwEvaluationById(id));
    }

    /**
     * shlo:批量删除商品评价
     *
     * @param ids
     * @return
     */
    @ApiOperation("批量删除商品评价(传商品评价id数组)")
    @DeleteMapping("/removeEvaluation/{ids}")
    public Result removeEvaluation(@PathVariable String[] ids) {
        return success(evaluationService.deleteFwEvaluationByIds(ids));
    }

}

