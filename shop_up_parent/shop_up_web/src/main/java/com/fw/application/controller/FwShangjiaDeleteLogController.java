package com.fw.application.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商甲销毁日志表 前端控制器
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
@RestController
@RequestMapping("/fw.system.web/fw-shangjia-delete-log")
public class FwShangjiaDeleteLogController {

}

