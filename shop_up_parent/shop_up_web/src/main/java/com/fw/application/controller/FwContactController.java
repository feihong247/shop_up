package com.fw.application.controller;


import cn.hutool.core.bean.BeanUtil;
import com.alibaba.druid.sql.visitor.functions.Concat;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.mes.Result;
import com.fw.system.web.model.entity.FwBanner;
import com.fw.system.web.model.entity.FwContact;
import com.fw.system.web.model.vo.BannerVo;
import com.fw.system.web.model.vo.ContactVo;
import com.fw.system.web.service.IFwContactService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static com.fw.mes.ResultUtils.success;

/**
 * <p>
 * 关于我们 前端控制器
 * </p>
 *
 * @author
 * @since 2021-05-24
 */
@RestController
@RequestMapping("/web/contact")
@RequiredArgsConstructor
@Api(tags = "系统设置相关")
public class FwContactController {
    private final IFwContactService contactService;

    /**
     * shlo:查询关于我们列表
     *
     * @return
     */
    @GetMapping("/findContactList")
    @ApiOperation("shlo:查询关于我们")
    public Result<ContactVo> findContactList() {
        List<FwContact> list = contactService.list();
        FwContact fwContact = list.get(0);
        ContactVo contactVo = new ContactVo();
        BeanUtil.copyProperties(fwContact, contactVo);
        return success(contactVo);
    }

    /**
     * shlo:根据id查询关于我们
     *
     * @param id
     * @return
     */
   /* @GetMapping("/findContactById/{id}")
    @ApiOperation("根据id查询关于我们")
    public Result<ContactVo> findContactById(@PathVariable String id) {
        FwContact contact = contactService.selectFwContactById(id);
        ContactVo contactVo = new ContactVo();
        BeanUtil.copyProperties(contact, contactVo);
        return success(contactVo);
    }*/

    @GetMapping("getContactByFlag/{flag}")
    @ApiOperation("根据标识获取文案.1：竞猜规则， 2，用户协议，3：商户协议，4代理电子合同")
    public Result<FwContact> getContactByFlag(@PathVariable int flag){
        FwContact contact = null;
        switch (flag) {
            case 1 :
                contact = contactService.getOne(Wrappers.<FwContact>lambdaQuery().eq(FwContact::getTitle, "竞猜规则"));
                break;
            case 2 :
                contact = contactService.getOne(Wrappers.<FwContact>lambdaQuery().eq(FwContact::getTitle, "用户协议"));
                break;
            case 3 :
                contact = contactService.getOne(Wrappers.<FwContact>lambdaQuery().eq(FwContact::getTitle, "商户协议"));
                break;
            case 4 :
                contact = contactService.getOne(Wrappers.<FwContact>lambdaQuery().eq(FwContact::getTitle, "代理电子合同"));
                break;
            default:
                break;
        }
        return success(contact);
    }

    @GetMapping("getContactByTitle/{title}")
    @ApiOperation("根据标题获取文案.竞猜规则，用户协议")
    public Result<FwContact> getContactByTitle(@PathVariable String title){
        FwContact contact = contactService.getOne(Wrappers.<FwContact>lambdaQuery().eq(FwContact::getTitle, title));
        return success(contact);
    }

    @GetMapping("/redPackageRule")
    @ApiOperation("查询红包规则")
    public Result<FwContact> redPackageRule(){
        FwContact contact = contactService.getById("123");
        return success(contact);
    }

}
