package com.fw.application.controller.v2.shop.mes;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.application.controller.base.BaseController;
import com.fw.mes.Result;
import com.fw.system.web.model.entity.FwMsg;
import com.fw.system.web.model.form.PageQuery;
import com.fw.system.web.service.IFwMsgService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.fw.mes.ResultUtils.success;

@RestController
@RequestMapping("/v2/mes")
@Api(tags = "商户端 站内信 相关api")
public class V2MesController implements BaseController {

    @Autowired
    private IFwMsgService msgService;

    @GetMapping("/isRead")
    @ApiOperation("未读得一共多少条")
    public Result<Integer> isRead(){
        return success(msgService.count(Wrappers.<FwMsg>lambdaQuery().eq(FwMsg::getUserId,getShopDto().getShopId()).orderByDesc(FwMsg::getCreateTime)));
    }

    @PostMapping("/pageList")
    @ApiOperation("分页查询消息")
    public Result<PageInfo<FwMsg>> pageList(@RequestBody PageQuery pageQuery){
        PageHelper.startPage(pageQuery.getPageNum(),pageQuery.getPageSize());
        List<FwMsg> fwMsgs = msgService.list(Wrappers.<FwMsg>lambdaQuery().eq(FwMsg::getUserId, getShopDto().getShopId()).orderByDesc(FwMsg::getCreateTime));
        PageInfo<FwMsg> pageInfo = new PageInfo(fwMsgs);
        return success(pageInfo);
    }

    @GetMapping("/get/{id}")
    @ApiOperation("查询详情，未 读变已读")
    public Result<FwMsg> getId(@PathVariable String id){
        FwMsg fwMsg = msgService.getById(id);
        fwMsg.setIsRead(Integer.valueOf(1));
        fwMsg.updateById();
        return success(fwMsg);
    }
}
