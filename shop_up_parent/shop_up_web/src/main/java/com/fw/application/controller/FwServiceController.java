package com.fw.application.controller;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.mes.Result;
import com.fw.system.web.model.entity.FwService;
import com.fw.system.web.model.vo.ServiceVo;
import com.fw.system.web.service.IFwServiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import static com.fw.mes.ResultUtils.success;

/**
 * <p>
 * 在线客服 前端控制器
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@RestController
@RequestMapping("/service")
@Api(tags = "联系客服")
@RequiredArgsConstructor
public class FwServiceController {
    private final IFwServiceService serviceService;

    /**
     * shlo:
     * 联系客服
     */
    @GetMapping("/getService")
    @ApiOperation("shlo:查询联系客服")
    public Result<ServiceVo> getService(){
        FwService service = serviceService.getOne(Wrappers.lambdaQuery());
        ServiceVo serviceVo = new ServiceVo();
        BeanUtil.copyProperties(service,serviceVo);
        return success(serviceVo);
    }
}

