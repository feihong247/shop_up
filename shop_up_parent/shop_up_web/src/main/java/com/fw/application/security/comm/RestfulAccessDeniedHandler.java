package com.fw.application.security.comm;

import cn.hutool.json.JSONUtil;
import com.fw.enums.CodeStatus;
import com.fw.mes.Result;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 当访问接口没有权限时，自定义的送回结果
 *
 * @Author:yanwei
 * @Date: 2020/10/14 - 17:12
 */
@Component
public class RestfulAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().println(JSONUtil.parse(new Result<>(CodeStatus.REALNAME_INCORRECT)));
        response.getWriter().flush();
    }
}
