package com.fw.application.controller.base;

import com.fw.application.security.comm.BaseUserDetails;
import com.fw.system.web.model.dto.UserDto;
import com.fw.system.web.model.entity.FwUser;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Objects;

public interface BaseController {


    /**
     * 强制 获取用户，不可能获取是null
     * @return
     */
    default FwUser getUser(){
       return ((BaseUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
    }

    /**
     * 商户端才可以调用
     */
    default UserDto getShopDto(){
        return ((BaseUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getShopUser();

    }


    /**
     * 测试用户
     */
    default FwUser getTestUser(){
        FwUser fwUser = new FwUser();
        fwUser.setId("1");
        return fwUser;
    }

    /**
     * 获取用户，获取不到为 null
     * @return
     */
    default FwUser getDefaultUser(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return (Objects.isNull(principal) || principal instanceof  String  ) ? null :  ((BaseUserDetails) principal).getDefaultUser();

    }

}
