package com.fw.application.security.comm;

import com.fw.exception.user.UserPasswordNotMatchException;
import com.fw.system.web.model.dto.UserDto;
import com.fw.system.web.model.entity.FwUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 *
 * 用户继承的属性
 * @Author:yanwei
 * @Date: 2020/10/14 - 17:05
 */


public class BaseUserDetails implements UserDetails {

    private FwUser user;
    private UserDto userDto;
    public BaseUserDetails(FwUser user){

        this.user = user;
    }

    public BaseUserDetails(FwUser user,UserDto userDto){
        this.user = user;
        this.userDto = userDto;
    }


    public FwUser getUser() {
        if (user == null) throw new RuntimeException("无效用户!");
        return user;
    }
    public UserDto getShopUser() {
        if (userDto == null) throw new RuntimeException("无效用户!");
        return userDto;
    }

    public FwUser getDefaultUser(){
        return user;
    }

    /**
     * 送回用户角色&权限
     * @return
     *
     * 角色必須加 ROLE_ 前綴
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays.asList("sss").stream()
                .filter(permission -> permission!=null)
                .map(permission ->new SimpleGrantedAuthority(permission))
                .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return "$2a$10$0rfXlmjl68YWKS/bghBTEOPytuA0kH8vhkc2b2DaH1Mo9iV5mRQbi";
    }

    @Override
    public String getUsername() {
        return "yanwei";
    }

    @Override
    public boolean isAccountNonExpired() {
        return Boolean.TRUE;
    }

    @Override
    public boolean isAccountNonLocked() {
        return Boolean.TRUE;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return Boolean.TRUE;
    }

    /**
     * 是否禁用
     * @return
     */
    @Override
    public boolean isEnabled() {
        return false;
    }
}
