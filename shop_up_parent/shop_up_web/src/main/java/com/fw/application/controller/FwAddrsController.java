package com.fw.application.controller;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.application.controller.base.BaseController;
import com.fw.constant.Constant;
import com.fw.mes.Result;
import com.fw.system.comm.mode.entity.District;
import com.fw.system.comm.service.DistrictService;
import com.fw.system.web.model.entity.FwAddrs;
import com.fw.system.web.model.entity.FwUser;
import com.fw.system.web.model.vo.AddrsVo;
import com.fw.system.web.service.IFwAddrsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static com.fw.mes.ResultUtils.success;

/**
 * <p>
 * 用户收货地址表 前端控制器
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@RestController
@RequestMapping("/addrs")
@Api(tags = "用户收货地址")
@RequiredArgsConstructor
public class FwAddrsController implements BaseController {

    private final IFwAddrsService addrsService;
    private final DistrictService districtService;

    /**
     * shlo:查询用户收货地址列表
     * @return
     */
    @GetMapping("/findAddrList")
    @ApiOperation("shlo:查询用户收货地址列表")
    public Result<List<AddrsVo>> findAddrList() {
        List<FwAddrs> addrs = addrsService.list(Wrappers.<FwAddrs>lambdaQuery().eq(FwAddrs::getUserId, getUser().getId()));
        ArrayList<AddrsVo> addrsVos = new ArrayList<>();
        for (FwAddrs addr : addrs) {
            AddrsVo addrsVo = new AddrsVo();
            BeanUtil.copyProperties(addr,addrsVo);
            addrsVo.setAreaName(districtService.codeConversionName(addr.getArea()));
            addrsVo.setCityName(districtService.codeConversionName(addr.getCity()));
            addrsVo.setProvinceName(districtService.codeConversionName(addr.getProvince()));
            addrsVos.add(addrsVo);
        }
        return success(addrsVos);
    }

    /**
     * shlo:根据id查询用户收货地址
     * @param id
     * @return
     */
    @GetMapping("/findAddrById/{id}")
    @ApiOperation("shlo:根据id查询用户收货地址")
    public Result<AddrsVo> findAddrById(@PathVariable String id) {
        FwAddrs fwAddrs = addrsService.selectFwAddrsById(id);
        AddrsVo addrsVo = new AddrsVo();
        BeanUtil.copyProperties(fwAddrs,addrsVo);
        addrsVo.setAreaName(districtService.codeConversionName(fwAddrs.getArea()));
        addrsVo.setCityName(districtService.codeConversionName(fwAddrs.getCity()));
        addrsVo.setProvinceName(districtService.codeConversionName(fwAddrs.getProvince()));
        return success(addrsVo);
    }



    /**
     * shlo: 修改用户收货地址
     * @param fwAddrs
     * @return
     */
    @PutMapping("/updateAddr")
    @ApiOperation(("修改用户收货地址"))
    public Result updateAddr(@RequestBody FwAddrs fwAddrs){

        //ceshi
        //判断该收货地址是否设置为默认收货地址
        if (fwAddrs.getIsDefault()== Constant.AddrIsDefault.IS_DEFAULT_YES) {
            FwUser user = getUser();
            //查询数据库中该用户是否有默认地址
            FwAddrs one = addrsService.getOne(Wrappers.<FwAddrs>lambdaQuery()
                    .eq(FwAddrs::getUserId, user.getId())
                    .eq(FwAddrs::getIsDefault,Constant.AddrIsDefault.IS_DEFAULT_YES));
            if (one!=null){
                //如果有,将其默认地址状态改为否
                one.setIsDefault(Constant.AddrIsDefault.IS_DEFAULT_NO);
                addrsService.updateFwAddrs(one);
            }
        }
        return success(addrsService.updateFwAddrs(fwAddrs));
    }

    /**
     * shlo:设置默认地址
     * @param id
     * @return
     */
    @PutMapping("/defaultAddr/{id}")
    @ApiOperation(("shlo:设置默认地址(传收货地址id)"))
    public Result defaultAddr(@PathVariable String id){
        FwUser user = getUser();
        //先查询数据库中该用户是否有默认地址
        FwAddrs one = addrsService.getOne(Wrappers.<FwAddrs>lambdaQuery()
                .eq(FwAddrs::getUserId, user.getId())
                .eq(FwAddrs::getIsDefault,Constant.AddrIsDefault.IS_DEFAULT_YES));
        if (one!=null){
            //如果有,将其默认地址状态改为否
            one.setIsDefault(Constant.AddrIsDefault.IS_DEFAULT_NO);
            addrsService.updateFwAddrs(one);
        }
        //如果没有,直接将其设置成默认地址
        FwAddrs addrs = addrsService.selectFwAddrsById(id);
        addrs.setIsDefault(Constant.AddrIsDefault.IS_DEFAULT_YES);
        return success(addrsService.updateFwAddrs(addrs));
    }

    /**
     * shlo:新增收货地址
     * @param fwAddrs
     * @return
     */
    @PostMapping("/addAddr")
    @ApiOperation(("新增用户收货地址"))
    public Result addAddr(@RequestBody FwAddrs fwAddrs){

        //判断该新增的收货地址是否设置为默认收货地址
        if (fwAddrs.getIsDefault()== Constant.AddrIsDefault.IS_DEFAULT_YES) {
            FwUser user = getUser();
            //查询数据库中该用户是否有默认地址
            FwAddrs one = addrsService.getOne(Wrappers.<FwAddrs>lambdaQuery()
                    .eq(FwAddrs::getUserId, user.getId())
                    .eq(FwAddrs::getIsDefault,Constant.AddrIsDefault.IS_DEFAULT_YES));
            if (one!=null){
                //如果有,将其默认地址状态改为否
                one.setIsDefault(Constant.AddrIsDefault.IS_DEFAULT_NO);
                addrsService.updateFwAddrs(one);
            }
        }
        //如果没有,直接新增
        return success(addrsService.insertFwAddrs(fwAddrs));
    }


    /**
     * shlo:批量删除收货地址
     * @param ids
     * @return
     */
    @ApiOperation("批量删除收货地址")
    @DeleteMapping("/delAllAddr/{ids}")
    public Result delAllAddr(@PathVariable String[] ids) {
        return success(addrsService.deleteFwAddrsByIds(ids));
    }

    /**
     * shlo:删除收货地址
     * @param id
     * @return
     */
    @ApiOperation("根据id删除收货地址")
    @DeleteMapping("/delAddr/{id}")
    public Result delAddr(@PathVariable String id) {
        return success(addrsService.deleteFwAddrsById(id));
    }

}

