package com.fw.application.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 通用日志记录表 前端控制器
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
@RestController
@RequestMapping("/fw.system.web/fw-currency-log")
public class FwCurrencyLogController {

}

