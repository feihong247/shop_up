package com.fw.application.controller;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.mes.Result;
import com.fw.system.web.model.entity.FwNotice;
import com.fw.system.web.model.form.NoticeQuery;
import com.fw.system.web.model.vo.NoticeVo;
import com.fw.system.web.service.IFwNoticeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static com.fw.mes.ResultUtils.success;

/**
 * <p>
 * 系统公告&通知 前端控制器
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@RestController
@RequestMapping("/notice")
@RequiredArgsConstructor
@Api(tags = "系统公告&通知")
public class FwNoticeController {

    private final IFwNoticeService noticeService;

    @ApiOperation("shlo:查询系统公告&通知列表")
    @PostMapping("/listNotice")
    public Result< PageInfo<NoticeVo>> listNotice(@RequestBody NoticeQuery noticeQuery){
        PageHelper.startPage(noticeQuery.getPageNum(),noticeQuery.getPageSize());
        List<FwNotice> list = noticeService.list();
        PageInfo<FwNotice> info = new PageInfo<FwNotice>(list);
        ArrayList<NoticeVo> noticeVos = new ArrayList<>();
        for (FwNotice fwNotice : list) {
            NoticeVo noticeVo = new NoticeVo();
            BeanUtil.copyProperties(fwNotice, noticeVo);
            noticeVos.add(noticeVo);
        }
        PageInfo<NoticeVo> resultInfo = new PageInfo<NoticeVo>(noticeVos);
        resultInfo.setTotal(info.getTotal());
        return success(resultInfo);
    }

    /**
     * shlo:根据id查询系统公告&通知
     *
     * @return
     */
    @ApiOperation("shlo:根据id查询系统公告&通知")
    @GetMapping("/findNoticeById/{id}")
    public Result<NoticeVo> findNoticeById(@PathVariable String id) {
        FwNotice notice = noticeService.getOne(Wrappers.<FwNotice>lambdaQuery().eq(FwNotice::getId, id));
        NoticeVo noticeVo = new NoticeVo();
        BeanUtil.copyProperties(notice, noticeVo);
        return success(noticeVo);
    }

}

