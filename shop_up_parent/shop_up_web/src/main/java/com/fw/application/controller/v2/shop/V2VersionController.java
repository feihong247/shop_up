package com.fw.application.controller.v2.shop;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.mes.Result;
import com.fw.system.web.model.entity.FwVersion;
import com.fw.system.web.service.IFwVersionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import static com.fw.mes.ResultUtils.success;

/**
 * <p>
 * 版本表 前端控制器
 * </p>
 *
 * @author  
 * @since 2021-09-08
 */
@RestController
@RequestMapping("/version")
@CrossOrigin
@Api(tags = "版本检测")
public class V2VersionController {

    @Autowired
    private IFwVersionService versionService;

    @GetMapping("/checkAndroidVersion")
    @ApiOperation("安卓检测版本")
    public Result<FwVersion> checkAndroidVersion(){
        FwVersion version = versionService.getOne(Wrappers.<FwVersion>lambdaQuery().eq(FwVersion::getIsType, 1).orderByDesc(FwVersion::getCreateTime).last("LIMIT 1"));
        return success(version);
    }

    //苹果检测版本
    @GetMapping("/checkAppleVersion")
    @ApiOperation("检测苹果版本号")
    public Result checkAppleVersion(){
        FwVersion version = versionService.getOne(Wrappers.<FwVersion>lambdaQuery().eq(FwVersion::getIsType, 2).orderByDesc(FwVersion::getCreateTime).last("LIMIT 1"));
        return success(version);
    }
}

