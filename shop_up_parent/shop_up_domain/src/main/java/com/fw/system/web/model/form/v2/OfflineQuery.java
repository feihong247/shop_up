package com.fw.system.web.model.form.v2;

import com.fw.system.web.model.form.PageQuery;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("线下消费查询")
public class OfflineQuery extends PageQuery {




}
