package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwUsjoinMapper;
import com.fw.system.admin.domain.FwUsjoin;
import com.fw.system.admin.service.IFwUsjoinService;

/**
 * 我的商铺收藏中间Service业务层处理
 *
 * @author yanwei
 * @date 2021-05-24
 */
@Service
public class FwUsjoinServiceImpl extends ServiceImpl<FwUsjoinMapper, FwUsjoin> implements IFwUsjoinService
{
    @Autowired
    private FwUsjoinMapper fwUsjoinMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询我的商铺收藏中间
     *
     * @param id 我的商铺收藏中间ID
     * @return 我的商铺收藏中间
     */
    @Override
    public FwUsjoin selectFwUsjoinById(String id)
    {
        return fwUsjoinMapper.selectFwUsjoinById(id);
    }

    /**
     * 查询我的商铺收藏中间列表
     *
     * @param fwUsjoin 我的商铺收藏中间
     * @return 我的商铺收藏中间
     */
    @Override
    public List<FwUsjoin> selectFwUsjoinList(FwUsjoin fwUsjoin)
    {
        return fwUsjoinMapper.selectFwUsjoinList(fwUsjoin);
    }

    /**
     * 新增我的商铺收藏中间
     *
     * @param fwUsjoin 我的商铺收藏中间
     * @return 结果
     */
    @Override
    public int insertFwUsjoin(FwUsjoin fwUsjoin)
    {
        fwUsjoin.setCreateTime(DateUtils.getNowDate());
        fwUsjoin.setId(idXD.nextId());
        return fwUsjoinMapper.insertFwUsjoin(fwUsjoin);
    }

    /**
     * 修改我的商铺收藏中间
     *
     * @param fwUsjoin 我的商铺收藏中间
     * @return 结果
     */
    @Override
    public int updateFwUsjoin(FwUsjoin fwUsjoin)
    {
        fwUsjoin.setUpdateTime(DateUtils.getNowDate());
        return fwUsjoinMapper.updateFwUsjoin(fwUsjoin);
    }

    /**
     * 批量删除我的商铺收藏中间
     *
     * @param ids 需要删除的我的商铺收藏中间ID
     * @return 结果
     */
    @Override
    public int deleteFwUsjoinByIds(String[] ids)
    {
        return fwUsjoinMapper.deleteFwUsjoinByIds(ids);
    }

    /**
     * 删除我的商铺收藏中间信息
     *
     * @param id 我的商铺收藏中间ID
     * @return 结果
     */
    @Override
    public int deleteFwUsjoinById(String id)
    {
        return fwUsjoinMapper.deleteFwUsjoinById(id);
    }
}
