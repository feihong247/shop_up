package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品类目
 * </p>
 *
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwSpucate extends Model<FwSpucate> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键 主键
     */
    @TableId("id")
    private String id;

    /**
     * 类目名称
     */
    @TableField("cate_name")
    private String cateName;

    /**
     * 类目icon地址
     */
    @TableField("cate_url")
    private String cateUrl;

    /**
     * 上级类目 -1代表顶级
     */
    @TableField("parent_id")
    private String parentId;

    /**
     * 创建人 创建人
     */
    @TableField("create_by")
    private String createBy;

    /**
     * 创建时间 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 更新人 更新人
     */
    @TableField("update_by")
    private String updateBy;

    /**
     * 更新时间 更新时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;

    /** 子类目 */
    @TableField(exist = false)
    private List<FwSpucate> spucateList;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
