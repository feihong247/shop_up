package com.fw.system.web.service;

import com.fw.enums.PayTypeEnum;
import com.fw.system.web.model.entity.FwOffline;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.form.v2.OfflineQuery;
import com.fw.system.web.model.vo.v2.OfflineVo;
import com.fw.system.web.model.vo.v2.OrderMoneyVo;
import com.github.pagehelper.PageInfo;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 线下消费记录表 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface IFwOfflineService extends IService<FwOffline> {

    void shopSave(String userId, String shopId, BigDecimal money, BigDecimal realAmount, PayTypeEnum payAli, String message);

    PageInfo<OfflineVo> pageList(OfflineQuery offlineQuery, String shopId);

    List<OrderMoneyVo> manageOffOrder(String shopId, Integer queryRange, Integer limit);
}
