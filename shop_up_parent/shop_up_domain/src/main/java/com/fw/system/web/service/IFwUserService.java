package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwUser;
import com.fw.system.web.model.form.ShopIngAgencyForm;
import com.fw.system.web.model.form.ShopIngShangJiaForm;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
public interface IFwUserService extends IService<FwUser> {

    /**
     * 查询用户
     *
     * @param id 用户ID
     * @return 用户
     */
    public FwUser selectFwUserById(String id);

    /**
     * 查询用户列表
     *
     * @param fwUser 用户
     * @return 用户集合
     */
    public List<FwUser> selectFwUserList(FwUser fwUser);

    /**
     * 新增用户
     *
     * @param fwUser 用户
     * @return 结果
     */
    public int insertFwUser(FwUser fwUser);

    /**
     * 修改用户
     *
     * @param fwUser 用户
     * @return 结果
     */
    public int updateFwUser(FwUser fwUser);

    /**
     * 批量删除用户
     *
     * @param ids 需要删除的用户ID
     * @return 结果
     */
    public int deleteFwUserByIds(String[] ids);

    /**
     * 删除用户信息
     *
     * @param id 用户ID
     * @return 结果
     */
    public int deleteFwUserById(String id);

    /**
     * 验证验证码
     *
     * @param phone
     * @param code
     * @return
     */
    public Integer checkCode(String phone, String code);


    /**
     * 解绑设备
     *
     * @param fwUser 用户
     * @return 结果
     */
    public int imeiNull(FwUser fwUser);

    /**
     * 购买代理
     *
     * @param shopIngAgencyForm
     * @return
     */
    public void ShopIngAgent(ShopIngAgencyForm shopIngAgencyForm);

    /**
     * 认购原始商甲
     *
     * @param shopIngShangJiaForm
     * @return
     */
    public void ShopIngShangJia(ShopIngShangJiaForm shopIngShangJiaForm);


    /** 绑定上级后续逻辑 */
    public void bindingParent(FwUser user);

    /** 查询120天未登录的用户,将其删除 */
    public void delUser();

    @Deprecated
    public void exe();

}
