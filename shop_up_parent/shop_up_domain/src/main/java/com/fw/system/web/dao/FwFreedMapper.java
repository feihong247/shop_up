package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwFreed;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 意见反馈 Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
public interface FwFreedMapper extends BaseMapper<FwFreed> {

    /**
     * 查询意见反馈
     *
     * @param id 意见反馈ID
     * @return 意见反馈
     */
    public FwFreed selectFwFreedById(String id);

    /**
     * 查询意见反馈列表
     *
     * @param fwFreed 意见反馈
     * @return 意见反馈集合
     */
    public List<FwFreed> selectFwFreedList(FwFreed fwFreed);

    /**
     * 新增意见反馈
     *
     * @param fwFreed 意见反馈
     * @return 结果
     */
    public int insertFwFreed(FwFreed fwFreed);

    /**
     * 修改意见反馈
     *
     * @param fwFreed 意见反馈
     * @return 结果
     */
    public int updateFwFreed(FwFreed fwFreed);

    /**
     * 删除意见反馈
     *
     * @param id 意见反馈ID
     * @return 结果
     */
    public int deleteFwFreedById(String id);

    /**
     * 批量删除意见反馈
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwFreedByIds(String[] ids);
}
