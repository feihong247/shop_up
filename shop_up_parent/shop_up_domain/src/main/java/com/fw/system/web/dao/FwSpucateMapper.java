package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwSpucate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品类目 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwSpucateMapper extends BaseMapper<FwSpucate> {

}
