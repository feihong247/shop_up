package com.fw.system.web.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 地图表
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Data
public class ShopAddVo implements Serializable {


    /**
     * 主键
     */
    @ApiModelProperty("地图主键")
    private String pid;

    /**
     * 业务id
     */
    @ApiModelProperty("业务主键")
    private String busId;


    /**
     * 省
     */
    @ApiModelProperty("省")
    private String province;

    /**
     * 市
     */
    @ApiModelProperty("市")
    private String city;

    /**
     * 区/县
     */
    @ApiModelProperty("区/县")
    private String area;

    /**
     * 省
     */
    @ApiModelProperty("省名称")
    private String provinceName;

    /**
     * 市
     */
    @ApiModelProperty("市名称")
    private String cityName;

    /**
     * 区/县
     */
    @ApiModelProperty("区/县名称")
    private String areaName;

    /**
     * 详细地址
     */
    @ApiModelProperty("详细地址")
    private String addrInfo;

    /**
     * 纬度
     */
    @ApiModelProperty("纬度")
    private BigDecimal latitude;

    /**
     * 经度
     */
    @ApiModelProperty("经度")
    private BigDecimal longitude;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;






}
