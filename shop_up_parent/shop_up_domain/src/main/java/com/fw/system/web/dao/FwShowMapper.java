package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwShow;

/**
 * <p>
 * 用户消费产出额度 Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-06-19
 */
public interface FwShowMapper extends BaseMapper<FwShow> {

}
