package com.fw.system.web.model.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 积分销毁商甲表
 * </p>
 *
 * @author  
 * @since 2021-07-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwWithdrawShangjia extends Model<FwWithdrawShangjia> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("id")
    private String id;

    /**
     * 剩余已兑现积分
     */
    @TableField("integral")
    private BigDecimal integral;

    /**
     * 已销毁商甲积分
     */
    @TableField("integral_shangjia")
    private BigDecimal integralShangjia;

    /**
     * 已销毁商甲
     */
    @TableField("destory_shangjia")
    private BigDecimal destoryShangjia;

    /**
     * 更新人
     */
    @TableField("update_by")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
