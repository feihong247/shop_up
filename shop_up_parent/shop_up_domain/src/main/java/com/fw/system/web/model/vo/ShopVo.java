package com.fw.system.web.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 商铺信息vo
 */
@Data
@Api(tags = "数据规则vo")
public class ShopVo implements Serializable {

    @ApiModelProperty("经营类型名称")
    private String operatingName;

    @ApiModelProperty("经营类型编号")
    private String oId;

    @ApiModelProperty("商铺收藏量")
    private Integer shopLikeVal;

    @ApiModelProperty("线上 + 线下 商品总数")
    private Integer shopSpuVal;

    @ApiModelProperty("销量")
    private Integer processVal;
    /**
     * 主键
     */
    @ApiModelProperty("商铺主键")
    private String id;

    /**
     * 编号_平台管理的用户ID
     */
    @ApiModelProperty("平台管理的用户主键")
    private String userId;


    /**
     * 商铺线下质押多少商甲
     */
    @ApiModelProperty("商铺线下质押多少商甲")
    private BigDecimal shangjia;


    /**
     * 数据设定-收入设定
     */
    @ApiModelProperty("数据设定-收入设定 数据格式为 10000~30000")
    private String dataIncome;

    /**
     * 数据设定-性别,0=女，1=男，2=不限
     */
    @ApiModelProperty("数据设定-性别,0=女，1=男，2=不限")
    private Integer dataSex;



    /**
     * 商铺线下质押多少商甲
     */
    @ApiModelProperty("商铺线下质押多少商甲")
    private BigDecimal offlineShangjia;

    /**
     * 管理人名称
     */
    @ApiModelProperty("管理人名称")
    private String managerName;

    /**
     * 管理人电话，数据需要脱敏
     */
    @ApiModelProperty("管理人电话")
    private String managerPhone;


    /**
     * 商铺名称
     */
    @ApiModelProperty("商铺名称")
    private String shopName;

    /**
     * 商铺简介
     */
    @ApiModelProperty("商铺简介")
    private String shopInfo;

    /**
     * 商铺营业执照图片
     */
    @ApiModelProperty("商铺营业执照图片")
    private String shopImage;



    /**
     * 商铺状态,0-待审核，1-审核通过，2-审核失败，驳回
     */
    @ApiModelProperty("商铺状态 0-待审核，1-审核通过，2-审核失败，驳回")
    private Integer status;

    /**
     * 商铺审核状态为2时，次字段生效
     */
    @ApiModelProperty("驳回信息 - 状态 2 有效")
    private String turndown;

    /**
     * 线下消费
     */
    @TableField("line_customer")
    private BigDecimal lineCustomer;

    /**
     * 营业时间
     */
    @ApiModelProperty("营业时间")
    private  String operatingTime;


    /**
     * 商铺电话
     */
    @ApiModelProperty("商铺电话")
    private String shopMobile;

    /**
     * 商铺公告
     */
    @ApiModelProperty("商铺公告")
    private String shopNotice;

    /**
     * 商铺公告
     */
    @ApiModelProperty("商铺Logo")
    private String shopLogo;

    /**
     * 商铺 人均消费
     */
    @ApiModelProperty("人均消费")
    private String shopSvg;

    /**
     * 商铺评分
     */
    @ApiModelProperty("商铺评分")
    private String shopScore;

    /**
     * 商铺封面
     */
    @ApiModelProperty("商铺封面")
    private String shopCover;


    /**
     * 步行多少米
     */
    @ApiModelProperty("步行多少米")
    private String step;

    /**
     * 时间
     */
    @ApiModelProperty("时间")
    private String stepTime;


    /**
     * 经纬度
     */
    @ApiModelProperty("经纬度信息")
    private ShopAddVo shopAddVo;


    /**
     * 次店铺是否被收藏
     */
    @ApiModelProperty("次店铺是否被收藏，0 = 未收藏，1 = 收藏")
    private Integer isLike = 0;

    /**
     * 店铺收藏量
     */
    @ApiModelProperty("店铺收藏量")
    private Integer likeCount = 1;

    /**
     * 商铺收款码
     */
    @ApiModelProperty("商铺支付码")
    private String shopPay;

    /**
     * 店铺收藏量
     */
    @ApiModelProperty("商铺客服二维码")
    private String shopQr;

    /**
     * 商铺线上质押多少商甲
     */
    @ApiModelProperty(value = "身份证-正面",required = true)
    private String realJust;

    /**
     * 商铺线下质押多少商甲
     */
    @ApiModelProperty(value = "身份证-反面",required = true)
    private String realBack;








}
