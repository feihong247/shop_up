package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwReadPackage;

/**
 * <p>
 * 红包类型表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-09-03
 */
public interface FwReadPackageMapper extends BaseMapper<FwReadPackage> {

}
