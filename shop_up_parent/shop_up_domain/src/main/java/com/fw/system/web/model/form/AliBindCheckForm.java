package com.fw.system.web.model.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@ApiModel("验证表单")
public class AliBindCheckForm implements Serializable {

    @NotBlank(message = "verifyId 不可为空")
    @ApiModelProperty("verifyId")
    private String verifyId;

    @NotBlank(message = "token 不可为空")
    @ApiModelProperty("token , 用户token")
    private String token;

}
