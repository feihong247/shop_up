package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import com.fw.utils.DateUtils;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

/**
 * 业务日志对象 fw_logs
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_logs")
@ApiModel(value="业务日志", description="业务日志")
public class FwLogs implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 主键 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 业务编号 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "业务编号")
    @TableField("bui_id")
    private String buiId;

    /** 类型 */
    @ApiModelProperty(value = "业务编号")
    @Excel(name = "类型")
    @TableField("is_type")
    private String isType;

    /** 模块编号 */
    @ApiModelProperty(value = "类型")
    @Excel(name = "模块编号")
    @TableField("model_name")
    private String modelName;

    /** 大文本记录内容 */
    @ApiModelProperty(value = "模块编号")
    @Excel(name = "大文本记录内容")
    @TableField("json_txt")
    private String jsonTxt;

    /** 自定义数据 */
    @ApiModelProperty(value = "大文本记录内容")
    @Excel(name = "自定义数据")
    @TableField("form_info")
    private String formInfo;

    @TableField(exist = false)
    private String userName;
    @TableField(exist = false)
    private String phone;

    @TableField(exist = false)
    private String aliUserName;
    @TableField(exist = false)
    private String aliPhone;

    @ApiModelProperty(value = "自定义数据")
    @Excel(name = "创建人")
    @TableField(value = "create_by")
    private String createBy;

    /** 创建时间 */
    @ApiModelProperty(value = "创建人")
    @Excel(name = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time")
    private Date createTime;

    /** 更新者 */
    @ApiModelProperty(value = "创建时间")
    @Excel(name = "更新人")
    @TableField(value = "update_by")
    private String updateBy;

    /** 更新时间 */
    @ApiModelProperty(value = "更新人")
    @Excel(name = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "update_time")
    private Date updateTime;



    public void createD(String nextId) {

        setCreateBy(nextId);
        setCreateTime(DateUtils.getNowDate());
        if (Objects.nonNull(getCreateTime()))
           setUpdateTime(getCreateTime());
        if (Objects.nonNull(getCreateBy()))
           setUpdateBy(getCreateBy());
    }


}
