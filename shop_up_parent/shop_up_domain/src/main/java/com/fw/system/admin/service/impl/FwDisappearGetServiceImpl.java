package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwDisappearGetMapper;
import com.fw.system.admin.domain.FwDisappearGet;
import com.fw.system.admin.service.IFwDisappearGetService;

/**
 * 消证获取Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwDisappearGetServiceImpl extends ServiceImpl<FwDisappearGetMapper, FwDisappearGet> implements IFwDisappearGetService
{
    @Autowired
    private FwDisappearGetMapper fwDisappearGetMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询消证获取
     * 
     * @param id 消证获取ID
     * @return 消证获取
     */
    @Override
    public FwDisappearGet selectFwDisappearGetById(String id)
    {
        return fwDisappearGetMapper.selectFwDisappearGetById(id);
    }

    /**
     * 查询消证获取列表
     * 
     * @param fwDisappearGet 消证获取
     * @return 消证获取
     */
    @Override
    public List<FwDisappearGet> selectFwDisappearGetList(FwDisappearGet fwDisappearGet)
    {
        return fwDisappearGetMapper.selectFwDisappearGetList(fwDisappearGet);
    }

    /**
     * 新增消证获取
     * 
     * @param fwDisappearGet 消证获取
     * @return 结果
     */
    @Override
    public int insertFwDisappearGet(FwDisappearGet fwDisappearGet)
    {
        fwDisappearGet.setCreateTime(DateUtils.getNowDate());
        fwDisappearGet.setId(idXD.nextId());
        return fwDisappearGetMapper.insertFwDisappearGet(fwDisappearGet);
    }

    /**
     * 修改消证获取
     * 
     * @param fwDisappearGet 消证获取
     * @return 结果
     */
    @Override
    public int updateFwDisappearGet(FwDisappearGet fwDisappearGet)
    {
        fwDisappearGet.setUpdateTime(DateUtils.getNowDate());
        return fwDisappearGetMapper.updateFwDisappearGet(fwDisappearGet);
    }

    /**
     * 批量删除消证获取
     * 
     * @param ids 需要删除的消证获取ID
     * @return 结果
     */
    @Override
    public int deleteFwDisappearGetByIds(String[] ids)
    {
        return fwDisappearGetMapper.deleteFwDisappearGetByIds(ids);
    }

    /**
     * 删除消证获取信息
     * 
     * @param id 消证获取ID
     * @return 结果
     */
    @Override
    public int deleteFwDisappearGetById(String id)
    {
        return fwDisappearGetMapper.deleteFwDisappearGetById(id);
    }
}
