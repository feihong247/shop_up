package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 常见问题对象 fw_problem
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_problem")
@ApiModel(value="常见问题", description="常见问题")
public class FwProblem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 问题类目编号 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "问题类目编号")
    @TableField("cate_id")
    private String cateId;

    /** 问题标题 */
    @ApiModelProperty(value = "问题类目编号")
    @Excel(name = "问题标题")
    @TableField("title")
    private String title;

    /** 问题详细解决方案 富文本形式 */
    @ApiModelProperty(value = "问题标题")
    @Excel(name = "问题详细解决方案 富文本形式")
    @TableField("content")
    private String content;




}
