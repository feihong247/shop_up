package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 数据规则对象 fw_rule_datas
 *
 * @author yanwei
 * @date 2021-06-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_rule_datas")
@ApiModel(value = "数据规则", description = "数据规则表")
public class FwRuleDatas extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /**
     * 规则数据名称
     */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "规则数据名称")
    @TableField("rule_name")
    private String ruleName;

    /**
     * 数值
     */
    @ApiModelProperty(value = "规则数据名称")
    @Excel(name = "数值")
    @TableField("rule_count")
    private BigDecimal ruleCount;

    /**
     * 规则类型
     */
    @ApiModelProperty(value = "数值")
    @Excel(name = "规则类型")
    @TableField("rule_type")
    private Long ruleType;


}
