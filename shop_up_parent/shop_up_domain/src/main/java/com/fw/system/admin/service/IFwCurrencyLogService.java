package com.fw.system.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.enums.CurrencyLogEnum;
import com.fw.system.admin.domain.FwCurrencyLog;

import java.math.BigDecimal;

/**
 * <p>
 * 通用日志记录表 服务类
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
public interface IFwCurrencyLogService extends IService<FwCurrencyLog> {

    void saveCurrencyAsync(BigDecimal logCount, CurrencyLogEnum currencyLogEnum, String userId, Integer type);
}
