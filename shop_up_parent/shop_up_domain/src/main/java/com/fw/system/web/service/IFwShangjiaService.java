package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwShangjia;

/**
 * <p>
 * 商甲发行表 服务类
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
public interface IFwShangjiaService extends IService<FwShangjia> {
    /**
     * 商甲增长
     */
    public void addTurnover(String userId);

    /**
     * 商甲增长
     */
    public void output();

    /**
     * 商甲存储数据统计
     */
    @Deprecated
    public void shangJiaStorage();

    /**
     * 消证兑换,转为积分
     * 每天调用一次
     */
    public void usersReleaseDisappear();

    /**
     * 商甲锁仓
     */
    public void lockStorage(String userId);
    /**
     * 商甲兑换
     * 将所有锁仓时间满一年的标记改为兑换
     */
    public void releaseFlag();
    /**
     * 商甲兑换
     * 将所有标记为兑换,且上次兑换时间满七天的兑换
     */
    public void releaseJia();
    //执行商甲退回
    void returnShangJiaById();
}
