package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品评价
 * </p>
 *
 * @author
 * @since 2021-05-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwEvaluation extends Model<FwEvaluation> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键 主键
     */
    @TableId("id")
    @ApiModelProperty("主键")
    private String id;

    /**
     * 用户编号
     */
    @TableField("user_id")
    @ApiModelProperty("用户编号")
    private String userId;

    /**
     * 商品编号
     */
    @TableField("item_id")
    @ApiModelProperty("商品编号")
    private String itemId;

    /**
     * 评价图片,多个以逗号分隔
     */
    @TableField("eva_img")
    @ApiModelProperty("评价图片,多个以逗号分隔")
    private String evaImg;

    /**
     * 评价内容
     */
    @TableField("infos")
    @ApiModelProperty("评价内容")
    private String infos;

    /**
     * 评价星级 0-5之间
     */
    @TableField("star_num")
    @ApiModelProperty("评价星级 0-5之间")
    private Integer starNum;

    /**
     * 创建人 创建人
     */
    @TableField("create_by")
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间 创建时间
     */
    @TableField("create_time")
    @ApiModelProperty("创建时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新人 更新人
     */
    @TableField("update_by")
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间 更新时间
     */
    @TableField("update_time")
    @ApiModelProperty("更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 订单编号
     */
    @TableField("order_id")
    @ApiModelProperty("订单编号")
    private String orderId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
