package com.fw.system.web.model.vo;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * <p>
 * 用户消息
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
@Api(tags = "用户新消息vo")
public class MsgVo  {



    /**
     * 主键
     */
    @ApiModelProperty("id")
    private String id;

    /**
     * 用户编号
     */
    @ApiModelProperty("用户编号")
    private String userId;

    /**
     * 消息标题
     */
    @ApiModelProperty("消息标题")
    private String title;

    /**
     * 消息内容
     */
    @ApiModelProperty("消息内容")
    private String content;



}
