package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwOffline;

/**
 * 线下消费记录Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwOfflineService extends IService<FwOffline>
{
    /**
     * 查询线下消费记录
     * 
     * @param id 线下消费记录ID
     * @return 线下消费记录
     */
    public FwOffline selectFwOfflineById(String id);

    /**
     * 查询线下消费记录列表
     * 
     * @param fwOffline 线下消费记录
     * @return 线下消费记录集合
     */
    public List<FwOffline> selectFwOfflineList(FwOffline fwOffline);

    /**
     * 新增线下消费记录
     * 
     * @param fwOffline 线下消费记录
     * @return 结果
     */
    public int insertFwOffline(FwOffline fwOffline);

    /**
     * 修改线下消费记录
     * 
     * @param fwOffline 线下消费记录
     * @return 结果
     */
    public int updateFwOffline(FwOffline fwOffline);

    /**
     * 批量删除线下消费记录
     * 
     * @param ids 需要删除的线下消费记录ID
     * @return 结果
     */
    public int deleteFwOfflineByIds(String[] ids);

    /**
     * 删除线下消费记录信息
     * 
     * @param id 线下消费记录ID
     * @return 结果
     */
    public int deleteFwOfflineById(String id);
}
