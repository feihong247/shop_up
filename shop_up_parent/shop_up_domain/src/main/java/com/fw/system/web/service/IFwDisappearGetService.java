package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwDisappearGet;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;

/**
 * <p>
 * 消证获取表 服务类
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
public interface IFwDisappearGetService extends IService<FwDisappearGet> {
    public BigDecimal insetDisappearGet(String userId, String source, BigDecimal disappear);
}
