package com.fw.system.web.model.vo.v2;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel("线下消费记录")
public class OfflineVo implements Serializable {


    /**
     * 主键
     */
    @ApiModelProperty("记录编号")
    private String id;

    /**
     * 用户编号
     */
    @ApiModelProperty("消费用户编号")
    private String userId;

    /**
     * 用户编号
     */
    @ApiModelProperty("消费用户名称")
    private String userName;


    /**
     * 用户编号
     */
    @ApiModelProperty("消费用户手机号")
    private String phone;



    /**
     * 消费金额
     */
    @ApiModelProperty("消费金额,用户实际支付的金额.")
    private BigDecimal price;

    /**
     * 支付类型,0=支付宝，1=微信,2 = 云支付
     */
    @ApiModelProperty("支付类型,0=支付宝，1=微信,2 = 云支付")
    private Integer payType;

    /**
     * 消费反馈,支付成功后给的反馈信息
     */
    @ApiModelProperty("消费反馈,说明了商户让利多少，商户实际得到的钱")
    private String infos;



    /**
     * 创建时间
     */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("消费时间")
    private LocalDateTime createTime;
}
