package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwProblemMapper;
import com.fw.system.admin.domain.FwProblem;
import com.fw.system.admin.service.IFwProblemService;

/**
 * 常见问题Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwProblemServiceImpl extends ServiceImpl<FwProblemMapper, FwProblem> implements IFwProblemService
{
    @Autowired
    private FwProblemMapper fwProblemMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询常见问题
     * 
     * @param id 常见问题ID
     * @return 常见问题
     */
    @Override
    public FwProblem selectFwProblemById(String id)
    {
        return fwProblemMapper.selectFwProblemById(id);
    }

    /**
     * 查询常见问题列表
     * 
     * @param fwProblem 常见问题
     * @return 常见问题
     */
    @Override
    public List<FwProblem> selectFwProblemList(FwProblem fwProblem)
    {
        return fwProblemMapper.selectFwProblemList(fwProblem);
    }

    /**
     * 新增常见问题
     * 
     * @param fwProblem 常见问题
     * @return 结果
     */
    @Override
    public int insertFwProblem(FwProblem fwProblem)
    {
        fwProblem.setCreateTime(DateUtils.getNowDate());
        fwProblem.setId(idXD.nextId());
        return fwProblemMapper.insertFwProblem(fwProblem);
    }

    /**
     * 修改常见问题
     * 
     * @param fwProblem 常见问题
     * @return 结果
     */
    @Override
    public int updateFwProblem(FwProblem fwProblem)
    {
        fwProblem.setUpdateTime(DateUtils.getNowDate());
        return fwProblemMapper.updateFwProblem(fwProblem);
    }

    /**
     * 批量删除常见问题
     * 
     * @param ids 需要删除的常见问题ID
     * @return 结果
     */
    @Override
    public int deleteFwProblemByIds(String[] ids)
    {
        return fwProblemMapper.deleteFwProblemByIds(ids);
    }

    /**
     * 删除常见问题信息
     * 
     * @param id 常见问题ID
     * @return 结果
     */
    @Override
    public int deleteFwProblemById(String id)
    {
        return fwProblemMapper.deleteFwProblemById(id);
    }
}
