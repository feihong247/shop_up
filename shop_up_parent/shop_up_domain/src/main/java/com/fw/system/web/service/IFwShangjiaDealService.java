package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwShangjiaDeal;
import com.fw.system.web.model.form.ShopIngShangJiaForm;
import com.fw.system.web.model.vo.ShangjiaDealVo;

import java.util.List;

/**
 * <p>
 * 商甲交易,买卖表 服务类
 * </p>
 *
 * @author
 * @since 2021-06-08
 */
public interface IFwShangjiaDealService extends IService<FwShangjiaDeal> {
    /**
     * 交易大厅 买方支付
     * @param shopIngShangJiaForm
     */
    public void ShopIngShangJia(ShopIngShangJiaForm shopIngShangJiaForm);

    /**
     * 交易大厅,商家支付
     * @param shopIngShangJiaForm
     */
    public void shopPay(ShopIngShangJiaForm shopIngShangJiaForm);


    /**
     * 根据type查询交易列表
     * @param type
     * @return
     */
    public List<ShangjiaDealVo> findDealList(String type);

    /**
     * 分红
     */
    public void bonus();
}
