package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 红包类型表
 * </p>
 *
 * @author  
 * @since 2021-09-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwReadPackage extends Model<FwReadPackage> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    /**
     * 红包名称
     */
    @TableField("read_name")
    private String readName;

    /**
     * 红包大小
     */
    @TableField("amount_num")
    private BigDecimal amountNum;

    /**
     * 起始累加金额
     */
    @TableField("start_num")
    private BigDecimal startNum;

    /**
     * 红包截止日期
     */
    @TableField("end_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 0 不发布 1发布 2删除
     */
    @TableField("is_state")
    private Integer isState;

    /**
     * 红包完成难易程度 1简单 2中都 3困难 4压根完不成
     */
    @TableField("is_type")
    private Integer isType;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
