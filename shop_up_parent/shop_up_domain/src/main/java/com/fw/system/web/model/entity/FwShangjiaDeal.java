package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 商甲交易,买卖表
 * </p>
 *
 * @author
 * @since 2021-06-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwShangjiaDeal extends Model<FwShangjiaDeal> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    /**
     * 卖出人编号
     */
    @TableField("user_id")
    @ApiModelProperty("卖出人编号")
    private String userId;

    /**
     * 商甲单价
     */
    @TableField("money")
    @ApiModelProperty("商甲单价")
    private BigDecimal money;

    /**
     * 商甲剩余数量
     */
    @TableField("count")
    @ApiModelProperty("商甲剩余数量")
    private BigDecimal count;

    /**
     *交易锁定标志 1锁定 0未锁定
     */
    @TableField("type")
    @ApiModelProperty("交易锁定标志 1锁定 0未锁定")
    private Integer type;

    /**
     * 交易状态,0:售卖中,1:已卖出
     */
    @TableField("is_lock")
    @ApiModelProperty("交易状态,0:售卖中,1:已锁定")
    private Integer isLock;

    /**
     * 创建时间/寄售时间
     */
    @TableField("create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("创建时间/寄售时间")
    private LocalDateTime createTime;
    /**
     * 锁定时间
     */
    @TableField("lock_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("创建时间/寄售时间")
    private LocalDateTime lockTime;

    /**
     * 更新人
     */
    @TableField("update_by")
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间/卖出时间
     */
    @TableField("update_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("更新时间/卖出时间")
    private LocalDateTime updateTime;


    /** 商甲总量 */
    @ApiModelProperty(value = "商甲总量")
    @TableField("count_jia")
    private BigDecimal countJia;




    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
