package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 系统公告&通知对象 fw_notice
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_notice")
@ApiModel(value="系统公告&通知", description="系统公告&通知")
public class FwNotice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 标题 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "标题")
    @TableField("notice_title")
    private String noticeTitle;

    /** 类型，0=公告，1=通知 0=公告,1=通知 */
    @ApiModelProperty(value = "标题")
    @Excel(name = "类型，0=公告，1=通知 0=公告,1=通知")
    @TableField("notice_type")
    private Integer noticeType;

    /** 内容 */
    @ApiModelProperty(value = "类型，0=公告，1=通知 0=公告,1=通知")
    @Excel(name = "内容")
    @TableField("content")
    private String content;

    /** 封面 */
    @ApiModelProperty(value = "内容")
    @Excel(name = "封面")
    @TableField("url_image")
    private String urlImage;




}
