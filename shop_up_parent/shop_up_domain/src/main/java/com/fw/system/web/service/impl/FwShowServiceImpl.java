package com.fw.system.web.service.impl;

import com.fw.system.web.dao.FwShowMapper;
import com.fw.system.web.model.entity.FwShow;
import com.fw.system.web.service.IFwShowService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户消费产出额度 服务实现类
 * </p>
 *
 * @author
 * @since 2021-06-19
 */
@Service
public class FwShowServiceImpl extends ServiceImpl<FwShowMapper, FwShow> implements IFwShowService {

}
