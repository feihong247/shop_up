package com.fw.system.web.service.impl;

import com.fw.system.web.model.entity.FwImages;
import com.fw.system.web.dao.FwImagesMapper;
import com.fw.system.web.service.IFwImagesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品图片列 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Service
public class FwImagesServiceImpl extends ServiceImpl<FwImagesMapper, FwImages> implements IFwImagesService {

}
