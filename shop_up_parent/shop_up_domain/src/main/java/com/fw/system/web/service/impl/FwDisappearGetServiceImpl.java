package com.fw.system.web.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.common.IdXD;
import com.fw.constant.Constant;
import com.fw.system.web.model.entity.FwDisappearGet;
import com.fw.system.web.dao.FwDisappearGetMapper;
import com.fw.system.web.model.entity.FwShangjia;
import com.fw.system.web.service.IFwDisappearGetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.system.web.service.IFwShangjiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * <p>
 * 消证获取表 服务实现类
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Service
public class FwDisappearGetServiceImpl extends ServiceImpl<FwDisappearGetMapper, FwDisappearGet> implements IFwDisappearGetService {

    @Autowired
    private IFwShangjiaService shangjiaService;

    @Autowired
    private IFwDisappearGetService disappearGetService;

    @Autowired
    private IdXD idXD;

    @Override
    public BigDecimal insetDisappearGet(String userId, String source, BigDecimal disappear) {
        //新增消证获取数据
        FwDisappearGet fwDisappearGet = new FwDisappearGet();
        fwDisappearGet.setId(idXD.nextId());
        fwDisappearGet.setUserId(userId);
        fwDisappearGet.setDisappear(disappear);
        fwDisappearGet.setDisappearSource(source);
        //写入数据库
        disappearGetService.save(fwDisappearGet);
        //调用商甲增长方法
        shangjiaService.addTurnover(userId);
        //获取商甲现价
        FwShangjia shangjia = shangjiaService.getOne(Wrappers.<FwShangjia>lambdaQuery().eq(FwShangjia::getId, Constant.ShangJia.CONSUME_ID));
        BigDecimal presentPrice = shangjia.getPresentPrice();
        //返回商甲现价
        return presentPrice;
    }

}
