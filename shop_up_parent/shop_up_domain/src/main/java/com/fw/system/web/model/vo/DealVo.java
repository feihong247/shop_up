package com.fw.system.web.model.vo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@Api(tags = "商甲-我要买vo")
public class DealVo {
    @ApiModelProperty("卖出信息数组")
    private List<ShangjiaDealVo> shangjiaDealVos;
    @ApiModelProperty("我的商甲余额")
    private BigDecimal moneyShangJia;
    @ApiModelProperty("市场价")
    private BigDecimal presentPrice;
}
