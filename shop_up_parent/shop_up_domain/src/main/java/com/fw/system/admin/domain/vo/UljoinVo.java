package com.fw.system.admin.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 用户和身份中间对象 fw_uljoin
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)

@ApiModel(value="用户和身份VO", description="用户和身份VO")
public class UljoinVo

{

    /** 中间表主键 */
    @ApiModelProperty(value = "中间表主键")
    @TableId("id")
    private String id;

    /** 用户编号 */
    @ApiModelProperty(value = "用户编号")
    @Excel(name = "用户编号")
    @TableField("user_id")
    private String userId;

    /** 身份编号 */
    @ApiModelProperty(value = "身份编号")
    @Excel(name = "身份编号")
    private String identityId;

    /** 是否正在使用此身份,一个用户只能使用一个。1=是，0=否 */
    @ApiModelProperty(value = "身份编号")
    @Excel(name = "是否正在使用此身份",readConverterExp = "1=是,0=否")
    private Integer isUse;

    /** 身份名称 */
    @ApiModelProperty(value = "身份名称")
    @Excel(name = "身份名称")
    private String identityName;





}
