package com.fw.system.web.model.form.v2;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@ApiModel("发货订单提交表单")
public class OrderPublishForm implements Serializable {


    /**
     * 主键
     */
    @ApiModelProperty(value = "订单号",required = true)
    @NotBlank(message = "订单号不可为空")
    private String id;

    /**
     * 运单公司名称
     */
    @ApiModelProperty(value = "运单公司名称",required = true)
    @NotBlank(message = "运单公司名不可为空")
    private String courierName;

    /**
     * 运单编号
     */
    @ApiModelProperty(value = "运单编号",required = true)
    @NotBlank(message = "运单编号不可为空")
    private String courierNumber;
}
