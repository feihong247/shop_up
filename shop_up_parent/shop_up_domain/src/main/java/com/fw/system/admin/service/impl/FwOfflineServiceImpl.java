package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwOfflineMapper;
import com.fw.system.admin.domain.FwOffline;
import com.fw.system.admin.service.IFwOfflineService;

/**
 * 线下消费记录Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwOfflineServiceImpl extends ServiceImpl<FwOfflineMapper, FwOffline> implements IFwOfflineService
{
    @Autowired
    private FwOfflineMapper fwOfflineMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询线下消费记录
     * 
     * @param id 线下消费记录ID
     * @return 线下消费记录
     */
    @Override
    public FwOffline selectFwOfflineById(String id)
    {
        return fwOfflineMapper.selectFwOfflineById(id);
    }

    /**
     * 查询线下消费记录列表
     * 
     * @param fwOffline 线下消费记录
     * @return 线下消费记录
     */
    @Override
    public List<FwOffline> selectFwOfflineList(FwOffline fwOffline)
    {
        return fwOfflineMapper.selectFwOfflineList(fwOffline);
    }

    /**
     * 新增线下消费记录
     * 
     * @param fwOffline 线下消费记录
     * @return 结果
     */
    @Override
    public int insertFwOffline(FwOffline fwOffline)
    {
        fwOffline.setCreateTime(DateUtils.getNowDate());
        fwOffline.setId(idXD.nextId());
        return fwOfflineMapper.insertFwOffline(fwOffline);
    }

    /**
     * 修改线下消费记录
     * 
     * @param fwOffline 线下消费记录
     * @return 结果
     */
    @Override
    public int updateFwOffline(FwOffline fwOffline)
    {
        fwOffline.setUpdateTime(DateUtils.getNowDate());
        return fwOfflineMapper.updateFwOffline(fwOffline);
    }

    /**
     * 批量删除线下消费记录
     * 
     * @param ids 需要删除的线下消费记录ID
     * @return 结果
     */
    @Override
    public int deleteFwOfflineByIds(String[] ids)
    {
        return fwOfflineMapper.deleteFwOfflineByIds(ids);
    }

    /**
     * 删除线下消费记录信息
     * 
     * @param id 线下消费记录ID
     * @return 结果
     */
    @Override
    public int deleteFwOfflineById(String id)
    {
        return fwOfflineMapper.deleteFwOfflineById(id);
    }
}
