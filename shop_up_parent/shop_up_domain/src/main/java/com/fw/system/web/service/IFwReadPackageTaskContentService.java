package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwReadPackageTaskContent;

/**
 * <p>
 * 红包关联任务用户完成情况表 服务类
 * </p>
 *
 * @author  
 * @since 2021-09-03
 */
public interface IFwReadPackageTaskContentService extends IService<FwReadPackageTaskContent> {

}
