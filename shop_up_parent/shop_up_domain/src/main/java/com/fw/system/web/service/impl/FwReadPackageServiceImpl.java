package com.fw.system.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.system.web.dao.FwReadPackageMapper;
import com.fw.system.web.model.entity.FwReadPackage;
import com.fw.system.web.service.IFwReadPackageService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 红包类型表 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-09-03
 */
@Service
public class FwReadPackageServiceImpl extends ServiceImpl<FwReadPackageMapper, FwReadPackage> implements IFwReadPackageService {

}
