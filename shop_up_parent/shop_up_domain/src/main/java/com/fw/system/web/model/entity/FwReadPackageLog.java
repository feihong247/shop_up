package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 红包抽取记录表
 * </p>
 *
 * @author  
 * @since 2021-09-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwReadPackageLog extends Model<FwReadPackageLog> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    @TableField("user_id")
    private String userId;

    /**
     * 抽取的红包金额
     */
    @TableField("amount_num")
    private BigDecimal amountNum;

    /**
     * 抽取日期
     */
    @TableField("create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 红包状态: 1进行中 0已结束
     */
    @TableField("is_state")
    private Integer isState;

    /**
     * 该红包截止日期
     */
    @TableField("end_time")
    private LocalDateTime endTime;

    /**
     * 起始金额
     */
    @TableField("start_num")
    private BigDecimal startNum;

    /**
     * 关联红包id
     */
    @TableField("read_id")
    private String readId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
