package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwOneAreaItemMapper;
import com.fw.system.admin.domain.FwOneAreaItem;
import com.fw.system.admin.service.IFwOneAreaItemService;

/**
 * 一县一品Service业务层处理
 * 
 * @author yanwei
 * @date 2021-11-09
 */
@Service
public class FwOneAreaItemServiceImpl extends ServiceImpl<FwOneAreaItemMapper, FwOneAreaItem> implements IFwOneAreaItemService
{
    @Autowired
    private FwOneAreaItemMapper fwOneAreaItemMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询一县一品
     * 
     * @param id 一县一品ID
     * @return 一县一品
     */
    @Override
    public FwOneAreaItem selectFwOneAreaItemById(String id)
    {
        return fwOneAreaItemMapper.selectFwOneAreaItemById(id);
    }

    /**
     * 查询一县一品列表
     * 
     * @param fwOneAreaItem 一县一品
     * @return 一县一品
     */
    @Override
    public List<FwOneAreaItem> selectFwOneAreaItemList(FwOneAreaItem fwOneAreaItem)
    {
        return fwOneAreaItemMapper.selectFwOneAreaItemList(fwOneAreaItem);
    }

    /**
     * 新增一县一品
     * 
     * @param fwOneAreaItem 一县一品
     * @return 结果
     */
    @Override
    public int insertFwOneAreaItem(FwOneAreaItem fwOneAreaItem)
    {
        fwOneAreaItem.setCreateTime(DateUtils.getNowDate());
        fwOneAreaItem.setId(idXD.nextId());
        return fwOneAreaItemMapper.insertFwOneAreaItem(fwOneAreaItem);
    }

    /**
     * 修改一县一品
     * 
     * @param fwOneAreaItem 一县一品
     * @return 结果
     */
    @Override
    public int updateFwOneAreaItem(FwOneAreaItem fwOneAreaItem)
    {
        return fwOneAreaItemMapper.updateFwOneAreaItem(fwOneAreaItem);
    }

    /**
     * 批量删除一县一品
     * 
     * @param ids 需要删除的一县一品ID
     * @return 结果
     */
    @Override
    public int deleteFwOneAreaItemByIds(String[] ids)
    {
        return fwOneAreaItemMapper.deleteFwOneAreaItemByIds(ids);
    }

    /**
     * 删除一县一品信息
     * 
     * @param id 一县一品ID
     * @return 结果
     */
    @Override
    public int deleteFwOneAreaItemById(String id)
    {
        return fwOneAreaItemMapper.deleteFwOneAreaItemById(id);
    }
}
