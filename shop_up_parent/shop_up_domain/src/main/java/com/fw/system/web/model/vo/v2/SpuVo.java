package com.fw.system.web.model.vo.v2;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fw.system.web.model.entity.FwImages;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@ApiModel("商品详细表单")
public class SpuVo implements Serializable {


    /**
     * 商品编号
     */
    @ApiModelProperty("商品编号")
    private String id;

    /**
     * 商品标题
     */
    @ApiModelProperty("商品标题")
    private String title;

    /**
     * 商品 分类名称
     */
    @ApiModelProperty("分类名称")
    private String categoryName;

    /** 类目 */
    @ApiModelProperty("类目编号")
    private String category;


    /** 是否热卖 */
    @ApiModelProperty("是否热销 （0否1是）")
    private Integer hotSale;

    /** 是否推荐 */
    @ApiModelProperty("是否推荐 （0否1是）")
    private Integer recommend;



    /** 是否线下(0线上1线下) */
    @ApiModelProperty("是否线下(0否1是)")
    private String isOffline;

    /** 是否竞猜(0线上1线下) */
    @ApiModelProperty("是否竞猜(0否1是)")
    private Integer isQuiz;

    /** 是否特卖（0否1是） */
    @ApiModelProperty("是否特卖（0否1是）")
    private Integer isSale;



    /**
     * 商品logo
     */
    @ApiModelProperty("商品logo")
    private String logo;

    /**
     * 商品销量
     */
    @ApiModelProperty("商品销量")
    private Integer tops;

    /**
     * 商品原价格
     */
    @ApiModelProperty("商品原价格")
    private BigDecimal virtualPrice;

    /**
     * 商品现价格
     */
    @ApiModelProperty("商品现价格")
    private BigDecimal realPrice;

    /**
     * 商品利润值,后管看的
     */
    @ApiModelProperty("商品利润值")
    private BigDecimal price;

    /**
     * 商品消证,后管自营人员填写
     */
    @ApiModelProperty("商品消证")
    private BigDecimal disappear;

    /**
     * 商品总库存
     */
    @ApiModelProperty("商品总库存")
    private Integer balance;

    /**
     * 商品详情-富文本形式
     */
    @ApiModelProperty("商品详情")
    private String infos;

    /**
     * 商品状态 0=正常，1=下架
     */
    @ApiModelProperty("商品状态 0=正常，1=下架")
    private Integer status;

    /** 商品运费 */
    @ApiModelProperty("商品运费")
    private BigDecimal shipping;

    /**
     * 商品满额减运费 需要一套模版表,地区减额，单独商品减额，还是商家总商品减额等。
     */
    @ApiModelProperty("商品满额减运费")
    private BigDecimal topShipping;

    /** 是否满减运费0否1是 */
    @ApiModelProperty("是否满减运费0否1是")
    private Integer isTopShipping;



    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @ApiModelProperty("规格列表")
    private List<SkuVo> skuVoList = new ArrayList<>();


    /** 图片列表 */
    @ApiModelProperty("图片列表")
    private List<ImagesVo> images = new ArrayList<>();


}
