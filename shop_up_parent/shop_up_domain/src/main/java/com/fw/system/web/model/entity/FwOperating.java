package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商家经营平台分类
 * </p>
 *
 * @author  
 * @since 2021-05-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwOperating extends Model<FwOperating> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    @TableField("operating_name")
    private String operatingName;

    /**
     * 图标照片
     */
    @TableField("icon_url")
    private String iconUrl;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
