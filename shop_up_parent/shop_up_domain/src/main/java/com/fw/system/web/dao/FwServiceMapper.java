package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwService;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 在线客服 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwServiceMapper extends BaseMapper<FwService> {

}
