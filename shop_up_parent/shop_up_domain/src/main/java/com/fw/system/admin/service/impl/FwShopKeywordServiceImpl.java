package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwShopKeywordMapper;
import com.fw.system.admin.domain.FwShopKeyword;
import com.fw.system.admin.service.IFwShopKeywordService;

/**
 * 商铺环境展示Service业务层处理
 * 
 * @author yanwei
 * @date 2021-07-16
 */
@Service
public class FwShopKeywordServiceImpl extends ServiceImpl<FwShopKeywordMapper, FwShopKeyword> implements IFwShopKeywordService
{
    @Autowired
    private FwShopKeywordMapper fwShopKeywordMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询商铺环境展示
     * 
     * @param id 商铺环境展示ID
     * @return 商铺环境展示
     */
    @Override
    public FwShopKeyword selectFwShopKeywordById(String id)
    {
        return fwShopKeywordMapper.selectFwShopKeywordById(id);
    }

    /**
     * 查询商铺环境展示列表
     * 
     * @param fwShopKeyword 商铺环境展示
     * @return 商铺环境展示
     */
    @Override
    public List<FwShopKeyword> selectFwShopKeywordList(FwShopKeyword fwShopKeyword)
    {
        return fwShopKeywordMapper.selectFwShopKeywordList(fwShopKeyword);
    }

    /**
     * 新增商铺环境展示
     * 
     * @param fwShopKeyword 商铺环境展示
     * @return 结果
     */
    @Override
    public int insertFwShopKeyword(FwShopKeyword fwShopKeyword)
    {
        fwShopKeyword.setCreateTime(DateUtils.getNowDate());
        fwShopKeyword.setId(idXD.nextId());
        return fwShopKeywordMapper.insertFwShopKeyword(fwShopKeyword);
    }

    /**
     * 修改商铺环境展示
     * 
     * @param fwShopKeyword 商铺环境展示
     * @return 结果
     */
    @Override
    public int updateFwShopKeyword(FwShopKeyword fwShopKeyword)
    {
        fwShopKeyword.setUpdateTime(DateUtils.getNowDate());
        return fwShopKeywordMapper.updateFwShopKeyword(fwShopKeyword);
    }

    /**
     * 批量删除商铺环境展示
     * 
     * @param ids 需要删除的商铺环境展示ID
     * @return 结果
     */
    @Override
    public int deleteFwShopKeywordByIds(String[] ids)
    {
        return fwShopKeywordMapper.deleteFwShopKeywordByIds(ids);
    }

    /**
     * 删除商铺环境展示信息
     * 
     * @param id 商铺环境展示ID
     * @return 结果
     */
    @Override
    public int deleteFwShopKeywordById(String id)
    {
        return fwShopKeywordMapper.deleteFwShopKeywordById(id);
    }
}
