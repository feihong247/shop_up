package com.fw.system.web.model.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户消费产出额度
 * </p>
 *
 * @author
 * @since 2021-06-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwShow extends Model<FwShow> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("id")
    private String id;

    /**
     * 用户编号
     */
    @TableField("user_id")
    private String userId;

    /**
     * 保存临时消费的金额,当此金额达到商甲产出标准,发放商甲,减去额度
     */
    @TableField("standard")
    private BigDecimal standard;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
