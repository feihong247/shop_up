package com.fw.system.admin.service.impl;

import java.util.List;

import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwBannerMapper;
import com.fw.system.admin.domain.FwBanner;
import com.fw.system.admin.service.IFwBannerService;

/**
 * 首页轮播Service业务层处理
 *
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwBannerServiceImpl extends ServiceImpl<FwBannerMapper, FwBanner> implements IFwBannerService {
    @Autowired
    private FwBannerMapper fwBannerMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询首页轮播
     *
     * @param id 首页轮播ID
     * @return 首页轮播
     */
    @Override
    public FwBanner selectFwBannerById(String id) {
        return fwBannerMapper.selectFwBannerById(id);
    }

    /**
     * 查询首页轮播列表
     *
     * @param fwBanner 首页轮播
     * @return 首页轮播
     */
    @Override
    public List<FwBanner> selectFwBannerList(FwBanner fwBanner) {
        return fwBannerMapper.selectFwBannerList(fwBanner);
    }

    /**
     * 新增首页轮播
     *
     * @param fwBanner 首页轮播
     * @return 结果
     */
    @Override
    public int insertFwBanner(FwBanner fwBanner) {
        fwBanner.setCreateTime(DateUtils.getNowDate());
        fwBanner.setId(idXD.nextId());
        return fwBannerMapper.insertFwBanner(fwBanner);
    }

    /**
     * 修改首页轮播
     *
     * @param fwBanner 首页轮播
     * @return 结果
     */
    @Override
    public int updateFwBanner(FwBanner fwBanner) {
        fwBanner.setUpdateTime(DateUtils.getNowDate());
        return fwBannerMapper.updateFwBanner(fwBanner);
    }

    /**
     * 批量删除首页轮播
     *
     * @param ids 需要删除的首页轮播ID
     * @return 结果
     */
    @Override
    public int deleteFwBannerByIds(String[] ids) {
        return fwBannerMapper.deleteFwBannerByIds(ids);
    }

    /**
     * 删除首页轮播信息
     *
     * @param id 首页轮播ID
     * @return 结果
     */
    @Override
    public int deleteFwBannerById(String id) {
        return fwBannerMapper.deleteFwBannerById(id);
    }
}
