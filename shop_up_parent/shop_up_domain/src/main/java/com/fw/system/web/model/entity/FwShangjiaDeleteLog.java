package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 商甲销毁日志表
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwShangjiaDeleteLog extends Model<FwShangjiaDeleteLog> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    @TableField("user_id")
    private String userId;

    /**
     * 销毁数量
     */
    @TableField("log_count")
    private BigDecimal logCount;

    /**
     * 因产出多少商甲而销毁的数量
     */
    @TableField("shangjia_num")
    private BigDecimal shangjiaNum;

    /**
     * 日志记录时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 用户名称
     */
    @TableField("user_name")
    private String userName;

    /**
     * 用户电话
     */
    @TableField("user_phone")
    private String userPhone;

    @TableField("log_type")
    private Integer logType;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
