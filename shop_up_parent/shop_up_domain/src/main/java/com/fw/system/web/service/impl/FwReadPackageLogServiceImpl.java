package com.fw.system.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.system.web.dao.FwReadPackageLogMapper;
import com.fw.system.web.model.entity.FwReadPackageLog;
import com.fw.system.web.service.IFwReadPackageLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 红包抽取记录表 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-09-03
 */
@Service
public class FwReadPackageLogServiceImpl extends ServiceImpl<FwReadPackageLogMapper, FwReadPackageLog> implements IFwReadPackageLogService {

}
