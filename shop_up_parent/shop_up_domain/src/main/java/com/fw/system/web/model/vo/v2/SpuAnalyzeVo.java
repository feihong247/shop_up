package com.fw.system.web.model.vo.v2;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("商品分析")
public class SpuAnalyzeVo implements Serializable {
    @ApiModelProperty("商品收藏量")
    private Integer spuLickVal;


    @ApiModelProperty("商品购买量")
    private Integer spuTopVal;


    @ApiModelProperty("商品列表")
    private List<SpuVo> spuVos;




}
