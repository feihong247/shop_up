package com.fw.system.admin.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwOrderMapper;
import com.fw.system.admin.domain.FwOrder;
import com.fw.system.admin.service.IFwOrderService;

/**
 * 订单Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwOrderServiceImpl extends ServiceImpl<FwOrderMapper, FwOrder> implements IFwOrderService
{
    @Autowired
    private FwOrderMapper fwOrderMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询订单
     * 
     * @param id 订单ID
     * @return 订单
     */
    @Override
    public FwOrder selectFwOrderById(String id)
    {
        return fwOrderMapper.selectFwOrderById(id);
    }

    /**
     * 查询订单列表
     * 
     * @param fwOrder 订单
     * @return 订单
     */
    @Override
    public List<FwOrder> selectFwOrderList(FwOrder fwOrder)
    {
        return fwOrderMapper.selectFwOrderList(fwOrder);
    }

    /**
     * 新增订单
     * 
     * @param fwOrder 订单
     * @return 结果
     */
    @Override
    public int insertFwOrder(FwOrder fwOrder)
    {
        fwOrder.setCreateTime(LocalDateTime.now());
        fwOrder.setId(idXD.nextId());
        return fwOrderMapper.insertFwOrder(fwOrder);
    }

    /**
     * 修改订单
     * 
     * @param fwOrder 订单
     * @return 结果
     */
    @Override
    public int updateFwOrder(FwOrder fwOrder)
    {
        fwOrder.setUpdateTime(LocalDateTime.now());
        return fwOrderMapper.updateFwOrder(fwOrder);
    }

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的订单ID
     * @return 结果
     */
    @Override
    public int deleteFwOrderByIds(String[] ids)
    {
        return fwOrderMapper.deleteFwOrderByIds(ids);
    }

    /**
     * 删除订单信息
     * 
     * @param id 订单ID
     * @return 结果
     */
    @Override
    public int deleteFwOrderById(String id)
    {
        return fwOrderMapper.deleteFwOrderById(id);
    }
}
