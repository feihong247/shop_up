package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwIdentityMapper;
import com.fw.system.admin.domain.FwIdentity;
import com.fw.system.admin.service.IFwIdentityService;

/**
 * 身份Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwIdentityServiceImpl extends ServiceImpl<FwIdentityMapper, FwIdentity> implements IFwIdentityService
{
    @Autowired
    private FwIdentityMapper fwIdentityMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询身份
     * 
     * @param id 身份ID
     * @return 身份
     */
    @Override
    public FwIdentity selectFwIdentityById(String id)
    {
        return fwIdentityMapper.selectFwIdentityById(id);
    }

    /**
     * 查询身份列表
     * 
     * @param fwIdentity 身份
     * @return 身份
     */
    @Override
    public List<FwIdentity> selectFwIdentityList(FwIdentity fwIdentity)
    {
        return fwIdentityMapper.selectFwIdentityList(fwIdentity);
    }

    /**
     * 新增身份
     * 
     * @param fwIdentity 身份
     * @return 结果
     */
    @Override
    public int insertFwIdentity(FwIdentity fwIdentity)
    {
        fwIdentity.setCreateTime(DateUtils.getNowDate());
        fwIdentity.setId(idXD.nextId());
        return fwIdentityMapper.insertFwIdentity(fwIdentity);
    }

    /**
     * 修改身份
     * 
     * @param fwIdentity 身份
     * @return 结果
     */
    @Override
    public int updateFwIdentity(FwIdentity fwIdentity)
    {
        fwIdentity.setUpdateTime(DateUtils.getNowDate());
        return fwIdentityMapper.updateFwIdentity(fwIdentity);
    }

    /**
     * 批量删除身份
     * 
     * @param ids 需要删除的身份ID
     * @return 结果
     */
    @Override
    public int deleteFwIdentityByIds(String[] ids)
    {
        return fwIdentityMapper.deleteFwIdentityByIds(ids);
    }

    /**
     * 删除身份信息
     * 
     * @param id 身份ID
     * @return 结果
     */
    @Override
    public int deleteFwIdentityById(String id)
    {
        return fwIdentityMapper.deleteFwIdentityById(id);
    }
}
