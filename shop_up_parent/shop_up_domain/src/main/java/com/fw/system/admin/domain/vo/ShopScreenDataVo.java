package com.fw.system.admin.domain.vo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Api("商户大屏VO")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShopScreenDataVo implements Serializable {

    @ApiModelProperty("label")
    private String label;

    @ApiModelProperty("val")
    private Object val;

    @ApiModelProperty("是否可点击")
    private boolean isClick = false;


    @ApiModelProperty("参数")
    private Object params;


 /*   @ApiModelProperty("总订单量")
    private Integer orderCount;

    @ApiModelProperty("平台新增用户总量")
    private Integer newAddUserCount;

    @ApiModelProperty("待支付订单量")
    private Integer PayOrderCount;

    @ApiModelProperty("待发货订单量")
    private Integer awaitDeliveryOrderCount;

    @ApiModelProperty("待收货订单量")
    private Integer awaitGoodsOrderCount;

    @ApiModelProperty("待评价订单量")
    private Integer awaitEvaluationOrderCount;

    @ApiModelProperty("退货退款订单量")
    private Integer refundOrderCount;

    @ApiModelProperty("已收货订单量")
    private Integer completeOrderCount;

    @ApiModelProperty("竞猜订单量")
    private Integer quizOrderCount;

    @ApiModelProperty("我的质押商甲:线上")
    private BigDecimal onlinePledge;

    @ApiModelProperty("我的质押商甲:线下")
    private BigDecimal offlinePledge;

    @ApiModelProperty("商家的商品收藏量")
    private Integer itemCollectionCount;

    @ApiModelProperty("商家的商铺收藏量")
    private Integer shopCollectionCount;

    @ApiModelProperty("商家线上累计收入金额")
    private BigDecimal onlineMoneyCount;

    @ApiModelProperty("商家线下累计收入金额")
    private BigDecimal offlineMoneyCount;*/


}
