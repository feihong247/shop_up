package com.fw.system.web.model.form.v2;

import com.fw.system.web.model.form.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("订单分页拆线呢")
@Data
public class OrderPageQuery extends PageQuery {
    @ApiModelProperty("订单状态( -1全部，0待付款，1待发货，2待收货,6确认收货，7申请售后)")
    private Integer orderState;

    @ApiModelProperty("售后状态（1申请，2同意，3拒绝）")
    private Integer afterSaleState;

    @ApiModelProperty("是否是竞猜订单,0=否，1=是")
    private Integer isQuiz;
}
