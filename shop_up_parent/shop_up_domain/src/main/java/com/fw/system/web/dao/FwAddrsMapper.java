package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwAddrs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 用户收货地址表 Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
public interface FwAddrsMapper extends BaseMapper<FwAddrs> {
    /**
     * 查询用户收货地址
     *
     * @param id 用户收货地址ID
     * @return 用户收货地址
     */
    public FwAddrs selectFwAddrsById(String id);

    /**
     * 查询用户收货地址列表
     *
     * @param fwAddrs 用户收货地址
     * @return 用户收货地址集合
     */
    public List<FwAddrs> selectFwAddrsList(FwAddrs fwAddrs);

    /**
     * 新增用户收货地址
     *
     * @param fwAddrs 用户收货地址
     * @return 结果
     */
    public int insertFwAddrs(FwAddrs fwAddrs);

    /**
     * 修改用户收货地址
     *
     * @param fwAddrs 用户收货地址
     * @return 结果
     */
    public int updateFwAddrs(FwAddrs fwAddrs);

    /**
     * 删除用户收货地址
     *
     * @param id 用户收货地址ID
     * @return 结果
     */
    public int deleteFwAddrsById(String id);

    /**
     * 批量删除用户收货地址
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwAddrsByIds(String[] ids);
}
