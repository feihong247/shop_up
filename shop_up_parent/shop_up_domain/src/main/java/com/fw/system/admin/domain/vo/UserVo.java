package com.fw.system.admin.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 用户对象 fw_user
 *
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="用户VO", description="用户VOs")
public class UserVo
{


    /** 主键 */
    @ApiModelProperty(value = "主键")
    private String id;

    /** 支付宝唯一标示 */
    @ApiModelProperty(value = "支付宝唯一标示")
    @Excel(name = "支付宝唯一标示")
    private String appAuthToken;

    /** 微信唯一标示 */
    @ApiModelProperty(value = "微信唯一标示")
    @Excel(name = "微信唯一标示")
    private String openId;

    /** 用户名称 */
    @ApiModelProperty(value = "用户名称")
    @Excel(name = "用户名称")
    private String userName;

    /** 用户手机号 */
    @ApiModelProperty(value = "用户手机号")
    @Excel(name = "用户手机号")
    private String phone;

    /** 用户性别 */
    @ApiModelProperty(value = "用户性别")
    @Excel(name = "用户性别",readConverterExp = "1=男,0=女")
    private Integer sex;

    /** 用户月收入 */
    @ApiModelProperty(value = "用户月收入")
    @Excel(name = "用户月收入")
    private String rangeMoney;

    /** 用户标签,存标签名称即可，使用,号分隔开 */
    @ApiModelProperty(value = "用户标签")
    @Excel(name = "用户标签,存标签名称即可，使用,号分隔开")
    private String labels;

    /** 用户生日 */
    @ApiModelProperty(value = "用户生日")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "用户生日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birthday;

    /** 用户编号_平台编号，可以充当邀请码来使用 */
    @ApiModelProperty(value = "用户编号_平台编号")
    @Excel(name = "用户编号_平台编号")
    private String systemHost;

    /** 用户头像 */
    @ApiModelProperty(value = "用户头像")
    @Excel(name = "用户头像")
    private String headImage;

    /** 我的上级编号 */
    @ApiModelProperty(value = "我的上级编号")
    @Excel(name = "我的上级编号")
    private String parentId;

    /** 用户密码 */
    @ApiModelProperty(value = "用户密码")
    @Excel(name = "用户密码")
    private String passWord;

    /** 设备号 */
    @ApiModelProperty(value = "设备号")
    @Excel(name = "设备号")
    private String imei;

    /** 支付密码 */
    @ApiModelProperty(value = "支付密码")
    @Excel(name = "支付密码")
    private String payPassWord;


    /** 用户是否可多级得消证,0:不可,1可 */
    @ApiModelProperty(value = "用户是否可多级得消证")
    @Excel(name = "用户是否可多级得消证", readConverterExp = "0=不可,1=可以")
    private Integer disappearIsMore;

    /** 用户是否可提现,0不可,1可 */
    @ApiModelProperty(value = "用户是否可提现")
    @Excel(name = "用户是否可提现", readConverterExp = "0=不可,1=可以")
    private Integer isWithdraw;

    /** 支付宝真实姓名 */
    @ApiModelProperty(value = "")
    @Excel(name = "支付宝真实姓名")
    private String aliUserName;

    /** 支付宝绑定手机号 */
    @ApiModelProperty(value = "支付宝绑定手机号")
    @Excel(name = "支付宝绑定手机号")
    private String aliPhone;

    /** 积分兑换比例 */
    @ApiModelProperty(value = "支付宝绑定手机号")
    @Excel(name = "积分兑换比例")
    private BigDecimal toRelease;

    /**
     * 管理标识
     * @return
     */
    @ApiModelProperty(value = "管理标识 0 = 普通用户，1= 技术，2= 管理,3 = 原始账号")
    @Excel(name = "管理标识", readConverterExp = "0=普通账号,1=技术账号,2=管理账号,3=原始账号")
    private Integer isAccount;

    /**
     * 身份信息
     */
    @ApiModelProperty("身份信息")
    private List<UljoinVo> uljoinVos = Lists.newArrayList();

}
