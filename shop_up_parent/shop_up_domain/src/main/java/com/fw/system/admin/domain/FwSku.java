package com.fw.system.admin.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 商品sku规格对象 fw_sku
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_sku")
@ApiModel(value="商品sku规格", description="商品sku规格表")
public class FwSku extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "主键")
    @TableId("id")
    private String id;

    /** spu主键 */
    @ApiModelProperty(value = "spu主键")
    @Excel(name = "spu主键")
    @TableField("spu_id")
    private String spuId;

    /** sku价格 */
    @ApiModelProperty(value = "sku价格")
    @Excel(name = "sku价格")
    @TableField("sku_money")
    private BigDecimal skuMoney;

    /** sku规格名称 */
    @ApiModelProperty(value = "sku规格名称")
    @Excel(name = "sku规格名称")
    @TableField("sku_name")
    private String skuName;

    /** sku余量 */
    @ApiModelProperty(value = "sku余量")
    @Excel(name = "sku余量")
    @TableField("sku_balance")
    private Long skuBalance;




}
