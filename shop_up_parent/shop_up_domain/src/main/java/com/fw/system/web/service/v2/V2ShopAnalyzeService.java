package com.fw.system.web.service.v2;

import com.fw.system.web.model.dto.UserDto;
import com.fw.system.web.model.form.v2.OrderManageQuery;
import com.fw.system.web.model.form.v2.ShopAnalyzeQuery;
import com.fw.system.web.model.form.v2.SpuAnalyzeQuery;
import com.fw.system.web.model.vo.v2.OrderAnalyzeVo;
import com.fw.system.web.model.vo.v2.OrderMoneyVo;
import com.fw.system.web.model.vo.v2.ShopAnalyzeVo;
import com.fw.system.web.model.vo.v2.SpuAnalyzeVo;

import java.util.List;

public interface V2ShopAnalyzeService {
    ShopAnalyzeVo shopAnalyze(String shopId, ShopAnalyzeQuery shopAnalyzeQuery);

    SpuAnalyzeVo spuAnalyze(String shopId, SpuAnalyzeQuery spuAnalyzeQuery);

    OrderAnalyzeVo orderAnalyze(UserDto shopDto, ShopAnalyzeQuery shopAnalyzeQuery);

    List<OrderMoneyVo> manageOnlineOrder(String shopId, OrderManageQuery orderManageQuery);

    List<OrderMoneyVo> manageOffOrder(String shopId, OrderManageQuery orderManageQuery);
}
