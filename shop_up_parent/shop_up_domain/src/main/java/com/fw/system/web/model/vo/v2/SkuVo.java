package com.fw.system.web.model.vo.v2;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel("商品规格")
public class SkuVo implements Serializable {



    /**
     * sku价格
     */
    @ApiModelProperty("sku价格")
    private BigDecimal skuMoney;

    /**
     * sku规格名称
     */
    @ApiModelProperty("sku规格名称")
    private String skuName;

    /**
     * sku余量
     */
    @ApiModelProperty("sku余量")
    private Integer skuBalance;


    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

}
