package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwLogShopingAgency;

/**
 * <p>
 * 用户购买代理日志 Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-06-07
 */
public interface FwLogShopingAgencyMapper extends BaseMapper<FwLogShopingAgency> {

}
