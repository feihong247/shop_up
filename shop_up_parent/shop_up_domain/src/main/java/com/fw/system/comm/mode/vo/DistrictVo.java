package com.fw.system.comm.mode.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("地域信息")
public class DistrictVo implements Serializable {

    @ApiModelProperty("id")
    private Integer id;

    @ApiModelProperty("父ID,pid = 0 为 总节点")
    private Integer pid;

    @ApiModelProperty("区划编码")
    private String code;

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("0:省/自治区/直辖市 1:市级 2:县级")
    private Integer level;

    @ApiModelProperty("经纬度")
    private String center;


}
