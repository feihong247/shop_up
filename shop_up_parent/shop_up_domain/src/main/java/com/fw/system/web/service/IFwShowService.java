package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwShow;

/**
 * <p>
 * 用户消费产出额度 服务类
 * </p>
 *
 * @author
 * @since 2021-06-19
 */
public interface IFwShowService extends IService<FwShow> {

}
