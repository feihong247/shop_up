package com.fw.system.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.additional.query.impl.QueryChainWrapper;
import com.fw.common.IdXD;
import com.fw.constant.Constant;
import com.fw.enums.LogsModelEnum;
import com.fw.enums.LogsTypeEnum;
import com.fw.system.web.dao.FwWithdrawShangjiaMapper;
import com.fw.system.web.model.entity.FwShangjia;
import com.fw.system.web.model.entity.FwWithdrawShangjia;
import com.fw.system.web.service.IFwLogsService;
import com.fw.system.web.service.IFwShangjiaService;
import com.fw.system.web.service.IFwWithdrawShangjiaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 积分销毁商甲表 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-07-08
 */
@Service
public class FwWithdrawShangjiaServiceImpl extends ServiceImpl<FwWithdrawShangjiaMapper, FwWithdrawShangjia> implements IFwWithdrawShangjiaService {
    @Autowired
    private FwWithdrawShangjiaMapper fwWithdrawShangjiaMapper;
    @Autowired
    private IFwShangjiaService fwShangjiaService;
    @Autowired
    private IFwLogsService logsService;
    @Autowired
    private IdXD idXD;
    @Override
    public void updateIntegral(BigDecimal integral,String userId) {
        List<FwWithdrawShangjia> fwWithdrawShangjias = fwWithdrawShangjiaMapper.selectList(new QueryWrapper<>());
        FwShangjia consumer = fwShangjiaService.getOne(Wrappers.<FwShangjia>lambdaQuery().eq(FwShangjia::getId, Constant.ShangJia.CONSUME_ID));
        BigDecimal presentPrice = consumer.getPresentPrice();
        if(fwWithdrawShangjias.size()>0){
            FwWithdrawShangjia fwWithdrawShangjia = fwWithdrawShangjias.get(0);
            BigDecimal integralHaving = fwWithdrawShangjia.getIntegral();
            logsService.saveLogs(userId,userId, LogsTypeEnum.INTEGRAL_DESTRUCTION_PRE.getTypeName(), LogsModelEnum.SHANG_JIA_MODEL.getModelName(), "积分提现销毁消费商甲" , integral.divide(presentPrice,0,BigDecimal.ROUND_FLOOR).toString());
            integralHaving=integralHaving.add(integral.divide(presentPrice,0,BigDecimal.ROUND_FLOOR));
            fwWithdrawShangjia.setIntegral(integralHaving);
            fwWithdrawShangjia.setUpdateBy(userId);
            fwWithdrawShangjia.setUpdateTime(LocalDateTime.now());
            fwWithdrawShangjiaMapper.updateById(fwWithdrawShangjia);
        }else{
            FwWithdrawShangjia fwWithdrawShangjia=new FwWithdrawShangjia();
            logsService.saveLogs(userId,userId, LogsTypeEnum.INTEGRAL_DESTRUCTION_PRE.getTypeName(), LogsModelEnum.SHANG_JIA_MODEL.getModelName(), "积分提现销毁消费商甲" , integral.divide(presentPrice,0,BigDecimal.ROUND_FLOOR).toString());
            fwWithdrawShangjia.setIntegral(integral.divide(presentPrice,0,BigDecimal.ROUND_FLOOR));
            fwWithdrawShangjia.setUpdateBy(userId);
            fwWithdrawShangjia.setUpdateTime(LocalDateTime.now());
            fwWithdrawShangjia.setId(idXD.nextId());
            fwWithdrawShangjia.setDestoryShangjia(new BigDecimal(0));
            fwWithdrawShangjia.setIntegralShangjia(new BigDecimal(0));
            fwWithdrawShangjiaMapper.insert(fwWithdrawShangjia);
        }
    }
}
