package com.fw.system.web.model.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel("商铺单独查询条件")
public class ShopQuickQuery  implements Serializable {

    /**
     * 商铺编号
     */

    @ApiModelProperty(value = "商铺主键",required = true)
    @NotBlank(message = "商铺主键 不可为空")
    private String shopId;

    /**
     * 纬度
     */
    @ApiModelProperty(value = "当前用户的纬度",required = true)
    @NotNull(message = "纬度 不可为空" )
    private BigDecimal latitude;

    /**
     * 经度
     */
    @ApiModelProperty(value = "当前用户的经度",required = true)
    @NotNull(message = "经度 不可为空")
    private BigDecimal longitude;
}
