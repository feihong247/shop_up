package com.fw.system.web.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import com.fw.common.IdXD;
import com.fw.system.web.dao.FwUgJoinMapper;
import com.fw.system.web.model.entity.FwUgJoin;
import com.fw.system.web.service.IFwUgJoinService;
import com.fw.system.web.service.IFwUserService;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 用户竞猜中间Service业务层处理
 * 
 * @author yanwei
 * @date 2021-06-23
 */
@Service
public class FwUgJoinServiceImpl extends ServiceImpl<FwUgJoinMapper, FwUgJoin> implements IFwUgJoinService
{
    @Autowired
    private FwUgJoinMapper fwUgJoinMapper;
    @Autowired
    private IFwUserService userService;

    @Autowired
    private IdXD idXD;

    /**
     * 查询用户竞猜中间
     * 
     * @param ugId 用户竞猜中间ID
     * @return 用户竞猜中间
     */
    @Override
    public FwUgJoin selectFwUgJoinById(String ugId) {
        FwUgJoin fwUgJoin = fwUgJoinMapper.selectById(ugId);
        fwUgJoin.setUser( userService.getById(fwUgJoin.getUserId()) );
        return fwUgJoin;
    }

    /**
     * 查询用户竞猜中间列表
     * 
     * @param fwUgJoin 用户竞猜中间
     * @return 用户竞猜中间
     */
    @Override
    public List<FwUgJoin> selectFwUgJoinList(FwUgJoin fwUgJoin)
    {
        return fwUgJoinMapper.selectFwUgJoinList(fwUgJoin);
    }

    /**
     * 新增用户竞猜中间
     * 
     * @param fwUgJoin 用户竞猜中间
     * @return 结果
     */
    @Override
    public int insertFwUgJoin(FwUgJoin fwUgJoin)
    {
        fwUgJoin.setCreateTime(LocalDateTime.now());
        fwUgJoin.setUgId(idXD.nextId());
        return fwUgJoinMapper.insertFwUgJoin(fwUgJoin);
    }

    /**
     * 修改用户竞猜中间
     * 
     * @param fwUgJoin 用户竞猜中间
     * @return 结果
     */
    @Override
    public int updateFwUgJoin(FwUgJoin fwUgJoin)
    {
        fwUgJoin.setUpdateTime(LocalDateTime.now());
        return fwUgJoinMapper.updateFwUgJoin(fwUgJoin);
    }

    /**
     * 批量删除用户竞猜中间
     * 
     * @param ugIds 需要删除的用户竞猜中间ID
     * @return 结果
     */
    @Override
    public int deleteFwUgJoinByIds(String[] ugIds)
    {
        return fwUgJoinMapper.deleteFwUgJoinByIds(ugIds);
    }

    /**
     * 删除用户竞猜中间信息
     * 
     * @param ugId 用户竞猜中间ID
     * @return 结果
     */
    @Override
    public int deleteFwUgJoinById(String ugId)
    {
        return fwUgJoinMapper.deleteFwUgJoinById(ugId);
    }
}
