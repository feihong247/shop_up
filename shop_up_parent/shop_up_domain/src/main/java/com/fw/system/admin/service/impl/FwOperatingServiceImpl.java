package com.fw.system.admin.service.impl;

import java.util.List;

import com.fw.common.IdXD;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwOperatingMapper;
import com.fw.system.admin.domain.FwOperating;
import com.fw.system.admin.service.IFwOperatingService;

/**
 * 商家经营平台分类Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-19
 */
@Service
public class FwOperatingServiceImpl extends ServiceImpl<FwOperatingMapper, FwOperating> implements IFwOperatingService
{
    @Autowired
    private FwOperatingMapper fwOperatingMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询商家经营平台分类
     * 
     * @param id 商家经营平台分类ID
     * @return 商家经营平台分类
     */
    @Override
    public FwOperating selectFwOperatingById(String id)
    {
        return fwOperatingMapper.selectFwOperatingById(id);
    }

    /**
     * 查询商家经营平台分类列表
     * 
     * @param fwOperating 商家经营平台分类
     * @return 商家经营平台分类
     */
    @Override
    public List<FwOperating> selectFwOperatingList(FwOperating fwOperating)
    {
        return fwOperatingMapper.selectFwOperatingList(fwOperating);
    }

    /**
     * 新增商家经营平台分类
     * 
     * @param fwOperating 商家经营平台分类
     * @return 结果
     */
    @Override
    public int insertFwOperating(FwOperating fwOperating)
    {
        fwOperating.setId(idXD.nextId());
        return fwOperatingMapper.insertFwOperating(fwOperating);
    }

    /**
     * 修改商家经营平台分类
     * 
     * @param fwOperating 商家经营平台分类
     * @return 结果
     */
    @Override
    public int updateFwOperating(FwOperating fwOperating)
    {
        return fwOperatingMapper.updateFwOperating(fwOperating);
    }

    /**
     * 批量删除商家经营平台分类
     * 
     * @param ids 需要删除的商家经营平台分类ID
     * @return 结果
     */
    @Override
    public int deleteFwOperatingByIds(String[] ids)
    {
        return fwOperatingMapper.deleteFwOperatingByIds(ids);
    }

    /**
     * 删除商家经营平台分类信息
     * 
     * @param id 商家经营平台分类ID
     * @return 结果
     */
    @Override
    public int deleteFwOperatingById(String id)
    {
        return fwOperatingMapper.deleteFwOperatingById(id);
    }
}
