package com.fw.system.web.model.vo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@ApiModel("验证结果vo")
public class AliBindResultVo implements Serializable {
    @ApiModelProperty("校验是否一致：T-一致、F-不一致")
    private String passed;
    @ApiModelProperty("校验不一致时，描述不一致的原因    示例值: 姓名不一致")
    private String fail_reason;



}
