package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwUljoin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户和身份中间表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwUljoinMapper extends BaseMapper<FwUljoin> {

    int countVip(@Param("userId") String userId);
}
