package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 购物车Mapper接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface FwCartMapper extends BaseMapper<FwCart>
{
    /**
     * 查询购物车
     * 
     * @param id 购物车ID
     * @return 购物车
     */
    public FwCart selectFwCartById(String id);

    /**
     * 查询购物车列表
     * 
     * @param fwCart 购物车
     * @return 购物车集合
     */
    public List<FwCart> selectFwCartList(FwCart fwCart);

    /**
     * 新增购物车
     * 
     * @param fwCart 购物车
     * @return 结果
     */
    public int insertFwCart(FwCart fwCart);

    /**
     * 修改购物车
     * 
     * @param fwCart 购物车
     * @return 结果
     */
    public int updateFwCart(FwCart fwCart);

    /**
     * 删除购物车
     * 
     * @param id 购物车ID
     * @return 结果
     */
    public int deleteFwCartById(String id);

    /**
     * 批量删除购物车
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwCartByIds(String[] ids);
}
