package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwOffline;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.vo.v2.OfflineVo;
import com.fw.system.web.model.vo.v2.OrderMoneyVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 线下消费记录表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwOfflineMapper extends BaseMapper<FwOffline> {

    List<OfflineVo> pageList(@Param("shopId") String shopId);

    List<OrderMoneyVo> manageOffOrder(@Param("shopId") String shopId,@Param("queryRange") Integer queryRange,@Param("limit") Integer limit);
}
