package com.fw.system.web.model.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author:yanwei
 * @Date: 2020/8/5 - 15:38
 */
@ApiModel("示例Example DTO")
@Data
public class ExampleDto implements Serializable {
}
