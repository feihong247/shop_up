package com.fw.system.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

import com.fw.system.admin.domain.FwUsjoin;

/**
 * 我的商铺收藏中间Service接口
 *
 * @author yanwei
 * @date 2021-05-24
 */
public interface IFwUsjoinService extends IService<FwUsjoin> {
    /**
     * 查询我的商铺收藏中间
     *
     * @param id 我的商铺收藏中间ID
     * @return 我的商铺收藏中间
     */
    public FwUsjoin selectFwUsjoinById(String id);

    /**
     * 查询我的商铺收藏中间列表
     *
     * @param fwUsjoin 我的商铺收藏中间
     * @return 我的商铺收藏中间集合
     */
    public List<FwUsjoin> selectFwUsjoinList(FwUsjoin fwUsjoin);

    /**
     * 新增我的商铺收藏中间
     *
     * @param fwUsjoin 我的商铺收藏中间
     * @return 结果
     */
    public int insertFwUsjoin(FwUsjoin fwUsjoin);

    /**
     * 修改我的商铺收藏中间
     *
     * @param fwUsjoin 我的商铺收藏中间
     * @return 结果
     */
    public int updateFwUsjoin(FwUsjoin fwUsjoin);

    /**
     * 批量删除我的商铺收藏中间
     *
     * @param ids 需要删除的我的商铺收藏中间ID
     * @return 结果
     */
    public int deleteFwUsjoinByIds(String[] ids);

    /**
     * 删除我的商铺收藏中间信息
     *
     * @param id 我的商铺收藏中间ID
     * @return 结果
     */
    public int deleteFwUsjoinById(String id);
}
