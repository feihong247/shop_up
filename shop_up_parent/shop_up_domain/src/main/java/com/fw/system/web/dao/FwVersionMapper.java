package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwVersion;

/**
 * <p>
 * 版本表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-09-08
 */
public interface FwVersionMapper extends BaseMapper<FwVersion> {

}
