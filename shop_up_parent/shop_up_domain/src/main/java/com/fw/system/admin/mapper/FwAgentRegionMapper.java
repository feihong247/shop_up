package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwAgentRegion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 代理区域Mapper接口
 * 
 * @author yanwei
 * @date 2021-07-23
 */
public interface FwAgentRegionMapper extends BaseMapper<FwAgentRegion>
{

    /**
     * 查询代理区域
     * 
     * @param id 代理区域ID
     * @return 代理区域
     */
    public FwAgentRegion selectFwAgentRegionById(String id);

    /**
     * 查询代理区域列表
     * 
     * @param fwAgentRegion 代理区域
     * @return 代理区域集合
     */
    public List<FwAgentRegion> selectFwAgentRegionList(FwAgentRegion fwAgentRegion);

    /**
     * 新增代理区域
     * 
     * @param fwAgentRegion 代理区域
     * @return 结果
     */
    public int insertFwAgentRegion(FwAgentRegion fwAgentRegion);

    /**
     * 修改代理区域
     * 
     * @param fwAgentRegion 代理区域
     * @return 结果
     */
    public int updateFwAgentRegion(FwAgentRegion fwAgentRegion);

    /**
     * 删除代理区域
     * 
     * @param id 代理区域ID
     * @return 结果
     */
    public int deleteFwAgentRegionById(String id);

    /**
     * 批量删除代理区域
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwAgentRegionByIds(String[] ids);
}
