package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

import com.fw.enums.LogsModelEnum;
import com.fw.enums.LogsTypeEnum;
import com.fw.system.admin.domain.FwLogs;
import com.fw.system.admin.domain.vo.DisappearLogsVo;
import com.fw.system.admin.domain.vo.UserLogsDisappearCountVo;

/**
 * 业务日志Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwLogsService extends IService<FwLogs>
{
    /**
     * 查询业务日志
     * 
     * @param id 业务日志ID
     * @return 业务日志
     */
    public FwLogs selectFwLogsById(String id);

    /**
     * 查询业务日志列表
     * 
     * @param fwLogs 业务日志
     * @return 业务日志集合
     */
    public List<FwLogs> selectFwLogsList(FwLogs fwLogs);

    /**
     * 新增业务日志
     * 
     * @param fwLogs 业务日志
     * @return 结果
     */
    public int insertFwLogs(FwLogs fwLogs);

    /**
     * 修改业务日志
     * 
     * @param fwLogs 业务日志
     * @return 结果
     */
    public int updateFwLogs(FwLogs fwLogs);

    /**
     * 批量删除业务日志
     * 
     * @param ids 需要删除的业务日志ID
     * @return 结果
     */
    public int deleteFwLogsByIds(String[] ids);

    /**
     * 删除业务日志信息
     * 
     * @param id 业务日志ID
     * @return 结果
     */
    public int deleteFwLogsById(String id);

    void saveLogs(String createId, String buiId, String isType, String modelName, String jsonTxt, String formInfo);

    void saveLogs(String createId, String buiId, LogsTypeEnum logsTypeEnum, LogsModelEnum logsModel, String jsonTxt, String formInfo);
    /**
     * 年商甲产出
     * @return
     */
    public List<FwLogs> getShangJiaYearTotal();

    /**
     * 月商甲产出
     * @return
     */
    public List<FwLogs> getShangJiaMonthTotal();
    /**
     * 日商甲产出
     * @return
     */
    public List<FwLogs> getShangJiaDayTotal();


    List<FwLogs> getShangJiaLtOneDay(String userId);

    List<UserLogsDisappearCountVo> userLogsDisappearCount(FwLogs fwLogs);

    List<FwLogs> userLogsDisappearInfos(FwLogs fwLogs);

    List<UserLogsDisappearCountVo> userLogsIntegralCount(FwLogs fwLogs);

    List<FwLogs> userLogsIntegralInfos(FwLogs fwLogs);

    Double toDayUserLogsIntegralCount(FwLogs fwLogs);
    Double toDayUserLogsDisappearCount(FwLogs fwLogs);

    List<DisappearLogsVo> selectCommLogsList(FwLogs fwLogs);

    List<DisappearLogsVo> selectCommLogsInList(FwLogs fwLogs);
}
