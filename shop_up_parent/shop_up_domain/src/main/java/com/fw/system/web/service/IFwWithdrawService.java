package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwWithdraw;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户提现表 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface IFwWithdrawService extends IService<FwWithdraw> {

}
