package com.fw.system.admin.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * 商铺对象 fw_shop
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_shop")
@ApiModel(value="商铺", description="商铺表")
public class FwShop implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 编号_平台管理的用户ID */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "编号_平台管理的用户ID")
    @TableField("user_id")
    private String userId;

    /** 商铺质押多少商甲 */
    @ApiModelProperty(value = "编号_平台管理的用户ID")
    @Excel(name = "商铺质押多少商甲")
    @TableField("shangjia")
    private BigDecimal shangjia;

    /** 经营类型,存储类型名称 */
    @ApiModelProperty(value = "商铺质押多少商甲")
    @Excel(name = "经营类型,存储类型名称")
    @TableField("operating_name")
    private String operatingName;

    /** 管理人名称 */
    @ApiModelProperty(value = "经营类型,存储类型名称")
    @Excel(name = "管理人名称")
    @TableField("manager_name")
    private String managerName;

    /** 管理人电话，数据需要脱敏 */
    @ApiModelProperty(value = "管理人名称")
    @Excel(name = "管理人电话，数据需要脱敏")
    @TableField("manager_phone")
    private String managerPhone;

    /** 系统商户编号 */
    @ApiModelProperty(value = "管理人电话，数据需要脱敏")
    @Excel(name = "系统商户编号")
    @TableField("shop_host")
    private String shopHost;

    /** 商铺名称 */
    @ApiModelProperty(value = "系统商户编号")
    @Excel(name = "商铺名称")
    @TableField("shop_name")
    private String shopName;

    /** 商铺简介 */
    @ApiModelProperty(value = "商铺名称")
    @Excel(name = "商铺简介")
    @TableField("shop_info")
    private String shopInfo;

    /** 商铺营业执照图片 */
    @ApiModelProperty(value = "商铺简介")
    @Excel(name = "商铺营业执照图片")
    @TableField("shop_image")
    private String shopImage;

    /** 数据设定-范围m为单位 */
    @ApiModelProperty(value = "商铺营业执照图片")
    @Excel(name = "数据设定-范围m为单位")
    @TableField("data_range")
    private String dataRange;

    /** 数据设定-性别,0=女，1=男，2=不限 */
    @ApiModelProperty(value = "数据设定-范围m为单位")
    @Excel(name = "数据设定-性别,0=女，1=男，2=不限")
    @TableField("data_sex")
    private Integer dataSex;

    /** 数据设定-年龄设定 */
    @ApiModelProperty(value = "数据设定-性别,0=女，1=男，2=不限")
    @Excel(name = "数据设定-年龄设定")
    @TableField("data_age")
    private String dataAge;

    /** 数据设定-收入设定 */
    @ApiModelProperty(value = "数据设定-年龄设定")
    @Excel(name = "数据设定-收入设定")
    @TableField("data_income")
    private String dataIncome;

    /** 商铺状态,0-待审核，1-审核通过，2-审核失败，驳回 */
    @ApiModelProperty(value = "数据设定-收入设定")
    @Excel(name = "商铺状态,0-待审核，1-审核通过，2-审核失败，驳回")
    @TableField("status")
    private Integer status;

    /** 商铺审核状态为2时，次字段生效 */
    @ApiModelProperty(value = "商铺状态,0-待审核，1-审核通过，2-审核失败，驳回")
    @Excel(name = "商铺审核状态为2时，次字段生效")
    @TableField("turndown")
    private String turndown;


    /**
     * 营业时间
     */
    @TableField("operating_time")
    @Excel(name = "营业时间")
    private  String operatingTime;


    /**
     * 商铺电话
     */
    @TableField("shop_mobile")
    @Excel(name = "商铺电话")
    private String shopMobile;

    /**
     * 商铺公告
     */
    @TableField("shop_notice")
    @Excel(name = "商铺公告")
    private String shopNotice;

    /**
     * 商铺logo
     */
    @TableField("shop_logo")
    @Excel(name = "商铺logo")
    private String shopLogo;

    /**
     * 线下消费比例
     */

    @TableField("line_customer")
    @Excel(name = "线下消费比例")
    private BigDecimal lineCustomer;

    /**
     * 商家  线下支付图谱
     */
    @TableField("shop_pay")
    @Excel(name = "线下支付图谱")
    private String shopPay;

    /**
     * 线下 商品质押商甲
     */
    @TableField("offline_shangjia")
    @Excel(name = "offline_shangjia")
    private BigDecimal offlineShangjia;

    /**
     * 商铺 人均消费
     */
    @TableField("shop_svg")
    private String shopSvg;

    /**
     * 商铺评分
     */
    @TableField("shop_score")
    private String shopScore;

    /**
     * 商铺封面
     */
    @TableField("shop_cover")
    private String shopCover;


    /**
     * 商铺客服二维码
     */
    @TableField("shop_qr")
    private String shopQr;

    /** 纬度 */
    @ApiModelProperty(value = "纬度")
    @Excel(name = "纬度")
    @TableField(value = "latitude",exist = false)
    private BigDecimal latitude;

    /** 经度 */
    @ApiModelProperty(value = "经度")
    @Excel(name = "经度")
    @TableField(value = "longitude",exist = false)
    private BigDecimal longitude;

    /** 创建者 */
    @TableField(value = "create_by")
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time")
    private Date createTime;

    /** 更新者 */
    @TableField(value = "update_by")
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 商铺线上质押多少商甲
     */
    @ApiModelProperty(value = "身份证-正面",required = true)
    @TableField("real_just")
    private String realJust;

    /**
     * 商铺线下质押多少商甲
     */
    @ApiModelProperty(value = "身份证-反面",required = true)
    @TableField("real_back")
    private String realBack;



}
