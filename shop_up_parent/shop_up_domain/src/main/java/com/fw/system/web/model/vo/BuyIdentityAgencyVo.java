package com.fw.system.web.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 购买代理身份数据
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
@Api(tags = "购买代理身份数据")
public class BuyIdentityAgencyVo {

    /**
     * 当前用户的身份信息
     */
    private String identity;

    /**
     * 身份信息列表
     */
    private List<IdentityAgencyVo> identityAgencyVos;

}
