package com.fw.system.web.model.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品sku规格表
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel("商品sku规格")
public class FwSku extends Model<FwSku> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("id")
    @ApiModelProperty("主键")
    private String id;

    /**
     * spu主键
     */
    @TableField("spu_id")
    @ApiModelProperty("spu主键")
    private String spuId;

    /**
     * sku价格
     */
    @TableField("sku_money")
    @ApiModelProperty("sku价格")
    private BigDecimal skuMoney;

    /**
     * sku规格名称
     */
    @TableField("sku_name")
    @ApiModelProperty("sku规格名称")
    private String skuName;

    /**
     * sku余量
     */
    @TableField("sku_balance")
    @ApiModelProperty("sku余量")
    private Integer skuBalance;

    /**
     * 创建人
     */
    @TableField("create_by")
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @TableField("update_by")
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    public void createD(String createId){
        this.createBy = createId;
        this.createTime = LocalDateTime.now();
        if (this.createTime != null)
            this.updateTime = this.createTime;
        this.updateBy = this.createBy;
    }

    public void updateD(String updateId){
        this.updateBy = updateId;
        this.updateTime = LocalDateTime.now();
    }
}
