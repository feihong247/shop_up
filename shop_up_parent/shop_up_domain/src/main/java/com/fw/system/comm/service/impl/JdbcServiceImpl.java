package com.fw.system.comm.service.impl;

import com.fw.system.comm.service.JdbcService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class JdbcServiceImpl implements JdbcService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    // 查询 商户端 后台管理的密码。
    @Override
    public String findAdminPwd(String phone){
     String sql = " select password from sys_user where  phonenumber = ? ";
     try {
         return jdbcTemplate.queryForObject(sql,String.class,phone);
     }catch (Exception e){
         return null;
     }

    }
}
