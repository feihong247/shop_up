package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwReadPackageTask;

/**
 * <p>
 * 红包任务类型列表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-09-03
 */
public interface FwReadPackageTaskMapper extends BaseMapper<FwReadPackageTask> {

}
