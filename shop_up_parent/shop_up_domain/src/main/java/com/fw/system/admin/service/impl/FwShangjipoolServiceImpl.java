package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwShangjipoolMapper;
import com.fw.system.admin.domain.FwShangjipool;
import com.fw.system.admin.service.IFwShangjipoolService;

/**
 * 商甲池Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwShangjipoolServiceImpl extends ServiceImpl<FwShangjipoolMapper, FwShangjipool> implements IFwShangjipoolService
{
    @Autowired
    private FwShangjipoolMapper fwShangjipoolMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询商甲池
     * 
     * @param id 商甲池ID
     * @return 商甲池
     */
    @Override
    public FwShangjipool selectFwShangjipoolById(String id)
    {
        return fwShangjipoolMapper.selectFwShangjipoolById(id);
    }

    /**
     * 查询商甲池列表
     * 
     * @param fwShangjipool 商甲池
     * @return 商甲池
     */
    @Override
    public List<FwShangjipool> selectFwShangjipoolList(FwShangjipool fwShangjipool)
    {
        return fwShangjipoolMapper.selectFwShangjipoolList(fwShangjipool);
    }

    /**
     * 新增商甲池
     * 
     * @param fwShangjipool 商甲池
     * @return 结果
     */
    @Override
    public int insertFwShangjipool(FwShangjipool fwShangjipool)
    {
        fwShangjipool.setCreateTime(DateUtils.getNowDate());
        fwShangjipool.setId(idXD.nextId());
        return fwShangjipoolMapper.insertFwShangjipool(fwShangjipool);
    }

    /**
     * 修改商甲池
     * 
     * @param fwShangjipool 商甲池
     * @return 结果
     */
    @Override
    public int updateFwShangjipool(FwShangjipool fwShangjipool)
    {
        fwShangjipool.setUpdateTime(DateUtils.getNowDate());
        return fwShangjipoolMapper.updateFwShangjipool(fwShangjipool);
    }

    /**
     * 批量删除商甲池
     * 
     * @param ids 需要删除的商甲池ID
     * @return 结果
     */
    @Override
    public int deleteFwShangjipoolByIds(String[] ids)
    {
        return fwShangjipoolMapper.deleteFwShangjipoolByIds(ids);
    }

    /**
     * 删除商甲池信息
     * 
     * @param id 商甲池ID
     * @return 结果
     */
    @Override
    public int deleteFwShangjipoolById(String id)
    {
        return fwShangjipoolMapper.deleteFwShangjipoolById(id);
    }
}
