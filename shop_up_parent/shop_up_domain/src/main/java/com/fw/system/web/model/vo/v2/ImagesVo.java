package com.fw.system.web.model.vo.v2;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("图片")
public class ImagesVo implements Serializable {

    /**
     * 图片链接
     */
    @ApiModelProperty("图片链接")
    private String linkUrl;

}
