package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwNotice;

/**
 * 系统公告&通知Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwNoticeService extends IService<FwNotice>
{
    /**
     * 查询系统公告&通知
     * 
     * @param id 系统公告&通知ID
     * @return 系统公告&通知
     */
    public FwNotice selectFwNoticeById(String id);

    /**
     * 查询系统公告&通知列表
     * 
     * @param fwNotice 系统公告&通知
     * @return 系统公告&通知集合
     */
    public List<FwNotice> selectFwNoticeList(FwNotice fwNotice);

    /**
     * 新增系统公告&通知
     * 
     * @param fwNotice 系统公告&通知
     * @return 结果
     */
    public int insertFwNotice(FwNotice fwNotice);

    /**
     * 修改系统公告&通知
     * 
     * @param fwNotice 系统公告&通知
     * @return 结果
     */
    public int updateFwNotice(FwNotice fwNotice);

    /**
     * 批量删除系统公告&通知
     * 
     * @param ids 需要删除的系统公告&通知ID
     * @return 结果
     */
    public int deleteFwNoticeByIds(String[] ids);

    /**
     * 删除系统公告&通知信息
     * 
     * @param id 系统公告&通知ID
     * @return 结果
     */
    public int deleteFwNoticeById(String id);
}
