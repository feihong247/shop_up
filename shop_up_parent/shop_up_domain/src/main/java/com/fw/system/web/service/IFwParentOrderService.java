package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwParentOrder;

public interface IFwParentOrderService extends IService<FwParentOrder> {
}
