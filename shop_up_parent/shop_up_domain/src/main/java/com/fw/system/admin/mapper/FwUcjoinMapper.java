package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwUcjoin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 我的收藏中间Mapper接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface FwUcjoinMapper extends BaseMapper<FwUcjoin>
{
    /**
     * 查询我的收藏中间
     * 
     * @param id 我的收藏中间ID
     * @return 我的收藏中间
     */
    public FwUcjoin selectFwUcjoinById(String id);

    /**
     * 查询我的收藏中间列表
     * 
     * @param fwUcjoin 我的收藏中间
     * @return 我的收藏中间集合
     */
    public List<FwUcjoin> selectFwUcjoinList(FwUcjoin fwUcjoin);

    /**
     * 新增我的收藏中间
     * 
     * @param fwUcjoin 我的收藏中间
     * @return 结果
     */
    public int insertFwUcjoin(FwUcjoin fwUcjoin);

    /**
     * 修改我的收藏中间
     * 
     * @param fwUcjoin 我的收藏中间
     * @return 结果
     */
    public int updateFwUcjoin(FwUcjoin fwUcjoin);

    /**
     * 删除我的收藏中间
     * 
     * @param id 我的收藏中间ID
     * @return 结果
     */
    public int deleteFwUcjoinById(String id);

    /**
     * 批量删除我的收藏中间
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwUcjoinByIds(String[] ids);
}
