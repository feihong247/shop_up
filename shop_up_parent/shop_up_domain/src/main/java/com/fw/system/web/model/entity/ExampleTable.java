package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author yanwei
 * @since 2020-08-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ExampleTable extends Model<ExampleTable> {

    private static final long serialVersionUID=1L;

    /**
     * 主键ID
     */
    @TableId("id")
    private String id;

    /**
     * 名称
     */
    @TableField("example_name")
    private String exampleName;

    /**
     * 类型
     */
    @TableField("example_type")
    private Integer exampleType;

    /**
     * 创建日期
     */
    @TableField("create_date")
    private LocalDateTime createDate;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
