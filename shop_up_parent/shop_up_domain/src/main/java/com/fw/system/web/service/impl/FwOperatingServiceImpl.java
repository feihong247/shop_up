package com.fw.system.web.service.impl;

import com.fw.system.web.dao.FwOperatingMapper;
import com.fw.system.web.model.entity.FwOperating;
import com.fw.system.web.model.vo.OperatingVo;
import com.fw.system.web.service.IFwOperatingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 商家经营平台分类 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-05-19
 */
@Service
public class FwOperatingServiceImpl extends ServiceImpl<FwOperatingMapper, FwOperating> implements IFwOperatingService {

    @Autowired
    private FwOperatingMapper operatingMapper;

    @Override
    public List<OperatingVo> findAll() {
        return operatingMapper.findAll();
    }

}
