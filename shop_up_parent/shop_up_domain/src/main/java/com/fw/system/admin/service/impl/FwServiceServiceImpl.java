package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwServiceMapper;
import com.fw.system.admin.domain.FwService;
import com.fw.system.admin.service.IFwServiceService;

/**
 * 在线客服Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwServiceServiceImpl extends ServiceImpl<FwServiceMapper, FwService> implements IFwServiceService
{
    @Autowired
    private FwServiceMapper fwServiceMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询在线客服
     * 
     * @param id 在线客服ID
     * @return 在线客服
     */
    @Override
    public FwService selectFwServiceById(String id)
    {
        return fwServiceMapper.selectFwServiceById(id);
    }

    /**
     * 查询在线客服列表
     * 
     * @param fwService 在线客服
     * @return 在线客服
     */
    @Override
    public List<FwService> selectFwServiceList(FwService fwService)
    {
        return fwServiceMapper.selectFwServiceList(fwService);
    }

    /**
     * 新增在线客服
     * 
     * @param fwService 在线客服
     * @return 结果
     */
    @Override
    public int insertFwService(FwService fwService)
    {
        fwService.setCreateTime(DateUtils.getNowDate());
        fwService.setId(idXD.nextId());
        return fwServiceMapper.insertFwService(fwService);
    }

    /**
     * 修改在线客服
     * 
     * @param fwService 在线客服
     * @return 结果
     */
    @Override
    public int updateFwService(FwService fwService)
    {
        fwService.setUpdateTime(DateUtils.getNowDate());
        return fwServiceMapper.updateFwService(fwService);
    }

    /**
     * 批量删除在线客服
     * 
     * @param ids 需要删除的在线客服ID
     * @return 结果
     */
    @Override
    public int deleteFwServiceByIds(String[] ids)
    {
        return fwServiceMapper.deleteFwServiceByIds(ids);
    }

    /**
     * 删除在线客服信息
     * 
     * @param id 在线客服ID
     * @return 结果
     */
    @Override
    public int deleteFwServiceById(String id)
    {
        return fwServiceMapper.deleteFwServiceById(id);
    }
}
