package com.fw.system.web.model.vo;

import com.fw.system.web.model.entity.FwLogs;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@Api(tags = "代理管理-收入明细Vo")
public class AgentDetailVo {

    @ApiModelProperty("省")
    private String province;

    @ApiModelProperty("市")
    private String city;

    @ApiModelProperty("区")
    private String area;

    @ApiModelProperty("消证收入")
    private BigDecimal disappear;

    @ApiModelProperty("消证收入明细")
    private List<FwLogs> fwLogs;

}
