package com.fw.system.web.service.v2;

import com.fw.system.web.model.entity.FwOneAreaItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 一县一品表 服务类
 * </p>
 *
 * @author  
 * @since 2021-09-29
 */
public interface IFwOneAreaItemService extends IService<FwOneAreaItem> {

}
