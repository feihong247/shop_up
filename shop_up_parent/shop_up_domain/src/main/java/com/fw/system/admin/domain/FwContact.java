package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 关于我们对象 fw_contact
 *
 * @author yanwei
 * @date 2021-05-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_contact")
@ApiModel(value = "关于我们", description = "关于我们")
public class FwContact extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键 主键
     */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /**
     * 标题
     */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "标题")
    @TableField("title")
    private String title;

    /**
     * 内容富文本
     */
    @ApiModelProperty(value = "标题")
    @Excel(name = "内容富文本")
    @TableField("content_text")
    private String contentText;


}
