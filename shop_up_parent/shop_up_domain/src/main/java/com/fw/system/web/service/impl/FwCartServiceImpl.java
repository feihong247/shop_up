package com.fw.system.web.service.impl;

import com.fw.system.web.model.entity.FwCart;
import com.fw.system.web.dao.FwCartMapper;
import com.fw.system.web.service.IFwCartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 购物车 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Service
public class FwCartServiceImpl extends ServiceImpl<FwCartMapper, FwCart> implements IFwCartService {

}
