package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 红包关联任务用户完成情况表
 * </p>
 *
 * @author  
 * @since 2021-09-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwReadPackageTaskContent extends Model<FwReadPackageTaskContent> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    /**
     * 关联的任务ID
     */
    @TableField("task_id")
    private String taskId;

    /**
     * 领取人id
     */
    @TableField("user_id")
    private String userId;

    /**
     * 任务需要完成的数量
     */
    @TableField("task_num")
    private Integer taskNum;

    /**
     * 累加的红包额度
     */
    @TableField("task_amount")
    private BigDecimal taskAmount;

    /**
     * 用户已完成的次数
     */
    @TableField("user_num")
    private Integer userNum;

    /**
     * 红包ID
     */
    @TableField("read_id")
    private String readId;

    /**
     * 1 已完成 0进行中
     */
    @TableField("is_state")
    private Integer isState;

    /**
     * 这天任务完成的日期
     */
    @TableField("create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
