package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author  
 * @since 2021-07-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("xx_user")
public class XxUser extends Model<XxUser> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户名
     */
    @TableField("username")
    private String username;

    /**
     * 密码 md5(md5()+创建时间)
     */
    @TableField("password")
    private String password;

    /**
     * 二级密码
     */
    @TableField("secpwd")
    private String secpwd;

    /**
     * 手机号
     */
    @TableField("mobile")
    private String mobile;

    /**
     * 1=男 2=女 3=未知
     */
    @TableField("sex")
    private Integer sex;

    /**
     * 生日
     */
    @TableField("birthday")
    private LocalDate birthday;

    /**
     * 头像
     */
    @TableField("avatar")
    private String avatar;

    /**
     * 昵称
     */
    @TableField("nickname")
    private String nickname;

    /**
     * 实名
     */
    @TableField("truename")
    private String truename;

    /**
     * 会员类型：0 普通，1 技术，2 管理
     */
    @TableField("type")
    private Boolean type;

    /**
     * 技术锁仓
     */
    @TableField("jishu")
    private Integer jishu;

    /**
     * 管理锁仓
     */
    @TableField("guanli")
    private Integer guanli;

    /**
     * 身份证号
     */
    @TableField("idcard")
    private String idcard;

    /**
     * 余额
     */
    @TableField("balance")
    private BigDecimal balance;

    /**
     * 锁定金额
     */
    @TableField("freeze")
    private BigDecimal freeze;

    @TableField("scores")
    private BigDecimal scores;

    @TableField("isfirst_recharge")
    private Integer isfirstRecharge;

    /**
     * 积分
     */
    @TableField("point")
    private Integer point;

    @TableField("xftz")
    private BigDecimal xftz;

    @TableField("shtz")
    private BigDecimal shtz;

    /**
     * 商积
     */
    @TableField("shangji")
    private BigDecimal shangji;

    /**
     * 用户等级
     */
    @TableField("grade")
    private Integer grade;

    /**
     * 代理奖等级，发放完上周奖金后改变
     */
    @TableField("agent_grade")
    private Integer agentGrade;

    /**
     * 是否商户：0 不是，1 是
     */
    @TableField("is_shop")
    private Boolean isShop;

    /**
     * 原始股认购额度
     */
    @TableField("yuanshi_quota")
    private Integer yuanshiQuota;

    /**
     * 现有股
     */
    @TableField("xianyou")
    private Integer xianyou;

    /**
     * 质押的商甲
     */
    @TableField("pledge")
    private Integer pledge;

    /**
     * 锁仓数量
     */
    @TableField("lockups")
    private Integer lockups;


    /**
     * 交易仓
     */
    @TableField("jiaoyi")
    private Integer jiaoyi;

    @TableField("ctime")
    private Long ctime;

    @TableField("utime")
    private Long utime;

    @TableField("uptime")
    private Long uptime;

    /**
     * 是否激活：0 未激活，1 已激活
     */
    @TableField("isactive")
    private Boolean isactive;

    /**
     * 激活费用是否返还
     */
    @TableField("isback")
    private Boolean isback;


    /**
     * 推荐人
     */
    @TableField("pid")
    private Integer pid;

    @TableField("pids")
    private String pids;

    @TableField("depth")
    private Integer depth;

    /**
     * 收货地址ID
     */
    @TableField("area_id")
    private Integer areaId;

    @TableField("total")
    private BigDecimal total;

    /**
     * 删除标志 有数据就是删除
     */
    @TableField("isdel")
    private Long isdel;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
