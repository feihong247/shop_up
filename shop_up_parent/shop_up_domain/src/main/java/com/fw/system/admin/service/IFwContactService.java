package com.fw.system.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

import com.fw.system.admin.domain.FwContact;

/**
 * 关于我们Service接口
 *
 * @author yanwei
 * @date 2021-05-24
 */
public interface IFwContactService extends IService<FwContact> {
    /**
     * 查询关于我们
     *
     * @param id 关于我们ID
     * @return 关于我们
     */
    public FwContact selectFwContactById(String id);

    /**
     * 查询关于我们列表
     *
     * @param fwContact 关于我们
     * @return 关于我们集合
     */
    public List<FwContact> selectFwContactList(FwContact fwContact);

    /**
     * 新增关于我们
     *
     * @param fwContact 关于我们
     * @return 结果
     */
    public int insertFwContact(FwContact fwContact);

    /**
     * 修改关于我们
     *
     * @param fwContact 关于我们
     * @return 结果
     */
    public int updateFwContact(FwContact fwContact);

    /**
     * 批量删除关于我们
     *
     * @param ids 需要删除的关于我们ID
     * @return 结果
     */
    public int deleteFwContactByIds(String[] ids);

    /**
     * 删除关于我们信息
     *
     * @param id 关于我们ID
     * @return 结果
     */
    public int deleteFwContactById(String id);
}
