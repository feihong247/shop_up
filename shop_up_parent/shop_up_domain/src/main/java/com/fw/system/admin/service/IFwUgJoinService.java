package com.fw.system.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

import com.fw.system.admin.domain.FwUgJoin;

/**
 * 用户竞猜中间Service接口
 *
 * @author yanwei
 * @date 2021-07-03
 */
public interface IFwUgJoinService extends IService<FwUgJoin> {
    /**
     * 查询用户竞猜中间
     *
     * @param ugId 用户竞猜中间ID
     * @return 用户竞猜中间
     */
    FwUgJoin selectFwUgJoinById(String ugId);

    /**
     * 查询用户竞猜中间列表
     *
     * @param fwUgJoin 用户竞猜中间
     * @return 用户竞猜中间集合
     */
    List<FwUgJoin> selectFwUgJoinList(FwUgJoin fwUgJoin);

    /**
     * 新增用户竞猜中间
     *
     * @param fwUgJoin 用户竞猜中间
     * @return 结果
     */
    int insertFwUgJoin(FwUgJoin fwUgJoin);

    /**
     * 修改用户竞猜中间
     *
     * @param fwUgJoin 用户竞猜中间
     * @return 结果
     */
    int updateFwUgJoin(FwUgJoin fwUgJoin);

    /**
     * 批量删除用户竞猜中间
     *
     * @param ugIds 需要删除的用户竞猜中间ID
     * @return 结果
     */
    int deleteFwUgJoinByIds(String[] ugIds);

    /**
     * 删除用户竞猜中间信息
     *
     * @param ugId 用户竞猜中间ID
     * @return 结果
     */
    int deleteFwUgJoinById(String ugId);
}
