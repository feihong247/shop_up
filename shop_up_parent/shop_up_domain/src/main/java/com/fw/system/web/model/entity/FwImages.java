package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品图片列
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwImages extends Model<FwImages> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键 主键
     */
    @TableId("id")
    private String id;

    /**
     * 图片链接
     */
    @TableField("link_url")
    private String linkUrl;

    /**
     * 业务编号
     */
    @TableField("business_id")
    private String businessId;

    /**
     * 列状态 0=禁用，1=启用
     */
    @TableField("status")
    private Integer status;

    /**
     * 创建人 创建人
     */
    @TableField("create_by")
    private String createBy;

    /**
     * 创建时间 创建时间
     */
    @TableField("create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新人 更新人
     */
    @TableField("update_by")
    private String updateBy;

    /**
     * 更新时间 更新时间
     */
    @TableField("update_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    public void createD(String createId){
        this.createBy = createId;
        this.createTime = LocalDateTime.now();
        if (this.createTime != null)
            this.updateTime = this.createTime;
        this.updateBy = this.createBy;
    }

    public void updateD(String updateId){
        this.updateBy = updateId;
        this.updateTime = LocalDateTime.now();
    }
}
