package com.fw.system.web.model.form.v2;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fw.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("审批订单表单")
public class OrderApprovalForm implements Serializable {



    /**
     * 主键
     */
    @ApiModelProperty("订单号")
    private String id;

    /** 售后状态（1申请，2同意，3拒绝） */
    @ApiModelProperty(value = "售后状态（1申请，2同意，3拒绝）")
    private Integer afterSaleState;

    /** 售后备注 */
    @ApiModelProperty(value = "拒绝原因")
    private String afterSaleRemark;


}
