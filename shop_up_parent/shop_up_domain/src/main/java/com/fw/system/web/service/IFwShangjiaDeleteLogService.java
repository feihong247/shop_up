package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwShangjiaDeleteLog;

import java.math.BigDecimal;

/**
 * <p>
 * 商甲销毁日志表 服务类
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
public interface IFwShangjiaDeleteLogService extends IService<FwShangjiaDeleteLog> {

    void saveLabLog(BigDecimal num,String userId);

}
