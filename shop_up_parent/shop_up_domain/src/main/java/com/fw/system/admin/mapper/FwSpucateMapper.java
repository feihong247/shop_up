package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwSpucate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 商品类目Mapper接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface FwSpucateMapper extends BaseMapper<FwSpucate>
{
    /**
     * 查询商品类目
     * 
     * @param id 商品类目ID
     * @return 商品类目
     */
    public FwSpucate selectFwSpucateById(String id);

    /**
     * 查询商品类目列表
     * 
     * @param fwSpucate 商品类目
     * @return 商品类目集合
     */
    public List<FwSpucate> selectFwSpucateList(FwSpucate fwSpucate);

    /**
     * 新增商品类目
     * 
     * @param fwSpucate 商品类目
     * @return 结果
     */
    public int insertFwSpucate(FwSpucate fwSpucate);

    /**
     * 修改商品类目
     * 
     * @param fwSpucate 商品类目
     * @return 结果
     */
    public int updateFwSpucate(FwSpucate fwSpucate);

    /**
     * 删除商品类目
     * 
     * @param id 商品类目ID
     * @return 结果
     */
    public int deleteFwSpucateById(String id);

    /**
     * 批量删除商品类目
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwSpucateByIds(String[] ids);
}
