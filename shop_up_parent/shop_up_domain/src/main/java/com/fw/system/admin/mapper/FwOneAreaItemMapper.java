package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwOneAreaItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 一县一品Mapper接口
 * 
 * @author yanwei
 * @date 2021-11-09
 */
public interface FwOneAreaItemMapper extends BaseMapper<FwOneAreaItem>
{
    /**
     * 查询一县一品
     * 
     * @param id 一县一品ID
     * @return 一县一品
     */
    public FwOneAreaItem selectFwOneAreaItemById(String id);

    /**
     * 查询一县一品列表
     * 
     * @param fwOneAreaItem 一县一品
     * @return 一县一品集合
     */
    public List<FwOneAreaItem> selectFwOneAreaItemList(FwOneAreaItem fwOneAreaItem);

    /**
     * 新增一县一品
     * 
     * @param fwOneAreaItem 一县一品
     * @return 结果
     */
    public int insertFwOneAreaItem(FwOneAreaItem fwOneAreaItem);

    /**
     * 修改一县一品
     * 
     * @param fwOneAreaItem 一县一品
     * @return 结果
     */
    public int updateFwOneAreaItem(FwOneAreaItem fwOneAreaItem);

    /**
     * 删除一县一品
     * 
     * @param id 一县一品ID
     * @return 结果
     */
    public int deleteFwOneAreaItemById(String id);

    /**
     * 批量删除一县一品
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwOneAreaItemByIds(String[] ids);
}
