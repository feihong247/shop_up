package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwRuleDatas;

import java.math.BigDecimal;

/**
 * <p>
 * 数据规则表 服务类
 * </p>
 *
 * @author
 * @since 2021-06-02
 */
public interface IFwRuleDatasService extends IService<FwRuleDatas> {

    BigDecimal getBigDecimal(String ruleType);

    Integer getInteger(String ruleType);
}
