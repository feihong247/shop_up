package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwShangjiaCreateLog;

import java.util.List;

/**
 * <p>
 * 商甲产出日志表 服务类
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
public interface IFwShangjiaCreateLogService extends IService<FwShangjiaCreateLog> {

    Integer yueDetail();

    List<FwShangjiaCreateLog> dayDetail();

    List<FwShangjiaCreateLog> createAndReleaseYear();

    List<FwShangjiaCreateLog> createAndReleaseMonth();

    List<FwShangjiaCreateLog> createAndReleaseDay();
    //统计总产出商甲
    int sumAllCreateShangJia();

    int sumYearCreateShangJia();
}
