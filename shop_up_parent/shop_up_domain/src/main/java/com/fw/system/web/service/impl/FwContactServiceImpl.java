package com.fw.system.web.service.impl;


import com.fw.common.IdXD;
import com.fw.system.web.dao.FwContactMapper;
import com.fw.system.web.model.entity.FwContact;
import com.fw.system.web.service.IFwContactService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 关于我们 服务实现类
 * </p>
 *
 * @author
 * @since 2021-05-24
 */
@Service
public class FwContactServiceImpl extends ServiceImpl<FwContactMapper, FwContact> implements IFwContactService {
    @Autowired
    private FwContactMapper fwContactMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询关于我们
     *
     * @param id 关于我们ID
     * @return 关于我们
     */
    @Override
    public FwContact selectFwContactById(String id) {
        return fwContactMapper.selectFwContactById(id);
    }

    /**
     * 查询关于我们列表
     *
     * @param fwContact 关于我们
     * @return 关于我们
     */
    @Override
    public List<FwContact> selectFwContactList(FwContact fwContact) {
        return fwContactMapper.selectFwContactList(fwContact);
    }

}
