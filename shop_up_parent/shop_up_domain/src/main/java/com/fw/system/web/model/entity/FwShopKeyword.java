package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商铺环境展示
 * </p>
 *
 * @author  
 * @since 2021-07-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwShopKeyword extends Model<FwShopKeyword> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("id")
    private String id;

    /**
     * 商铺编号
     */
    @TableField("shop_id")
    private String shopId;

    /**
     * 环境标题
     */
    @TableField("word_title")
    private String wordTitle;

    @TableField("word_show")
    private String wordShow;

    /**
     * 文案内容
     */
    @TableField("word_text")
    private String wordText;

    @TableField("create_by")
    private String createBy;

    @TableField("create_time")
    private LocalDateTime createTime;

    @TableField("update_by")
    private String updateBy;

    @TableField("update_time")
    private LocalDateTime updateTime;


    public void createD(String createId){
        this.createBy = createId;
        this.createTime = LocalDateTime.now();
        if (this.createTime != null)
            this.updateTime = this.createTime;
        this.updateBy = this.createBy;
    }

    public void updateD(String updateId){
        this.updateBy = updateId;
        this.updateTime = LocalDateTime.now();
    }


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
