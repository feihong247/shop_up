package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwBanner;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 首页轮播 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface IFwBannerService extends IService<FwBanner> {

}
