package com.fw.system.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.system.web.dao.FwParentOrderMapper;
import com.fw.system.web.model.entity.FwParentOrder;
import com.fw.system.web.service.IFwParentOrderService;
import org.springframework.stereotype.Service;

@Service
public class FwParentOrderServiceImpl extends ServiceImpl<FwParentOrderMapper, FwParentOrder> implements IFwParentOrderService {
}
