package com.fw.system.web.model.form;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fw.annotation.IsPhone;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel("商户注册表单")
public class ShopRegisteredForm implements Serializable {

    @ApiModelProperty(value = "商铺的主键，有则修改，没有新增",required = false)
    private String shopId;

    @ApiModelProperty(value = "经营类型编号",required = true)
    @NotBlank(message = "经营类型编号不可为空")
    private String operatingName;


    @ApiModelProperty(value = "管理人姓名",required = true)
    @NotBlank(message = "管理人姓名不可为空")
    private String managerName;

    @ApiModelProperty(value = "管理人电话",required = true)
    @NotBlank(message = "管理人电话不可为空")
    @IsPhone
    private String managerPhone;

    @ApiModelProperty(value = "商铺名称",required = true)
    @NotBlank(message = "商铺名称不可为空")
    private String shopName;

    @ApiModelProperty(value = "商铺简介",required = true)
    @NotBlank(message = "商铺简介不可为空")
    private String shopInfo;

    @ApiModelProperty(value = "商铺营业执照图片",required = true)
    @NotBlank(message = "商铺营业执照图片不可为空")
    private String shopImage;


    /**
     * 省
     */
    @ApiModelProperty(value = "省",required = true)
    @NotBlank(message = "省不可为空")    private String province;

    /**
     * 市
     */
    @ApiModelProperty(value = "市",required = true)
    @NotBlank(message = "市不可为空")    private String city;

    /**
     * 纬度
     */
    @ApiModelProperty("纬度")
    private BigDecimal latitude;

    /**
     * 经度
     */
    @ApiModelProperty("经度")
    private BigDecimal longitude;

    /**
     * 区/县
     */
    @ApiModelProperty(value = "区/县",required = true)
    @NotBlank(message = "区/县不可为空")    private String area;

    /**
     * 详细地址
     */
    @ApiModelProperty(value = "详细地址",required = true)
    @NotBlank(message = "详细地址不可为空")    private String addrInfo;



    /**
     * 营业时间
     */
    @ApiModelProperty(value = "营业时间   9:00 ~ 12:00 已这种格式传入",required = true)
    @NotBlank(message = "营业时间不可为空")     private  String operatingTime;


    /**
     * 商铺电话
     */
    @ApiModelProperty(value = "商铺电话",required = true)
    @NotBlank(message = "商铺电话不可为空")     private String shopMobile;

    /**
     * 商铺公告
     */
    @ApiModelProperty(value = "商铺公告",required = true)
    @NotBlank(message = "商铺公告不可为空")      private String shopNotice;

    /**
     * 商铺logo
     */
    @ApiModelProperty(value = "商铺shop_logo",required = true)
    @NotBlank(message = "商铺shop_logo不可为空")      private String shopLogo;


    /**
     * 商铺线上质押多少商甲
     */
    @ApiModelProperty(value = "商铺线上质押多少商甲",required = true)
    private BigDecimal shangjia;

    /**
     * 商铺线下质押多少商甲
     */
    @ApiModelProperty(value = "商铺线下质押多少商甲",required = true)
    private BigDecimal offlineShangjia;


    /**
     * 商铺线上质押多少商甲
     */
    @ApiModelProperty(value = "身份证-正面",required = true)
    //@NotBlank(message = "身份证-正面不可为空")
    private String realJust;

    /**
     * 商铺线下质押多少商甲
     */
    @ApiModelProperty(value = "身份证-反面",required = true)
    //@NotBlank(message = "身份证-反面不可为空")
    private String realBack;



    /**
     * 商铺客服二维码
     */
    @ApiModelProperty(value = "商铺客服二维码",required = true)
    //@NotBlank(message = "商铺客服二维码不可为空")
    private String shopQr;

}
