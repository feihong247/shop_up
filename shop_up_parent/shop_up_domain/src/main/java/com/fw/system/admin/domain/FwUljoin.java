package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户和身份中间对象 fw_uljoin
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_uljoin")
@ApiModel(value="用户和身份中间", description="用户和身份中间表")
public class FwUljoin implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 用户编号 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "用户编号")
    @TableField("user_id")
    private String userId;

    /** 身份编号 */
    @ApiModelProperty(value = "用户编号")
    @Excel(name = "身份编号")
    @TableField("Identity_id")
    private String identityId;

    /** 是否正在使用此身份,一个用户只能使用一个。1=是，0=否 */
    @ApiModelProperty(value = "身份编号")
    @Excel(name = "是否正在使用此身份,一个用户只能使用一个。1=是，0=否")
    @TableField("is_use")
    private Integer isUse;

    /** 系统平台身份的标识码_反三范氏设计 */
    @ApiModelProperty(value = "是否正在使用此身份,一个用户只能使用一个。1=是，0=否")
    @Excel(name = "系统平台身份的标识码_反三范氏设计")
    @TableField("sys_code")
    private String sysCode;


    /** 创建者 */
    @TableField(value = "create_by")
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time")
    private Date createTime;

    /** 更新者 */
    @TableField(value = "update_by")
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "update_time")
    private Date updateTime;


}
