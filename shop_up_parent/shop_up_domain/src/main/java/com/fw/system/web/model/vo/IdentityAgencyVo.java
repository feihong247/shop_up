package com.fw.system.web.model.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 代理身份数据
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
@Api(tags = "代理身份数据")
public class IdentityAgencyVo {

    /**
     * 主键
     */
    @ApiModelProperty("id")
    private String id;

    /**
     * 身份名称
     */
    @ApiModelProperty("身份名称")
    private String identityName;

    /**
     * 条件 条件比较复杂，满额升级的，分享升级的，充值升级的
     */
    @ApiModelProperty("条件 条件比较复杂，满额升级的，分享升级的，充值升级的")
    private String conditionTxt;

    /**
     * 条件说明
     */
    @ApiModelProperty("条件说明")
    private String conditionInfo;

    /**
     * 身份说明
     */
    @ApiModelProperty("身份说明")
    private String identityInfo;

    /**
     * 身份标识
     */
    @ApiModelProperty("身份标识")
    private String sysCode;

    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 所需价格
     */
    @ApiModelProperty("所需价格")
    private BigDecimal Money;

    /**
     * 是否开发
     */
    @ApiModelProperty("是否开发")
    private String isOpen;


}
