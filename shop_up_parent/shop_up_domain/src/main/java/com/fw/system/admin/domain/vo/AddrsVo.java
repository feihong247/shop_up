package com.fw.system.admin.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * 用户收货地址表
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
@Api(tags = "收货地址vo")
public class AddrsVo {


    /**
     * 主键
     */
    @ApiModelProperty("id")
    private String id;

    /**
     * 用户编号
     */
    @ApiModelProperty("用户编号")
    private String userId;

    /**
     * 是否默认地址,1=是，0=否
     */
    @ApiModelProperty("是否默认地址,1=是，0=否")
    private Integer isDefault;

    /**
     * 联系人姓名
     */
    @ApiModelProperty("联系人姓名")
    private String contactName;

    /**
     * 联系人电话
     */
    @ApiModelProperty("联系人电话")
    private String contactPhone;

    /**
     * 省
     */
    @ApiModelProperty("省")
    private String province;

    /**
     * 市
     */
    @ApiModelProperty("市")
    private String city;

    /**
     * 区/县
     */
    @ApiModelProperty("区/县")
    private String area;

    /**
     * 省
     */
    @ApiModelProperty("省名称")
    private String provinceName;

    /**
     * 市
     */
    @ApiModelProperty("市名称")
    private String cityName;

    /**
     * 区/县
     */
    @ApiModelProperty("区/县名称")
    private String areaName;

    /**
     * 详细地址
     */
    @ApiModelProperty("详细地址")
    private String addrInfo;

    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;



}
