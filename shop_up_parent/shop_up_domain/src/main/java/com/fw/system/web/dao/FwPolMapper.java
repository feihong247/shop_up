package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwPol;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 地图表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwPolMapper extends BaseMapper<FwPol> {

}
