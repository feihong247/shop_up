package com.fw.system.admin.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 红包任务类型列对象 fw_read_package_task
 * 
 * @author yanwei
 * @date 2021-10-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_read_package_task")
@ApiModel(value="红包任务类型列", description="红包任务类型列表")
public class FwReadPackageTask extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 任务类型名称 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "任务类型名称")
    @TableField("task_name")
    private String taskName;

    /** 任务可以完成的次数 */
    @ApiModelProperty(value = "任务类型名称")
    @Excel(name = "任务可以完成的次数")
    @TableField("task_num")
    private Long taskNum;

    /** 任务累加红包的额度 */
    @ApiModelProperty(value = "任务可以完成的次数")
    @Excel(name = "任务累加红包的额度")
    @TableField("task_amount")
    private BigDecimal taskAmount;

    /** 关联的红包ID */
    @ApiModelProperty(value = "任务累加红包的额度")
    @Excel(name = "关联的红包ID")
    @TableField("read_id")
    private String readId;




}
