package com.fw.system.admin.service.impl;

import java.util.List;

import com.fw.common.IdXD;

import com.fw.system.admin.service.IFwContactService;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwContactMapper;
import com.fw.system.admin.domain.FwContact;

/**
 * 关于我们Service业务层处理
 *
 * @author yanwei
 * @date 2021-05-24
 */
@Service
public class FwContactServiceImpl extends ServiceImpl<FwContactMapper, FwContact> implements IFwContactService {
    @Autowired
    private FwContactMapper fwContactMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询关于我们
     *
     * @param id 关于我们ID
     * @return 关于我们
     */
    @Override
    public FwContact selectFwContactById(String id) {
        return fwContactMapper.selectFwContactById(id);
    }

    /**
     * 查询关于我们列表
     *
     * @param fwContact 关于我们
     * @return 关于我们
     */
    @Override
    public List<FwContact> selectFwContactList(FwContact fwContact) {
        return fwContactMapper.selectFwContactList(fwContact);
    }

    /**
     * 新增关于我们
     *
     * @param fwContact 关于我们
     * @return 结果
     */
    @Override
    public int insertFwContact(FwContact fwContact) {
        fwContact.setCreateTime(DateUtils.getNowDate());
        fwContact.setId(idXD.nextId());
        return fwContactMapper.insertFwContact(fwContact);
    }

    /**
     * 修改关于我们
     *
     * @param fwContact 关于我们
     * @return 结果
     */
    @Override
    public int updateFwContact(FwContact fwContact) {
        fwContact.setUpdateTime(DateUtils.getNowDate());
        return fwContactMapper.updateFwContact(fwContact);
    }

    /**
     * 批量删除关于我们
     *
     * @param ids 需要删除的关于我们ID
     * @return 结果
     */
    @Override
    public int deleteFwContactByIds(String[] ids) {
        return fwContactMapper.deleteFwContactByIds(ids);
    }

    /**
     * 删除关于我们信息
     *
     * @param id 关于我们ID
     * @return 结果
     */
    @Override
    public int deleteFwContactById(String id) {
        return fwContactMapper.deleteFwContactById(id);
    }
}
