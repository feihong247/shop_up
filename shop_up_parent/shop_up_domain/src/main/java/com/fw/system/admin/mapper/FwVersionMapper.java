package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwVersion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 版本Mapper接口
 * 
 * @author yanwei
 * @date 2021-10-12
 */
public interface FwVersionMapper extends BaseMapper<FwVersion>
{
    /**
     * 查询版本
     * 
     * @param id 版本ID
     * @return 版本
     */
    public FwVersion selectFwVersionById(String id);

    /**
     * 查询版本列表
     * 
     * @param fwVersion 版本
     * @return 版本集合
     */
    public List<FwVersion> selectFwVersionList(FwVersion fwVersion);

    /**
     * 新增版本
     * 
     * @param fwVersion 版本
     * @return 结果
     */
    public int insertFwVersion(FwVersion fwVersion);

    /**
     * 修改版本
     * 
     * @param fwVersion 版本
     * @return 结果
     */
    public int updateFwVersion(FwVersion fwVersion);

    /**
     * 删除版本
     * 
     * @param id 版本ID
     * @return 结果
     */
    public int deleteFwVersionById(String id);

    /**
     * 批量删除版本
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwVersionByIds(String[] ids);
}
