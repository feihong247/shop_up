package com.fw.system.web.model.form.v2;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("商户环境")
public class ShopKeyWordForm implements Serializable {


    /**
     * 主键
     */
    @ApiModelProperty("编号 有则更新，否则新增")
    private String id;

    /**
     * 环境标题
     */
    @ApiModelProperty("环境标题")
    private String wordTitle;

    @ApiModelProperty("环境封面")
    private String wordShow;

    /**
     * 文案内容
     */
    @ApiModelProperty("文案内容 —— 富文本形式,支持图片，视频等。")
    private String wordText;

}
