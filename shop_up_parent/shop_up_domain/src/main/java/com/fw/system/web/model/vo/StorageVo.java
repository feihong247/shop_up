package com.fw.system.web.model.vo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Api(tags = "商甲存储Vo")
public class StorageVo {

    @ApiModelProperty("总资产")
    private BigDecimal moneyCount;
    @ApiModelProperty("当前商甲价格")
    private BigDecimal presentPrice;
    @ApiModelProperty("持有商甲总数")
    private Integer holdCount;
    @ApiModelProperty("现有商甲")
    private BigDecimal nowCount;
    @ApiModelProperty("锁仓")
    private BigDecimal lockCount;
    @ApiModelProperty("系统分红")
    private BigDecimal systemCount;

}
