package com.fw.system.web.service.impl;

import cn.hutool.core.thread.ThreadUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.common.Builder;
import com.fw.common.IdXD;
import com.fw.enums.CurrencyLogEnum;
import com.fw.system.web.dao.FwCurrencyLogMapper;
import com.fw.system.web.model.entity.FwCurrencyLog;
import com.fw.system.web.service.IFwCurrencyLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 通用日志记录表 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
@Service
public class FwCurrencyLogServiceImpl extends ServiceImpl<FwCurrencyLogMapper, FwCurrencyLog> implements IFwCurrencyLogService {

    @Autowired
    private IdXD idXD;

    @Override
    public void saveCurrencyAsync(BigDecimal logCount, CurrencyLogEnum currencyLogEnum,String userId,Integer type){
        ThreadUtil.execute(()->{
            save( Builder.of(FwCurrencyLog::new).with(FwCurrencyLog::setId, idXD.nextId()).
                    with(FwCurrencyLog::setCreateTime, LocalDateTime.now()).
                    with(FwCurrencyLog::setLogCount, logCount).
                    with(FwCurrencyLog::setInfoFrom, currencyLogEnum.getInfoForm()).with(FwCurrencyLog::setLogName, currencyLogEnum.getLogName()).
                    with(FwCurrencyLog::setUserId, userId).with(FwCurrencyLog::setLogType, type).build());
        });
    }
}
