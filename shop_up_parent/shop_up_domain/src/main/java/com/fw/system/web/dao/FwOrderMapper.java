package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.vo.v2.OrderMoneyVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwOrderMapper extends BaseMapper<FwOrder> {

    List<OrderMoneyVo> manageOnlineOrder(@Param("shopId") String shopId,@Param("queryRange") Integer queryRange,@Param("limit") Integer limit);
}
