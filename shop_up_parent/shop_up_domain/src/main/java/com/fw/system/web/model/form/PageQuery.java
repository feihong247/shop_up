package com.fw.system.web.model.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@ApiModel("分页组件")
@Data
public class PageQuery implements Serializable {

    @ApiModelProperty(value = "起始页码",required = true)
    private  Integer pageNum;

    @ApiModelProperty(value = "查询每页多少条",required = true)
    private  Integer pageSize;
}
