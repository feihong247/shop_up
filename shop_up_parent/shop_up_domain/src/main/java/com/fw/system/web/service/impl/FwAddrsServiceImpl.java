package com.fw.system.web.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.fw.common.IdXD;
import com.fw.system.comm.service.DistrictService;
import com.fw.system.web.model.entity.FwAddrs;
import com.fw.system.web.dao.FwAddrsMapper;
import com.fw.system.web.model.vo.AddrsVo;
import com.fw.system.web.service.IFwAddrsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 用户收货地址表 服务实现类
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Service
public class FwAddrsServiceImpl extends ServiceImpl<FwAddrsMapper, FwAddrs> implements IFwAddrsService {
    @Autowired
    private FwAddrsMapper fwAddrsMapper;
    @Autowired
    private DistrictService districtService;

    @Autowired
    private IdXD idXD;

    /**
     * 查询用户收货地址
     *
     * @param id 用户收货地址ID
     * @return 用户收货地址
     */
    @Override
    public FwAddrs selectFwAddrsById(String id) {
        return fwAddrsMapper.selectFwAddrsById(id);
    }

    /**
     * 查询用户收货地址列表
     *
     * @param fwAddrs 用户收货地址
     * @return 用户收货地址
     */
    @Override
    public List<FwAddrs> selectFwAddrsList(FwAddrs fwAddrs) {
        return fwAddrsMapper.selectFwAddrsList(fwAddrs);
    }

    /**
     * 新增用户收货地址
     *
     * @param fwAddrs 用户收货地址
     * @return 结果
     */
    @Override
    public int insertFwAddrs(FwAddrs fwAddrs) {
        fwAddrs.setCreateTime(LocalDateTime.now());
        fwAddrs.setId(idXD.nextId());
        return fwAddrsMapper.insertFwAddrs(fwAddrs);
    }

    /**
     * 修改用户收货地址
     *
     * @param fwAddrs 用户收货地址
     * @return 结果
     */
    @Override
    public int updateFwAddrs(FwAddrs fwAddrs) {
        fwAddrs.setUpdateTime(LocalDateTime.now());
        return fwAddrsMapper.updateFwAddrs(fwAddrs);
    }

    /**
     * 批量删除用户收货地址
     *
     * @param ids 需要删除的用户收货地址ID
     * @return 结果
     */
    @Override
    public int deleteFwAddrsByIds(String[] ids) {
        return fwAddrsMapper.deleteFwAddrsByIds(ids);
    }

    /**
     * 删除用户收货地址信息
     *
     * @param id 用户收货地址ID
     * @return 结果
     */
    @Override
    public int deleteFwAddrsById(String id) {
        return fwAddrsMapper.deleteFwAddrsById(id);
    }

    @Override
    public AddrsVo getVoById(String id) {
        FwAddrs fwAddrs = fwAddrsMapper.selectFwAddrsById(id);
        AddrsVo addrsVo = new AddrsVo();
        if (fwAddrs == null) return addrsVo;
        BeanUtil.copyProperties(fwAddrs,addrsVo);
        addrsVo.setAreaName(districtService.codeConversionName(fwAddrs.getArea()));
        addrsVo.setCityName(districtService.codeConversionName(fwAddrs.getCity()));
        addrsVo.setProvinceName(districtService.codeConversionName(fwAddrs.getProvince()));
        return addrsVo;
    }
}
