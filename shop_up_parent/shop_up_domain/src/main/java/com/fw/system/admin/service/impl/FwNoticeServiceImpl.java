package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwNoticeMapper;
import com.fw.system.admin.domain.FwNotice;
import com.fw.system.admin.service.IFwNoticeService;

/**
 * 系统公告&通知Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwNoticeServiceImpl extends ServiceImpl<FwNoticeMapper, FwNotice> implements IFwNoticeService
{
    @Autowired
    private FwNoticeMapper fwNoticeMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询系统公告&通知
     * 
     * @param id 系统公告&通知ID
     * @return 系统公告&通知
     */
    @Override
    public FwNotice selectFwNoticeById(String id)
    {
        return fwNoticeMapper.selectFwNoticeById(id);
    }

    /**
     * 查询系统公告&通知列表
     * 
     * @param fwNotice 系统公告&通知
     * @return 系统公告&通知
     */
    @Override
    public List<FwNotice> selectFwNoticeList(FwNotice fwNotice)
    {
        return fwNoticeMapper.selectFwNoticeList(fwNotice);
    }

    /**
     * 新增系统公告&通知
     * 
     * @param fwNotice 系统公告&通知
     * @return 结果
     */
    @Override
    public int insertFwNotice(FwNotice fwNotice)
    {
        fwNotice.setCreateTime(DateUtils.getNowDate());
        fwNotice.setId(idXD.nextId());
        return fwNoticeMapper.insertFwNotice(fwNotice);
    }

    /**
     * 修改系统公告&通知
     * 
     * @param fwNotice 系统公告&通知
     * @return 结果
     */
    @Override
    public int updateFwNotice(FwNotice fwNotice)
    {
        fwNotice.setUpdateTime(DateUtils.getNowDate());
        return fwNoticeMapper.updateFwNotice(fwNotice);
    }

    /**
     * 批量删除系统公告&通知
     * 
     * @param ids 需要删除的系统公告&通知ID
     * @return 结果
     */
    @Override
    public int deleteFwNoticeByIds(String[] ids)
    {
        return fwNoticeMapper.deleteFwNoticeByIds(ids);
    }

    /**
     * 删除系统公告&通知信息
     * 
     * @param id 系统公告&通知ID
     * @return 结果
     */
    @Override
    public int deleteFwNoticeById(String id)
    {
        return fwNoticeMapper.deleteFwNoticeById(id);
    }
}
