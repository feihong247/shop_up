package com.fw.system.web.service.impl;


import com.fw.common.IdXD;
import com.fw.system.web.dao.FwUsjoinMapper;
import com.fw.system.web.model.entity.FwUsjoin;
import com.fw.system.web.model.vo.UsjoinVo;
import com.fw.system.web.service.IFwUsjoinService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 我的商铺收藏中间表 服务实现类
 * </p>
 *
 * @author
 * @since 2021-05-24
 */
@Service
public class FwUsjoinServiceImpl extends ServiceImpl<FwUsjoinMapper, FwUsjoin> implements IFwUsjoinService {
    @Autowired
    private FwUsjoinMapper fwUsjoinMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询我的商铺收藏中间
     *
     * @param id 我的商铺收藏中间ID
     * @return 我的商铺收藏中间
     */
    @Override
    public FwUsjoin selectFwUsjoinById(String id)
    {
        return fwUsjoinMapper.selectFwUsjoinById(id);
    }

    /**
     * 查询我的商铺收藏中间列表
     *
     * @param fwUsjoin 我的商铺收藏中间
     * @return 我的商铺收藏中间
     */
    @Override
    public List<UsjoinVo> selectFwUsjoinList(FwUsjoin fwUsjoin)
    {
        return fwUsjoinMapper.selectFwUsjoinList(fwUsjoin);
    }

    /**
     * 新增我的商铺收藏中间
     *
     * @param fwUsjoin 我的商铺收藏中间
     * @return 结果
     */
    @Override
    public int insertFwUsjoin(FwUsjoin fwUsjoin)
    {
        fwUsjoin.setCreateTime(LocalDateTime.now());
        fwUsjoin.setId(idXD.nextId());
        return fwUsjoinMapper.insertFwUsjoin(fwUsjoin);
    }

    /**
     * 修改我的商铺收藏中间
     *
     * @param fwUsjoin 我的商铺收藏中间
     * @return 结果
     */
    @Override
    public int updateFwUsjoin(FwUsjoin fwUsjoin)
    {
        fwUsjoin.setUpdateTime(LocalDateTime.now());
        return fwUsjoinMapper.updateFwUsjoin(fwUsjoin);
    }

    /**
     * 批量删除我的商铺收藏中间
     *
     * @param ids 需要删除的我的商铺收藏中间ID
     * @return 结果
     */
    @Override
    public int deleteFwUsjoinByIds(String[] ids)
    {
        return fwUsjoinMapper.deleteFwUsjoinByIds(ids);
    }

    /**
     * 删除我的商铺收藏中间信息
     *
     * @param id 我的商铺收藏中间ID
     * @return 结果
     */
    @Override
    public int deleteFwUsjoinById(String id)
    {
        return fwUsjoinMapper.deleteFwUsjoinById(id);
    }
}
