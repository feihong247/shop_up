package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwProblem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 常见问题 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwProblemMapper extends BaseMapper<FwProblem> {

}
