package com.fw.system.web.model.vo.v2;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Api("评价查询 数据统计 返回VO")
@Builder
public class EvaluationCountVo implements Serializable {

    @ApiModelProperty("有图")
    private Long imgCount = 0L;

    @ApiModelProperty("最新")
    private Long newCount = 0L;


    @ApiModelProperty("总数")
    private Integer totalCount = 0;




}
