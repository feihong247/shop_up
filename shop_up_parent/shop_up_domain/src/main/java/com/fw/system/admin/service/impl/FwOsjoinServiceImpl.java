package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwOsjoinMapper;
import com.fw.system.admin.domain.FwOsjoin;
import com.fw.system.admin.service.IFwOsjoinService;

/**
 * 订单与spu中间Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwOsjoinServiceImpl extends ServiceImpl<FwOsjoinMapper, FwOsjoin> implements IFwOsjoinService
{
    @Autowired
    private FwOsjoinMapper fwOsjoinMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询订单与spu中间
     * 
     * @param id 订单与spu中间ID
     * @return 订单与spu中间
     */
    @Override
    public FwOsjoin selectFwOsjoinById(String id)
    {
        return fwOsjoinMapper.selectFwOsjoinById(id);
    }

    /**
     * 查询订单与spu中间列表
     * 
     * @param fwOsjoin 订单与spu中间
     * @return 订单与spu中间
     */
    @Override
    public List<FwOsjoin> selectFwOsjoinList(FwOsjoin fwOsjoin)
    {
        return fwOsjoinMapper.selectFwOsjoinList(fwOsjoin);
    }

    /**
     * 新增订单与spu中间
     * 
     * @param fwOsjoin 订单与spu中间
     * @return 结果
     */
    @Override
    public int insertFwOsjoin(FwOsjoin fwOsjoin)
    {
        fwOsjoin.setCreateTime(DateUtils.getNowDate());
        fwOsjoin.setId(idXD.nextId());
        return fwOsjoinMapper.insertFwOsjoin(fwOsjoin);
    }

    /**
     * 修改订单与spu中间
     * 
     * @param fwOsjoin 订单与spu中间
     * @return 结果
     */
    @Override
    public int updateFwOsjoin(FwOsjoin fwOsjoin)
    {
        fwOsjoin.setUpdateTime(DateUtils.getNowDate());
        return fwOsjoinMapper.updateFwOsjoin(fwOsjoin);
    }

    /**
     * 批量删除订单与spu中间
     * 
     * @param ids 需要删除的订单与spu中间ID
     * @return 结果
     */
    @Override
    public int deleteFwOsjoinByIds(String[] ids)
    {
        return fwOsjoinMapper.deleteFwOsjoinByIds(ids);
    }

    /**
     * 删除订单与spu中间信息
     * 
     * @param id 订单与spu中间ID
     * @return 结果
     */
    @Override
    public int deleteFwOsjoinById(String id)
    {
        return fwOsjoinMapper.deleteFwOsjoinById(id);
    }
}
