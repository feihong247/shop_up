package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 商品评价对象 fw_evaluation
 *
 * @author yanwei
 * @date 2021-05-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_evaluation")
@ApiModel(value = "商品评价", description = "商品评价")
public class FwEvaluation extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键 主键
     */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /**
     * 用户编号
     */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "用户编号")
    @TableField("user_id")
    private String userId;

    /**
     * 商品编号
     */
    @ApiModelProperty(value = "用户编号")
    @Excel(name = "商品编号")
    @TableField("item_id")
    private String itemId;

    /**
     * 评价内容
     */
    @ApiModelProperty(value = "商品编号")
    @Excel(name = "评价内容")
    @TableField("infos")
    private String infos;

    /** 评价图片,多个以逗号分隔 */
    @ApiModelProperty(value = "评价内容")
    @Excel(name = "评价图片,多个以逗号分隔")
    @TableField("eva_img")
    private String evaImg;


    /**
     * 评价星级 0-5之间
     */
    @ApiModelProperty(value = "评价内容")
    @Excel(name = "评价星级 0-5之间")
    @TableField("star_num")
    private Integer starNum;

    /**
     * 订单编号
     */
    @ApiModelProperty(value = "评价星级 0-5之间")
    @Excel(name = "订单编号")
    @TableField("order_id")
    private String orderId;

    /** 用户名称 */
    @ApiModelProperty(value = "微信唯一标示")
    @TableField(exist = false)
    private String userName;

    /** 用户手机号 */
    @ApiModelProperty(value = "用户名称")
    @TableField(exist = false)
    private String phone;

    /** 用户头像 */
    @ApiModelProperty(value = "用户编号_平台编号，可以充当邀请码来使用")
    @TableField(exist = false)
    private String headImage;

    /** 商品标题 */
    @ApiModelProperty(value = "商铺编号,自营另外区分")
    @TableField(exist = false)
    private String title;

}
