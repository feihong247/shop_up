package com.fw.system.admin.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 代理区域对象 fw_agent_region
 * 
 * @author yanwei
 * @date 2021-07-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="代理区域VO", description="代理区域表Vos")
public class AgentRegionVo
{


    /** 主键 */
    @ApiModelProperty(value = "主键")
    private String id;

    /** 用户编号 */
    @ApiModelProperty(value = "用户编号}")
    @Excel(name = "用户编号")
    private String userId;

    @ApiModelProperty("用户详细信息")
    private UserAgentVo userAgentVo;

    /** 代理身份编号,5:区县代理,6:市级代理,7:省级代理 */
    @ApiModelProperty(value = "代理身份")
    @Excel(name = "代理身份编号",readConverterExp = "5=区县代理,6=市级代理,7=省级代理")
    private String agent;

    /** 省 */
    @ApiModelProperty(value = "省")
    @Excel(name = "省")
    private String province;

    /** 市 */
    @ApiModelProperty(value = "市")
    @Excel(name = "市")
    private String city;

    /** 区/县 */
    @ApiModelProperty(value = "区/县")
    @Excel(name = "区/县")
    private String area;

    /** 省 */
    @ApiModelProperty(value = "省")
    @Excel(name = "省")
    private String provinceName;

    /** 市 */
    @ApiModelProperty(value = "市")
    @Excel(name = "市")
    private String cityName;

    /** 区/县 */
    @ApiModelProperty(value = "区/县")
    @Excel(name = "区/县")
    private String areaName;

    /** 是否正在使用该地址,1=是，0=否 */
    @ApiModelProperty(value = "是否正在使用该地址")
    @Excel(name = "是否正在使用该地址",readConverterExp = "1=是,0=否")
    private Integer isDefault;










}
