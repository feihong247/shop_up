package com.fw.system.web.model.vo;


import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


import java.time.LocalDateTime;

/**
 * <p>
 * 关于我们
 * </p>
 *
 * @author
 * @since 2021-05-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ContactVo extends Model<ContactVo> {


    /**
     * 主键 主键
     */
    @ApiModelProperty("id")
    private String id;

    /**
     * 标题
     */
    @ApiModelProperty("标题")
    private String title;

    /**
     * 内容
     */
    @ApiModelProperty("内容")
    private String contentText;

    /**
     * 创建人 创建人
     */
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新人 更新人
     */
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;


}
