package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwMoneys;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwShop;

import java.math.BigDecimal;

/**
 * <p>
 * 用户钱包表 服务类
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
public interface IFwMoneysService extends IService<FwMoneys> {

    boolean disappearReturnParent(String userId,String childId, BigDecimal disappear, String jsonTxt, String rootUserId);
    void disappearReturn(String userId, BigDecimal disappear, String jsonTxt);
    /**
     * 根据消费金额产出商甲
     *
     * @param userId
     * @param disappear
     */
    public void disappearOutput(String userId, BigDecimal disappear);

    void shopDisappearReturn(FwShop fwShop, BigDecimal shopDisappear, String sourceInfo,String template);
    void extracted(String userId, FwMoneys moneysOld);
}
