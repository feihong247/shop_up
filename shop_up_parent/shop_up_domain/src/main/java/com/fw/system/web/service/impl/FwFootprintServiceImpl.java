package com.fw.system.web.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.common.IdXD;
import com.fw.system.web.dao.FwFootprintMapper;
import com.fw.system.web.model.entity.FwFootprint;
import com.fw.system.web.model.vo.FootprintVo;
import com.fw.system.web.service.IFwFootprintService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 足迹表 服务实现类
 * </p>
 *
 * @author
 * @since 2021-06-23
 */
@Service
public class FwFootprintServiceImpl extends ServiceImpl<FwFootprintMapper, FwFootprint> implements IFwFootprintService {
    @Autowired
    private FwFootprintMapper fwFootprintMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询足迹
     *
     * @param id 足迹ID
     * @return 足迹
     */
    @Override
    public FootprintVo selectFwFootprintById(String id) {
        return fwFootprintMapper.selectFwFootprintById(id);
    }

    /**
     * 查询足迹列表
     *
     * @return 足迹
     */
    @Override
    public List<FootprintVo> selectFwFootprintList(String userId) {
        return fwFootprintMapper.selectFwFootprintList(userId);
    }

    /**
     * 新增足迹
     *
     * @param itemId 足迹
     * @return 结果
     */
    @Override
    public int insertFwFootprint(String itemId,String userId) {
        FwFootprint footprint = fwFootprintMapper.selectOne(Wrappers.<FwFootprint>lambdaQuery().eq(FwFootprint::getItemId, itemId).eq(FwFootprint::getUserId, userId));
        if (footprint!=null){
            String id = footprint.getId();
            fwFootprintMapper.deleteFwFootprintById(id);
        }
        FwFootprint fwFootprint = new FwFootprint();
        fwFootprint.setId(idXD.nextId());
        fwFootprint.setUserId(userId);
        fwFootprint.setCreateBy(userId);
        fwFootprint.setItemId(itemId);
        fwFootprint.setCreateTime(LocalDateTime.now());
        return fwFootprintMapper.insertFwFootprint(fwFootprint);
    }

    /**
     * 修改足迹
     *
     * @param fwFootprint 足迹
     * @return 结果
     */
    @Override
    public int updateFwFootprint(FwFootprint fwFootprint) {
        fwFootprint.setUpdateTime(LocalDateTime.now());
        return fwFootprintMapper.updateFwFootprint(fwFootprint);
    }

    /**
     * 批量删除足迹
     *
     * @param ids 需要删除的足迹ID
     * @return 结果
     */
    @Override
    public int deleteFwFootprintByIds(String[] ids) {
        return fwFootprintMapper.deleteFwFootprintByIds(ids);
    }

    /**
     * 删除足迹信息
     *
     * @param id 足迹ID
     * @return 结果
     */
    @Override
    public int deleteFwFootprintById(String id) {
        return fwFootprintMapper.deleteFwFootprintById(id);
    }
}
