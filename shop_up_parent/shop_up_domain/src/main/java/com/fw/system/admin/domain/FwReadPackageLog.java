package com.fw.system.admin.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 红包抽取记录对象 fw_read_package_log
 * 
 * @author yanwei
 * @date 2021-10-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_read_package_log")
@ApiModel(value="红包抽取记录", description="红包抽取记录表")
public class FwReadPackageLog implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** $column.columnComment */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @TableField("user_id")
    private String userId;



    /** 抽取的红包金额 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "抽取的红包金额")
    @TableField("amount_num")
    private Long amountNum;

    /** 红包状态: isState =1第一次 2进行中 3可拆  */
    @ApiModelProperty(value = "抽取的红包金额")
    @Excel(name = "红包状态: isState =1第一次 2进行中 3可拆 ")
    @TableField("is_state")
    private Long isState;

    /** 该红包截止日期 */
    @ApiModelProperty(value = "红包状态: isState =1第一次 2进行中 3可拆 ")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "该红包截止日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @TableField("end_time")
    private Date endTime;

    /** 起始金额 */
    @ApiModelProperty(value = "该红包截止日期")
    @Excel(name = "起始金额")
    @TableField("start_num")
    private BigDecimal startNum;

    /** 关联红包id */
    @ApiModelProperty(value = "起始金额")
    @Excel(name = "关联红包id")
    @TableField("read_id")
    private String readId;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time",exist = false)
    private Date createTime;


    /** 用户名称 */
    @ApiModelProperty(value = "用户名称")
    @Excel(name = "用户名称")
    @TableField(exist = false)
    private String userName;

    /** 用户手机号 */
    @ApiModelProperty(value = "用户名称")
    @Excel(name = "用户手机号")
    @TableField(exist = false)
    private String phone;




}
