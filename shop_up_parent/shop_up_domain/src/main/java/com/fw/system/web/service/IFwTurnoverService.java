package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwTurnover;

/**
 * <p>
 * 营业额表,以此表为依据增长商甲 服务类
 * </p>
 *
 * @author
 * @since 2021-06-07
 */
public interface IFwTurnoverService extends IService<FwTurnover> {

}
