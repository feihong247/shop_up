package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwPolMapper;
import com.fw.system.admin.domain.FwPol;
import com.fw.system.admin.service.IFwPolService;

/**
 * 地图Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwPolServiceImpl extends ServiceImpl<FwPolMapper, FwPol> implements IFwPolService
{
    @Autowired
    private FwPolMapper fwPolMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询地图
     * 
     * @param id 地图ID
     * @return 地图
     */
    @Override
    public FwPol selectFwPolById(String id)
    {
        return fwPolMapper.selectFwPolById(id);
    }

    /**
     * 查询地图列表
     * 
     * @param fwPol 地图
     * @return 地图
     */
    @Override
    public List<FwPol> selectFwPolList(FwPol fwPol)
    {
        return fwPolMapper.selectFwPolList(fwPol);
    }

    /**
     * 新增地图
     * 
     * @param fwPol 地图
     * @return 结果
     */
    @Override
    public int insertFwPol(FwPol fwPol)
    {
        fwPol.setCreateTime(DateUtils.getNowDate());
        fwPol.setId(idXD.nextId());
        return fwPolMapper.insertFwPol(fwPol);
    }

    /**
     * 修改地图
     * 
     * @param fwPol 地图
     * @return 结果
     */
    @Override
    public int updateFwPol(FwPol fwPol)
    {
        fwPol.setUpdateTime(DateUtils.getNowDate());
        return fwPolMapper.updateFwPol(fwPol);
    }

    /**
     * 批量删除地图
     * 
     * @param ids 需要删除的地图ID
     * @return 结果
     */
    @Override
    public int deleteFwPolByIds(String[] ids)
    {
        return fwPolMapper.deleteFwPolByIds(ids);
    }

    /**
     * 删除地图信息
     * 
     * @param id 地图ID
     * @return 结果
     */
    @Override
    public int deleteFwPolById(String id)
    {
        return fwPolMapper.deleteFwPolById(id);
    }
}
