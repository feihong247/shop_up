package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwShopKeyword;

/**
 * <p>
 * 商铺环境展示 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-07-16
 */
public interface FwShopKeywordMapper extends BaseMapper<FwShopKeyword> {

}
