package com.fw.system.web.model.form;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("商品Form")
public class SpuForm {
    /** 类目 */
    @ApiModelProperty("类目编号")
    private String category = "";

    @ApiModelProperty("是否热卖")
    private Integer hotSale = -1;

    @ApiModelProperty("是否推荐")
    private Integer recommend = -1;

    @ApiModelProperty("商铺编号")
    private String shopId;

    @ApiModelProperty("商品标题")
    private String title;

    @ApiModelProperty("是否线下(0否1是),必传")
    private Integer isOffline = -1;

    @ApiModelProperty("价格区间-启")
    private BigDecimal priceBetween;

    @ApiModelProperty("价格区间-止")
    private BigDecimal priceAnd;

    @ApiModelProperty("等于1按销量排序")
    private Integer orderByTops = 0;

    @ApiModelProperty("等于1按价格排序")
    private Integer orderByPrice = 0;

    /** 是否竞猜(0线上1线下) */
    @ApiModelProperty("是否竞猜(0否1是)")
    private Integer isQuiz = -1;

    /** 是否特卖（0否1是） */
    @ApiModelProperty("是否特卖（0否1是）")
    private Integer isSale = -1;

    @ApiModelProperty(value = "起始页码",required = true)
    private  Integer pageNum;

    @ApiModelProperty(value = "查询每页多少条",required = true)
    private  Integer pageSize;

}
