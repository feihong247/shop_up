package com.fw.system.admin.domain.vo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@Api(tags = "系统管理员大屏Vo")
public class ScreenDataVo {


    @ApiModelProperty("总订单量")
    private Integer orderCount;

    @ApiModelProperty("待支付订单量")
    private Integer PayOrderCount;

    @ApiModelProperty("待发货订单量")
    private Integer awaitDeliveryOrderCount;

    @ApiModelProperty("待收货订单量")
    private Integer awaitGoodsOrderCount;

    @ApiModelProperty("待评价订单量")
    private Integer awaitEvaluationOrderCount;

    @ApiModelProperty("退货退款订单量")
    private Integer refundOrderCount;

    @ApiModelProperty("已收货订单量")
    private Integer completeOrderCount;

    @ApiModelProperty("竞猜订单量")
    private Integer quizOrderCount;

    @ApiModelProperty("平台商甲产出记录")
    private List logs;

    @ApiModelProperty("全平台总消证")
    private BigDecimal disappearCount;

    @ApiModelProperty("全平台总积分")
    private BigDecimal integralCount;

    @ApiModelProperty("平台商户累计收入top榜")
    private List shopIncomeList;

    @ApiModelProperty("平台商户新增量")
    private Integer newAddShopCount;

    @ApiModelProperty("vip总量")
    private Integer vipCount;

    @ApiModelProperty("1星总量")
    private Integer oneVipCount;

    @ApiModelProperty("2星总量")
    private Integer twoVipCount;

    @ApiModelProperty("3星总量")
    private Integer threeVipCount;

    @ApiModelProperty("区县代理总量")
    private Integer areaVipCount;

    @ApiModelProperty("市级代理总量")
    private Integer cityVipCount;

    @ApiModelProperty("省级代理总量")
    private Integer provinceVipCount;


}
