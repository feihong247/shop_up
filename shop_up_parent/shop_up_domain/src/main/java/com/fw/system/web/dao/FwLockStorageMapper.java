package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwLockStorage;

import java.util.List;

/**
 * <p>
 * 锁仓池 Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-06-28
 */
public interface FwLockStorageMapper extends BaseMapper<FwLockStorage> {

    List<FwLockStorage> listStorages();
}
