package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwCart;

/**
 * 购物车Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwCartService extends IService<FwCart>
{
    /**
     * 查询购物车
     * 
     * @param id 购物车ID
     * @return 购物车
     */
    public FwCart selectFwCartById(String id);

    /**
     * 查询购物车列表
     * 
     * @param fwCart 购物车
     * @return 购物车集合
     */
    public List<FwCart> selectFwCartList(FwCart fwCart);

    /**
     * 新增购物车
     * 
     * @param fwCart 购物车
     * @return 结果
     */
    public int insertFwCart(FwCart fwCart);

    /**
     * 修改购物车
     * 
     * @param fwCart 购物车
     * @return 结果
     */
    public int updateFwCart(FwCart fwCart);

    /**
     * 批量删除购物车
     * 
     * @param ids 需要删除的购物车ID
     * @return 结果
     */
    public int deleteFwCartByIds(String[] ids);

    /**
     * 删除购物车信息
     * 
     * @param id 购物车ID
     * @return 结果
     */
    public int deleteFwCartById(String id);
}
