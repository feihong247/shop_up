package com.fw.system.web.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 商铺环境展示
 * </p>
 *
 * @author  
 * @since 2021-07-16
 */
@Data

public class ShopKeywordVo implements Serializable {


    /**
     * 主键
     */
    @ApiModelProperty("主键")
    private String id;

    /**
     * 商铺编号
     */
    @ApiModelProperty("商铺编号")
    private String shopId;

    /**
     * 环境标题
     */
    @ApiModelProperty("环境标题")
    private String wordTitle;

    @ApiModelProperty("环境封面")
    private String wordShow;

    /**
     * 文案内容
     */
    @ApiModelProperty("文案内容 - 富文本形式")
    private String wordText;



    @ApiModelProperty("创建时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime createTime;





}
