package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwNotice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统公告&通知 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface IFwNoticeService extends IService<FwNotice> {

}
