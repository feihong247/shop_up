package com.fw.system.admin.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 线下消费记录对象 fw_offline
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_offline")
@ApiModel(value="线下消费记录", description="线下消费记录表")
public class FwOffline extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 用户编号 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "用户编号")
    @TableField("user_id")
    private String userId;

    /** 商户编号 */
    @ApiModelProperty(value = "用户编号")
    @Excel(name = "商户编号")
    @TableField("shop_id")
    private String shopId;

    /** 消费金额 */
    @ApiModelProperty(value = "商户编号")
    @Excel(name = "消费金额")
    @TableField("price")
    private BigDecimal price;

    /** 支付类型,0=支付宝，1=微信 */
    @ApiModelProperty(value = "消费金额")
    @Excel(name = "支付类型,0=支付宝，1=微信")
    @TableField("pay_type")
    private Integer payType;

    /** 消费反馈,支付成功后给的反馈信息 */
    @ApiModelProperty(value = "支付类型,0=支付宝，1=微信")
    @Excel(name = "消费反馈,支付成功后给的反馈信息")
    @TableField("infos")
    private String infos;




}
