package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwUljoin;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户和身份中间表 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface IFwUljoinService extends IService<FwUljoin> {

    int countVip(String userId);
}
