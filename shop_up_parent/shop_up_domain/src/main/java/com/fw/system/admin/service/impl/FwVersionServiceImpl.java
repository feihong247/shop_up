package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwVersionMapper;
import com.fw.system.admin.domain.FwVersion;
import com.fw.system.admin.service.IFwVersionService;

/**
 * 版本Service业务层处理
 * 
 * @author yanwei
 * @date 2021-10-12
 */
@Service
public class FwVersionServiceImpl extends ServiceImpl<FwVersionMapper, FwVersion> implements IFwVersionService
{
    @Autowired
    private FwVersionMapper fwVersionMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询版本
     * 
     * @param id 版本ID
     * @return 版本
     */
    @Override
    public FwVersion selectFwVersionById(String id)
    {
        return fwVersionMapper.selectFwVersionById(id);
    }

    /**
     * 查询版本列表
     * 
     * @param fwVersion 版本
     * @return 版本
     */
    @Override
    public List<FwVersion> selectFwVersionList(FwVersion fwVersion)
    {
        return fwVersionMapper.selectFwVersionList(fwVersion);
    }

    /**
     * 新增版本
     * 
     * @param fwVersion 版本
     * @return 结果
     */
    @Override
    public int insertFwVersion(FwVersion fwVersion)
    {
        fwVersion.setCreateTime(DateUtils.getNowDate());
        fwVersion.setId(idXD.nextId());
        return fwVersionMapper.insertFwVersion(fwVersion);
    }

    /**
     * 修改版本
     * 
     * @param fwVersion 版本
     * @return 结果
     */
    @Override
    public int updateFwVersion(FwVersion fwVersion)
    {
        return fwVersionMapper.updateFwVersion(fwVersion);
    }

    /**
     * 批量删除版本
     * 
     * @param ids 需要删除的版本ID
     * @return 结果
     */
    @Override
    public int deleteFwVersionByIds(String[] ids)
    {
        return fwVersionMapper.deleteFwVersionByIds(ids);
    }

    /**
     * 删除版本信息
     * 
     * @param id 版本ID
     * @return 结果
     */
    @Override
    public int deleteFwVersionById(String id)
    {
        return fwVersionMapper.deleteFwVersionById(id);
    }
}
