package com.fw.system.web.model.vo.v2;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("商户分析返回结果集")
@Builder
public class ShopAnalyzeVo implements Serializable {

    @ApiModelProperty("用户关注量")
    private Integer shopLikeCount;

    @ApiModelProperty(" 用户关注量 是否正波比,true = 正波比,false = 反波比")
    private Boolean shopLickMoveFlag = Boolean.TRUE;

    @ApiModelProperty("用户关注量，波比值")
    private Integer shopLickMoveVal;

    @ApiModelProperty("用户来访量")
    private Integer shopLookCount;






    @ApiModelProperty("用户来访量 是否正波比,true = 正波比,false = 反波比")
    private Boolean shopLookMoveFlag = Boolean.TRUE;

    @ApiModelProperty("用户来访量，波比值")
    private Integer shopLookMoveVal;





    @ApiModelProperty("总评价数量 用来计算 进度条 用 求百分比  ")
    private Integer evaluationCount;

    @ApiModelProperty("商户综合评分")
    private Integer shopSvgCount;


    @ApiModelProperty("评论0星数量")
    private Integer otherStars;

    @ApiModelProperty("评论1星数量")
    private Integer oneStars;

    @ApiModelProperty("评论2星数量")
    private Integer twoStars;

    @ApiModelProperty("评论3星数量")
    private Integer threeStars;

    @ApiModelProperty("评论4星数量")
    private Integer fourStars;

    @ApiModelProperty("评论5星数量")
    private Integer fiveStars;


}
