package com.fw.system.web.model.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商甲产出日志表
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwShangjiaCreateLog extends Model<FwShangjiaCreateLog> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    /**
     * 产出的商甲数量
     */
    @TableField("log_count")
    private BigDecimal logCount;

    /**
     * 产出人id
     */
    @TableField("user_id")
    private String userId;

    /**
     * 产出人姓名
     */
    @TableField("user_name")
    private String userName;

    /**
     * 电话
     */
    @TableField("phone")
    private String phone;

    /**
     * 产出时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 日志名称
     */
    @TableField("log_name")
    private String logName;

    /**
     * 可变type ....
     */
    @TableField("log_type")
    private Integer logType;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
