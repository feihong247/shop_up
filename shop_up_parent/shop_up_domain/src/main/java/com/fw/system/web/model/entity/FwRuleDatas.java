package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 数据规则表
 * </p>
 *
 * @author
 * @since 2021-06-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwRuleDatas extends Model<FwRuleDatas> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId("id")
    private String id;

    /**
     * 规则数据名称
     */
    @TableField("rule_name")
    private String ruleName;

    /**
     * 数值
     */
    @TableField("rule_count")
    private BigDecimal ruleCount;

    /**
     * 规则类型
     */
    @TableField("rule_type")
    private Integer ruleType;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
