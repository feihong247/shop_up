package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwProblem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 常见问题 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface IFwProblemService extends IService<FwProblem> {

}
