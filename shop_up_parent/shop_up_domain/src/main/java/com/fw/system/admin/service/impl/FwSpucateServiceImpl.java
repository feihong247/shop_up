package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwSpucateMapper;
import com.fw.system.admin.domain.FwSpucate;
import com.fw.system.admin.service.IFwSpucateService;

/**
 * 商品类目Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwSpucateServiceImpl extends ServiceImpl<FwSpucateMapper, FwSpucate> implements IFwSpucateService
{
    @Autowired
    private FwSpucateMapper fwSpucateMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询商品类目
     * 
     * @param id 商品类目ID
     * @return 商品类目
     */
    @Override
    public FwSpucate selectFwSpucateById(String id)
    {
        return fwSpucateMapper.selectFwSpucateById(id);
    }

    /**
     * 查询商品类目列表
     * 
     * @param fwSpucate 商品类目
     * @return 商品类目
     */
    @Override
    public List<FwSpucate> selectFwSpucateList(FwSpucate fwSpucate)
    {
        return fwSpucateMapper.selectFwSpucateList(fwSpucate);
    }

    /**
     * 新增商品类目
     * 
     * @param fwSpucate 商品类目
     * @return 结果
     */
    @Override
    public int insertFwSpucate(FwSpucate fwSpucate)
    {
        fwSpucate.setCreateTime(DateUtils.getNowDate());
        fwSpucate.setId(idXD.nextId());
        return fwSpucateMapper.insertFwSpucate(fwSpucate);
    }

    /**
     * 修改商品类目
     * 
     * @param fwSpucate 商品类目
     * @return 结果
     */
    @Override
    public int updateFwSpucate(FwSpucate fwSpucate)
    {
        fwSpucate.setUpdateTime(DateUtils.getNowDate());
        return fwSpucateMapper.updateFwSpucate(fwSpucate);
    }

    /**
     * 批量删除商品类目
     * 
     * @param ids 需要删除的商品类目ID
     * @return 结果
     */
    @Override
    public int deleteFwSpucateByIds(String[] ids)
    {
        return fwSpucateMapper.deleteFwSpucateByIds(ids);
    }

    /**
     * 删除商品类目信息
     * 
     * @param id 商品类目ID
     * @return 结果
     */
    @Override
    public int deleteFwSpucateById(String id)
    {
        return fwSpucateMapper.deleteFwSpucateById(id);
    }
}
