package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 订单与spu中间对象 fw_osjoin
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_osjoin")
@ApiModel(value="订单与spu中间", description="订单与spu中间表")
public class FwOsjoin extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** spu的主键 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "spu的主键")
    @TableField("spu_id")
    private String spuId;

    /** sku的主键 */
    @ApiModelProperty(value = "spu的主键")
    @Excel(name = "sku的主键")
    @TableField("sku_id")
    private String skuId;

    /** 订单的主键 */
    @ApiModelProperty(value = "sku的主键")
    @Excel(name = "订单的主键")
    @TableField("order_id")
    private String orderId;




}
