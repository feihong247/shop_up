package com.fw.system.web.model.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * <p>
 * 意见反馈
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Api(tags = "意见反馈vo")
public class FreedVo {


    /**
     * 主键
     */
    @ApiModelProperty("主键")
    private String id;

    /**
     * 用户编号
     */
    @ApiModelProperty("用户编号")
    private String userId;

    /**
     * 反馈类型
     */
    @ApiModelProperty("反馈类型")
    private String freedType;

    /**
     * 反馈意见
     */
    @ApiModelProperty("反馈意见")
    private String freedInfo;

    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;



}
