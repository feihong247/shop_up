package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwLogShopingAgency;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户购买代理日志Mapper接口
 *
 * @author yanwei
 * @date 2021-06-05
 */
public interface FwLogShopingAgencyMapper extends BaseMapper<FwLogShopingAgency>
{
    /**
     * 查询用户购买代理日志
     *
     * @param id 用户购买代理日志ID
     * @return 用户购买代理日志
     */
    public FwLogShopingAgency selectFwLogShopingAgencyById(String id);

    /**
     * 查询用户购买代理日志列表
     *
     * @param fwLogShopingAgency 用户购买代理日志
     * @return 用户购买代理日志集合
     */
    public List<FwLogShopingAgency> selectFwLogShopingAgencyList(FwLogShopingAgency fwLogShopingAgency);

    /**
     * 新增用户购买代理日志
     *
     * @param fwLogShopingAgency 用户购买代理日志
     * @return 结果
     */
    public int insertFwLogShopingAgency(FwLogShopingAgency fwLogShopingAgency);

    /**
     * 修改用户购买代理日志
     *
     * @param fwLogShopingAgency 用户购买代理日志
     * @return 结果
     */
    public int updateFwLogShopingAgency(FwLogShopingAgency fwLogShopingAgency);

    /**
     * 删除用户购买代理日志
     *
     * @param id 用户购买代理日志ID
     * @return 结果
     */
    public int deleteFwLogShopingAgencyById(String id);

    /**
     * 批量删除用户购买代理日志
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwLogShopingAgencyByIds(String[] ids);
}
