package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户消息
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwMsg extends Model<FwMsg> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("id")
    @ApiModelProperty("消息主键")
    private String id;

    /**
     * 用户编号
     */
    @TableField("user_id")
    @ApiModelProperty("商铺编号")
    private String userId;

    /**
     * 消息标题
     */
    @TableField("title")
    @ApiModelProperty("消息标题")
    private String title;

    /**
     * 消息内容
     */
    @TableField("content")
    @ApiModelProperty("消息内容")
    private String content;

    /**
     * 是否 已读
     * 0 未读，1已读
     */
    @TableField("is_read")
    @ApiModelProperty("0 未读，1已读")
    private Integer isRead;
    /**
     * 创建人
     */
    @TableField("create_by")
    @ApiModelProperty("创建人 可以不展示")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @TableField("update_by")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public void createD(String createId){
        this.createBy = createId;
        this.createTime = LocalDateTime.now();
        if (this.createTime != null)
            this.updateTime = this.createTime;
        this.updateBy = this.createBy;
    }

    public void updateD(String updateId){
        this.updateBy = updateId;
        this.updateTime = LocalDateTime.now();
    }

}
