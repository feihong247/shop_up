package com.fw.system.web.service.impl;

import com.fw.system.web.model.entity.FwWithdraw;
import com.fw.system.web.dao.FwWithdrawMapper;
import com.fw.system.web.service.IFwLogsService;
import com.fw.system.web.service.IFwWithdrawService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户提现表 服务实现类
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Service
public class FwWithdrawServiceImpl extends ServiceImpl<FwWithdrawMapper, FwWithdraw> implements IFwWithdrawService {
    @Autowired
    private IFwLogsService logsService;
    public void withdrawIntegral(){

    }
}
