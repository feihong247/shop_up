package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwWithdrawShangjia;

/**
 * <p>
 * 积分销毁商甲表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-07-08
 */
public interface FwWithdrawShangjiaMapper extends BaseMapper<FwWithdrawShangjia> {

}
