package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.math.BigDecimal;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fw.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwUser extends Model<FwUser> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("id")
    private String id;

    /**
     * 支付宝唯一标示
     */
    @ApiModelProperty("支付宝唯一标示")
    @TableField("app_auth_token")
    private String appAuthToken;

    /**
     * 微信唯一标示
     */
    @ApiModelProperty("微信唯一标示")
    @TableField("open_id")
    private String openId;

    /**
     * 用户名称
     */
    @ApiModelProperty("用户名称")
    @TableField("user_name")
    private String userName;

    /**
     * 用户手机号
     */
    @ApiModelProperty("用户手机号")
    @TableField("phone")
    private String phone;

    /**
     * 用户性别
     */
    @ApiModelProperty("用户性别")
    @TableField("sex")
    private Integer sex;

    /**
     * 用户月收入
     */
    @ApiModelProperty("用户月收入")
    @TableField("range_money")
    private String rangeMoney;

    /**
     * 用户标签,存标签名称即可，使用,号分隔开
     */
    @ApiModelProperty("用户标签,存标签名称即可，使用,号分隔开")
    @TableField("labels")
    private String labels;

    /**
     * 用户生日
     */
    @ApiModelProperty("用户生日")
    @TableField("birthday")
    private LocalDate birthday;

    /**
     * 用户编号_平台编号，可以充当邀请码来使用
     */
    @ApiModelProperty("用户编号_平台编号")
    @TableField("system_host")
    private String systemHost;

    /**
     * 用户头像
     */
    @ApiModelProperty("用户头像")
    @TableField("head_image")
    private String headImage;

    /**
     * 我的上级编号
     */
    @ApiModelProperty("我的上级编号")
    @TableField("parent_id")
    private String parentId;

    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    @TableField("create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty("更新人")
    @TableField("update_by")
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @TableField("update_time")
    private LocalDateTime updateTime;

    /** 用户密码 */
    @ApiModelProperty("用户密码")
    @TableField("pass_word")
    private String passWord;

    /** 设备号 */
    @ApiModelProperty("设备号")
    @TableField("imei")
    private String imei;

    /** 支付密码 */
    @ApiModelProperty("支付密码")
    @TableField("pay_pass_word")
    private String payPassWord;


    /** 用户是否可多级得消证,0:不可,1可 */
    @ApiModelProperty(value = "用户是否可多级得消证,0:不可,1可")
    @TableField("disappear_is_more")
    private Integer disappearIsMore;

    /** 用户是否可提现,0不可,1可 */
    @ApiModelProperty(value = "用户是否可提现,0不可,1可")
    @TableField("is_withdraw")
    private Integer isWithdraw;

    /** 支付宝真实姓名 */
    @ApiModelProperty(value = "支付宝真实姓名")
    @TableField("ali_user_name")
    private String aliUserName;

    /** 支付宝绑定手机号 */
    @ApiModelProperty(value = "支付宝绑定手机号")
    @TableField("ali_phone")
    private String aliPhone;


    /** 积分兑换比例 */
    @ApiModelProperty(value = "积分兑换比例")
    @TableField("to_release")
    private BigDecimal toRelease;

    /**
     * 管理标识
     * @return
     */
    @ApiModelProperty(value = "0 = 普通用户，1= 技术，2= 管理 3=原始")
    @TableField("is_account")
    private Integer isAccount;

    @ApiModelProperty(value = "二级密码-作为条件使用")
    @TableField(exist = false)
    private String twoPassWord;

    @ApiModelProperty(value = "身份证号")
    @TableField("cert_no")
    private String certNo;
    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
