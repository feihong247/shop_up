package com.fw.system.web.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 商甲交易,买卖表
 * </p>
 *
 * @author
 * @since 2021-06-08
 */
@Data
@Api(tags = "商甲交易vo")
public class ShangjiaDealVo {

    @TableId("id")
    private String id;

    /**
     * 卖出人编号
     */
    @ApiModelProperty("卖出人编号")
    private String userId;

    /** 用户昵称 */
    @ApiModelProperty("用户昵称")
    private String userName;

    /** 用户头像 */
    @ApiModelProperty("用户头像")
    private String headImage;


    /** 商甲总量 */
    @ApiModelProperty(value = "商甲总量")
    private BigDecimal countJia;


    /**
     * 商甲单价
     */
    @ApiModelProperty("商甲单价")
    private BigDecimal money;

    /**
     * 商甲卖出数量
     */
    @ApiModelProperty("商甲卖出数量")
    private Integer count;

    /**
     * 交易状态,0:售卖中,1:已卖出
     */
    @ApiModelProperty("交易状态,0:售卖中,1:已卖出")
    private Integer type;

    /**
     * 创建时间/寄售时间
     */
    @ApiModelProperty("创建时间/寄售时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty("买入人")
    private String updateBy;

    /**
     * 更新时间/卖出时间
     */
    @ApiModelProperty("更新时间/卖出时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;


}
