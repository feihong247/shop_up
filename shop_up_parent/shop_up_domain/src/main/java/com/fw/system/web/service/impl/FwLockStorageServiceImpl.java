package com.fw.system.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.system.web.dao.FwLockStorageMapper;
import com.fw.system.web.model.entity.FwLockStorage;
import com.fw.system.web.service.IFwLockStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 锁仓池 服务实现类
 * </p>
 *
 * @author
 * @since 2021-06-28
 */
@Service
public class FwLockStorageServiceImpl extends ServiceImpl<FwLockStorageMapper, FwLockStorage> implements IFwLockStorageService {

    @Autowired
    private FwLockStorageMapper storageMapper;
    @Override
    public List<FwLockStorage> listStorages() {


        return  storageMapper.listStorages();
    }
}
