package com.fw.system.web.model.vo;

import com.fw.system.web.model.entity.FwCart;
import com.fw.system.web.model.entity.FwShop;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

/**
 * @Effect 购物车VO
 * @Author 姚自强
 * @Date 2021/6/10
 **/
@Data
@ApiModel("购物车VO")
public class CartVo {

    private FwShop shop;

    private List<FwCart> cartList;
}
