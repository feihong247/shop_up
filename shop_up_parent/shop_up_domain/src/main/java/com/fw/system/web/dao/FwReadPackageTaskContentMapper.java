package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwReadPackageTaskContent;

/**
 * <p>
 * 红包关联任务用户完成情况表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-09-03
 */
public interface FwReadPackageTaskContentMapper extends BaseMapper<FwReadPackageTaskContent> {

}
