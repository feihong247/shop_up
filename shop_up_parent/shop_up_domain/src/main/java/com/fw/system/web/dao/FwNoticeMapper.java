package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwNotice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统公告&通知 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwNoticeMapper extends BaseMapper<FwNotice> {

}
