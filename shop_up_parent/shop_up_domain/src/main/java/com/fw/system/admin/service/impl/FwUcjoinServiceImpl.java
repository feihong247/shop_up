package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwUcjoinMapper;
import com.fw.system.admin.domain.FwUcjoin;
import com.fw.system.admin.service.IFwUcjoinService;

/**
 * 我的收藏中间Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwUcjoinServiceImpl extends ServiceImpl<FwUcjoinMapper, FwUcjoin> implements IFwUcjoinService
{
    @Autowired
    private FwUcjoinMapper fwUcjoinMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询我的收藏中间
     * 
     * @param id 我的收藏中间ID
     * @return 我的收藏中间
     */
    @Override
    public FwUcjoin selectFwUcjoinById(String id)
    {
        return fwUcjoinMapper.selectFwUcjoinById(id);
    }

    /**
     * 查询我的收藏中间列表
     * 
     * @param fwUcjoin 我的收藏中间
     * @return 我的收藏中间
     */
    @Override
    public List<FwUcjoin> selectFwUcjoinList(FwUcjoin fwUcjoin)
    {
        return fwUcjoinMapper.selectFwUcjoinList(fwUcjoin);
    }

    /**
     * 新增我的收藏中间
     * 
     * @param fwUcjoin 我的收藏中间
     * @return 结果
     */
    @Override
    public int insertFwUcjoin(FwUcjoin fwUcjoin)
    {
        fwUcjoin.setCreateTime(DateUtils.getNowDate());
        fwUcjoin.setId(idXD.nextId());
        return fwUcjoinMapper.insertFwUcjoin(fwUcjoin);
    }

    /**
     * 修改我的收藏中间
     * 
     * @param fwUcjoin 我的收藏中间
     * @return 结果
     */
    @Override
    public int updateFwUcjoin(FwUcjoin fwUcjoin)
    {
        fwUcjoin.setUpdateTime(DateUtils.getNowDate());
        return fwUcjoinMapper.updateFwUcjoin(fwUcjoin);
    }

    /**
     * 批量删除我的收藏中间
     * 
     * @param ids 需要删除的我的收藏中间ID
     * @return 结果
     */
    @Override
    public int deleteFwUcjoinByIds(String[] ids)
    {
        return fwUcjoinMapper.deleteFwUcjoinByIds(ids);
    }

    /**
     * 删除我的收藏中间信息
     * 
     * @param id 我的收藏中间ID
     * @return 结果
     */
    @Override
    public int deleteFwUcjoinById(String id)
    {
        return fwUcjoinMapper.deleteFwUcjoinById(id);
    }
}
