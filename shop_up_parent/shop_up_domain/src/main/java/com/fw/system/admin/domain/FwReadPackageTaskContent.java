package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 红包关联任务用户完成情况对象 fw_read_package_task_content
 * 
 * @author yanwei
 * @date 2021-10-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_read_package_task_content")
@ApiModel(value="红包关联任务用户完成情况", description="红包关联任务用户完成情况表")
public class FwReadPackageTaskContent extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 关联的任务ID */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "关联的任务ID")
    @TableField("task_id")
    private String taskId;

    /** 领取人id */
    @ApiModelProperty(value = "关联的任务ID")
    @Excel(name = "领取人id")
    @TableField("user_id")
    private String userId;

    /** 任务需要完成的数量 */
    @ApiModelProperty(value = "领取人id")
    @Excel(name = "任务需要完成的数量")
    @TableField("task_num")
    private Long taskNum;

    /** 累加的红包额度 */
    @ApiModelProperty(value = "任务需要完成的数量")
    @Excel(name = "累加的红包额度")
    @TableField("task_amount")
    private Long taskAmount;

    /** 用户已完成的次数 */
    @ApiModelProperty(value = "累加的红包额度")
    @Excel(name = "用户已完成的次数")
    @TableField("user_num")
    private Long userNum;

    /** 红包ID */
    @ApiModelProperty(value = "用户已完成的次数")
    @Excel(name = "红包ID")
    @TableField("read_id")
    private String readId;

    /** 1 已完成 0进行中 */
    @ApiModelProperty(value = "红包ID")
    @Excel(name = "1 已完成 0进行中")
    @TableField("is_state")
    private Long isState;

    /** 用户名称 */
    @ApiModelProperty(value = "用户名称")
    @Excel(name = "用户名称")
    @TableField(exist = false)
    private String userName;

    /** 用户手机号 */
    @ApiModelProperty(value = "用户名称")
    @Excel(name = "用户手机号")
    @TableField(exist = false)
    private String phone;


}
