package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 一县一品对象 fw_one_area_item
 * 
 * @author yanwei
 * @date 2021-11-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_one_area_item")
@ApiModel(value="一县一品", description="一县一品表")
public class FwOneAreaItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 省 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "省")
    @TableField("province")
    private String province;

    /** 市 */
    @ApiModelProperty(value = "省")
    @Excel(name = "市")
    @TableField("city")
    private String city;

    /** 县 */
    @ApiModelProperty(value = "市")
    @Excel(name = "县")
    @TableField("area")
    private String area;

    /** 推荐官简介 */
    @ApiModelProperty(value = "县")
    @Excel(name = "推荐官简介")
    @TableField("user_content")
    private String userContent;

    /** 图片地址多个逗号隔开 */
    @ApiModelProperty(value = "推荐官简介")
    @Excel(name = "图片地址多个逗号隔开")
    @TableField("img_url")
    private String imgUrl;

    /** 视频地址 */
    @ApiModelProperty(value = "图片地址多个逗号隔开")
    @Excel(name = "视频地址")
    @TableField("video_url")
    private String videoUrl;

    /** 关联的一县一品商品id */
    @ApiModelProperty(value = "视频地址")
    @Excel(name = "关联的一县一品商品id")
    @TableField("spu_id")
    private String spuId;

    /** 1展示 0删除 */
    @ApiModelProperty(value = "关联的一县一品商品id")
    @Excel(name = "1展示 0删除")
    @TableField("is_state")
    private Long isState;




}
