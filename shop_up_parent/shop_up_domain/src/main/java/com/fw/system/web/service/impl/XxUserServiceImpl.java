package com.fw.system.web.service.impl;

import com.fw.system.web.dao.XxUserMapper;
import com.fw.system.web.model.entity.XxUser;
import com.fw.system.web.service.IXxUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-07-31
 */
@Service
public class XxUserServiceImpl extends ServiceImpl<XxUserMapper, XxUser> implements IXxUserService {

}
