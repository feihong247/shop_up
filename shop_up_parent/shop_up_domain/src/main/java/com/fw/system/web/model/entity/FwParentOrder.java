package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fw.annotation.Excel;
import com.fw.system.web.model.vo.AddrsVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/** 大订单 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel("大订单")
public class FwParentOrder extends Model<FwParentOrder> {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("大订单id")
    @TableId("parent_order_id")
    private String parentOrderId;

    @ApiModelProperty("用户编号")
    @TableField("user_id")
    private String userId;

    @ApiModelProperty("总订单金额")
    @TableField("parent_order_money")
    private BigDecimal parentOrderMoney;

    /**
     * 订单运费
     */
    @TableField("shopping")
    @ApiModelProperty("订单运费")
    private BigDecimal shopping;

    /**
     * 订单满减
     */
    @TableField("full_discount")
    @ApiModelProperty("订单满减")
    private BigDecimal fullDiscount;

    /**
     * 订单应付（不计算优惠）
     */
    @TableField("total_full_price")
    @ApiModelProperty("订单应付（不计算优惠）")
    private BigDecimal totalFullPrice;

    @TableField(exist = false)
    @ApiModelProperty("订单列表")
    private List<FwOrder> orderList;

    @ApiModelProperty("大订单状态")
    @TableField("state")
    private Integer state;

    @ApiModelProperty("大订单状态")
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @TableField(exist = false)
    @ApiModelProperty("地址对象")
    private AddrsVo fwAddr;

    @TableField(exist = false)
    @ApiModelProperty("地址字符串，&连接")
    private String addrString;

    @TableField(exist = false)
    @ApiModelProperty("竞猜房间号")
    private String guessId;
}
