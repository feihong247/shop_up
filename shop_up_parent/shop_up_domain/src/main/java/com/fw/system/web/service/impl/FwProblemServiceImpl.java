package com.fw.system.web.service.impl;

import com.fw.system.web.model.entity.FwProblem;
import com.fw.system.web.dao.FwProblemMapper;
import com.fw.system.web.service.IFwProblemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 常见问题 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Service
public class FwProblemServiceImpl extends ServiceImpl<FwProblemMapper, FwProblem> implements IFwProblemService {

}
