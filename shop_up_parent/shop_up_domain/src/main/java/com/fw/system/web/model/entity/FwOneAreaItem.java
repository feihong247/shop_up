package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 一县一品表
 * </p>
 *
 * @author  
 * @since 2021-09-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwOneAreaItem extends Model<FwOneAreaItem> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    /**
     * 省
     */
    @TableField("province")
    private String province;

    /**
     * 市
     */
    @TableField("city")
    private String city;

    /**
     * 县
     */
    @TableField("area")
    private String area;

    /**
     * 推荐官简介
     */
    @TableField("user_content")
    private String userContent;

    /**
     * 图片地址多个逗号隔开
     */
    @TableField("img_url")
    private String imgUrl;

    /**
     * 视频地址
     */
    @TableField("video_url")
    private String videoUrl;

    /**
     * 关联的一县一品商品id
     */
    @TableField("spu_id")
    private String spuId;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 1展示 0删除
     */
    @TableField("is_state")
    private Integer isState;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
