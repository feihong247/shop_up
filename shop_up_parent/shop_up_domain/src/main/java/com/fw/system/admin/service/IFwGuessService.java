package com.fw.system.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

import com.fw.system.admin.domain.FwGuess;

/**
 * 竞猜Service接口
 *
 * @author yanwei
 * @Date 2021-06-23
 */
public interface IFwGuessService extends IService<FwGuess> {
    /**
     * 查询竞猜
     *
     * @param guessId 竞猜ID
     * @return 竞猜
     */
    FwGuess selectFwGuessById(String guessId);

    /**
     * 查询竞猜列表
     *
     * @param fwGuess 竞猜
     * @return 竞猜集合
     */
    List<FwGuess> selectFwGuessList(FwGuess fwGuess);

    /**
     * 新增竞猜
     *
     * @param fwGuess 竞猜
     * @return 结果
     */
    int insertFwGuess(FwGuess fwGuess);

    /**
     * 修改竞猜
     *
     * @param fwGuess 竞猜
     * @return 结果
     */
    int updateFwGuess(FwGuess fwGuess);

    /**
     * 批量删除竞猜
     *
     * @param guessIds 需要删除的竞猜ID
     * @return 结果
     */
    int deleteFwGuessByIds(String[] guessIds);

    /**
     * 删除竞猜信息
     *
     * @param guessId 竞猜ID
     * @return 结果
     */
    int deleteFwGuessById(String guessId);
}
