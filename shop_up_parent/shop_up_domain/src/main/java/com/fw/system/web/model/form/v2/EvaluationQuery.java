package com.fw.system.web.model.form.v2;

import com.fw.system.web.model.form.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("商户评价查询结果")
public class EvaluationQuery extends PageQuery {

    @ApiModelProperty("有图 = 1")
    private Integer isImg;

    @ApiModelProperty("最新 = 1 , 代表今天的")
    private Integer isNew;


    @ApiModelProperty("评价分 =  0，1，2，3，4，5")
    private Integer rangeSvg;
}
