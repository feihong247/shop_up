package com.fw.system.web.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.constant.Constant;
import com.fw.enums.LogsModelEnum;
import com.fw.enums.LogsTypeEnum;
import com.fw.system.web.dao.FwWithdrawMapper;
import com.fw.system.web.dao.FwWithdrawShangjiaMapper;
import com.fw.system.web.model.entity.FwShangjia;
import com.fw.system.web.model.entity.FwShangjipool;
import com.fw.system.web.dao.FwShangjipoolMapper;
import com.fw.system.web.model.entity.FwWithdraw;
import com.fw.system.web.model.entity.FwWithdrawShangjia;
import com.fw.system.web.service.IFwLogsService;
import com.fw.system.web.service.IFwShangjiaService;
import com.fw.system.web.service.IFwShangjipoolService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.system.web.service.IFwWithdrawService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 商甲池表 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Service

public class FwShangjipoolServiceImpl extends ServiceImpl<FwShangjipoolMapper, FwShangjipool> implements IFwShangjipoolService {

    @Autowired
    private FwWithdrawShangjiaMapper fwWithdrawShangjiaMapper;
    @Autowired
    private IFwShangjiaService fwShangjiaService;
    @Autowired
    private IFwLogsService logsService;
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void destoryShangjia(String userId) {
        List<FwWithdrawShangjia> fwWithdrawShangjias = fwWithdrawShangjiaMapper.selectList(new QueryWrapper<>());
        FwWithdrawShangjia fwWithdrawShangjia = fwWithdrawShangjias.get(0);
        BigDecimal count = fwWithdrawShangjia.getIntegral().divide(new BigDecimal(48), 0, BigDecimal.ROUND_DOWN);
        if(count.compareTo(new BigDecimal(0))>0){
            fwWithdrawShangjia.setIntegral(fwWithdrawShangjia.getIntegral().subtract(count.multiply(new BigDecimal(48))));
            fwWithdrawShangjia.setDestoryShangjia(fwWithdrawShangjia.getDestoryShangjia().add(count.multiply(new BigDecimal(48))));
            fwWithdrawShangjia.setUpdateTime(LocalDateTime.now());
            fwWithdrawShangjia.setUpdateBy(userId);
            fwWithdrawShangjiaMapper.updateById(fwWithdrawShangjia);//修改积分销毁商甲记录表
            List<FwShangjia> list = fwShangjiaService.list();
            list.forEach(item->{
                item.setUpdateTime(LocalDateTime.now());
                item.setUpdateBy(userId);
                switch (item.getId()){
                    case Constant.ShangJia.CONSUME_ID:
                        //原始、技术、管理、推广市场、消费对应2、1、1、4、40,原始销毁2个，技术1个，管理1个，市场4个，消费40个
                        BigDecimal destory=item.getShopIssue().compareTo(new BigDecimal(40).multiply(count))>=0?new BigDecimal(40).multiply(count):item.getShopIssue();
                        item.setShangjiaQuota(item.getShopIssue().subtract(destory));
                        logsService.saveLogs(userId,userId, LogsTypeEnum.MONEY_DESTORY.getTypeName(), LogsModelEnum.SHANG_JIA_MODEL.getModelName(), "积分提现销毁消费商甲" , destory.toString());
                        fwShangjiaService.updateById(item);
                        break;
                    case Constant.ShangJia.ORIGINAL_ID:
                        BigDecimal destoryOri=item.getShopIssue().compareTo(new BigDecimal(2).multiply(count))>=0?new BigDecimal(2).multiply(count):item.getShopIssue();
                        item.setShangjiaQuota(item.getShopIssue().subtract(destoryOri));
                        logsService.saveLogs(userId,userId, LogsTypeEnum.MONEY_DESTORY.getTypeName(), LogsModelEnum.SHANG_JIA_MODEL.getModelName(), "积分提现销毁原始商甲" , destoryOri.toString());
                        fwShangjiaService.updateById(item);
                        break;
                    case Constant.ShangJia.BAZAAR_ID:
                        BigDecimal destoryBaz=item.getShopIssue().compareTo(new BigDecimal(4).multiply(count))>=0?new BigDecimal(4).multiply(count):item.getShopIssue();
                        item.setShangjiaQuota(item.getShopIssue().subtract(destoryBaz));
                        logsService.saveLogs(userId,userId, LogsTypeEnum.MONEY_DESTORY.getTypeName(), LogsModelEnum.SHANG_JIA_MODEL.getModelName(), "积分提现销毁市场商甲" , destoryBaz.toString());
                        fwShangjiaService.updateById(item);
                        break;
                    case Constant.ShangJia.TECHNOLOGY_ID:
                        BigDecimal destoryTec=item.getShopIssue().compareTo(new BigDecimal(1).multiply(count))>=0?new BigDecimal(1).multiply(count):item.getShopIssue();
                        item.setShangjiaQuota(item.getShopIssue().subtract(destoryTec));
                        logsService.saveLogs(userId,userId, LogsTypeEnum.MONEY_DESTORY.getTypeName(), LogsModelEnum.SHANG_JIA_MODEL.getModelName(), "积分提现销毁技术商甲" , destoryTec.toString());
                        fwShangjiaService.updateById(item);
                        break;
                    case Constant.ShangJia.MANAGEMENT_ID:
                        BigDecimal destoryMan=item.getShopIssue().compareTo(new BigDecimal(1).multiply(count))>=0?new BigDecimal(1).multiply(count):item.getShopIssue();
                        item.setShangjiaQuota(item.getShopIssue().subtract(destoryMan));
                        logsService.saveLogs(userId,userId, LogsTypeEnum.MONEY_DESTORY.getTypeName(), LogsModelEnum.SHANG_JIA_MODEL.getModelName(), "积分提现销毁管理商甲" , destoryMan.toString());
                        fwShangjiaService.updateById(item);
                }
            });
        }

    }
}
