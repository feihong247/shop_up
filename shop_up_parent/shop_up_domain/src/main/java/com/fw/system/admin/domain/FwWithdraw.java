package com.fw.system.admin.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 用户提现对象 fw_withdraw
 *
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_withdraw")
@ApiModel(value="用户提现", description="用户提现表")
public class FwWithdraw extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 用户编号 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "用户编号")
    @TableField("user_id")
    private String userId;

    /** 提现积分 */
    @ApiModelProperty(value = "用户编号")
    @Excel(name = "提现积分")
    @TableField("integral")
    private BigDecimal integral;

    /** 实际提现金额 */
    @ApiModelProperty(value = "提现积分")
    @Excel(name = "实际提现金额")
    @TableField("money")
    private BigDecimal money;

    /** 审核状态,0=待审核，1=审核成功，2=审核失败 */
    @ApiModelProperty(value = "实际提现金额")
    @Excel(name = "审核状态,0=待审核，1=审核成功，2=审核失败")
    @TableField("status")
    private Integer status;

    /**
     * 用户名称
     */
    @TableField(exist = false)
    private String userName;

    /**
     * 用户手机号
     */
    @TableField(exist = false)
    private String phone;

    /**
     * 用户头像
     */
    @TableField(exist = false)
    private String headImage;



}
