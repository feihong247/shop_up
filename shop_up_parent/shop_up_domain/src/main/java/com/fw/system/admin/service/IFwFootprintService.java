package com.fw.system.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

import com.fw.system.admin.domain.FwFootprint;

/**
 * 足迹Service接口
 *
 * @author yanwei
 * @date 2021-06-23
 */
public interface IFwFootprintService extends IService<FwFootprint> {
    /**
     * 查询足迹
     *
     * @param id 足迹ID
     * @return 足迹
     */
    public FwFootprint selectFwFootprintById(String id);

    /**
     * 查询足迹列表
     *
     * @param fwFootprint 足迹
     * @return 足迹集合
     */
    public List<FwFootprint> selectFwFootprintList(FwFootprint fwFootprint);

    /**
     * 新增足迹
     *
     * @param fwFootprint 足迹
     * @return 结果
     */
    public int insertFwFootprint(FwFootprint fwFootprint);

    /**
     * 修改足迹
     *
     * @param fwFootprint 足迹
     * @return 结果
     */
    public int updateFwFootprint(FwFootprint fwFootprint);

    /**
     * 批量删除足迹
     *
     * @param ids 需要删除的足迹ID
     * @return 结果
     */
    public int deleteFwFootprintByIds(String[] ids);

    /**
     * 删除足迹信息
     *
     * @param id 足迹ID
     * @return 结果
     */
    public int deleteFwFootprintById(String id);
}
