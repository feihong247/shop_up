package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwShangjiaReleaseLog;

/**
 * <p>
 * 技术商甲管理商甲推广商甲释放明细表 服务类
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
public interface IFwShangjiaReleaseLogService extends IService<FwShangjiaReleaseLog> {

}
