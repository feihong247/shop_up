package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwImagesMapper;
import com.fw.system.admin.domain.FwImages;
import com.fw.system.admin.service.IFwImagesService;

/**
 * 商品图片列Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwImagesServiceImpl extends ServiceImpl<FwImagesMapper, FwImages> implements IFwImagesService
{
    @Autowired
    private FwImagesMapper fwImagesMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询商品图片列
     * 
     * @param id 商品图片列ID
     * @return 商品图片列
     */
    @Override
    public FwImages selectFwImagesById(String id)
    {
        return fwImagesMapper.selectFwImagesById(id);
    }

    /**
     * 查询商品图片列列表
     * 
     * @param fwImages 商品图片列
     * @return 商品图片列
     */
    @Override
    public List<FwImages> selectFwImagesList(FwImages fwImages)
    {
        return fwImagesMapper.selectFwImagesList(fwImages);
    }

    /**
     * 新增商品图片列
     * 
     * @param fwImages 商品图片列
     * @return 结果
     */
    @Override
    public int insertFwImages(FwImages fwImages)
    {
        fwImages.setCreateTime(DateUtils.getNowDate());
        fwImages.setId(idXD.nextId());
        return fwImagesMapper.insertFwImages(fwImages);
    }

    /**
     * 修改商品图片列
     * 
     * @param fwImages 商品图片列
     * @return 结果
     */
    @Override
    public int updateFwImages(FwImages fwImages)
    {
        fwImages.setUpdateTime(DateUtils.getNowDate());
        return fwImagesMapper.updateFwImages(fwImages);
    }

    /**
     * 批量删除商品图片列
     * 
     * @param ids 需要删除的商品图片列ID
     * @return 结果
     */
    @Override
    public int deleteFwImagesByIds(String[] ids)
    {
        return fwImagesMapper.deleteFwImagesByIds(ids);
    }

    /**
     * 删除商品图片列信息
     * 
     * @param id 商品图片列ID
     * @return 结果
     */
    @Override
    public int deleteFwImagesById(String id)
    {
        return fwImagesMapper.deleteFwImagesById(id);
    }
}
