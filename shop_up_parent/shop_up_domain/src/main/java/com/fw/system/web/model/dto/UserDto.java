package com.fw.system.web.model.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fw.system.web.model.entity.FwUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserDto implements Serializable {

    /**
     * 用户主键
     */
    private String id;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 用户手机号
     */
    private String phone;

    /**
     * 我的上级编号
     */
    private String parentId;

    /** 设备号 */
    private String imei;

    /**
     * 商户编号
     */
    private String shopId;


}
