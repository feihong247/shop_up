package com.fw.system.web.service.impl;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.*;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.common.IdXD;
import com.fw.system.web.dao.FwEvaluationMapper;
import com.fw.system.web.dao.FwSpuMapper;
import com.fw.system.web.dao.FwUserMapper;
import com.fw.system.web.model.entity.FwEvaluation;
import com.fw.system.web.model.entity.FwSpu;
import com.fw.system.web.model.entity.FwUser;
import com.fw.system.web.model.form.v2.EvaluationQuery;
import com.fw.system.web.model.vo.EvaluationVo;
import com.fw.system.web.model.vo.v2.EvaluationCountVo;
import com.fw.system.web.model.vo.v2.EvaluationV2Vo;
import com.fw.system.web.service.IFwEvaluationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.utils.DateUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 商品评价 服务实现类
 * </p>
 *
 * @author
 * @since 2021-05-24
 */
@Service
public class FwEvaluationServiceImpl extends ServiceImpl<FwEvaluationMapper, FwEvaluation> implements IFwEvaluationService {
    @Autowired
    private FwEvaluationMapper fwEvaluationMapper;

    @Autowired
    private FwSpuMapper spuMapper;

    @Autowired
    private FwUserMapper userMapper;



    @Autowired
    private IdXD idXD;

    /**
     * 查询商品评价
     *
     * @param id 商品评价ID
     * @return 商品评价
     */
    @Override
    public EvaluationVo selectFwEvaluationById(String id) {
        return fwEvaluationMapper.selectFwEvaluationById(id);
    }

    /**
     * 查询商品评价列表
     *
     * @param fwEvaluation 商品评价
     * @return 商品评价
     */
    @Override
    public List<EvaluationVo> selectFwEvaluationList(FwEvaluation fwEvaluation) {
        return fwEvaluationMapper.selectFwEvaluationList(fwEvaluation);
    }

    /**
     * 新增商品评价
     *
     * @param fwEvaluation 商品评价
     * @return 结果
     */
    @Override
    public int insertFwEvaluation(FwEvaluation fwEvaluation) {
        fwEvaluation.setCreateTime(LocalDateTime.now());
        fwEvaluation.setId(idXD.nextId());
        return fwEvaluationMapper.insertFwEvaluation(fwEvaluation);
    }

    /**
     * 修改商品评价
     *
     * @param fwEvaluation 商品评价
     * @return 结果
     */
    @Override
    public int updateFwEvaluation(FwEvaluation fwEvaluation) {
        fwEvaluation.setUpdateTime(LocalDateTime.now());
        return fwEvaluationMapper.updateFwEvaluation(fwEvaluation);
    }

    /**
     * 批量删除商品评价
     *
     * @param ids 需要删除的商品评价ID
     * @return 结果
     */
    @Override
    public int deleteFwEvaluationByIds(String[] ids) {
        return fwEvaluationMapper.deleteFwEvaluationByIds(ids);
    }

    /**
     * 删除商品评价信息
     *
     * @param id 商品评价ID
     * @return 结果
     */
    @Override
    public int deleteFwEvaluationById(String id) {
        return fwEvaluationMapper.deleteFwEvaluationById(id);
    }

    @Override
    public EvaluationCountVo countEva(String shopId) {
        EvaluationCountVo.EvaluationCountVoBuilder builder = EvaluationCountVo.builder();
        List<FwSpu> fwSpus = spuMapper.selectList(Wrappers.<FwSpu>lambdaQuery().eq(FwSpu::getShopId, shopId));
        List<FwEvaluation> fwEvaluations = null;
        if (fwSpus.isEmpty())return builder.build();

         fwEvaluations = list(Wrappers.<FwEvaluation>lambdaQuery().in(FwEvaluation::getItemId, fwSpus.parallelStream().map(item -> item.getId()).collect(Collectors.toSet())));
         if (fwEvaluations.isEmpty()) return builder.build();

        builder.imgCount(fwEvaluations.parallelStream().filter(item -> StringUtils.isNotBlank(item.getEvaImg())).count());
        builder.newCount(fwEvaluations.parallelStream().filter(item -> LocalDate.now().compareTo(item.getCreateTime().toLocalDate())  == 0).count());
        builder.totalCount(fwEvaluations.size());
        return builder.build();
    }

    @Override
    public PageInfo<EvaluationV2Vo> pageList(EvaluationQuery evaluationQuery,String shopId) {
        // 把所有商户的商品统计出来
        List<FwSpu> fwSpus = spuMapper.selectList(Wrappers.<FwSpu>lambdaQuery().eq(FwSpu::getShopId, shopId));
        if (fwSpus.isEmpty())
            return new PageInfo<>();

        PageHelper.startPage(evaluationQuery.getPageNum(),evaluationQuery.getPageSize());
        List<FwEvaluation> fwEvaluations = list(Wrappers.<FwEvaluation>lambdaQuery()
                .in(FwEvaluation::getItemId,fwSpus.parallelStream().map(item -> item.getId()).collect(Collectors.toSet()))
                .eq(Objects.nonNull(evaluationQuery.getRangeSvg()), FwEvaluation::getStarNum, evaluationQuery.getRangeSvg())
                .isNotNull(Integer.valueOf(1).equals(evaluationQuery.getIsImg()), FwEvaluation::getEvaImg)
                .apply(Integer.valueOf(1).equals(evaluationQuery.getIsNew()), "  date_format(create_time,'%Y-%m-%d') = date_format(NOW(),'%Y-%m-%d')")
                .orderByDesc(FwEvaluation::getCreateTime)
        );
        PageInfo pageInfo = new PageInfo(fwEvaluations);
        // 开始操作
        List<EvaluationV2Vo> evaluationV2Vos = fwEvaluations.parallelStream().map(item -> {
            EvaluationV2Vo evaluationV2Vo = new EvaluationV2Vo();
            BeanUtil.copyProperties(item, evaluationV2Vo);
            FwUser fwUser = userMapper.selectById(item.getUserId());
            evaluationV2Vo.setHeadImage(fwUser.getHeadImage());
            evaluationV2Vo.setUserName(fwUser.getUserName());
            // 时间差值
            evaluationV2Vo.setTimeDesc(DateUtil.formatBetween(Date.from(item.getCreateTime().atZone(ZoneId.systemDefault()).toInstant()), DateUtils.getNowDate(), BetweenFormater.Level.MINUTE));
            return evaluationV2Vo;
        }).collect(Collectors.toList());
        pageInfo.setList(evaluationV2Vos);
        return pageInfo;
    }
}

