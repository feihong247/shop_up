package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.ExampleTable;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yanwei
 * @since 2020-08-05
 */
public interface IExampleTableService extends IService<ExampleTable> {

}
