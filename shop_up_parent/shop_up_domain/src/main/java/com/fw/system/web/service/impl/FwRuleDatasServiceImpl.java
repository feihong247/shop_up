package com.fw.system.web.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.system.web.dao.FwRuleDatasMapper;
import com.fw.system.web.model.entity.FwRuleDatas;
import com.fw.system.web.service.IFwRuleDatasService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * <p>
 * 数据规则表 服务实现类
 * </p>
 *
 * @author
 * @since 2021-06-02
 */
@Service
public class FwRuleDatasServiceImpl extends ServiceImpl<FwRuleDatasMapper, FwRuleDatas> implements IFwRuleDatasService {

    @Override
    public BigDecimal getBigDecimal(String ruleType) {
        return getOne(Wrappers.<FwRuleDatas>lambdaQuery().eq(FwRuleDatas::getRuleType, ruleType)).getRuleCount();
    }

    @Override
    public Integer getInteger(String ruleType) {
        return getBigDecimal(ruleType).intValue();
    }
}
