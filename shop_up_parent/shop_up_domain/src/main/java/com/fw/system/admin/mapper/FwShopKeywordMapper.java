package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwShopKeyword;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 商铺环境展示Mapper接口
 * 
 * @author yanwei
 * @date 2021-07-16
 */
public interface FwShopKeywordMapper extends BaseMapper<FwShopKeyword>
{
    /**
     * 查询商铺环境展示
     * 
     * @param id 商铺环境展示ID
     * @return 商铺环境展示
     */
    public FwShopKeyword selectFwShopKeywordById(String id);

    /**
     * 查询商铺环境展示列表
     * 
     * @param fwShopKeyword 商铺环境展示
     * @return 商铺环境展示集合
     */
    public List<FwShopKeyword> selectFwShopKeywordList(FwShopKeyword fwShopKeyword);

    /**
     * 新增商铺环境展示
     * 
     * @param fwShopKeyword 商铺环境展示
     * @return 结果
     */
    public int insertFwShopKeyword(FwShopKeyword fwShopKeyword);

    /**
     * 修改商铺环境展示
     * 
     * @param fwShopKeyword 商铺环境展示
     * @return 结果
     */
    public int updateFwShopKeyword(FwShopKeyword fwShopKeyword);

    /**
     * 删除商铺环境展示
     * 
     * @param id 商铺环境展示ID
     * @return 结果
     */
    public int deleteFwShopKeywordById(String id);

    /**
     * 批量删除商铺环境展示
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwShopKeywordByIds(String[] ids);
}
