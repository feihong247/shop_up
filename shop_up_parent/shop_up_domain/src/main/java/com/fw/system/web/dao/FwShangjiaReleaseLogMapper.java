package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwShangjiaReleaseLog;

/**
 * <p>
 * 技术商甲管理商甲推广商甲释放明细表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
public interface FwShangjiaReleaseLogMapper extends BaseMapper<FwShangjiaReleaseLog> {

}
