package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwShangjipool;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 商甲池Mapper接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface FwShangjipoolMapper extends BaseMapper<FwShangjipool>
{
    /**
     * 查询商甲池
     * 
     * @param id 商甲池ID
     * @return 商甲池
     */
    public FwShangjipool selectFwShangjipoolById(String id);

    /**
     * 查询商甲池列表
     * 
     * @param fwShangjipool 商甲池
     * @return 商甲池集合
     */
    public List<FwShangjipool> selectFwShangjipoolList(FwShangjipool fwShangjipool);

    /**
     * 新增商甲池
     * 
     * @param fwShangjipool 商甲池
     * @return 结果
     */
    public int insertFwShangjipool(FwShangjipool fwShangjipool);

    /**
     * 修改商甲池
     * 
     * @param fwShangjipool 商甲池
     * @return 结果
     */
    public int updateFwShangjipool(FwShangjipool fwShangjipool);

    /**
     * 删除商甲池
     * 
     * @param id 商甲池ID
     * @return 结果
     */
    public int deleteFwShangjipoolById(String id);

    /**
     * 批量删除商甲池
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwShangjipoolByIds(String[] ids);
}
