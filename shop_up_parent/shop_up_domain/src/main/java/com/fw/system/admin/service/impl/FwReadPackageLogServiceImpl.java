package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwReadPackageLogMapper;
import com.fw.system.admin.domain.FwReadPackageLog;
import com.fw.system.admin.service.IFwReadPackageLogService;

/**
 * 红包抽取记录Service业务层处理
 * 
 * @author yanwei
 * @date 2021-10-12
 */
@Service
public class FwReadPackageLogServiceImpl extends ServiceImpl<FwReadPackageLogMapper, FwReadPackageLog> implements IFwReadPackageLogService
{
    @Autowired
    private FwReadPackageLogMapper fwReadPackageLogMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询红包抽取记录
     * 
     * @param id 红包抽取记录ID
     * @return 红包抽取记录
     */
    @Override
    public FwReadPackageLog selectFwReadPackageLogById(String id)
    {
        return fwReadPackageLogMapper.selectFwReadPackageLogById(id);
    }

    /**
     * 查询红包抽取记录列表
     * 
     * @param fwReadPackageLog 红包抽取记录
     * @return 红包抽取记录
     */
    @Override
    public List<FwReadPackageLog> selectFwReadPackageLogList(FwReadPackageLog fwReadPackageLog)
    {
        return fwReadPackageLogMapper.selectFwReadPackageLogList(fwReadPackageLog);
    }

    /**
     * 新增红包抽取记录
     * 
     * @param fwReadPackageLog 红包抽取记录
     * @return 结果
     */
    @Override
    public int insertFwReadPackageLog(FwReadPackageLog fwReadPackageLog)
    {
        fwReadPackageLog.setCreateTime(DateUtils.getNowDate());
        fwReadPackageLog.setId(idXD.nextId());
        return fwReadPackageLogMapper.insertFwReadPackageLog(fwReadPackageLog);
    }

    /**
     * 修改红包抽取记录
     * 
     * @param fwReadPackageLog 红包抽取记录
     * @return 结果
     */
    @Override
    public int updateFwReadPackageLog(FwReadPackageLog fwReadPackageLog)
    {
        return fwReadPackageLogMapper.updateFwReadPackageLog(fwReadPackageLog);
    }

    /**
     * 批量删除红包抽取记录
     * 
     * @param ids 需要删除的红包抽取记录ID
     * @return 结果
     */
    @Override
    public int deleteFwReadPackageLogByIds(String[] ids)
    {
        return fwReadPackageLogMapper.deleteFwReadPackageLogByIds(ids);
    }

    /**
     * 删除红包抽取记录信息
     * 
     * @param id 红包抽取记录ID
     * @return 结果
     */
    @Override
    public int deleteFwReadPackageLogById(String id)
    {
        return fwReadPackageLogMapper.deleteFwReadPackageLogById(id);
    }
}
