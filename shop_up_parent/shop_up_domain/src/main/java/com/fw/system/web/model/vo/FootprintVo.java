package com.fw.system.web.model.vo;


import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 足迹表
 * </p>
 *
 * @author
 * @since 2021-06-23
 */
@Data
public class FootprintVo{

    /**
     * 主键
     */
    @ApiModelProperty("id")
    private String id;

    /**
     * 用户编号
     */
    @ApiModelProperty("用户编号")
    private String userId;

    /**
     * 商品编号
     */
    @ApiModelProperty("商品编号")
    private String itemId;

    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 商品标题
     */
    @ApiModelProperty("商品标题")
    private String title;

    /**
     * 商品logo
     */
    @ApiModelProperty("商品logo")
    private String logo;

    /**
     * 商品销量
     */
    @ApiModelProperty("商品销量")
    private Integer tops;

    /**
     * 商品原价格
     */
    @ApiModelProperty("商品原价格")
    private BigDecimal virtualPrice;

    /**
     * 商品现价格
     */
    @ApiModelProperty("商品现价格")
    private BigDecimal realPrice;


}
