package com.fw.system.web.model.form;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fw.annotation.IsPhone;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * @Author:yanwei
 * @Date: 2020/8/5 - 15:39
 */
@ApiModel("示例Example VO_ 表单提交")
@Data
public class ExampleForm {


    /**
     * 名称
     */
    @ApiModelProperty(value = "名称",required = true)
    @NotBlank(message = "名称不可为空")
    private String exampleName;

    /**
     * 类型
     */
    @ApiModelProperty("类型")
    @Min(value = 1,message = "类型不可小于1")
    @Max(value = 2,message = "类型不可大于2")
    private Integer exampleType;

    /**
     * 创建日期
     */
    @ApiModelProperty("创建日期")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;

    /**
     * 手机号
     */
    @IsPhone(required = false)
    private String phone;
}
