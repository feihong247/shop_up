package com.fw.system.web.model.form.v2;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel("查询 结果")
public class OrderManageQuery implements Serializable {

    @ApiModelProperty("要查多少条")
    @NotNull(message = "要查多少条不可为空")
    private Integer limit;

    @ApiModelProperty("年 = 1，月 = 2，日 = 3")
    @NotNull(message = "不可为空")
    @Min(value = 1,message = "数值错误")
    @Max(value = 3,message = "数值错误")
    private Integer queryRange;

}
