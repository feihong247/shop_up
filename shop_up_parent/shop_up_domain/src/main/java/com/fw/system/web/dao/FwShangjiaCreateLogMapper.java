package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwShangjiaCreateLog;

import java.util.List;

/**
 * <p>
 * 商甲产出日志表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
public interface FwShangjiaCreateLogMapper extends BaseMapper<FwShangjiaCreateLog> {

    Integer yueDetail();

    List<FwShangjiaCreateLog> dayDetail();
    List<FwShangjiaCreateLog> createAndReleaseYear();

    List<FwShangjiaCreateLog> createAndReleaseMonth();

    List<FwShangjiaCreateLog> createAndReleaseDay();

    int sumAllCreateShangJia();

    int sumYearCreateShangJia();
}
