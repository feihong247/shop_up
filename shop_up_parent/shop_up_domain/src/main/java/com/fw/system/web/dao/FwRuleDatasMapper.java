package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwRuleDatas;

/**
 * <p>
 * 数据规则表 Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-06-02
 */
public interface FwRuleDatasMapper extends BaseMapper<FwRuleDatas> {

}
