package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwFreedMapper;
import com.fw.system.admin.domain.FwFreed;
import com.fw.system.admin.service.IFwFreedService;

/**
 * 意见反馈Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwFreedServiceImpl extends ServiceImpl<FwFreedMapper, FwFreed> implements IFwFreedService
{
    @Autowired
    private FwFreedMapper fwFreedMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询意见反馈
     * 
     * @param id 意见反馈ID
     * @return 意见反馈
     */
    @Override
    public FwFreed selectFwFreedById(String id)
    {
        return fwFreedMapper.selectFwFreedById(id);
    }

    /**
     * 查询意见反馈列表
     * 
     * @param fwFreed 意见反馈
     * @return 意见反馈
     */
    @Override
    public List<FwFreed> selectFwFreedList(FwFreed fwFreed)
    {
        return fwFreedMapper.selectFwFreedList(fwFreed);
    }

    /**
     * 新增意见反馈
     * 
     * @param fwFreed 意见反馈
     * @return 结果
     */
    @Override
    public int insertFwFreed(FwFreed fwFreed)
    {
        fwFreed.setCreateTime(DateUtils.getNowDate());
        fwFreed.setId(idXD.nextId());
        return fwFreedMapper.insertFwFreed(fwFreed);
    }

    /**
     * 修改意见反馈
     * 
     * @param fwFreed 意见反馈
     * @return 结果
     */
    @Override
    public int updateFwFreed(FwFreed fwFreed)
    {
        fwFreed.setUpdateTime(DateUtils.getNowDate());
        return fwFreedMapper.updateFwFreed(fwFreed);
    }

    /**
     * 批量删除意见反馈
     * 
     * @param ids 需要删除的意见反馈ID
     * @return 结果
     */
    @Override
    public int deleteFwFreedByIds(String[] ids)
    {
        return fwFreedMapper.deleteFwFreedByIds(ids);
    }

    /**
     * 删除意见反馈信息
     * 
     * @param id 意见反馈ID
     * @return 结果
     */
    @Override
    public int deleteFwFreedById(String id)
    {
        return fwFreedMapper.deleteFwFreedById(id);
    }
}
