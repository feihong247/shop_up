package com.fw.system.web.model.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class RegisterForm {
    @ApiModelProperty("手机号")
    private String phone;
    @ApiModelProperty("设备号")
    private String imei;
    @ApiModelProperty("邀请码")
    private String parentCode;
    @ApiModelProperty("验证码")
    private String code;
}
