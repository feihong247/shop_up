package com.fw.system.web.model.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 提交订单
 */
@Data
@ApiModel("提交订单")
public class SubmitOrderForm {

    @ApiModelProperty("商品id")
    private String spuId;

    @ApiModelProperty("规格id")
    private String skuId;

    @ApiModelProperty("地址id")
    private String addrId;

    @ApiModelProperty("数量")
    private Integer number;

    @ApiModelProperty("是否是竞猜订单,0=否，1=是")
    private Integer isQuiz;
}
