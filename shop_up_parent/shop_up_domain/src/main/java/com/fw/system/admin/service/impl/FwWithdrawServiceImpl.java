package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwWithdrawMapper;
import com.fw.system.admin.domain.FwWithdraw;
import com.fw.system.admin.service.IFwWithdrawService;

/**
 * 用户提现Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwWithdrawServiceImpl extends ServiceImpl<FwWithdrawMapper, FwWithdraw> implements IFwWithdrawService
{
    @Autowired
    private FwWithdrawMapper fwWithdrawMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询用户提现
     * 
     * @param id 用户提现ID
     * @return 用户提现
     */
    @Override
    public FwWithdraw selectFwWithdrawById(String id)
    {
        return fwWithdrawMapper.selectFwWithdrawById(id);
    }

    /**
     * 查询用户提现列表
     * 
     * @param fwWithdraw 用户提现
     * @return 用户提现
     */
    @Override
    public List<FwWithdraw> selectFwWithdrawList(FwWithdraw fwWithdraw)
    {
        return fwWithdrawMapper.selectFwWithdrawList(fwWithdraw);
    }

    /**
     * 新增用户提现
     * 
     * @param fwWithdraw 用户提现
     * @return 结果
     */
    @Override
    public int insertFwWithdraw(FwWithdraw fwWithdraw)
    {
        fwWithdraw.setCreateTime(DateUtils.getNowDate());
        fwWithdraw.setId(idXD.nextId());
        return fwWithdrawMapper.insertFwWithdraw(fwWithdraw);
    }

    /**
     * 修改用户提现
     * 
     * @param fwWithdraw 用户提现
     * @return 结果
     */
    @Override
    public int updateFwWithdraw(FwWithdraw fwWithdraw)
    {
        fwWithdraw.setUpdateTime(DateUtils.getNowDate());
        return fwWithdrawMapper.updateFwWithdraw(fwWithdraw);
    }

    /**
     * 批量删除用户提现
     * 
     * @param ids 需要删除的用户提现ID
     * @return 结果
     */
    @Override
    public int deleteFwWithdrawByIds(String[] ids)
    {
        return fwWithdrawMapper.deleteFwWithdrawByIds(ids);
    }

    /**
     * 删除用户提现信息
     * 
     * @param id 用户提现ID
     * @return 结果
     */
    @Override
    public int deleteFwWithdrawById(String id)
    {
        return fwWithdrawMapper.deleteFwWithdrawById(id);
    }
}
