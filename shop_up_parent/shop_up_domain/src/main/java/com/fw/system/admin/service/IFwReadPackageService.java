package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwReadPackage;

/**
 * 红包类型Service接口
 * 
 * @author yanwei
 * @date 2021-10-12
 */
public interface IFwReadPackageService extends IService<FwReadPackage>
{
    /**
     * 查询红包类型
     * 
     * @param id 红包类型ID
     * @return 红包类型
     */
    public FwReadPackage selectFwReadPackageById(String id);

    /**
     * 查询红包类型列表
     * 
     * @param fwReadPackage 红包类型
     * @return 红包类型集合
     */
    public List<FwReadPackage> selectFwReadPackageList(FwReadPackage fwReadPackage);

    /**
     * 新增红包类型
     * 
     * @param fwReadPackage 红包类型
     * @return 结果
     */
    public int insertFwReadPackage(FwReadPackage fwReadPackage);

    /**
     * 修改红包类型
     * 
     * @param fwReadPackage 红包类型
     * @return 结果
     */
    public int updateFwReadPackage(FwReadPackage fwReadPackage);

    /**
     * 批量删除红包类型
     * 
     * @param ids 需要删除的红包类型ID
     * @return 结果
     */
    public int deleteFwReadPackageByIds(String[] ids);

    /**
     * 删除红包类型信息
     * 
     * @param id 红包类型ID
     * @return 结果
     */
    public int deleteFwReadPackageById(String id);
}
