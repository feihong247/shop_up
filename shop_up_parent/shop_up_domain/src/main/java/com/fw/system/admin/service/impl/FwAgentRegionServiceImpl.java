package com.fw.system.admin.service.impl;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import cn.hutool.http.HttpUtil;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.common.IdXD;
import com.fw.system.admin.domain.FwUljoin;
import com.fw.system.admin.service.IFwUljoinService;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwAgentRegionMapper;
import com.fw.system.admin.domain.FwAgentRegion;
import com.fw.system.admin.service.IFwAgentRegionService;

/**
 * 代理区域Service业务层处理
 *
 * @author yanwei
 * @date 2021-07-23
 */
@Service
public class FwAgentRegionServiceImpl extends ServiceImpl<FwAgentRegionMapper, FwAgentRegion> implements IFwAgentRegionService
{

    @Autowired
    private FwAgentRegionMapper fwAgentRegionMapper;


    @Autowired
    private IdXD idXD;

    /**
     * 查询代理区域
     *
     * @param id 代理区域ID
     * @return 代理区域
     */
    @Override
    public FwAgentRegion selectFwAgentRegionById(String id)
    {
        return fwAgentRegionMapper.selectFwAgentRegionById(id);
    }

    /**
     * 查询代理区域列表
     *
     * @param fwAgentRegion 代理区域
     * @return 代理区域
     */
    @Override
    public List<FwAgentRegion> selectFwAgentRegionList(FwAgentRegion fwAgentRegion)
    {
        return fwAgentRegionMapper.selectFwAgentRegionList(fwAgentRegion);
    }

    /**
     * 新增代理区域
     *
     * @param fwAgentRegion 代理区域
     * @return 结果
     */
    @Override
    public int insertFwAgentRegion(FwAgentRegion fwAgentRegion)
    {
        fwAgentRegion.setId(idXD.nextId());
        return fwAgentRegionMapper.insertFwAgentRegion(fwAgentRegion);
    }

    /**
     * 修改代理区域
     *
     * @param fwAgentRegion 代理区域
     * @return 结果
     */
    @Override
    public int updateFwAgentRegion(FwAgentRegion fwAgentRegion)
    {
        return fwAgentRegionMapper.updateFwAgentRegion(fwAgentRegion);
    }

    /**
     * 批量删除代理区域
     *
     * @param ids 需要删除的代理区域ID
     * @return 结果
     */
    @Override
    public int deleteFwAgentRegionByIds(String[] ids)
    {
        Set<String> userIds = list(Wrappers.<FwAgentRegion>lambdaQuery().in(FwAgentRegion::getId, ids)).parallelStream().map(item -> item.getUserId()).collect(Collectors.toSet());
        for (String userId : userIds) {
            HttpUtil.get("https://kuangshikeji.com/identity/deleteDl?userId="+userId);
        }
        return fwAgentRegionMapper.deleteFwAgentRegionByIds(ids);
    }

    /**
     * 删除代理区域信息
     *
     * @param id 代理区域ID
     * @return 结果
     */
    @Override
    public int deleteFwAgentRegionById(String id)
    {
        Set<String> userIds = list(Wrappers.<FwAgentRegion>lambdaQuery().eq(FwAgentRegion::getId, id)).parallelStream().map(item -> item.getUserId()).collect(Collectors.toSet());
        for (String userId : userIds) {
            HttpUtil.get("https://kuangshikeji.com/identity/deleteDl?userId="+userId);
        }
        return fwAgentRegionMapper.deleteFwAgentRegionById(id);
    }
}
