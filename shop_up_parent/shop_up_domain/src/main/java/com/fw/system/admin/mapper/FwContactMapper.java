package com.fw.system.admin.mapper;

import java.util.List;

import com.fw.system.admin.domain.FwContact;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 关于我们Mapper接口
 *
 * @author yanwei
 * @date 2021-05-24
 */
public interface FwContactMapper extends BaseMapper<FwContact> {
    /**
     * 查询关于我们
     *
     * @param id 关于我们ID
     * @return 关于我们
     */
    public FwContact selectFwContactById(String id);

    /**
     * 查询关于我们列表
     *
     * @param fwContact 关于我们
     * @return 关于我们集合
     */
    public List<FwContact> selectFwContactList(FwContact fwContact);

    /**
     * 新增关于我们
     *
     * @param fwContact 关于我们
     * @return 结果
     */
    public int insertFwContact(FwContact fwContact);

    /**
     * 修改关于我们
     *
     * @param fwContact 关于我们
     * @return 结果
     */
    public int updateFwContact(FwContact fwContact);

    /**
     * 删除关于我们
     *
     * @param id 关于我们ID
     * @return 结果
     */
    public int deleteFwContactById(String id);

    /**
     * 批量删除关于我们
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwContactByIds(String[] ids);
}
