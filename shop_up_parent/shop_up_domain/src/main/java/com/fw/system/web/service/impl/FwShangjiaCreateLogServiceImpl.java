package com.fw.system.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.system.web.dao.FwShangjiaCreateLogMapper;
import com.fw.system.web.model.entity.FwShangjiaCreateLog;
import com.fw.system.web.service.IFwShangjiaCreateLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 商甲产出日志表 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
@Service
public class FwShangjiaCreateLogServiceImpl extends ServiceImpl<FwShangjiaCreateLogMapper, FwShangjiaCreateLog> implements IFwShangjiaCreateLogService {

    @Autowired
    private FwShangjiaCreateLogMapper createLogMapper;

    @Override
    public Integer yueDetail() {

        return createLogMapper.yueDetail();
    }

    @Override
    public List<FwShangjiaCreateLog> dayDetail() {
        return createLogMapper.dayDetail();
    }

    @Override
    public List<FwShangjiaCreateLog> createAndReleaseYear() {
        return createLogMapper.createAndReleaseYear();
    }

    @Override
    public List<FwShangjiaCreateLog> createAndReleaseMonth() {
        return createLogMapper.createAndReleaseMonth();
    }

    @Override
    public List<FwShangjiaCreateLog> createAndReleaseDay() {
        return createLogMapper.createAndReleaseDay();
    }

    @Override
    public int sumAllCreateShangJia() {
        return createLogMapper.sumAllCreateShangJia();
    }

    @Override
    public int sumYearCreateShangJia() {
        return createLogMapper.sumYearCreateShangJia();
    }


}
