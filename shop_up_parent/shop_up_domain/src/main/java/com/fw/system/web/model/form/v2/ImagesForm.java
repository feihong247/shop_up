package com.fw.system.web.model.form.v2;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
@Data
@ApiModel("商品列表图片")
public class ImagesForm implements Serializable {
    /**
     * 图片链接
     */
    @ApiModelProperty("图片链接")
    @NotBlank(message = "图片链接 不可为空")
    private String linkUrl;
}
