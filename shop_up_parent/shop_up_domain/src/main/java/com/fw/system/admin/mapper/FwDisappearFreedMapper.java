package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwDisappearFreed;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 消证释放Mapper接口
 *
 * @author yanwei
 * @date 2021-05-10
 */
public interface FwDisappearFreedMapper extends BaseMapper<FwDisappearFreed>
{
    /**
     * 查询消证释放
     *
     * @param id 消证释放ID
     * @return 消证释放
     */
    public FwDisappearFreed selectFwDisappearFreedById(String id);

    /**
     * 查询消证释放列表
     *
     * @param fwDisappearFreed 消证释放
     * @return 消证释放集合
     */
    public List<FwDisappearFreed> selectFwDisappearFreedList(FwDisappearFreed fwDisappearFreed);

    /**
     * 新增消证释放
     *
     * @param fwDisappearFreed 消证释放
     * @return 结果
     */
    public int insertFwDisappearFreed(FwDisappearFreed fwDisappearFreed);

    /**
     * 修改消证释放
     *
     * @param fwDisappearFreed 消证释放
     * @return 结果
     */
    public int updateFwDisappearFreed(FwDisappearFreed fwDisappearFreed);

    /**
     * 删除消证释放
     *
     * @param id 消证释放ID
     * @return 结果
     */
    public int deleteFwDisappearFreedById(String id);

    /**
     * 批量删除消证兑换
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwDisappearFreedByIds(String[] ids);
}
