package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwShangjia;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商甲发行表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */

public interface FwShangjiaMapper extends BaseMapper<FwShangjia> {

}
