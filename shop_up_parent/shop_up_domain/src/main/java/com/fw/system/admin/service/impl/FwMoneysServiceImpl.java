package com.fw.system.admin.service.impl;

import cn.hutool.core.lang.Assert;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.common.IdXD;
import com.fw.constant.Constant;
import com.fw.enums.LogsModelEnum;
import com.fw.enums.LogsTypeEnum;
import com.fw.enums.ReleaseLogEnum;
import com.fw.system.admin.domain.*;
import com.fw.system.admin.mapper.FwMoneysMapper;
import com.fw.system.admin.mapper.FwUserMapper;
import com.fw.system.admin.service.*;
import com.fw.utils.DateUtils;
import com.fw.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * 用户钱包Service业务层处理
 *
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwMoneysServiceImpl extends ServiceImpl<FwMoneysMapper, FwMoneys> implements IFwMoneysService {
    @Autowired
    private FwMoneysMapper fwMoneysMapper;
    @Autowired
    private IFwRuleDatasService ruleDatasService;
    @Autowired
    private IFwShangjiaService shangjiaService;
    @Autowired
    private IFwLockStorageService lockStorageService;
    @Autowired
    private IFwLogsService logsService;

    @Autowired
    private IFwShangjiaReleaseLogService releaseLogService;

    @Autowired
    private FwUserMapper userMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询用户钱包
     *
     * @param id 用户钱包ID
     * @return 用户钱包
     */
    @Override
    public FwMoneys selectFwMoneysById(String id) {
        return fwMoneysMapper.selectFwMoneysById(id);
    }

    /**
     * 查询用户钱包列表
     *
     * @param fwMoneys 用户钱包
     * @return 用户钱包
     */
    @Override
    public List<FwMoneys> selectFwMoneysList(FwMoneys fwMoneys) {
        return fwMoneysMapper.selectFwMoneysList(fwMoneys);
    }

    /**
     * 新增用户钱包
     *
     * @param fwMoneys 用户钱包
     * @return 结果
     */
    @Override
    public int insertFwMoneys(FwMoneys fwMoneys) {
        fwMoneys.setCreateTime(DateUtils.getNowDate());
        fwMoneys.setId(idXD.nextId());
        return fwMoneysMapper.insertFwMoneys(fwMoneys);
    }

    /**
     * 修改用户钱包
     *
     * @param fwMoneys 用户钱包
     * @return 结果
     */
    @Override
    public int updateFwMoneys(FwMoneys fwMoneys) {
        //fwMoneys.setUpdateTime(DateUtils.getNowDate());
        //return fwMoneysMapper.updateFwMoneys(fwMoneys);
        FwMoneys moneys = getById(fwMoneys.getId());
        if (moneys.getDisappear().compareTo(fwMoneys.getDisappear()) != 0) {
            // 操作
            if (fwMoneys.getDisappear().compareTo(moneys.getDisappear()) == 1) {
                // +
                BigDecimal disapper = fwMoneys.getDisappear().subtract(moneys.getDisappear());
                fwMoneys.setDisappear(disapper);
                HttpUtil.post("https://kuangshikeji.com/web/open/addXz", JSONUtil.toJsonStr(fwMoneys));
            }else{
                // -
                BigDecimal disapper = moneys.getDisappear().subtract(fwMoneys.getDisappear());
                fwMoneys.setDisappear(disapper);
                HttpUtil.post("https://kuangshikeji.com/web/open/subNum", JSONUtil.toJsonStr(fwMoneys));

            }
        }
        return 1;
    }

    /**
     * 批量删除用户钱包
     *
     * @param ids 需要删除的用户钱包ID
     * @return 结果
     */
    @Override
    public int deleteFwMoneysByIds(String[] ids) {
        return fwMoneysMapper.deleteFwMoneysByIds(ids);
    }

    /**
     * 删除用户钱包信息
     *
     * @param id 用户钱包ID
     * @return 结果
     */
    @Override
    public int deleteFwMoneysById(String id) {
        return fwMoneysMapper.deleteFwMoneysById(id);
    }

    @Override
    @Transactional
    public int updateMoneysShangJia(BigDecimal shangjia, String userId, Integer isAccount) {
        String loginUserId = SecurityUtils.getLoginUser().getUser().getUserId().toString();
        //更新原始商甲剩余额度 //TODO 对应的也有 原始管理账号 此时逻辑走向 是该怎么走？
        FwShangjia oriShangJia = shangjiaService.getOne(Wrappers.<FwShangjia>lambdaQuery().eq(FwShangjia::getId, Constant.ShangJia.ORIGINAL_ID));
        if (shangjia.compareTo(oriShangJia.getShopIssue()) >= 0) {
            return 0;
        }
        oriShangJia.setShopIssue(oriShangJia.getShopIssue().subtract(shangjia));
        oriShangJia.setUpdateTime(new Date());
        oriShangJia.setUpdateBy(loginUserId);
        shangjiaService.updateById(oriShangJia);
        //更新用户钱包
        FwMoneys user = fwMoneysMapper.selectOne(Wrappers.<FwMoneys>lambdaQuery().eq(FwMoneys::getUserId, userId));
        user.setShangJia(shangjia.add(user.getShangJia()));
        user.setUpdateBy(loginUserId);
        user.setUpdateTime(new Date());
        int i = fwMoneysMapper.updateFwMoneys(user);
        //新增日志到新日志表
        //  logsService.saveLogs(loginUserId, user.getUserId(), LogsTypeEnum.MONEY_PLATFORM_ORI, LogsModelEnum.SHANG_JIA_MODEL, "原始商甲发放", shangjia.toString());
        releaseLogService.createReleaseLog(shangjia,userMapper.selectFwUserById(userId), ReleaseLogEnum.PUBLISH_SOURCE);
        //商甲锁仓
        extracted(user.getUserId(), user);
        return i;
    }

    @Override
    @Transactional
    public int updateMoneysTecShangJia(BigDecimal shangjia, String userId, Integer isAccount) {

        String loginUserId = getAccount(isAccount).getId();
        //更新原始商甲剩余额度
        /*FwShangjia tecShangJia = shangjiaService.getOne(Wrappers.<FwShangjia>lambdaQuery().eq(FwShangjia::getId, Constant.ShangJia.TECHNOLOGY_ID));
        if(shangjia.compareTo(tecShangJia.getShopIssue())>1){
            return 0;
        }
        tecShangJia.setShopIssue(tecShangJia.getShopIssue().subtract(shangjia));
        tecShangJia.setUpdateTime(new Date());
        tecShangJia.setUpdateBy(loginUserId);
        shangjiaService.updateById(tecShangJia);*/
        //从技术账号手里分配技术商甲
        FwMoneys loginUser = fwMoneysMapper.selectOne(Wrappers.<FwMoneys>lambdaQuery().eq(FwMoneys::getUserId, loginUserId));
        if (loginUser.getTecShangjia().compareTo(shangjia) > -1) {
            loginUser.setTecShangjia(loginUser.getTecShangjia().subtract(shangjia));
            loginUser.setUpdateBy(loginUserId);
            loginUser.setUpdateTime(new Date());
            fwMoneysMapper.updateById(loginUser);
        } else {
            return 0;
        }
        //更新用户钱包
        FwMoneys user = fwMoneysMapper.selectOne(Wrappers.<FwMoneys>lambdaQuery().eq(FwMoneys::getUserId, userId));
        user.setTecShangjia(shangjia.add(user.getShangJia()));
        user.setUpdateBy(loginUserId);
        user.setUpdateTime(new Date());
        int i = fwMoneysMapper.updateFwMoneys(user);
        // logsService.saveLogs(loginUserId, user.getUserId(), LogsTypeEnum.MONEY_PLATFORM_TEC, LogsModelEnum.SHANG_JIA_MODEL, "技术商甲分配", shangjia.toString());
        releaseLogService.createReleaseLog(shangjia,userMapper.selectFwUserById(userId), ReleaseLogEnum.PUBLISH_POWER);

        //商甲锁仓
        extractedTec(user.getUserId(), user);
        return i;
    }

    @Override
    @Transactional
    public int updateMoneysManShangJia(BigDecimal shangjia, String userId, Integer isAccount) {
        String loginUserId = getAccount(isAccount).getId();
        //更新原始商甲剩余额度
        /*FwShangjia tecShangJia = shangjiaService.getOne(Wrappers.<FwShangjia>lambdaQuery().eq(FwShangjia::getId, Constant.ShangJia.MANAGEMENT_ID));
        if(shangjia.compareTo(tecShangJia.getShopIssue())>1){
            return 0;
        }
        tecShangJia.setShopIssue(tecShangJia.getShopIssue().subtract(shangjia));
        tecShangJia.setUpdateTime(new Date());
        tecShangJia.setUpdateBy(loginUserId);
        shangjiaService.updateById(tecShangJia);*/
        //从技术账号手里分配技术商甲
        FwMoneys loginUser = fwMoneysMapper.selectOne(Wrappers.<FwMoneys>lambdaQuery().eq(FwMoneys::getUserId, loginUserId));
        if (loginUser.getManShangjia().compareTo(shangjia) > -1) {
            loginUser.setManShangjia(loginUser.getManShangjia().subtract(shangjia));
            loginUser.setUpdateBy(loginUserId);
            loginUser.setUpdateTime(new Date());
            fwMoneysMapper.updateById(loginUser);
        } else {
            return 0;
        }
        //更新用户钱包
        FwMoneys user = fwMoneysMapper.selectOne(Wrappers.<FwMoneys>lambdaQuery().eq(FwMoneys::getUserId, userId));
        user.setManShangjia(shangjia.add(user.getShangJia()));
        user.setUpdateBy(loginUserId);
        user.setUpdateTime(new Date());
        int i = fwMoneysMapper.updateFwMoneys(user);
        // logsService.saveLogs(loginUserId, user.getUserId(), LogsTypeEnum.MONEY_PLATFORM_ORI, LogsModelEnum.SHANG_JIA_MODEL, "原始商甲发放", shangjia.toString());
        releaseLogService.createReleaseLog(shangjia,userMapper.selectFwUserById(userId), ReleaseLogEnum.PUBLISH_MAN);

        //商甲锁仓
        extractedMan(user.getUserId(), user);
        return i;
    }

    @Override
    @Transactional
    public int recycleManShangJia(BigDecimal shangjia, String userId, Integer isAccount) {

        String loginId = getAccount(isAccount).getId();
        List<FwLockStorage> list = lockStorageService.list(Wrappers.<FwLockStorage>lambdaQuery().eq(FwLockStorage::getUserId, userId).gt(FwLockStorage::getManLockCount, 0).orderByAsc(FwLockStorage::getCreateTime));
        int i = 0;
        double sum = list.stream().mapToDouble(o -> o.getManLockCount()).sum();
        if (shangjia.compareTo(new BigDecimal(sum)) > 0) {
            return 0;
        }
        if (CollectionUtils.isNotEmpty(list)) {
            for (FwLockStorage item : list) {
                item.setUpdateBy(loginId);
                item.setUpdateTime(new Date());
                if (shangjia.compareTo(new BigDecimal(item.getManLockCount())) > 0) {
                    Long lockCount = item.getManLockCount();
                    item.setManLockCount(0l);
                    lockStorageService.updateFwLockStorage(item);
                    shangjia = shangjia.subtract(new BigDecimal(lockCount));
                    item.setId(idXD.nextId());//id
                    item.setLockTime(new Date());//锁仓时间
                    item.setUserId(loginId.toString());//用户编号
                    item.setManLockCount(lockCount);//锁仓数量
                    item.setNote(lockCount.toString());//锁仓总量
                    item.setFlag(Constant.LockStorage.LOCK_ING);//标记为锁仓
                    item.setReleaseTime(new Date());//兑换时间初始化
                    item.setCreateBy(loginId.toString());
                    item.setCreateTime(new Date());
                    lockStorageService.save(item);
                    i++;
                    continue;
                } else {
                    item.setManLockCount(item.getManLockCount() - shangjia.longValue());
                    lockStorageService.updateFwLockStorage(item);
                    item.setId(idXD.nextId());//id
                    item.setLockTime(new Date());//锁仓时间
                    item.setUserId(loginId.toString());//用户编号
                    item.setManLockCount(shangjia.longValue());//锁仓数量
                    item.setNote(shangjia.toString());//锁仓总量
                    item.setFlag(Constant.LockStorage.LOCK_ING);//标记为锁仓
                    item.setReleaseTime(new Date());//兑换时间初始化
                    item.setCreateBy(loginId.toString());
                    item.setCreateTime(new Date());
                    lockStorageService.save(item);
                    i++;
                    break;
                }
            }
            /*FwShangjia shangjiaStore = shangjiaService.getOne(Wrappers.<FwShangjia>lambdaQuery().eq(FwShangjia::getId, Constant.ShangJia.MANAGEMENT_ID));
            shangjiaStore.setShopIssue(shangjiaStore.getShopIssue().add(shangjia));
            i = shangjiaService.updateFwShangjia(shangjiaStore);*/
            /*FwMoneys loginUser = fwMoneysMapper.selectOne(Wrappers.<FwMoneys>lambdaQuery().eq(FwMoneys::getUserId, loginId));
            loginUser.setManShangjia(loginUser.getManShangjia().add(shangjia));
            loginUser.setUpdateBy(loginId);
            loginUser.setUpdateTime(new Date());
            fwMoneysMapper.updateById(loginUser);*/
            //TODO 管理账户 是否需要加入锁仓？要不然此时的管理账号 的商甲 都大于了？
            // getFwLogs(userId, shangjia, LogsTypeEnum.MONEY_RECYCLE_MAN.getTypeName(), LogsModelEnum.SHANG_JIA_MODEL.getModelName(), "回收用户管理商甲给用户id=" + loginId);//记录操作日志
            releaseLogService.createReleaseLog(shangjia,userMapper.selectFwUserById(userId), ReleaseLogEnum.PUBLISH_MAN_BACK);


        }
        return i;
    }

    @Override
    public int recycleTecShangJia(BigDecimal shangjia, String userId, Integer isAccount) {
        String loginId = getAccount(isAccount).getId();
        List<FwLockStorage> list = lockStorageService.list(Wrappers.<FwLockStorage>lambdaQuery().eq(FwLockStorage::getUserId, userId).gt(FwLockStorage::getTecLockCount, 0).orderByAsc(FwLockStorage::getCreateTime));
        int i = 0;
        double sum = list.stream().mapToDouble(o -> o.getTecLockCount()).sum();
        if (shangjia.compareTo(new BigDecimal(sum)) > 0) {
            return 0;
        }
        if (CollectionUtils.isNotEmpty(list)) {
            for (FwLockStorage item : list) {
                if (shangjia.compareTo(new BigDecimal(item.getTecLockCount())) > 0) {
                    Long lockCount = item.getTecLockCount();
                    item.setTecLockCount(0l);
                    lockStorageService.updateFwLockStorage(item);
                    shangjia = shangjia.subtract(new BigDecimal(lockCount));
                    item.setId(idXD.nextId());//id
                    item.setLockTime(new Date());//锁仓时间
                    item.setUserId(loginId.toString());//用户编号
                    item.setTecLockCount(lockCount);//锁仓数量
                    item.setNote(lockCount.toString());//锁仓总量
                    item.setFlag(Constant.LockStorage.LOCK_ING);//标记为锁仓
                    item.setReleaseTime(new Date());//兑换时间初始化
                    item.setCreateBy(loginId.toString());
                    item.setCreateTime(new Date());
                    lockStorageService.save(item);
                    i++;
                    continue;
                } else {
                    item.setTecLockCount(item.getTecLockCount() - shangjia.longValue());
                    lockStorageService.updateFwLockStorage(item);
                    item.setId(idXD.nextId());//id
                    item.setLockTime(new Date());//锁仓时间
                    item.setUserId(loginId.toString());//用户编号
                    item.setTecLockCount(shangjia.longValue());//锁仓数量
                    item.setNote(shangjia.toString());//锁仓总量
                    item.setFlag(Constant.LockStorage.LOCK_ING);//标记为锁仓
                    item.setReleaseTime(new Date());//兑换时间初始化
                    item.setCreateBy(loginId.toString());
                    item.setCreateTime(new Date());
                    lockStorageService.save(item);
                    i++;
                    break;
                }
            }
            /*FwShangjia shangjiaStore = shangjiaService.getOne(Wrappers.<FwShangjia>lambdaQuery().eq(FwShangjia::getId, Constant.ShangJia.TECHNOLOGY_ID));
            shangjiaStore.setShopIssue(shangjiaStore.getShopIssue().add(shangjia));
            i = shangjiaService.updateFwShangjia(shangjiaStore);*/
            /*FwMoneys loginUser = fwMoneysMapper.selectOne(Wrappers.<FwMoneys>lambdaQuery().eq(FwMoneys::getUserId, loginId));
            loginUser.setTecShangjia(loginUser.getTecShangjia().add(shangjia));
            loginUser.setUpdateBy(loginId.toString());
            loginUser.setUpdateTime(new Date());
            i=fwMoneysMapper.updateById(loginUser);*/
            // getFwLogs(userId, shangjia, LogsTypeEnum.MONEY_RECYCLE_TEC.getTypeName(), LogsModelEnum.SHANG_JIA_MODEL.getModelName(), "回收用户技术商甲给用户id=" + loginId);//记录操作日志

            releaseLogService.createReleaseLog(shangjia,userMapper.selectFwUserById(userId), ReleaseLogEnum.PUBLISH_POWER_BACK);

        }
        return i;
    }

    /**
     * 商甲满足一定条件锁仓
     *
     * @param userId
     * @param moneysOld
     */
    private void extracted(String userId, FwMoneys moneysOld) {
        FwRuleDatas lockNum = ruleDatasService.getOne(Wrappers.<FwRuleDatas>lambdaQuery().eq(FwRuleDatas::getId, Constant.IsRuleData.SHANGJIA_LOCK_NUMBER));

        String loginId = SecurityUtils.getLoginUser().getUser().getUserId().toString();
        if (moneysOld.getShangJia().compareTo(lockNum.getRuleCount()) > -1) {

            //判断是否要锁仓（释放超过24小时的商甲才能锁仓）
            List<FwLogs> shangJiaLtOneDay = logsService.getShangJiaLtOneDay(moneysOld.getUserId());
            BigDecimal val = new BigDecimal("1000");
            BigDecimal subtractOne = moneysOld.getShangJia();
            if (CollectionUtils.isNotEmpty(shangJiaLtOneDay)) {
                FwLogs log = shangJiaLtOneDay.get(0);
                if (new BigDecimal(log.getFormInfo()).compareTo(val) > 0) {
                    subtractOne = moneysOld.getShangJia().subtract(new BigDecimal(log.getFormInfo()));
                }
            }

            BigDecimal lockUnit = subtractOne.divide(lockNum.getRuleCount(), 0, BigDecimal.ROUND_DOWN);
            moneysOld.setShangJia(moneysOld.getShangJia().subtract(lockUnit.multiply(lockNum.getRuleCount())));
            moneysOld.setUpdateTime(new Date());
            moneysOld.setUpdateBy(loginId);
            fwMoneysMapper.updateById(moneysOld);
            //锁仓表
            for (int i = 0; i < Integer.parseInt(lockUnit.toString()); i++) {
                FwLockStorage lockStorage = new FwLockStorage();
                lockStorage.setId(idXD.nextId());
                lockStorage.setUserId(userId);
                lockStorage.setLockCount(lockNum.getRuleCount().longValue());
                lockStorage.setLockTime(new Date());
                lockStorage.setFlag("0");//0分红1不分红
                lockStorage.setUpdateTime(new Date());
                lockStorage.setUpdateBy(loginId);
                lockStorageService.saveOrUpdate(lockStorage);
            }
            getFwLogs(userId, lockUnit.multiply(lockNum.getRuleCount()), LogsTypeEnum.MONEY_LOCK.getTypeName(), LogsModelEnum.SHANG_JIA_MODEL.getModelName(), "客户满足条件商甲锁仓");//记录操作日志
        }
    }

    /**
     * 技术商甲满足一定条件锁仓
     *
     * @param userId
     * @param moneysOld
     */
    private void extractedTec(String userId, FwMoneys moneysOld) {
        FwRuleDatas lockNum = ruleDatasService.getOne(Wrappers.<FwRuleDatas>lambdaQuery().eq(FwRuleDatas::getId, Constant.IsRuleData.SHANGJIA_LOCK_NUMBER));
        String loginId = SecurityUtils.getLoginUser().getUser().getUserId().toString();
        if (moneysOld.getTecShangjia().compareTo(lockNum.getRuleCount()) > -1) {
            BigDecimal lockUnit = moneysOld.getTecShangjia().divide(lockNum.getRuleCount(), 0, BigDecimal.ROUND_DOWN);
            moneysOld.setTecShangjia(moneysOld.getTecShangjia().subtract(lockUnit.multiply(lockNum.getRuleCount())));
            moneysOld.setUpdateBy(loginId);
            moneysOld.setUpdateTime(new Date());
            fwMoneysMapper.updateById(moneysOld);
            //锁仓表
            for (int i = 0; i < Integer.parseInt(lockUnit.toString()); i++) {
                FwLockStorage lockStorage = new FwLockStorage();
                lockStorage.setId(idXD.nextId());
                lockStorage.setUserId(userId);
                lockStorage.setTecLockCount(lockNum.getRuleCount().longValue());
                lockStorage.setLockTime(new Date());
                lockStorage.setFlag("0");//0分红1不分红
                lockStorage.setUpdateBy(loginId);
                lockStorage.setUpdateTime(new Date());
                lockStorageService.saveOrUpdate(lockStorage);
            }
            getFwLogs(userId, lockUnit.multiply(lockNum.getRuleCount()), LogsTypeEnum.MONEY_LOCK.getTypeName(), LogsModelEnum.SHANG_JIA_MODEL.getModelName(), "客户满足条件技术商甲锁仓");//记录操作日志
        }
    }

    /**
     * 管理商甲满足一定条件锁仓
     *
     * @param userId
     * @param moneysOld
     */
    private void extractedMan(String userId, FwMoneys moneysOld) {
        FwRuleDatas lockNum = ruleDatasService.getOne(Wrappers.<FwRuleDatas>lambdaQuery().eq(FwRuleDatas::getId, Constant.IsRuleData.SHANGJIA_LOCK_NUMBER));
        String loginId = SecurityUtils.getLoginUser().getUser().getUserId().toString();
        if (moneysOld.getManShangjia().compareTo(lockNum.getRuleCount()) > -1) {
            BigDecimal lockUnit = moneysOld.getManShangjia().divide(lockNum.getRuleCount(), 0, BigDecimal.ROUND_DOWN);
            moneysOld.setManShangjia(moneysOld.getManShangjia().subtract(lockUnit.multiply(lockNum.getRuleCount())));
            moneysOld.setUpdateBy(loginId);
            moneysOld.setUpdateTime(new Date());
            fwMoneysMapper.updateById(moneysOld);
            //锁仓表
            for (int i = 0; i < Integer.parseInt(lockUnit.toString()); i++) {
                FwLockStorage lockStorage = new FwLockStorage();
                lockStorage.setId(idXD.nextId());
                lockStorage.setUserId(userId);
                lockStorage.setManLockCount(lockNum.getRuleCount().longValue());
                lockStorage.setLockTime(new Date());
                lockStorage.setFlag("0");//0分红1不分红
                lockStorage.setUpdateBy(loginId);
                lockStorage.setUpdateTime(new Date());
                lockStorageService.saveOrUpdate(lockStorage);
            }
            getFwLogs(userId, lockUnit.multiply(lockNum.getRuleCount()), LogsTypeEnum.MONEY_LOCK.getTypeName(), LogsModelEnum.SHANG_JIA_MODEL.getModelName(), "客户满足条件管理商甲锁仓");//记录操作日志
        }
    }

    /**
     * 根据规则修改商甲数量
     *
     * @param userId
     * @param divide
     * @return
     */
    private FwMoneys getFwMoneys(String userId, BigDecimal divide) {
        //修改用户商甲数量
        FwMoneys moneysOld = fwMoneysMapper.selectOne(Wrappers.<FwMoneys>lambdaQuery().eq(FwMoneys::getUserId, userId));
        FwShangjia consumerSJ = shangjiaService.getOne(Wrappers.<FwShangjia>lambdaQuery().eq(FwShangjia::getId, Constant.ShangJia.CONSUME_ID));
        consumerSJ.setShopIssue(consumerSJ.getShopIssue().subtract(divide));
        consumerSJ.setUpdateTime(new Date());
        consumerSJ.setUpdateBy(userId);
        moneysOld.setShangJia(moneysOld.getShangJia().add(divide));
        getFwLogs(userId, divide, LogsTypeEnum.MONEY_INCOME.getTypeName(), LogsModelEnum.SHANG_JIA_MODEL.getModelName(), "客户销证变化引起客户商甲修改");//记录操作日志
        shangjiaService.saveOrUpdate(consumerSJ);//修改消费商甲剩余商甲
        fwMoneysMapper.updateById(moneysOld);
        return moneysOld;
    }

    /**
     * 登记数据变更日志
     *
     * @param userId
     * @param divide
     * @param typeName
     * @param modelName
     * @param jsonText
     */
    private void getFwLogs(String userId, BigDecimal divide, String typeName, String modelName, String jsonText) {
        FwLogs fwLogs = new FwLogs();
        fwLogs.setId(idXD.nextId());
        fwLogs.setBuiId(userId);
        fwLogs.setIsType(typeName);
        fwLogs.setModelName(modelName);
        fwLogs.setJsonTxt(jsonText);
        fwLogs.setFormInfo(divide.toString());
        fwLogs.setCreateBy(userId);
        fwLogs.setCreateTime(new Date());
        fwLogs.setUpdateBy(userId);
        fwLogs.setUpdateTime(new Date());
        logsService.saveOrUpdate(fwLogs);//登记商甲增加日志
    }

    /**
     * @param isAccount
     * @return 找到对应的管理账号 0 = 普通用户，1= 技术，2= 管理，3 原始
     */
    public FwUser getAccount(Integer isAccount) {
        Assert.isTrue(Arrays.asList(1, 2, 3).contains(isAccount), "暂无对应管理标识编码");
        FwUser fwUser = userMapper.selectOne(Wrappers.<FwUser>lambdaQuery().eq(FwUser::getIsAccount, isAccount));
        Assert.isTrue(Objects.nonNull(fwUser), "系统暂未分配对应的管理账号");
        return fwUser;
    }
}
