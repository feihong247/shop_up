package com.fw.system.web.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.system.web.model.entity.FwSpu;
import com.fw.system.web.dao.FwSpuMapper;
import com.fw.system.web.service.IFwSpuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Service
public class FwSpuServiceImpl extends ServiceImpl<FwSpuMapper, FwSpu> implements IFwSpuService {

    /**
     * 减销量
     *
     * @param spuId  商品ID
     * @param number 数量
     * @return 是否减成功
     * @Author 姚
     * @Date 2021/7/6
     **/
    @Override
    public boolean subtractTops(String spuId, Integer number) {
        FwSpu spu = getById(spuId);
        return update(Wrappers.<FwSpu>lambdaUpdate().set(FwSpu::getTops, spu.getTops() - number));
    }

    /**
     * 加销量
     *
     * @param spuId  商品ID
     * @param number 数量
     * @return 是否加成功
     * @Author 姚
     * @Date 2021/7/6
     **/
    @Override
    public boolean addTops(String spuId, Integer number) {
        FwSpu spu = getById(spuId);
        if (spu.getTops() == null) { spu.setTops( number ); }
        else { spu.setTops( spu.getTops() - - number ); }
        return updateById(spu);
    }
}
