package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 商甲发行表
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwShangjia extends Model<FwShangjia> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("id")
    private String id;

    /**
     * 商甲名称
     */
    @ApiModelProperty(value = "商甲名称")
    @TableField("shangjia_name")
    private String shangjiaName;

    /**
     * 商甲额度，厘为单位
     */
    @ApiModelProperty(value = "商甲额度，厘为单位")
    @TableField("shangjia_quota")
    private BigDecimal shangjiaQuota;

    /**
     * 发行时间
     */
    @ApiModelProperty(value = "发行时间")
    @TableField("shangjia_publish_data")
    private LocalDate shangjiaPublishData;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    @TableField("create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    @TableField("update_by")
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField("update_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /** 商甲实际剩余额度,发放商甲时,更改此数据 */
    @ApiModelProperty(value = "商甲实际剩余额度")
    @TableField("shop_issue")
    private BigDecimal shopIssue;

    /** 商甲现价 */
    @ApiModelProperty(value = "商甲现价")
    @TableField("present_price")
    private BigDecimal presentPrice;

    /** 商甲现价 */
    @ApiModelProperty(value = "各个仓销毁数量")
    @TableField("delete_num")
    private BigDecimal deleteNum;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
