package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwDisappearFreed;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;

/**
 * <p>
 * 消证兑换表 服务类
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
public interface IFwDisappearFreedService extends IService<FwDisappearFreed> {

    public void createFreed(BigDecimal disappear, Integer isAccele, String userId,String info);

    boolean isFlag(String userId);
}
