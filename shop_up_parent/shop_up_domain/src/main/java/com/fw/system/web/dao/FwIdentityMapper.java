package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwIdentity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 身份表 Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
public interface FwIdentityMapper extends BaseMapper<FwIdentity> {

    /**
     * 查询身份
     *
     * @param id 身份ID
     * @return 身份
     */
    public FwIdentity selectFwIdentityById(String id);

    /**
     * 查询身份列表
     *
     * @param fwIdentity 身份
     * @return 身份集合
     */
    public List<FwIdentity> selectFwIdentityList(FwIdentity fwIdentity);

    /**
     * 新增身份
     *
     * @param fwIdentity 身份
     * @return 结果
     */
    public int insertFwIdentity(FwIdentity fwIdentity);

    /**
     * 修改身份
     *
     * @param fwIdentity 身份
     * @return 结果
     */
    public int updateFwIdentity(FwIdentity fwIdentity);

    /**
     * 删除身份
     *
     * @param id 身份ID
     * @return 结果
     */
    public int deleteFwIdentityById(String id);

    /**
     * 批量删除身份
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwIdentityByIds(String[] ids);
}
