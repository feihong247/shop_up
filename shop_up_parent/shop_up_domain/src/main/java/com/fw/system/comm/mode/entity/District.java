package com.fw.system.comm.mode.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName(value = "tb_district")
public class District implements Serializable {

    @TableField("id")
    private Integer id;

    @TableField("pid")
    private Integer pid;

    @TableField("code")
    private String code;

    @TableField("name")
    private String name;

    @TableField("pid")
    private String remark;

    @TableField("status")
    private Integer status;

    @TableField("level")
    private Integer level;

    @TableField("center")
    private String center;


}
