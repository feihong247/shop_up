package com.fw.system.admin.service.impl;

import java.util.List;

import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwFootprintMapper;
import com.fw.system.admin.domain.FwFootprint;
import com.fw.system.admin.service.IFwFootprintService;

/**
 * 足迹Service业务层处理
 *
 * @author yanwei
 * @date 2021-06-23
 */
@Service
public class FwFootprintServiceImpl extends ServiceImpl<FwFootprintMapper, FwFootprint> implements IFwFootprintService {
    @Autowired
    private FwFootprintMapper fwFootprintMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询足迹
     *
     * @param id 足迹ID
     * @return 足迹
     */
    @Override
    public FwFootprint selectFwFootprintById(String id) {
        return fwFootprintMapper.selectFwFootprintById(id);
    }

    /**
     * 查询足迹列表
     *
     * @param fwFootprint 足迹
     * @return 足迹
     */
    @Override
    public List<FwFootprint> selectFwFootprintList(FwFootprint fwFootprint) {
        return fwFootprintMapper.selectFwFootprintList(fwFootprint);
    }

    /**
     * 新增足迹
     *
     * @param fwFootprint 足迹
     * @return 结果
     */
    @Override
    public int insertFwFootprint(FwFootprint fwFootprint) {
        fwFootprint.setCreateTime(DateUtils.getNowDate());
        fwFootprint.setId(idXD.nextId());
        return fwFootprintMapper.insertFwFootprint(fwFootprint);
    }

    /**
     * 修改足迹
     *
     * @param fwFootprint 足迹
     * @return 结果
     */
    @Override
    public int updateFwFootprint(FwFootprint fwFootprint) {
        fwFootprint.setUpdateTime(DateUtils.getNowDate());
        return fwFootprintMapper.updateFwFootprint(fwFootprint);
    }

    /**
     * 批量删除足迹
     *
     * @param ids 需要删除的足迹ID
     * @return 结果
     */
    @Override
    public int deleteFwFootprintByIds(String[] ids) {
        return fwFootprintMapper.deleteFwFootprintByIds(ids);
    }

    /**
     * 删除足迹信息
     *
     * @param id 足迹ID
     * @return 结果
     */
    @Override
    public int deleteFwFootprintById(String id) {
        return fwFootprintMapper.deleteFwFootprintById(id);
    }
}
