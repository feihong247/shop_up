package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwMsg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户消息 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwMsgMapper extends BaseMapper<FwMsg> {

}
