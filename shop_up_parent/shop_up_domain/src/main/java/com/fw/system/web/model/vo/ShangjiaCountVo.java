package com.fw.system.web.model.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 商甲发行表
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
@Api(tags = "商甲发行Vo")
public class ShangjiaCountVo {

    /**
     * 商甲名称
     */
    @ApiModelProperty(value = "商甲名称")
    private List<ShangjiaVo> shangjiaVos;

    /**
     * 详细规则
     */
    @ApiModelProperty(value = "详细规则")
    private String detailedRules;



}
