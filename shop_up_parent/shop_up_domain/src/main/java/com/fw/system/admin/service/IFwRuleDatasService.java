package com.fw.system.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;
import java.util.List;

import com.fw.system.admin.domain.FwRuleDatas;

/**
 * 数据规则Service接口
 *
 * @author yanwei
 * @date 2021-06-02
 */
public interface IFwRuleDatasService extends IService<FwRuleDatas> {
    /**
     * 查询数据规则
     *
     * @param id 数据规则ID
     * @return 数据规则
     */
    public FwRuleDatas selectFwRuleDatasById(String id);

    /**
     * 查询数据规则列表
     *
     * @param fwRuleDatas 数据规则
     * @return 数据规则集合
     */
    public List<FwRuleDatas> selectFwRuleDatasList(FwRuleDatas fwRuleDatas);

    /**
     * 新增数据规则
     *
     * @param fwRuleDatas 数据规则
     * @return 结果
     */
    public int insertFwRuleDatas(FwRuleDatas fwRuleDatas);

    /**
     * 修改数据规则
     *
     * @param fwRuleDatas 数据规则
     * @return 结果
     */
    public int updateFwRuleDatas(FwRuleDatas fwRuleDatas);

    /**
     * 批量删除数据规则
     *
     * @param ids 需要删除的数据规则ID
     * @return 结果
     */
    public int deleteFwRuleDatasByIds(String[] ids);

    /**
     * 删除数据规则信息
     *
     * @param id 数据规则ID
     * @return 结果
     */
    public int deleteFwRuleDatasById(String id);

    BigDecimal getBigDecimal(String ruleType);

    Integer getInteger(String ruleType);
}
