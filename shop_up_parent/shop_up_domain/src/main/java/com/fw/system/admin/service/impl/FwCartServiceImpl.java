package com.fw.system.admin.service.impl;

import java.util.List;

import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwCartMapper;
import com.fw.system.admin.domain.FwCart;
import com.fw.system.admin.service.IFwCartService;

/**
 * 购物车Service业务层处理
 *
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwCartServiceImpl extends ServiceImpl<FwCartMapper, FwCart> implements IFwCartService {
    @Autowired
    private FwCartMapper fwCartMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询购物车
     *
     * @param id 购物车ID
     * @return 购物车
     */
    @Override
    public FwCart selectFwCartById(String id) {
        return fwCartMapper.selectFwCartById(id);
    }

    /**
     * 查询购物车列表
     *
     * @param fwCart 购物车
     * @return 购物车
     */
    @Override
    public List<FwCart> selectFwCartList(FwCart fwCart) {
        return fwCartMapper.selectFwCartList(fwCart);
    }

    /**
     * 新增购物车
     *
     * @param fwCart 购物车
     * @return 结果
     */
    @Override
    public int insertFwCart(FwCart fwCart) {
        fwCart.setCreateTime(DateUtils.getNowDate());
        fwCart.setId(idXD.nextId());
        return fwCartMapper.insertFwCart(fwCart);
    }

    /**
     * 修改购物车
     *
     * @param fwCart 购物车
     * @return 结果
     */
    @Override
    public int updateFwCart(FwCart fwCart) {
        fwCart.setUpdateTime(DateUtils.getNowDate());
        return fwCartMapper.updateFwCart(fwCart);
    }

    /**
     * 批量删除购物车
     *
     * @param ids 需要删除的购物车ID
     * @return 结果
     */
    @Override
    public int deleteFwCartByIds(String[] ids) {
        return fwCartMapper.deleteFwCartByIds(ids);
    }

    /**
     * 删除购物车信息
     *
     * @param id 购物车ID
     * @return 结果
     */
    @Override
    public int deleteFwCartById(String id) {
        return fwCartMapper.deleteFwCartById(id);
    }
}
