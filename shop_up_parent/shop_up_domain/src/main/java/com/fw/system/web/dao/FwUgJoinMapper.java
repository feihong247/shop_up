package com.fw.system.web.dao;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwUgJoin;
import org.springframework.stereotype.Repository;

/**
 * 用户竞猜中间Mapper接口
 * 
 * @author yanwei
 * @date 2021-06-23
 */
@Repository
public interface FwUgJoinMapper extends BaseMapper<FwUgJoin>
{
    /**
     * 查询用户竞猜中间
     * 
     * @param ugId 用户竞猜中间ID
     * @return 用户竞猜中间
     */
    public FwUgJoin selectFwUgJoinById(String ugId);

    /**
     * 查询用户竞猜中间列表
     * 
     * @param fwUgJoin 用户竞猜中间
     * @return 用户竞猜中间集合
     */
    public List<FwUgJoin> selectFwUgJoinList(FwUgJoin fwUgJoin);

    /**
     * 新增用户竞猜中间
     * 
     * @param fwUgJoin 用户竞猜中间
     * @return 结果
     */
    public int insertFwUgJoin(FwUgJoin fwUgJoin);

    /**
     * 修改用户竞猜中间
     * 
     * @param fwUgJoin 用户竞猜中间
     * @return 结果
     */
    public int updateFwUgJoin(FwUgJoin fwUgJoin);

    /**
     * 删除用户竞猜中间
     * 
     * @param ugId 用户竞猜中间ID
     * @return 结果
     */
    public int deleteFwUgJoinById(String ugId);

    /**
     * 批量删除用户竞猜中间
     * 
     * @param ugIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwUgJoinByIds(String[] ugIds);
}
