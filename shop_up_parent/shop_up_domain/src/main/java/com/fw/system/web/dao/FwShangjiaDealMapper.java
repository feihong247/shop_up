package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwShangjiaDeal;
import com.fw.system.web.model.vo.ShangjiaDealVo;

import java.util.List;

/**
 * <p>
 * 商甲交易,买卖表 Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-06-08
 */
public interface FwShangjiaDealMapper extends BaseMapper<FwShangjiaDeal> {

    /**
     * 根据type查询交易大厅
     */
    public List<ShangjiaDealVo> findDealList(String type);

}
