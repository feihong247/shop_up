package com.fw.system.web.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.system.web.dao.ExampleTableMapper;
import com.fw.system.web.model.entity.ExampleTable;
import com.fw.system.web.service.IExampleTableService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yanwei
 * @since 2020-08-05
 */
@Service
public class ExampleTableServiceImpl extends ServiceImpl<ExampleTableMapper, ExampleTable> implements IExampleTableService {

}
