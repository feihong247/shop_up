package com.fw.system.web.model.form.v2;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fw.system.web.model.entity.FwImages;
import com.fw.system.web.model.entity.FwSku;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@ApiModel("商品表单")
public class SpuForm implements Serializable {
    /**
     * 主键
     */
    @ApiModelProperty("商品主键,没有则不填写")
    private String id;


    /**
     * 商品标题
     */
    @ApiModelProperty("商品标题")
    @NotBlank(message = "商品标题 不可为空")
    private String title;

    /**
     * 商品logo
     */
    @ApiModelProperty("商品logo")
    @NotBlank(message = "商品logo 不可为空")
    private String logo;

    /**
     * 商品销量
     */
    @ApiModelProperty("商品销量")
    private Integer tops;


    @ApiModelProperty(hidden = true,value = "内部使用")
    private String shopId;


    /**
     * 商品原价格
     */
    @ApiModelProperty("商品原价格")
    @NotNull(message = "商品原价格 不可为空")
    @DecimalMin(value = "0",message = "商品原价格 必须大于0")
    private BigDecimal virtualPrice;

    /**
     * 商品现价格
     */
    @ApiModelProperty("商品现价格")
    @NotNull(message = "商品现价格 不可为空")
    @DecimalMin(value = "0",message = "商品现价格 必须大于0")
    private BigDecimal realPrice;

    /**
     * 商品利润值,后管看的
     */
    @ApiModelProperty("商品利润值")
    private BigDecimal price;

    /**
     * 商品消证,后管自营人员填写
     */
    @ApiModelProperty("商品消证")
    private BigDecimal disappear;

    /**
     * 商品总库存
     */
    @ApiModelProperty("商品总库存,商户不可编辑，所有sku 的库存加起来才是总库存")
    private Integer balance;

    /**
     * 商品详情-富文本形式
     */
    @ApiModelProperty("商品详情 - 富文本形式")
    @NotBlank(message = "商品详情 不可为空")
    @Length(min = 20,message = "商品详情 字数必须大于20和字数")
    private String infos;

    /**
     * 商品状态 0=正常，1=下架
     */
    @ApiModelProperty("商品状态 0=正常，1=下架")
    @NotNull(message = "商品状态 不可为空")
    @Min(value = 0L,message = "必须是0或者1")
    @Max(value = 1L,message = "必须是0或者1")
    private Integer status;

    /** 商品运费 */
    @ApiModelProperty("商品运费")
    private BigDecimal shipping;

    /**
     * 商品满额减运费 需要一套模版表,地区减额，单独商品减额，还是商家总商品减额等。
     */
    @ApiModelProperty("商品满额减运费")
    private BigDecimal topShipping;

    /** 是否满减运费0否1是 */
    @ApiModelProperty("是否满减运费0否1是")
    private Integer isTopShipping;


    /** 是否热卖 */
    @ApiModelProperty("是否热销 （0否1是）")
    private Integer hotSale;

    /** 是否推荐 */
    @ApiModelProperty("是否推荐 （0否1是）")
    private Integer recommend;

    /** 类目 */
    @ApiModelProperty("类目 编号")
    private String category;


    /** 是否线下(0线上1线下) */
    @ApiModelProperty("是否线下(0否1是)")
    private String isOffline;

    /** 是否竞猜(0线上1线下) */
    @ApiModelProperty("是否竞猜(0否1是)")
    private Integer isQuiz;

    /** 是否特卖（0否1是） */
    @ApiModelProperty("是否特卖（0否1是）")
    private Integer isSale;

    /** 规格列表 */
    @ApiModelProperty("规格列表")
    private List<SkuForm> skuList = new ArrayList<>();

    /** 图片列表 */
    @ApiModelProperty("图片列表")
    private List<ImagesForm> images = new ArrayList<>();

    @ApiModelProperty(value = "省",hidden = true)
    private String province;

    @ApiModelProperty(value = "市",hidden = true)
    private String city;

    @ApiModelProperty(value = "县",hidden = true)
    private String area;
}
