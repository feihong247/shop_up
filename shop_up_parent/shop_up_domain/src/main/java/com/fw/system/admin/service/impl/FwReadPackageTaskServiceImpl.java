package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwReadPackageTaskMapper;
import com.fw.system.admin.domain.FwReadPackageTask;
import com.fw.system.admin.service.IFwReadPackageTaskService;

/**
 * 红包任务类型列Service业务层处理
 * 
 * @author yanwei
 * @date 2021-10-12
 */
@Service
public class FwReadPackageTaskServiceImpl extends ServiceImpl<FwReadPackageTaskMapper, FwReadPackageTask> implements IFwReadPackageTaskService
{
    @Autowired
    private FwReadPackageTaskMapper fwReadPackageTaskMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询红包任务类型列
     * 
     * @param id 红包任务类型列ID
     * @return 红包任务类型列
     */
    @Override
    public FwReadPackageTask selectFwReadPackageTaskById(String id)
    {
        return fwReadPackageTaskMapper.selectFwReadPackageTaskById(id);
    }

    /**
     * 查询红包任务类型列列表
     * 
     * @param fwReadPackageTask 红包任务类型列
     * @return 红包任务类型列
     */
    @Override
    public List<FwReadPackageTask> selectFwReadPackageTaskList(FwReadPackageTask fwReadPackageTask)
    {
        return fwReadPackageTaskMapper.selectFwReadPackageTaskList(fwReadPackageTask);
    }

    /**
     * 新增红包任务类型列
     * 
     * @param fwReadPackageTask 红包任务类型列
     * @return 结果
     */
    @Override
    public int insertFwReadPackageTask(FwReadPackageTask fwReadPackageTask)
    {
        fwReadPackageTask.setCreateTime(DateUtils.getNowDate());
        fwReadPackageTask.setId(idXD.nextId());
        return fwReadPackageTaskMapper.insertFwReadPackageTask(fwReadPackageTask);
    }

    /**
     * 修改红包任务类型列
     * 
     * @param fwReadPackageTask 红包任务类型列
     * @return 结果
     */
    @Override
    public int updateFwReadPackageTask(FwReadPackageTask fwReadPackageTask)
    {
        return fwReadPackageTaskMapper.updateFwReadPackageTask(fwReadPackageTask);
    }

    /**
     * 批量删除红包任务类型列
     * 
     * @param ids 需要删除的红包任务类型列ID
     * @return 结果
     */
    @Override
    public int deleteFwReadPackageTaskByIds(String[] ids)
    {
        return fwReadPackageTaskMapper.deleteFwReadPackageTaskByIds(ids);
    }

    /**
     * 删除红包任务类型列信息
     * 
     * @param id 红包任务类型列ID
     * @return 结果
     */
    @Override
    public int deleteFwReadPackageTaskById(String id)
    {
        return fwReadPackageTaskMapper.deleteFwReadPackageTaskById(id);
    }
}
