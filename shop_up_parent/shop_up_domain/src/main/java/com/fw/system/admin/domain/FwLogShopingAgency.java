package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 用户购买代理日志对象 fw_log_shoping_agency
 *
 * @author yanwei
 * @date 2021-06-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_log_shoping_agency")
@ApiModel(value="用户购买代理日志", description="用户购买代理日志")
public class FwLogShopingAgency extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 用户编号 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "用户编号")
    @TableField("user_id")
    private String userId;

    /** 所购代理 */
    @ApiModelProperty(value = "用户编号")
    @Excel(name = "所购代理")
    @TableField("Identity_id")
    private String identityId;

    /** 审核状态:0已购买待审核,1审核通过,赋予身份,2驳回 */
    @ApiModelProperty(value = "所购代理")
    @Excel(name = "审核状态:0已购买待审核,1审核通过,赋予身份,2驳回")
    @TableField("is_type")
    private Integer isType;




}
