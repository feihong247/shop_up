package com.fw.system.web.model.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("商甲列表带代办")
public class ShopListVo {
}
