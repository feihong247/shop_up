package com.fw.system.web.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel("/商甲今年产出明细和用户信息vo")
public class ShangJiaLogUserVo {


    /**
     * 业务编号
     */
    @TableField("bui_id")
    private String buiId;


    /**
     * 自定义数据
     */
    @TableField("form_info")
    private String formInfo;


    /**
     * 创建时间 创建时间
     */
    @TableField("create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;


    /**
     * 自定义数据
     */
    @ApiModelProperty("用户姓名")
    private String userName;
    /**
     * 自定义数据
     */
    @ApiModelProperty("电话")
    private String phone;

}
