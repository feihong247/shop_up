package com.fw.system.admin.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 地图对象 fw_pol
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_pol")
@ApiModel(value="地图", description="地图表")
public class FwPol extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 业务id */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "业务id")
    @TableField("bus_id")
    private String busId;



    /**
     * 省
     */
    @TableField("province")
    private String province;

    /**
     * 市
     */
    @TableField("city")
    private String city;

    /**
     * 区/县
     */
    @TableField("area")
    private String area;

    /**
     * 详细地址
     */
    @TableField("addr_info")
    private String addrInfo;

    /** 纬度 */
    @ApiModelProperty(value = "业务id")
    @Excel(name = "纬度")
    @TableField("latitude")
    private BigDecimal latitude;

    /** 经度 */
    @ApiModelProperty(value = "纬度")
    @Excel(name = "经度")
    @TableField("longitude")
    private BigDecimal longitude;




}
