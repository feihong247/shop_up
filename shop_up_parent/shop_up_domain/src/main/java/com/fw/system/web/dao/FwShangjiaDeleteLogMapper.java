package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwShangjiaDeleteLog;

/**
 * <p>
 * 商甲销毁日志表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
public interface FwShangjiaDeleteLogMapper extends BaseMapper<FwShangjiaDeleteLog> {

}
