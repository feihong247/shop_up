package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwReadPackageTaskContent;

/**
 * 红包关联任务用户完成情况Service接口
 * 
 * @author yanwei
 * @date 2021-10-12
 */
public interface IFwReadPackageTaskContentService extends IService<FwReadPackageTaskContent>
{
    /**
     * 查询红包关联任务用户完成情况
     * 
     * @param id 红包关联任务用户完成情况ID
     * @return 红包关联任务用户完成情况
     */
    public FwReadPackageTaskContent selectFwReadPackageTaskContentById(String id);

    /**
     * 查询红包关联任务用户完成情况列表
     * 
     * @param fwReadPackageTaskContent 红包关联任务用户完成情况
     * @return 红包关联任务用户完成情况集合
     */
    public List<FwReadPackageTaskContent> selectFwReadPackageTaskContentList(FwReadPackageTaskContent fwReadPackageTaskContent);

    /**
     * 新增红包关联任务用户完成情况
     * 
     * @param fwReadPackageTaskContent 红包关联任务用户完成情况
     * @return 结果
     */
    public int insertFwReadPackageTaskContent(FwReadPackageTaskContent fwReadPackageTaskContent);

    /**
     * 修改红包关联任务用户完成情况
     * 
     * @param fwReadPackageTaskContent 红包关联任务用户完成情况
     * @return 结果
     */
    public int updateFwReadPackageTaskContent(FwReadPackageTaskContent fwReadPackageTaskContent);

    /**
     * 批量删除红包关联任务用户完成情况
     * 
     * @param ids 需要删除的红包关联任务用户完成情况ID
     * @return 结果
     */
    public int deleteFwReadPackageTaskContentByIds(String[] ids);

    /**
     * 删除红包关联任务用户完成情况信息
     * 
     * @param id 红包关联任务用户完成情况ID
     * @return 结果
     */
    public int deleteFwReadPackageTaskContentById(String id);
}
