package com.fw.system.web.service.impl;

import com.fw.system.web.dao.FwShopKeywordMapper;
import com.fw.system.web.model.entity.FwShopKeyword;
import com.fw.system.web.service.IFwShopKeywordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商铺环境展示 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-07-16
 */
@Service
public class FwShopKeywordServiceImpl extends ServiceImpl<FwShopKeywordMapper, FwShopKeyword> implements IFwShopKeywordService {

}
