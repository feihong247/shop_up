package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwLogs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 业务日志 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwLogsMapper extends BaseMapper<FwLogs> {
    /**
     * 商甲产出
     * @return
     */
    public List<FwLogs> getShangJiaTotal(String buiId );
    /**
     * 年商甲产出
     * @return
     */
    public List<FwLogs> getShangJiaYearTotal(String buiId);

    /**
     * 月商甲产出
     * @return
     */
    public List<FwLogs> getShangJiaMonthTotal(String buiId);
    /**
     * 月商甲产出
     * @return
     */
    public List<FwLogs> getShangJiaDayTotal(String buiId);

    /**
     * 消费获取销证总数
     * @return
     */
    public List<FwLogs> getDisappearTotal(String buiId);
    /**
     * 消费获取线上销证总数
     * @return
     */
    public List<Map> getDisappearUpTotal(String buiId);
    /**
     *消费获取线下销证总数
     * @return
     */
    public List<Map> getDisappearDownTotal(String buiId);
    /**
     *消费获取线上销证明细
     * @return
     */
    public List<Map> getDisappearUpDetail(@Param("buiId") String buiId, @Param("createTime") String createTime);
    /**
     *消费获取线下销证明细
     * @return
     */
    public List<Map> getDisappearDownDetail(@Param("buiId")String buiId,@Param("createTime")String createTime);
    /**
     *分红
     * @return
     */
    public List<FwLogs> getBonusDetail(String buiId);

    /**
     * 获取上一次分红
     * @return
     */
    public List<FwLogs> getLastBonusDetail(String buiId);
    /**
     * 获取商甲总产出包括产出释放
     * @return
     */
    public List<FwLogs> getShangJiaOutDetail();
    /**
     * 获取释放未满24h的商甲数
     * @return
     */
    public List<FwLogs> getShangJiaLtOneDay(String buiId);

    /**
     * 获取已用商甲列表商甲数
     * @param typeModel
     * @param modelName
     * @return
     */
    List<FwLogs> getShangJiaUseList(@Param("typeModel")String typeModel, @Param("modelName")String modelName);
    /**
     * 获取销毁商甲列表商甲数
     * @return
     */
    public List<FwLogs> getShangJiaDestoryList(String typeName, String modelName, String json);
}
