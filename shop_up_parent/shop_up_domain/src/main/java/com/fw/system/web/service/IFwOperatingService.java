package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwOperating;
import com.fw.system.web.model.vo.OperatingVo;

import java.util.List;

/**
 * <p>
 * 商家经营平台分类 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-19
 */
public interface IFwOperatingService extends IService<FwOperating> {

    List<OperatingVo> findAll();
}
