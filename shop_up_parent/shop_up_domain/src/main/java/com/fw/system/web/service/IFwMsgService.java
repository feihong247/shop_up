package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwMsg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户消息 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface IFwMsgService extends IService<FwMsg> {

}
