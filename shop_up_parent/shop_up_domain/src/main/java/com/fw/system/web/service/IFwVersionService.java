package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwVersion;

/**
 * <p>
 * 版本表 服务类
 * </p>
 *
 * @author  
 * @since 2021-09-08
 */
public interface IFwVersionService extends IService<FwVersion> {

}
