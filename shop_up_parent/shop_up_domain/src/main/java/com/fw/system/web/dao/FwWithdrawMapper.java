package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwWithdraw;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户提现表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwWithdrawMapper extends BaseMapper<FwWithdraw> {

}
