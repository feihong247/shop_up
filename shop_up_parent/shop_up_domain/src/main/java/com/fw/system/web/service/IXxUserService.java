package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.XxUser;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author  
 * @since 2021-07-31
 */
public interface IXxUserService extends IService<XxUser> {

}
