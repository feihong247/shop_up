package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwCart;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 购物车 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface IFwCartService extends IService<FwCart> {

}
