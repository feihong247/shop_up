package com.fw.system.web.model.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * 在线客服
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
@Api(tags = "联系客服vo")
public class ServiceVo {

    /**
     * 主键
     */
    @ApiModelProperty("id")
    private String id;

    /**
     * 客服联系方式 富文本形式
     */
    @ApiModelProperty("客服联系方式,富文本形式")
    private String serviceInfos;

    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty("update_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;



}
