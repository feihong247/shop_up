package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwService;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 在线客服Mapper接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface FwServiceMapper extends BaseMapper<FwService>
{
    /**
     * 查询在线客服
     * 
     * @param id 在线客服ID
     * @return 在线客服
     */
    public FwService selectFwServiceById(String id);

    /**
     * 查询在线客服列表
     * 
     * @param fwService 在线客服
     * @return 在线客服集合
     */
    public List<FwService> selectFwServiceList(FwService fwService);

    /**
     * 新增在线客服
     * 
     * @param fwService 在线客服
     * @return 结果
     */
    public int insertFwService(FwService fwService);

    /**
     * 修改在线客服
     * 
     * @param fwService 在线客服
     * @return 结果
     */
    public int updateFwService(FwService fwService);

    /**
     * 删除在线客服
     * 
     * @param id 在线客服ID
     * @return 结果
     */
    public int deleteFwServiceById(String id);

    /**
     * 批量删除在线客服
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwServiceByIds(String[] ids);
}
