package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwService;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 在线客服 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface IFwServiceService extends IService<FwService> {

}
