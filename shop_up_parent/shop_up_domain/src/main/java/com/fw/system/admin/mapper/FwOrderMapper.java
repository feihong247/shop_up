package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 订单Mapper接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface FwOrderMapper extends BaseMapper<FwOrder>
{
    /**
     * 查询订单
     * 
     * @param id 订单ID
     * @return 订单
     */
    public FwOrder selectFwOrderById(String id);

    /**
     * 查询订单列表
     * 
     * @param fwOrder 订单
     * @return 订单集合
     */
    public List<FwOrder> selectFwOrderList(FwOrder fwOrder);

    /**
     * 新增订单
     * 
     * @param fwOrder 订单
     * @return 结果
     */
    public int insertFwOrder(FwOrder fwOrder);

    /**
     * 修改订单
     * 
     * @param fwOrder 订单
     * @return 结果
     */
    public int updateFwOrder(FwOrder fwOrder);

    /**
     * 删除订单
     * 
     * @param id 订单ID
     * @return 结果
     */
    public int deleteFwOrderById(String id);

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwOrderByIds(String[] ids);
}
