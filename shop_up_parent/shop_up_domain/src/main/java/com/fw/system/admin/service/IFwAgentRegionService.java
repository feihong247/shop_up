package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwAgentRegion;

/**
 * 代理区域Service接口
 * 
 * @author yanwei
 * @date 2021-07-23
 */
public interface IFwAgentRegionService extends IService<FwAgentRegion>
{

    /**
     * 查询代理区域
     * 
     * @param id 代理区域ID
     * @return 代理区域
     */
    public FwAgentRegion selectFwAgentRegionById(String id);

    /**
     * 查询代理区域列表
     * 
     * @param fwAgentRegion 代理区域
     * @return 代理区域集合
     */
    public List<FwAgentRegion> selectFwAgentRegionList(FwAgentRegion fwAgentRegion);

    /**
     * 新增代理区域
     * 
     * @param fwAgentRegion 代理区域
     * @return 结果
     */
    public int insertFwAgentRegion(FwAgentRegion fwAgentRegion);

    /**
     * 修改代理区域
     * 
     * @param fwAgentRegion 代理区域
     * @return 结果
     */
    public int updateFwAgentRegion(FwAgentRegion fwAgentRegion);

    /**
     * 批量删除代理区域
     * 
     * @param ids 需要删除的代理区域ID
     * @return 结果
     */
    public int deleteFwAgentRegionByIds(String[] ids);

    /**
     * 删除代理区域信息
     * 
     * @param id 代理区域ID
     * @return 结果
     */
    public int deleteFwAgentRegionById(String id);
}
