package com.fw.system.admin.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fw.annotation.Excel;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 订单对象 fw_order
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_order")
@ApiModel(value="订单", description="订单")
public class FwOrder extends Model<FwOrder>
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 用户编号 */
    @ApiModelProperty(value = "用户编号")
    @TableField("user_id")
    private String userId;

    @Excel(name = "姓名")
    @TableField(exist = false)
    private String userName;

    @TableField(exist = false)
    @Excel(name = "手机号")
    private String phone;

    /** 是否是竞猜订单,0=否，1=是 */
    @ApiModelProperty(value = "是否是竞猜订单,0=否，1=是")
    @Excel(name = "竞猜订单",readConverterExp = "0=否,1=是")
    @TableField("is_quiz")
    private Integer isQuiz;

    /** 订单总金额 */
    @ApiModelProperty(value = "订单总金额")
    @Excel(name = "订单总金额")
    @TableField("total_price")
    private BigDecimal totalPrice;

    /** 订单状态,0=待付款，1=待发货，2=待收货，3=待评价，4=退货/退款,5=已收货 */
    @ApiModelProperty(value = "订单状态,0=待付款，1=待发货，2=待收货，3=待评价，4=退货/退款,5=已收货")
    @Excel(name = "订单状态",readConverterExp = "0=待付款,1=待发货,2=待收货,3=待评价,4=退货/退款,5=已收货,6=确认收货,7=申请售后")
    @TableField("status")
    private Integer status;

    /** 订单运费 */
    @ApiModelProperty(value = "订单运费")
//    @Excel(name = "订单运费")
    @TableField("shopping")
    private BigDecimal shopping;

    /** 订单消证 */
    @ApiModelProperty(value = "订单消证")
    @Excel(name = "订单消证")
    @TableField("disappear")
    private BigDecimal disappear;

    /** 商户编号 提交订单，多商户进行分裂即可 */
    @ApiModelProperty(value = "商户编号")
    @Excel(name = "商户编号")
    @TableField("shop_id")
    private String shopId;

    /** 运单公司名称 */
    @ApiModelProperty(value = "运单公司名称")
    @Excel(name = "运单公司名称")
    @TableField("courier_name")
    private String courierName;

    /** 运单编号 */
    @ApiModelProperty(value = "运单编号")
    @Excel(name = "运单编号")
    @TableField("courier_number")
    private String courierNumber;

    /** 订单系统编号 */
    @ApiModelProperty(value = "订单系统编号")
    @Excel(name = "订单系统编号")
    @TableField("out_trade_no")
    private String outTradeNo;

    /** 支付类型,0=支付宝，1=微信 */
    @ApiModelProperty(value = "支付类型")
    @Excel(name = "支付类型",readConverterExp = "0=支付宝,1=微信")
    @TableField("pay_type")
    private Integer payType;

    /** 是否评价,0=否，1=是 */
    @ApiModelProperty(value = "是否评价,0=否，1=是")
    @Excel(name = "是否评价",readConverterExp = "0=否,1=是")
    @TableField("is_appraise")
    private Integer isAppraise;

    /** 售后状态（1申请，2同意，3拒绝） */
    @ApiModelProperty(value = "售后状态（1申请，2同意，3拒绝）")
    @Excel(name = "售后状态",readConverterExp = "1=申请,2=同意,3=拒绝")
    @TableField("after_sale_state")
    private Integer afterSaleState;

    /** 售后备注 */
    @ApiModelProperty(value = "售后备注")
    @Excel(name = "售后备注")
    @TableField("after_sale_remark")
    private String afterSaleRemark;

    /**
     * 创建人
     */
    @TableField("create_by")
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @TableField("update_by")
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    @ApiModelProperty("更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /** 客服二维码 */
    @TableField("server_qr")
    @ApiModelProperty("客服二维码")
    private String serverQr;

    /** 大订单id */
    @TableField("parent_order_id")
    @ApiModelProperty("大订单id")
    private String parentOrderId;

    /** 商品ID */
    @TableField("spu_id")
    @ApiModelProperty("商品ID")
    private String spuId;

    @TableField(exist = false)
    private FwSpu spu;

    /** 规格ID */
    @TableField("sku_id")
    @ApiModelProperty("规格ID")
    private String skuId;

    @TableField(exist = false)
    private FwSku sku;

    @TableField("spu_number")
    @ApiModelProperty("购买商品数量")
    private Integer spuNumber;

    /** 地址Id */
    @TableField("addr_id")
    @ApiModelProperty("地址Id")
    private String addrId;

    @TableField("addr_string")
    @ApiModelProperty("地址字符串，&-fw-& 连接")
    private String addrString;

    /** 联系人姓名 */
    @ApiModelProperty("收货人姓名")
    @Excel(name = "收货人姓名")
    @TableField(exist = false)
    private String contactName;

    /** 联系人电话 */
    @ApiModelProperty("收货人电话")
    @Excel(name = "收货人电话")
    @TableField(exist = false)
    private String contactPhone;

    /** 收货地址 */
    @ApiModelProperty("收货地址")
    @TableField(exist = false)
    @Excel(name = "收货地址")
    private String addrInfo;

    /** 订单满减 */
    @TableField("full_discount")
    @ApiModelProperty("订单满减")
    @Excel(name = "订单满减")
    private BigDecimal fullDiscount;

    /** 删除标志（==1删除） */
    @ApiModelProperty(value = "删除标志（==1删除）")
    @TableField("remove_flag")
    private Integer removeFlag;

    /** 创建时间_起 */
    @TableField(exist = false)
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("创建时间_起")
    private Long createTimeBegin = 0L;

    /** 创建时间_止 */
    @TableField(exist = false)
//    @JsonFormat(timezone = "GMT+16", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("创建时间_止")
    private Long createTimeEnd = 0L;

    @TableField(exist = false)
    @ApiModelProperty("商品名称")
    private String spuTitle;



}
