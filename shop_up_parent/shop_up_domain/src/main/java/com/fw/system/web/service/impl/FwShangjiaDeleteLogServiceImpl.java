package com.fw.system.web.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.common.Builder;
import com.fw.common.IdXD;
import com.fw.system.web.dao.FwShangjiaDeleteLogMapper;
import com.fw.system.web.model.entity.FwCurrencyLog;
import com.fw.system.web.model.entity.FwShangjiaDeleteLog;
import com.fw.system.web.service.IFwCurrencyLogService;
import com.fw.system.web.service.IFwShangjiaDeleteLogService;
import com.fw.system.web.service.IFwShangjiaService;
import com.fw.system.web.service.IFwUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 商甲销毁日志表 服务实现类
 * </p>
 *
 * @author
 * @since 2021-08-01
 */
@Service
public class FwShangjiaDeleteLogServiceImpl extends ServiceImpl<FwShangjiaDeleteLogMapper, FwShangjiaDeleteLog> implements IFwShangjiaDeleteLogService {


    @Autowired
    private FwShangjiaDeleteLogMapper deleteLogMapper;
    @Autowired
    private IFwCurrencyLogService currencyLogService;
    @Autowired
    private IdXD idXD;
    @Autowired
    private IFwShangjiaService shangjiaService;
    @Autowired
    private IFwUserService userService;
    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Override
    @Async
    public  void saveLabLog(BigDecimal num, String userId) {
        //获取商甲价格计算要销毁的数量向上取整
        //num为提现金额
        BigDecimal divide = num.divide(shangjiaService.getById("1").getPresentPrice(),5,BigDecimal.ROUND_HALF_UP);
        long deleteNum  = divide.setScale( 0, BigDecimal.ROUND_UP ).longValue();
        if (divide.compareTo(new BigDecimal(1)) <= 0)
            //销毁数量
            divide = new BigDecimal(1);
        //增加日志销毁明细
        FwShangjiaDeleteLog builds = Builder.of(FwShangjiaDeleteLog::new).
                with(FwShangjiaDeleteLog::setId, idXD.nextId())
                .with(FwShangjiaDeleteLog::setCreateTime, LocalDateTime.now())
                .with(FwShangjiaDeleteLog::setShangjiaNum, new BigDecimal("40"))
                .with(FwShangjiaDeleteLog::setLogType, 6)
                .with(FwShangjiaDeleteLog::setUserId, userId)
                .with(FwShangjiaDeleteLog::setUserPhone, userService.getById(userId).getPhone())
                .with(FwShangjiaDeleteLog::setLogCount, new BigDecimal(deleteNum)).build();
        deleteLogMapper.insert(builds);
        FwCurrencyLog currencyLog = currencyLogService.getOne(Wrappers.<FwCurrencyLog>lambdaQuery().eq(FwCurrencyLog::getLogType, 4));
        if (currencyLog == null) {
             currencyLog = Builder.of(FwCurrencyLog::new).with(FwCurrencyLog::setId, idXD.nextId()).
                    with(FwCurrencyLog::setCreateTime, LocalDateTime.now()).
                    with(FwCurrencyLog::setLogCount, new BigDecimal(deleteNum)).
                    with(FwCurrencyLog::setInfoFrom, 4).with(FwCurrencyLog::setLogName, "销毁商甲标识").
                    with(FwCurrencyLog::setUserId, null).with(FwCurrencyLog::setLogType, 4).build();
            currencyLogService.save(currencyLog);
            if (currencyLog.getLogCount().compareTo(new BigDecimal("48")) >= 0) {
                deleteShangjia(currencyLog.getLogCount(), userId);
            }
        }else {
            currencyLog.setLogCount(currencyLog.getLogCount().add(new BigDecimal(deleteNum)));
            currencyLogService.updateById(currencyLog);
            deleteShangjia(currencyLog.getLogCount(), userId);
        }
    }
    //销毁逻辑
    public void deleteShangjia(BigDecimal num, String userId) {
        FwCurrencyLog currencyLog = currencyLogService.getOne(Wrappers.<FwCurrencyLog>lambdaQuery().eq(FwCurrencyLog::getLogType, 4));

        if (num.compareTo(new BigDecimal("48")) >= 0) {
            BigDecimal divide = currencyLog.getLogCount().divide(new BigDecimal("48"), 5, BigDecimal.ROUND_HALF_UP);
            long deleteNum = divide.setScale(0, BigDecimal.ROUND_DOWN).longValue();
            threadPoolTaskExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    for (int i =0;i<deleteNum;i++){
                        //执行销毁
                        shangjiaService.list().forEach(item -> {
                            if (item.getId().equals("1")) {
                                //消费商甲销毁40个
                                if (item.getShopIssue().compareTo(new BigDecimal(0)) > 0) {
                                    item.setShopIssue(item.getShopIssue().subtract(new BigDecimal("40")));
                                    item.setDeleteNum(item.getDeleteNum().add(new BigDecimal("40")));
                                    if (item.getShopIssue().compareTo(new BigDecimal("0")) < 0)
                                        item.setShopIssue(new BigDecimal("0"));
                                    shangjiaService.updateById(item);
                                }
                            }
                            if (item.getId().equals("2")) {
                                //消费商甲销毁40个
                                if (item.getShopIssue().compareTo(new BigDecimal(0)) > 0) {
                                    item.setShopIssue(item.getShopIssue().subtract(new BigDecimal("2")));
                                    item.setDeleteNum(item.getDeleteNum().add(new BigDecimal("2")));
                                    if (item.getShopIssue().compareTo(new BigDecimal("0")) < 0)
                                        item.setShopIssue(new BigDecimal("0"));
                                    shangjiaService.updateById(item);
                                }
                            }
                            if (item.getId().equals("3")) {
                                //消费商甲销毁40个
                                if (item.getShopIssue().compareTo(new BigDecimal(0)) > 0) {
                                    item.setShopIssue(item.getShopIssue().subtract(new BigDecimal("4")));
                                    item.setDeleteNum(item.getDeleteNum().add(new BigDecimal("4")));
                                    if (item.getShopIssue().compareTo(new BigDecimal("0")) < 0)
                                        item.setShopIssue(new BigDecimal("0"));
                                    shangjiaService.updateById(item);
                                }
                            }
                            if (item.getId().equals("4")) {
                                //消费商甲销毁40个
                                if (item.getShopIssue().compareTo(new BigDecimal(0)) > 0) {
                                    item.setShopIssue(item.getShopIssue().subtract(new BigDecimal("1")));
                                    item.setDeleteNum(item.getDeleteNum().add(new BigDecimal("1")));
                                    if (item.getShopIssue().compareTo(new BigDecimal("0")) < 0)
                                        item.setShopIssue(new BigDecimal("0"));
                                    shangjiaService.updateById(item);
                                }
                            }
                            if (item.getId().equals("5")) {
                                //消费商甲销毁40个
                                if (item.getShopIssue().compareTo(new BigDecimal(0)) > 0) {
                                    item.setShopIssue(item.getShopIssue().subtract(new BigDecimal("1")));
                                    item.setDeleteNum(item.getDeleteNum().add(new BigDecimal("1")));
                                    if (item.getShopIssue().compareTo(new BigDecimal("0")) < 0)
                                        item.setShopIssue(new BigDecimal("0"));
                                    shangjiaService.updateById(item);
                                }
                            }
                        });
                    }
                }
            });

            //修改下次的标识
            int i = 48;
            Long ll =  deleteNum * i;
            currencyLog.setLogCount(currencyLog.getLogCount().subtract(new BigDecimal(ll)));
            currencyLogService.updateById(currencyLog);
        }

    }

 /*   public static void main(String[] args) {
        BigDecimal divide = new BigDecimal("1").divide(new BigDecimal("4.09"),5,BigDecimal.ROUND_HALF_UP);
        System.out.println(divide);

        long l  = divide.setScale( 0, BigDecimal.ROUND_UP ).longValue(); // 向上取整
        System.out.println(l);

    }*/
}
