package com.fw.system.web.model.vo.v2;

import com.fw.system.web.model.entity.FwParentOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("订单列表返回VO")
@Builder
public class ShopOrderParentVo implements Serializable {

    @ApiModelProperty("一共多少条数")
    private Integer total;

    @ApiModelProperty("数据集合")
    private List<FwParentOrder> parentOrders;
}
