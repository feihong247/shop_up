package com.fw.system.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.system.web.dao.FwVersionMapper;
import com.fw.system.web.model.entity.FwVersion;
import com.fw.system.web.service.IFwVersionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 版本表 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-09-08
 */
@Service
public class FwVersionServiceImpl extends ServiceImpl<FwVersionMapper, FwVersion> implements IFwVersionService {

}
