package com.fw.system.admin.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import com.fw.utils.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwSpuMapper;
import com.fw.system.admin.domain.FwSpu;
import com.fw.system.admin.service.IFwSpuService;

/**
 * 商品Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwSpuServiceImpl extends ServiceImpl<FwSpuMapper, FwSpu> implements IFwSpuService
{
    @Autowired
    private FwSpuMapper fwSpuMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询商品
     * 
     * @param id 商品ID
     * @return 商品
     */
    @Override
    public FwSpu selectFwSpuById(String id)
    {
        return fwSpuMapper.selectFwSpuById(id);
    }

    /**
     * 查询商品列表
     * 
     * @param fwSpu 商品
     * @return 商品
     */
    @Override
    public List<FwSpu> selectFwSpuList(FwSpu fwSpu)
    {
        return fwSpuMapper.selectFwSpuList(fwSpu);
    }

    /**
     * 新增商品
     * 
     * @param fwSpu 商品
     * @return 结果
     */
    @Override
    public int insertFwSpu(FwSpu fwSpu) {
        fwSpu.setCreateTime(LocalDateTime.now());
        fwSpu.setId(idXD.nextId());
        return fwSpuMapper.insertFwSpu(fwSpu);
    }

    /**
     * 修改商品
     * 
     * @param fwSpu 商品
     * @return 结果
     */
    @Override
    public int updateFwSpu(FwSpu fwSpu) {
        fwSpu.setUpdateTime(LocalDateTime.now());
        return fwSpuMapper.updateFwSpu(fwSpu);
    }

    /**
     * 批量删除商品
     * 
     * @param ids 需要删除的商品ID
     * @return 结果
     */
    @Override
    public int deleteFwSpuByIds(String[] ids)
    {
        return fwSpuMapper.deleteFwSpuByIds(ids);
    }

    /**
     * 删除商品信息
     * 
     * @param id 商品ID
     * @return 结果
     */
    @Override
    public int deleteFwSpuById(String id)
    {
        return fwSpuMapper.deleteFwSpuById(id);
    }

    @Override
    public void checkTops() {
        // 产品销量，每天递增 10以内随机数
        List<FwSpu> list = list();
        for (FwSpu fwSpu : list) {
            fwSpu.setTops(fwSpu.getTops() + RandomUtils.getRandom(10));
            fwSpu.updateById();
        }
    }
}
