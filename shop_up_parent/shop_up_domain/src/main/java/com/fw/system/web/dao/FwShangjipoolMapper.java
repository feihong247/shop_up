package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwShangjipool;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商甲池表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwShangjipoolMapper extends BaseMapper<FwShangjipool> {

}
