package com.fw.system.web.service.impl;

import com.fw.system.web.model.entity.FwUljoin;
import com.fw.system.web.dao.FwUljoinMapper;
import com.fw.system.web.service.IFwUljoinService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户和身份中间表 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Service
public class FwUljoinServiceImpl extends ServiceImpl<FwUljoinMapper, FwUljoin> implements IFwUljoinService {


    @Autowired
    private FwUljoinMapper uljoinMapper;
    @Override
    public int countVip(String userId) {

        return uljoinMapper.countVip(userId);
    }
}
