package com.fw.system.web.model.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Effect
 * @Author 姚自强
 * @Date 2021/6/10
 **/
@Data
@ApiModel("提交订单FormForm")
public class ReadyOrderFormForm {
    private List<ReadyOrderForm> formList;

    @ApiModelProperty("是否购物车提交，0否1是")
    private Integer isCar = 0;

    @ApiModelProperty("竞猜订单Id（房间号）")
    private String guessOrderId = "";

    @ApiModelProperty("竞猜Id（房间号）")
    private String guessId = "";

    @ApiModelProperty("房间人数")
    private Integer peopleNumber = 2;
}
