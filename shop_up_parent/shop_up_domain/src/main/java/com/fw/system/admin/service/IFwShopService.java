package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwShop;

/**
 * 商铺Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwShopService extends IService<FwShop>
{
    /**
     * 查询商铺
     * 
     * @param id 商铺ID
     * @return 商铺
     */
    public FwShop selectFwShopById(String id);

    /**
     * 查询商铺列表
     * 
     * @param fwShop 商铺
     * @return 商铺集合
     */
    public List<FwShop> selectFwShopList(FwShop fwShop);

    /**
     * 新增商铺
     * 
     * @param fwShop 商铺
     * @return 结果
     */
    public int insertFwShop(FwShop fwShop);

    /**
     * 修改商铺
     * 
     * @param fwShop 商铺
     * @return 结果
     */
    public int updateFwShop(FwShop fwShop);

    /**
     * 批量删除商铺
     * 
     * @param ids 需要删除的商铺ID
     * @return 结果
     */
    public int deleteFwShopByIds(String[] ids);

    /**
     * 删除商铺信息
     * 
     * @param id 商铺ID
     * @return 结果
     */
    public int deleteFwShopById(String id);

    void shopPublishSpu(String shopId, String flag);
}
