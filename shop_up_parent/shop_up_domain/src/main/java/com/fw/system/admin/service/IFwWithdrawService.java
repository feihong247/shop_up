package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwWithdraw;

/**
 * 用户提现Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwWithdrawService extends IService<FwWithdraw>
{
    /**
     * 查询用户提现
     * 
     * @param id 用户提现ID
     * @return 用户提现
     */
    public FwWithdraw selectFwWithdrawById(String id);

    /**
     * 查询用户提现列表
     * 
     * @param fwWithdraw 用户提现
     * @return 用户提现集合
     */
    public List<FwWithdraw> selectFwWithdrawList(FwWithdraw fwWithdraw);

    /**
     * 新增用户提现
     * 
     * @param fwWithdraw 用户提现
     * @return 结果
     */
    public int insertFwWithdraw(FwWithdraw fwWithdraw);

    /**
     * 修改用户提现
     * 
     * @param fwWithdraw 用户提现
     * @return 结果
     */
    public int updateFwWithdraw(FwWithdraw fwWithdraw);

    /**
     * 批量删除用户提现
     * 
     * @param ids 需要删除的用户提现ID
     * @return 结果
     */
    public int deleteFwWithdrawByIds(String[] ids);

    /**
     * 删除用户提现信息
     * 
     * @param id 用户提现ID
     * @return 结果
     */
    public int deleteFwWithdrawById(String id);
}
