package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwReadPackageTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 红包任务类型列Mapper接口
 * 
 * @author yanwei
 * @date 2021-10-12
 */
public interface FwReadPackageTaskMapper extends BaseMapper<FwReadPackageTask>
{
    /**
     * 查询红包任务类型列
     * 
     * @param id 红包任务类型列ID
     * @return 红包任务类型列
     */
    public FwReadPackageTask selectFwReadPackageTaskById(String id);

    /**
     * 查询红包任务类型列列表
     * 
     * @param fwReadPackageTask 红包任务类型列
     * @return 红包任务类型列集合
     */
    public List<FwReadPackageTask> selectFwReadPackageTaskList(FwReadPackageTask fwReadPackageTask);

    /**
     * 新增红包任务类型列
     * 
     * @param fwReadPackageTask 红包任务类型列
     * @return 结果
     */
    public int insertFwReadPackageTask(FwReadPackageTask fwReadPackageTask);

    /**
     * 修改红包任务类型列
     * 
     * @param fwReadPackageTask 红包任务类型列
     * @return 结果
     */
    public int updateFwReadPackageTask(FwReadPackageTask fwReadPackageTask);

    /**
     * 删除红包任务类型列
     * 
     * @param id 红包任务类型列ID
     * @return 结果
     */
    public int deleteFwReadPackageTaskById(String id);

    /**
     * 批量删除红包任务类型列
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwReadPackageTaskByIds(String[] ids);
}
