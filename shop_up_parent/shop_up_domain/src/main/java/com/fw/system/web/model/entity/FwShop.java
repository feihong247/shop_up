package com.fw.system.web.model.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fw.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 商铺表
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwShop extends Model<FwShop> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("id")
    private String id;

    /**
     * 编号_平台管理的用户ID
     */
    @TableField("user_id")
    private String userId;

    /**
     * 商铺线上质押多少商甲
     */
    @TableField("shangjia")
    private BigDecimal shangjia;

    /**
     * 商铺线下质押多少商甲
     */
    @TableField("offline_shangjia")
    private BigDecimal offlineShangjia;


    /**
     * 经营类型 存储 类型ID
     */
    @TableField("operating_name")
    private String operatingName;

    /**
     * 管理人名称
     */
    @TableField("manager_name")
    private String managerName;

    /**
     * 管理人电话，数据需要脱敏
     */
    @TableField("manager_phone")
    private String managerPhone;

    /**
     * 系统商户编号
     */
    @TableField("shop_host")
    private String shopHost;

    /**
     * 商铺名称
     */
    @TableField("shop_name")
    private String shopName;

    /**
     * 商铺简介
     */
    @TableField("shop_info")
    private String shopInfo;

    /**
     * 商铺营业执照图片
     */
    @TableField("shop_image")
    private String shopImage;

    /**
     * 数据设定-范围m为单位
     */
    @TableField("data_range")
    private String dataRange;

    /**
     * 数据设定-性别,0=女，1=男，2=不限
     */
    @TableField("data_sex")
    private Integer dataSex;

    /**
     * 数据设定-年龄设定
     */
    @TableField("data_age")
    private String dataAge;

    /**
     * 数据设定-收入设定
     */
    @TableField("data_income")
    private String dataIncome;

    /**
     * 商铺状态,0-待审核，1-审核通过，2-审核失败，驳回
     */
    @TableField("status")
    private Integer status;

    /**
     * 商铺审核状态为2时，次字段生效
     */
    @TableField("turndown")
    private String turndown;

    /**
     * 线下消费
     */
    @TableField("line_customer")
    private BigDecimal lineCustomer;

    /**
     * 营业时间
     */
    @TableField("operating_time")
    private  String operatingTime;


    /**
     * 商铺电话
     */
    @TableField("shop_mobile")
    private String shopMobile;

    /**
     * 商铺公告
     */
    @TableField("shop_notice")
    private String shopNotice;

    /**
     * 商铺公告
     */
    @TableField("shop_logo")
    private String shopLogo;

    /**
     * 商家  线下支付图谱
     */
    @TableField("shop_pay")
    private String shopPay;





    /**
     * 商铺 人均消费
     */
    @TableField("shop_svg")
    private String shopSvg;

    /**
     * 商铺评分
     */
    @TableField("shop_score")
    private Integer shopScore;

    /**
     * 商铺封面
     */
    @TableField("shop_cover")
    private String shopCover;


    /**
     * 商铺客服二维码
     */
    @TableField("shop_qr")
    private String shopQr;


    /**
     * 创建人
     */
    @TableField("create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @TableField("update_by")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;

    /** 纬度 */
    @ApiModelProperty(value = "纬度")
    @Excel(name = "纬度")
    @TableField(value = "latitude",exist = false)
    private BigDecimal latitude;

    /** 经度 */
    @ApiModelProperty(value = "经度")
    @Excel(name = "经度")
    @TableField(value = "longitude",exist = false)
    private BigDecimal longitude;

    /**
     * 商铺线上质押多少商甲
     */
    @ApiModelProperty(value = "身份证-正面",required = true)
    @TableField("real_just")
    private String realJust;

    /**
     * 商铺线下质押多少商甲
     */
    @ApiModelProperty(value = "身份证-反面",required = true)
    @TableField("real_back")
    private String realBack;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }


    public void createD(String createId){
        this.createBy = createId;
        this.createTime = LocalDateTime.now();
        if (this.createTime != null)
            this.updateTime = this.createTime;
        this.updateBy = this.createBy;
    }

    public void updateD(String updateId){
        this.updateBy = updateId;
        this.updateTime = LocalDateTime.now();
    }
}
