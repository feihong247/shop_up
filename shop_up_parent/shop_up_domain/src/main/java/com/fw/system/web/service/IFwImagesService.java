package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwImages;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品图片列 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface IFwImagesService extends IService<FwImages> {

}
