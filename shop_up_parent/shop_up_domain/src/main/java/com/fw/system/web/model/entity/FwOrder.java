package com.fw.system.web.model.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fw.annotation.Excel;
import com.fw.system.web.model.vo.AddrsVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel("订单")
public class FwOrder extends Model<FwOrder> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("id")
    @ApiModelProperty("订单号")
    private String id;

    /**
     * 用户编号
     */
    @TableField("user_id")
    @ApiModelProperty("用户编号")
    private String userId;

    /**
     * 是否是竞猜订单,0=否，1=是
     */
    @TableField("is_quiz")
    @ApiModelProperty("是否是竞猜订单,0=否，1=是")
    private Integer isQuiz;

    /**
     * 订单总金额
     */
    @TableField("total_price")
    @ApiModelProperty("订单总金额")
    private BigDecimal totalPrice;

    /**
     * 订单应付（不计算优惠）
     */
    @TableField("total_full_price")
    @ApiModelProperty("订单应付（不计算优惠）")
    private BigDecimal totalFullPrice;

    /**
     * 订单状态,-1全部，0=待付款，1=待发货，2=待收货，3=待评价，4=待退货,5=退货/退款，6确认收货，7申请售后',
     */
    @TableField("status")
    @ApiModelProperty("订单状态,-1全部，0=待付款，1=待发货，2=待收货，3=待评价，4=待退货,5=退货/退款，6确认收货，7申请售后',")
    private Integer status;

    /**
     * 订单运费
     */
    @TableField("shopping")
    @ApiModelProperty("订单运费")
    private BigDecimal shopping;

    /**
     * 订单消证
     */
    @TableField("disappear")
    @ApiModelProperty("订单消证")
    private BigDecimal disappear;

    /**
     * 商户编号 提交订单，多商户进行分裂即可
     */
    @TableField("shop_id")
    @ApiModelProperty("商户编号 提交订单，多商户进行分裂即可")
    private String shopId;

    @TableField(exist = false)
    @ApiModelProperty("商户对象")
    private FwShop fwShop;

    /**
     * 运单公司名称
     */
    @TableField("courier_name")
    @ApiModelProperty("运单公司名称")
    private String courierName;

    /**
     * 运单编号
     */
    @TableField("courier_number")
    @ApiModelProperty("运单编号")
    private String courierNumber;

    /**
     * 订单系统编号
     */
    @TableField("out_trade_no")
    @ApiModelProperty("订单系统编号")
    private String outTradeNo;

    /**
     * 支付类型,0=支付宝，1=微信
     */
    @TableField("pay_type")
    @ApiModelProperty("支付类型,0=支付宝，1=微信")
    private Integer payType;

    /**
     * 创建人
     */
    @TableField("create_by")
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @TableField("update_by")
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    @ApiModelProperty("更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /** 客服二维码 */
    @TableField("server_qr")
    @ApiModelProperty("客服二维码")
    private String serverQr;

    /** 大订单id */
    @TableField("parent_order_id")
    @ApiModelProperty("大订单id")
    private String parentOrderId;

    /** 商品ID */
    @TableField("spu_id")
    @ApiModelProperty("商品ID")
    private String spuId;

    /** 规格ID */
    @TableField("sku_id")
    @ApiModelProperty("规格ID")
    private String skuId;

    @TableField(exist = false)
    @ApiModelProperty("商品对象")
    private FwSpu fwSpu;

    @TableField(exist = false)
    @ApiModelProperty("规格对象")
    private FwSku fwSku;

    @TableField("spu_number")
    @ApiModelProperty("购买商品数量")
    private Integer spuNumber;

    /** 地址Id */
    @TableField("addr_id")
    @ApiModelProperty("地址Id")
    private String addrId;

    @TableField(exist = false)
    @ApiModelProperty("地址对象")
    private AddrsVo fwAddr;

    @TableField("addr_string")
    @ApiModelProperty("地址字符串，&-fw-& 连接")
    private String addrString;

    /** 联系人姓名 */
    @ApiModelProperty("收货人姓名")
    @Excel(name = "收货人姓名")
    @TableField(exist = false)
    private String contactName;

    /** 联系人电话 */
    @ApiModelProperty("收货人电话")
    @Excel(name = "收货人电话")
    @TableField(exist = false)
    private String contactPhone;

    /** 收货地址 */
    @ApiModelProperty("收货地址")
    @TableField(exist = false)
    @Excel(name = "收货地址")
    private String addrInfo;

    /** 是否评价,0=否，1=是 */
    @ApiModelProperty(value = "是否评价,0=否，1=是")
    @Excel(name = "是否评价,0=否，1=是")
    @TableField("is_appraise")
    private Integer isAppraise;

    /** 售后状态（1申请，2同意，3拒绝） */
    @ApiModelProperty(value = "售后状态（1申请，2同意，3拒绝）")
    @Excel(name = "售后状态（1申请，2同意，3拒绝）")
    @TableField("after_sale_state")
    private Integer afterSaleState;

    /** 售后备注 */
    @ApiModelProperty(value = "售后备注")
    @Excel(name = "售后备注")
    @TableField("after_sale_remark")
    private String afterSaleRemark;

    /**
     * 订单满减
     */
    @TableField("full_discount")
    @ApiModelProperty("订单满减")
    @Excel(name = "订单满减")
    private BigDecimal fullDiscount;

    /** 删除标志（==1删除） */
    @ApiModelProperty(value = "删除标志（==1删除）")
    @TableField("remove_flag")
    private Integer removeFlag;

    @ApiModelProperty(value = "用户名")
    @TableField(exist = false)
    private String userName;

    @ApiModelProperty(value = "手机号")
    @TableField(exist = false)
    private String phone;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
