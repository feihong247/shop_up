package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwOsjoin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 订单与spu中间Mapper接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface FwOsjoinMapper extends BaseMapper<FwOsjoin>
{
    /**
     * 查询订单与spu中间
     * 
     * @param id 订单与spu中间ID
     * @return 订单与spu中间
     */
    public FwOsjoin selectFwOsjoinById(String id);

    /**
     * 查询订单与spu中间列表
     * 
     * @param fwOsjoin 订单与spu中间
     * @return 订单与spu中间集合
     */
    public List<FwOsjoin> selectFwOsjoinList(FwOsjoin fwOsjoin);

    /**
     * 新增订单与spu中间
     * 
     * @param fwOsjoin 订单与spu中间
     * @return 结果
     */
    public int insertFwOsjoin(FwOsjoin fwOsjoin);

    /**
     * 修改订单与spu中间
     * 
     * @param fwOsjoin 订单与spu中间
     * @return 结果
     */
    public int updateFwOsjoin(FwOsjoin fwOsjoin);

    /**
     * 删除订单与spu中间
     * 
     * @param id 订单与spu中间ID
     * @return 结果
     */
    public int deleteFwOsjoinById(String id);

    /**
     * 批量删除订单与spu中间
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwOsjoinByIds(String[] ids);
}
