package com.fw.system.web.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 商家经营平台分类
 * </p>
 *
 * @author  
 * @since 2021-05-19
 */
@Data
public class OperatingVo implements Serializable {


    @ApiModelProperty("编号")
    private String oid;

    @ApiModelProperty("经营类型名称")
    private String operatingName;

    /**
     * 图标照片
     */
    @ApiModelProperty("图标照片")
    private String iconUrl;


}
