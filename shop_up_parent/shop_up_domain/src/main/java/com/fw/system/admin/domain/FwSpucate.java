package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 商品类目对象 fw_spucate
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_spucate")
@ApiModel(value="商品类目", description="商品类目")
public class FwSpucate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 类目名称 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "类目名称")
    @TableField("cate_name")
    private String cateName;

    /** 类目icon地址 */
    @ApiModelProperty(value = "类目名称")
    @Excel(name = "类目icon地址")
    @TableField("cate_url")
    private String cateUrl;

    /** 上级类目 -1代表顶级 */
    @ApiModelProperty(value = "类目icon地址")
    @Excel(name = "上级类目 -1代表顶级")
    @TableField("parent_id")
    private String parentId;

    /** 子类目 */
    @TableField(exist = false)
    private List<FwSpucate> spucateList;

}
