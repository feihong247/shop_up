package com.fw.system.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.system.web.dao.FwShangjiaReleaseLogMapper;
import com.fw.system.web.model.entity.FwShangjiaReleaseLog;
import com.fw.system.web.service.IFwShangjiaReleaseLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 技术商甲管理商甲推广商甲释放明细表 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
@Service
public class FwShangjiaReleaseLogServiceImpl extends ServiceImpl<FwShangjiaReleaseLogMapper, FwShangjiaReleaseLog> implements IFwShangjiaReleaseLogService {

}
