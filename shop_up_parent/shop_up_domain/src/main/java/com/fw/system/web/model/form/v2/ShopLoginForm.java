package com.fw.system.web.model.form.v2;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@ApiModel("商户端登录 表单提交")
public class ShopLoginForm implements Serializable {

    @ApiModelProperty(value = "登录账号",required = true)
    @NotBlank(message = "登录账号不可为空")
    private String phone;

    @ApiModelProperty(value = "登录密码，支持app登录 和 后管商户端密码",required = true)
    @NotBlank(message = "登录密码不可为空")
    private String passWord;
}
