package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwMsg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户消息Mapper接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface FwMsgMapper extends BaseMapper<FwMsg>
{
    /**
     * 查询用户消息
     * 
     * @param id 用户消息ID
     * @return 用户消息
     */
    public FwMsg selectFwMsgById(String id);

    /**
     * 查询用户消息列表
     * 
     * @param fwMsg 用户消息
     * @return 用户消息集合
     */
    public List<FwMsg> selectFwMsgList(FwMsg fwMsg);

    /**
     * 新增用户消息
     * 
     * @param fwMsg 用户消息
     * @return 结果
     */
    public int insertFwMsg(FwMsg fwMsg);

    /**
     * 修改用户消息
     * 
     * @param fwMsg 用户消息
     * @return 结果
     */
    public int updateFwMsg(FwMsg fwMsg);

    /**
     * 删除用户消息
     * 
     * @param id 用户消息ID
     * @return 结果
     */
    public int deleteFwMsgById(String id);

    /**
     * 批量删除用户消息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwMsgByIds(String[] ids);
}
