package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwWithdrawShangjia;

import java.math.BigDecimal;

/**
 * <p>
 * 积分销毁商甲表 服务类
 * </p>
 *
 * @author  
 * @since 2021-07-08
 */
public interface IFwWithdrawShangjiaService extends IService<FwWithdrawShangjia> {

    void updateIntegral(BigDecimal integral,String userId);
}
