package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwEvaluation;
import com.fw.system.web.model.vo.EvaluationVo;

import java.util.List;

/**
 * <p>
 * 商品评价 Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-05-24
 */
public interface FwEvaluationMapper extends BaseMapper<FwEvaluation> {
    /**
     * 查询商品评价
     *
     * @param id 商品评价ID
     * @return 商品评价
     */
    public EvaluationVo selectFwEvaluationById(String id);

    /**
     * 查询商品评价列表
     *
     * @param fwEvaluation 商品评价
     * @return 商品评价集合
     */
    public List<EvaluationVo> selectFwEvaluationList(FwEvaluation fwEvaluation);

    /**
     * 新增商品评价
     *
     * @param fwEvaluation 商品评价
     * @return 结果
     */
    public int insertFwEvaluation(FwEvaluation fwEvaluation);

    /**
     * 修改商品评价
     *
     * @param fwEvaluation 商品评价
     * @return 结果
     */
    public int updateFwEvaluation(FwEvaluation fwEvaluation);

    /**
     * 删除商品评价
     *
     * @param id 商品评价ID
     * @return 结果
     */
    public int deleteFwEvaluationById(String id);

    /**
     * 批量删除商品评价
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwEvaluationByIds(String[] ids);
}
