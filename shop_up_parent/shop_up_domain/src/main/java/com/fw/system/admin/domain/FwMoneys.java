package com.fw.system.admin.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 用户钱包对象 fw_moneys
 *
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_moneys")
@ApiModel(value="用户钱包", description="用户钱包表")
public class FwMoneys extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 用户编号 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "用户编号")
    @TableField("user_id")
    private String userId;

    /** 消证_厘为单位 */
    @ApiModelProperty(value = "用户编号")
    @Excel(name = "消证_厘为单位")
    @TableField("disappear")
    private BigDecimal disappear;

    /** 积分_厘为单位 */
    @ApiModelProperty(value = "消证_厘为单位")
    @Excel(name = "积分_厘为单位")
    @TableField("integral")
    private BigDecimal integral;

    /** 商甲_厘为单位 */
    @ApiModelProperty(value = "积分_厘为单位")
    @Excel(name = "商甲_厘为单位")
    @TableField("shang_jia")
    private BigDecimal shangJia;

    /**
     * 持有技术商甲
     */
    @TableField("tec_shangjia")
    @ApiModelProperty(value = "持有技术商甲")
    @Excel(name = "持有技术商甲")
    private BigDecimal tecShangjia;

    /**
     * 持有管理商甲
     */
    @TableField("man_shangjia")
    @ApiModelProperty(value = "持有管理商甲")
    @Excel(name = "持有管理商甲")
    private BigDecimal manShangjia;

    /**
     * 用户名称
     */
    @TableField(exist = false)
    private String userName;

    /**
     * 用户手机号
     */
    @TableField(exist = false)
    private String phone;

    /**
     * 用户头像
     */
    @TableField(exist = false)
    private String headImage;


}
