package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwReadPackageMapper;
import com.fw.system.admin.domain.FwReadPackage;
import com.fw.system.admin.service.IFwReadPackageService;

/**
 * 红包类型Service业务层处理
 * 
 * @author yanwei
 * @date 2021-10-12
 */
@Service
public class FwReadPackageServiceImpl extends ServiceImpl<FwReadPackageMapper, FwReadPackage> implements IFwReadPackageService
{
    @Autowired
    private FwReadPackageMapper fwReadPackageMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询红包类型
     * 
     * @param id 红包类型ID
     * @return 红包类型
     */
    @Override
    public FwReadPackage selectFwReadPackageById(String id)
    {
        return fwReadPackageMapper.selectFwReadPackageById(id);
    }

    /**
     * 查询红包类型列表
     * 
     * @param fwReadPackage 红包类型
     * @return 红包类型
     */
    @Override
    public List<FwReadPackage> selectFwReadPackageList(FwReadPackage fwReadPackage)
    {
        return fwReadPackageMapper.selectFwReadPackageList(fwReadPackage);
    }

    /**
     * 新增红包类型
     * 
     * @param fwReadPackage 红包类型
     * @return 结果
     */
    @Override
    public int insertFwReadPackage(FwReadPackage fwReadPackage)
    {
        fwReadPackage.setCreateTime(DateUtils.getNowDate());
        fwReadPackage.setId(idXD.nextId());
        return fwReadPackageMapper.insertFwReadPackage(fwReadPackage);
    }

    /**
     * 修改红包类型
     * 
     * @param fwReadPackage 红包类型
     * @return 结果
     */
    @Override
    public int updateFwReadPackage(FwReadPackage fwReadPackage)
    {
        return fwReadPackageMapper.updateFwReadPackage(fwReadPackage);
    }

    /**
     * 批量删除红包类型
     * 
     * @param ids 需要删除的红包类型ID
     * @return 结果
     */
    @Override
    public int deleteFwReadPackageByIds(String[] ids)
    {
        return fwReadPackageMapper.deleteFwReadPackageByIds(ids);
    }

    /**
     * 删除红包类型信息
     * 
     * @param id 红包类型ID
     * @return 结果
     */
    @Override
    public int deleteFwReadPackageById(String id)
    {
        return fwReadPackageMapper.deleteFwReadPackageById(id);
    }
}
