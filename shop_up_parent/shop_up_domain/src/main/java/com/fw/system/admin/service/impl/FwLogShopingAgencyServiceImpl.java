package com.fw.system.admin.service.impl;

import java.util.List;

import com.fw.common.IdXD;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwLogShopingAgencyMapper;
import com.fw.system.admin.domain.FwLogShopingAgency;
import com.fw.system.admin.service.IFwLogShopingAgencyService;

/**
 * 用户购买代理日志Service业务层处理
 *
 * @author yanwei
 * @date 2021-06-05
 */
@Service
public class FwLogShopingAgencyServiceImpl extends ServiceImpl<FwLogShopingAgencyMapper, FwLogShopingAgency> implements IFwLogShopingAgencyService
{
    @Autowired
    private FwLogShopingAgencyMapper fwLogShopingAgencyMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询用户购买代理日志
     *
     * @param id 用户购买代理日志ID
     * @return 用户购买代理日志
     */
    @Override
    public FwLogShopingAgency selectFwLogShopingAgencyById(String id)
    {
        return fwLogShopingAgencyMapper.selectFwLogShopingAgencyById(id);
    }

    /**
     * 查询用户购买代理日志列表
     *
     * @param fwLogShopingAgency 用户购买代理日志
     * @return 用户购买代理日志
     */
    @Override
    public List<FwLogShopingAgency> selectFwLogShopingAgencyList(FwLogShopingAgency fwLogShopingAgency)
    {
        return fwLogShopingAgencyMapper.selectFwLogShopingAgencyList(fwLogShopingAgency);
    }

    /**
     * 新增用户购买代理日志
     *
     * @param fwLogShopingAgency 用户购买代理日志
     * @return 结果
     */
    @Override
    public int insertFwLogShopingAgency(FwLogShopingAgency fwLogShopingAgency)
    {
        fwLogShopingAgency.setId(idXD.nextId());
        return fwLogShopingAgencyMapper.insertFwLogShopingAgency(fwLogShopingAgency);
    }

    /**
     * 修改用户购买代理日志
     *
     * @param fwLogShopingAgency 用户购买代理日志
     * @return 结果
     */
    @Override
    public int updateFwLogShopingAgency(FwLogShopingAgency fwLogShopingAgency)
    {
        return fwLogShopingAgencyMapper.updateFwLogShopingAgency(fwLogShopingAgency);
    }

    /**
     * 批量删除用户购买代理日志
     *
     * @param ids 需要删除的用户购买代理日志ID
     * @return 结果
     */
    @Override
    public int deleteFwLogShopingAgencyByIds(String[] ids)
    {
        return fwLogShopingAgencyMapper.deleteFwLogShopingAgencyByIds(ids);
    }

    /**
     * 删除用户购买代理日志信息
     *
     * @param id 用户购买代理日志ID
     * @return 结果
     */
    @Override
    public int deleteFwLogShopingAgencyById(String id)
    {
        return fwLogShopingAgencyMapper.deleteFwLogShopingAgencyById(id);
    }
}
