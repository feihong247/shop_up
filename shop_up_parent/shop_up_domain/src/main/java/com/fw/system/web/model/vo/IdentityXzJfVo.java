package com.fw.system.web.model.vo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 用户代理身份消证积分总量vo封装类
 */
@Data
@Api(tags = "用户代理身份消证积分总量vo封装类")
public class IdentityXzJfVo {

    /**
     * 身份名称
     */
    @ApiModelProperty("身份名称")
    private String identityName;
    /**
     * 身份名称
     */
    @ApiModelProperty("省")
    private String province;
    /**
     * 身份名称
     */
    @ApiModelProperty("市")
    private String city;
    /**
     * 身份名称
     */
    @ApiModelProperty("区/县")
    private String area;

    /**
     * 身份标识
     */
    @ApiModelProperty("身份标识")
    private String sysCode;


    @ApiModelProperty("消证")
    private BigDecimal disappear;

    @ApiModelProperty("积分")
    private BigDecimal integral;


}
