package com.fw.system.web.model.vo.v2;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("订单分析返回VO")
public class OrderAnalyzeVo implements Serializable {

    @ApiModelProperty("成功结单数量")
    private Long overOrderVal;

    @ApiModelProperty("待发货订单")
    private Long waitOrderVal;


    @ApiModelProperty("取消订单")
    private Long cancelOrderVal;

    @ApiModelProperty("大客户统计,已经 排好序了,索引就是序号")
    private List<OrderUserAnalyzeVo> orderUserAnalyzeVos;



}
