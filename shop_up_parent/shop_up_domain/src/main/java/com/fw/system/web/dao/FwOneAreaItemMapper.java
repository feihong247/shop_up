package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwOneAreaItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 一县一品表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-09-29
 */
public interface FwOneAreaItemMapper extends BaseMapper<FwOneAreaItem> {

}
