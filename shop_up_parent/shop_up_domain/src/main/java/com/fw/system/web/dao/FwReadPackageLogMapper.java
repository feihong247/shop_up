package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwReadPackageLog;

/**
 * <p>
 * 红包抽取记录表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-09-03
 */
public interface FwReadPackageLogMapper extends BaseMapper<FwReadPackageLog> {

}
