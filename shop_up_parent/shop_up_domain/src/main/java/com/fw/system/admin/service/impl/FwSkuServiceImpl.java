package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwSkuMapper;
import com.fw.system.admin.domain.FwSku;
import com.fw.system.admin.service.IFwSkuService;

/**
 * 商品sku规格Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwSkuServiceImpl extends ServiceImpl<FwSkuMapper, FwSku> implements IFwSkuService
{
    @Autowired
    private FwSkuMapper fwSkuMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询商品sku规格
     * 
     * @param id 商品sku规格ID
     * @return 商品sku规格
     */
    @Override
    public FwSku selectFwSkuById(String id)
    {
        return fwSkuMapper.selectFwSkuById(id);
    }

    /**
     * 查询商品sku规格列表
     * 
     * @param fwSku 商品sku规格
     * @return 商品sku规格
     */
    @Override
    public List<FwSku> selectFwSkuList(FwSku fwSku)
    {
        return fwSkuMapper.selectFwSkuList(fwSku);
    }

    /**
     * 新增商品sku规格
     * 
     * @param fwSku 商品sku规格
     * @return 结果
     */
    @Override
    public int insertFwSku(FwSku fwSku)
    {
        fwSku.setCreateTime(DateUtils.getNowDate());
        fwSku.setId(idXD.nextId());
        return fwSkuMapper.insertFwSku(fwSku);
    }

    /**
     * 修改商品sku规格
     * 
     * @param fwSku 商品sku规格
     * @return 结果
     */
    @Override
    public int updateFwSku(FwSku fwSku)
    {
        fwSku.setUpdateTime(DateUtils.getNowDate());
        return fwSkuMapper.updateFwSku(fwSku);
    }

    /**
     * 批量删除商品sku规格
     * 
     * @param ids 需要删除的商品sku规格ID
     * @return 结果
     */
    @Override
    public int deleteFwSkuByIds(String[] ids)
    {
        return fwSkuMapper.deleteFwSkuByIds(ids);
    }

    /**
     * 删除商品sku规格信息
     * 
     * @param id 商品sku规格ID
     * @return 结果
     */
    @Override
    public int deleteFwSkuById(String id)
    {
        return fwSkuMapper.deleteFwSkuById(id);
    }
}
