package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwAddrs;
import com.fw.system.admin.domain.vo.AddrsVo;

/**
 * 用户收货地址Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwAddrsService extends IService<FwAddrs>
{
    /**
     * 查询用户收货地址
     * 
     * @param id 用户收货地址ID
     * @return 用户收货地址
     */
    public FwAddrs selectFwAddrsById(String id);

    /**
     * 查询用户收货地址列表
     * 
     * @param fwAddrs 用户收货地址
     * @return 用户收货地址集合
     */
    public List<FwAddrs> selectFwAddrsList(FwAddrs fwAddrs);

    /**
     * 新增用户收货地址
     * 
     * @param fwAddrs 用户收货地址
     * @return 结果
     */
    public int insertFwAddrs(FwAddrs fwAddrs);

    /**
     * 修改用户收货地址
     * 
     * @param fwAddrs 用户收货地址
     * @return 结果
     */
    public int updateFwAddrs(FwAddrs fwAddrs);

    /**
     * 批量删除用户收货地址
     * 
     * @param ids 需要删除的用户收货地址ID
     * @return 结果
     */
    public int deleteFwAddrsByIds(String[] ids);

    /**
     * 删除用户收货地址信息
     * 
     * @param id 用户收货地址ID
     * @return 结果
     */
    public int deleteFwAddrsById(String id);

    /**
     * @Effect 根据id获取vo
     * @Author 姚
     * @Date 2021/6/11
     **/
    public AddrsVo getVoById(String id);
}
