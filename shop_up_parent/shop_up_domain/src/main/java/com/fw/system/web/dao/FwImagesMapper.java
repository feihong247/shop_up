package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwImages;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品图片列 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwImagesMapper extends BaseMapper<FwImages> {

}
