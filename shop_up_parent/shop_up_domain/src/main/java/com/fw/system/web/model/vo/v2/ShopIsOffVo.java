package com.fw.system.web.model.vo.v2;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("商户是否是线上,还是线下")
public class ShopIsOffVo implements Serializable {


    @ApiModelProperty("如果为TRUE 线上,否则 不是_  两个都为TRUE 则线上，线下 商户")
    private Boolean isOnline;


    @ApiModelProperty("如果为TRUE 线下,否则 不是  两个都为TRUE 则线上，线下 商户")
    private Boolean isOff;
}
