package com.fw.system.comm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.comm.mode.entity.District;
import com.fw.system.comm.mode.vo.DistrictVo;

import java.util.List;

public interface DistrictService extends IService<District> {
    List<DistrictVo> findAll();

    void synchronizeDistrict();

    List<DistrictVo> findPid(Integer pid);

    String codeConversionName(String code);

}
