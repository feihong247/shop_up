package com.fw.system.web.service.impl;

import com.fw.system.web.model.entity.FwBanner;
import com.fw.system.web.dao.FwBannerMapper;
import com.fw.system.web.service.IFwBannerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 首页轮播 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Service
public class FwBannerServiceImpl extends ServiceImpl<FwBannerMapper, FwBanner> implements IFwBannerService {

}
