package com.fw.system.web.model.form;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * 购买代理form
 */
@Data
@Api(tags = "购买代理form")
@ToString
public class ShopIngAgencyForm {
    @ApiModelProperty("代理id,购买代理身份,1:购买区县代理,2:购买市级代理,3:购买省级代理")
    private String id;
    @ApiModelProperty("金额(元)")
    private BigDecimal money;

    @ApiModelProperty("用户编号,前端无需传参")
    private String userId;

    @ApiModelProperty("日志编号,前端无需传参")
    private String logShopIngAgencyId;

    @ApiModelProperty("省")
    private String province;

    @ApiModelProperty("市")
    private String city;

    @ApiModelProperty("区县")
    private String area;
}
