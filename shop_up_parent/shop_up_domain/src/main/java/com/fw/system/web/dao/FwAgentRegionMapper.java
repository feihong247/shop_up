package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwAgentRegion;

/**
 * <p>
 * 代理区域表 Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-06-08
 */
public interface FwAgentRegionMapper extends BaseMapper<FwAgentRegion> {

}
