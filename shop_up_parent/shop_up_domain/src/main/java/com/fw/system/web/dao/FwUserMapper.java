package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
public interface FwUserMapper extends BaseMapper<FwUser> {

    /**
     * 查询用户
     *
     * @param id 用户ID
     * @return 用户
     */
    public FwUser selectFwUserById(String id);

    /**
     * 查询用户列表
     *
     * @param fwUser 用户
     * @return 用户集合
     */
    public List<FwUser> selectFwUserList(FwUser fwUser);

    /**
     * 新增用户
     *
     * @param fwUser 用户
     * @return 结果
     */
    public int insertFwUser(FwUser fwUser);

    /**
     * 修改用户
     *
     * @param fwUser 用户
     * @return 结果
     */
    public int updateFwUser(FwUser fwUser);

    /**
     * 解绑设备
     *
     * @param fwUser 用户
     * @return 结果
     */
    public int imeiNull(FwUser fwUser);

    /**
     * 删除用户
     *
     * @param id 用户ID
     * @return 结果
     */
    public int deleteFwUserById(String id);

    /**
     * 批量删除用户
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwUserByIds(String[] ids);
}
