package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwAgentRegion;

/**
 * <p>
 * 代理区域表 服务类
 * </p>
 *
 * @author
 * @since 2021-06-08
 */
public interface IFwAgentRegionService extends IService<FwAgentRegion> {

}
