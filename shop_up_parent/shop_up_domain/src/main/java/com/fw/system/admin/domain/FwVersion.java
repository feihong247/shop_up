package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 版本对象 fw_version
 * 
 * @author yanwei
 * @date 2021-10-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_version")
@ApiModel(value="版本", description="版本表")
public class FwVersion extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 版本名称 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "版本名称")
    @TableField("name")
    private String name;

    /** 版本号 */
    @ApiModelProperty(value = "版本名称")
    @Excel(name = "版本号")
    @TableField("version_num")
    private String versionNum;

    /** 版本机型 1安卓 2苹果 3鸿蒙 */
    @ApiModelProperty(value = "版本号")
    @Excel(name = "版本机型 1安卓 2苹果 3鸿蒙")
    @TableField("is_type")
    private Long isType;

    /** 更新说明 */
    @ApiModelProperty(value = "版本机型 1安卓 2苹果 3鸿蒙")
    @Excel(name = "更新说明")
    @TableField("ver_content")
    private String verContent;

    /** 0 需要强制更新 1不需要 */
    @ApiModelProperty(value = "更新说明")
    @Excel(name = "0 需要强制更新 1不需要")
    @TableField("is_state")
    private Long isState;

    /** 下载链接 */
    @ApiModelProperty(value = "0 需要强制更新 1不需要")
    @Excel(name = "下载链接")
    @TableField("upload_uri")
    private String uploadUri;




}
