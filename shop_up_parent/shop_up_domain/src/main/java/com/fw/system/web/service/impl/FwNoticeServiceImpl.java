package com.fw.system.web.service.impl;

import com.fw.system.web.model.entity.FwNotice;
import com.fw.system.web.dao.FwNoticeMapper;
import com.fw.system.web.service.IFwNoticeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统公告&通知 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Service
public class FwNoticeServiceImpl extends ServiceImpl<FwNoticeMapper, FwNotice> implements IFwNoticeService {

}
