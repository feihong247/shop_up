package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwGuessOrder;

import java.util.List;

/**
 * 竞猜订单Service接口
 *
 * @author yanwei
 * @date 2021-06-15
 */
public interface IFwGuessOrderService extends IService<FwGuessOrder> {
    /**
     * 查询竞猜订单
     *
     * @param guessOrderId 竞猜订单ID
     * @return 竞猜订单
     */
    public FwGuessOrder selectFwGuessOrderById(String guessOrderId);

    /**
     * 查询竞猜订单列表
     *
     * @param fwGuessOrder 竞猜订单
     * @return 竞猜订单集合
     */
    public List<FwGuessOrder> selectFwGuessOrderList(FwGuessOrder fwGuessOrder);

    /**
     * 新增竞猜订单
     *
     * @param fwGuessOrder 竞猜订单
     * @return 结果
     */
    public int insertFwGuessOrder(FwGuessOrder fwGuessOrder);

    /**
     * 修改竞猜订单
     *
     * @param fwGuessOrder 竞猜订单
     * @return 结果
     */
    public int updateFwGuessOrder(FwGuessOrder fwGuessOrder);

    /**
     * 批量删除竞猜订单
     *
     * @param guessOrderIds 需要删除的竞猜订单ID
     * @return 结果
     */
    public int deleteFwGuessOrderByIds(String[] guessOrderIds);

    /**
     * 删除竞猜订单信息
     *
     * @param guessOrderId 竞猜订单ID
     * @return 结果
     */
    public int deleteFwGuessOrderById(String guessOrderId);
}
