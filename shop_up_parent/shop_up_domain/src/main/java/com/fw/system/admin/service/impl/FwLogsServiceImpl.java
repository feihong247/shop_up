package com.fw.system.admin.service.impl;

import java.util.Date;
import java.util.List;

import com.fw.common.Builder;
import com.fw.common.IdXD;
import com.fw.enums.LogsModelEnum;
import com.fw.enums.LogsTypeEnum;
import com.fw.system.admin.domain.vo.DisappearLogsVo;
import com.fw.system.admin.domain.vo.UserLogsDisappearCountVo;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwLogsMapper;
import com.fw.system.admin.domain.FwLogs;
import com.fw.system.admin.service.IFwLogsService;

/**
 * 业务日志Service业务层处理
 *
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwLogsServiceImpl extends ServiceImpl<FwLogsMapper, FwLogs> implements IFwLogsService
{
    @Autowired
    private FwLogsMapper fwLogsMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询业务日志
     *
     * @param id 业务日志ID
     * @return 业务日志
     */
    @Override
    public FwLogs selectFwLogsById(String id)
    {
        return fwLogsMapper.selectFwLogsById(id);
    }

    /**
     * 查询业务日志列表
     *
     * @param fwLogs 业务日志
     * @return 业务日志
     */
    @Override
    public List<FwLogs> selectFwLogsList(FwLogs fwLogs)
    {
        return fwLogsMapper.selectFwLogsList(fwLogs);
    }

    /**
     * 新增业务日志
     *
     * @param fwLogs 业务日志
     * @return 结果
     */
    @Override
    public int insertFwLogs(FwLogs fwLogs)
    {
        fwLogs.setCreateTime(DateUtils.getNowDate());
        fwLogs.setId(idXD.nextId());
        return fwLogsMapper.insertFwLogs(fwLogs);
    }

    /**
     * 修改业务日志
     *
     * @param fwLogs 业务日志
     * @return 结果
     */
    @Override
    public int updateFwLogs(FwLogs fwLogs)
    {
        fwLogs.setUpdateTime(DateUtils.getNowDate());
        return fwLogsMapper.updateFwLogs(fwLogs);
    }

    /**
     * 批量删除业务日志
     *
     * @param ids 需要删除的业务日志ID
     * @return 结果
     */
    @Override
    public int deleteFwLogsByIds(String[] ids)
    {
        return fwLogsMapper.deleteFwLogsByIds(ids);
    }

    /**
     * 删除业务日志信息
     *
     * @param id 业务日志ID
     * @return 结果
     */
    @Override
    public int deleteFwLogsById(String id)
    {
        return fwLogsMapper.deleteFwLogsById(id);
    }

    @Override
    public void saveLogs(String createId, String buiId, String isType, String modelName, String jsonTxt, String formInfo) {
        FwLogs fwLogs = Builder.<FwLogs>of(FwLogs::new).build();
        fwLogs.setId(idXD.nextId());
        fwLogs.createD(createId);
        fwLogs.setCreateBy(createId);
        fwLogs.setCreateTime(new Date());
        fwLogs.setBuiId(buiId);
        fwLogs.setIsType(isType);
        fwLogs.setJsonTxt(jsonTxt);
        fwLogs.setFormInfo(formInfo);
        fwLogs.setModelName(modelName);
        save(fwLogs);
    }

    @Override
    public void saveLogs(String createId, String buiId, LogsTypeEnum logsTypeEnum, LogsModelEnum logsModel, String jsonTxt, String formInfo) {
        saveLogs(createId, buiId, logsTypeEnum.getTypeName(), logsModel.getModelName(), jsonTxt, formInfo);
    }

    @Override
    public List<FwLogs> getShangJiaYearTotal() {
        return fwLogsMapper.getShangJiaYearTotal();
    }

    @Override
    public List<FwLogs> getShangJiaMonthTotal() {
        return fwLogsMapper.getShangJiaMonthTotal();
    }

    @Override
    public List<FwLogs> getShangJiaDayTotal() {
        return fwLogsMapper.getShangJiaDayTotal();
    }

    @Override
    public List<FwLogs> getShangJiaLtOneDay(String userId) {
        return fwLogsMapper.getShangJiaLtOneDay(userId);
    }

    @Override
    public List<UserLogsDisappearCountVo> userLogsDisappearCount(FwLogs fwLogs) {
        return fwLogsMapper.userLogsDisappearCount(fwLogs);
    }

    @Override
    public List<FwLogs> userLogsDisappearInfos(FwLogs fwLogs) {
        return fwLogsMapper.userLogsDisappearInfos(fwLogs);
    }

    @Override
    public List<UserLogsDisappearCountVo> userLogsIntegralCount(FwLogs fwLogs) {
        return fwLogsMapper.userLogsIntegralCount(fwLogs);
    }

    @Override
    public List<FwLogs> userLogsIntegralInfos(FwLogs fwLogs) {
        return fwLogsMapper.userLogsIntegralInfos(fwLogs);
    }

    @Override
    public Double toDayUserLogsIntegralCount(FwLogs fwLogs) {
        return fwLogsMapper.toDayUserLogsIntegralCount(fwLogs);
    }

    @Override
    public Double toDayUserLogsDisappearCount(FwLogs fwLogs) {
        return fwLogsMapper.toDayUserLogsDisappearCount(fwLogs);
    }

    @Override
    public List<DisappearLogsVo> selectCommLogsList(FwLogs fwLogs) {
        return fwLogsMapper.selectCommLogsList(fwLogs);
    }
    @Override
    public List<DisappearLogsVo> selectCommLogsInList(FwLogs fwLogs) {
        return fwLogsMapper.selectCommLogsInList(fwLogs);
    }

}
