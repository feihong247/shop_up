package com.fw.system.web.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class WithdrawVo {
    @ApiModelProperty("可用积分")
    private BigDecimal integral;

    @ApiModelProperty("手续费比例,百分之~")
    private BigDecimal poundage;

}
