package com.fw.system.web.service.impl;

import com.fw.system.web.model.entity.FwSpucate;
import com.fw.system.web.dao.FwSpucateMapper;
import com.fw.system.web.service.IFwSpucateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品类目 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Service
public class FwSpucateServiceImpl extends ServiceImpl<FwSpucateMapper, FwSpucate> implements IFwSpucateService {

}
