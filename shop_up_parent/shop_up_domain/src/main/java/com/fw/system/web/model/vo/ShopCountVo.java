package com.fw.system.web.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("代理商户统计待办等数量统计封装")
public class ShopCountVo {

    @ApiModelProperty("待办数量")
    private Integer count;

    @ApiModelProperty("商户数量")
    private Integer shopCount;
}
