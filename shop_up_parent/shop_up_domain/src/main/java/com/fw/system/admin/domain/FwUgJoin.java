package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 用户竞猜中间对象 fw_ug_join
 * 
 * @author yanwei
 * @date 2021-07-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_ug_join")
@ApiModel(value="用户竞猜中间", description="用户竞猜中间表")
public class FwUgJoin extends Model<FwUgJoin>
{
    private static final long serialVersionUID = 1L;

    /** 用户竞猜关联ID */
    @ApiModelProperty(value = "${comment}")
    @TableId("ug_id")
    private String ugId;

    /** 用户ID */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "用户ID")
    @TableField("user_id")
    private String userId;

    /** 竞猜Id */
    @ApiModelProperty(value = "用户ID")
    @Excel(name = "竞猜Id")
    @TableField("guess_id")
    private String guessId;

    /** 猜拳结果（1剪刀，2石头，3布） */
    @ApiModelProperty(value = "竞猜Id")
    @Excel(name = "猜拳结果", readConverterExp = "1=剪刀，2=石头，3=布")
    @TableField("result")
    private Integer result;

    /** 订单号 */
    @ApiModelProperty(value = "猜拳结果")
    @Excel(name = "订单号")
    @TableField("order_id")
    private String orderId;

    /** 是否机器人（0否1是） */
    @ApiModelProperty(value = "订单号")
    @Excel(name = "是否机器人", readConverterExp = "0=否,1=是")
    @TableField("is_auto")
    private Integer isAuto;

    /** 创建人 */
    @TableField("create_by")
    private String createBy;

    /** 创建时间 */
    @TableField("create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间")
    private LocalDateTime createTime;

    /** 更新人 */
    @TableField("update_by")
    private String updateBy;

    /** 更新时间 */
    @TableField("update_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;


}
