package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 商铺环境展示对象 fw_shop_keyword
 * 
 * @author yanwei
 * @date 2021-07-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_shop_keyword")
@ApiModel(value="商铺环境展示", description="商铺环境展示")
public class FwShopKeyword extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 商铺编号 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "商铺编号")
    @TableField("shop_id")
    private String shopId;

    /** 环境标题 */
    @ApiModelProperty(value = "商铺编号")
    @Excel(name = "环境标题")
    @TableField("word_title")
    private String wordTitle;

    /** $column.columnComment */
    @ApiModelProperty(value = "环境标题")
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @TableField("word_show")
    private String wordShow;

    /** 文案内容 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "文案内容")
    @TableField("word_text")
    private String wordText;




}
