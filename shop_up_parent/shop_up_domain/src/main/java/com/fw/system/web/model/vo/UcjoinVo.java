package com.fw.system.web.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 我的收藏中间表
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UcjoinVo extends Model<UcjoinVo> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键 主键
     */
    @TableField(exist = false)
    @ApiModelProperty("主键")
    private String id;

    /**
     * 用户编号
     */
    @TableField(exist = false)
    @ApiModelProperty("用户编号")
    private String userId;

    /**
     * 商品编号
     */
    @TableField(exist = false)
    @ApiModelProperty("商品编号")
    private String itemId;

    /**
     * 创建人 创建人
     */
    @TableField(exist = false)
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间 创建时间
     */
    @TableField(exist = false)
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 更新人 更新人
     */
    @TableField(exist = false)
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间 更新时间
     */
    @TableField(exist = false)
    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;


    /**
     * 商铺编号,自营另外区分
     */
    @TableField(exist = false)
    @ApiModelProperty("商铺编号")
    private String shopId;

    /**
     * 商品标题
     */
    @TableField(exist = false)
    @ApiModelProperty("商品标题")
    private String title;

    /**
     * 商品logo
     */
    @TableField(exist = false)
    @ApiModelProperty("商品logo")
    private String logo;

    /**
     * 商品销量
     */
    @TableField(exist = false)
    @ApiModelProperty("商品销量")
    private Integer tops;

    /**
     * 商品原价格
     */
    @TableField(exist = false)
    @ApiModelProperty("商品原价格")
    private BigDecimal virtualPrice;

    /**
     * 商品现价格
     */
    @TableField(exist = false)
    @ApiModelProperty("商品现价格")
    private BigDecimal realPrice;

    /**
     * 商品利润值,后管看的
     */
    @TableField(exist = false)
    @ApiModelProperty("商品利润值")
    private BigDecimal price;

    /**
     * 商品消证,后管自营人员填写
     */
    @TableField(exist = false)
    @ApiModelProperty("商品消证")
    private BigDecimal disappear;

    /**
     * 商品总库存
     */
    @TableField(exist = false)
    @ApiModelProperty("商品总库存")
    private Integer balance;

    /**
     * 商品详情-富文本形式
     */
    @TableField(exist = false)
    @ApiModelProperty("商品详情富文本形式")
    private String infos;

    /**
     * 商品状态 0=正常，1=下架
     */
    @TableField(exist = false)
    @ApiModelProperty("商品状态 0=正常，1=下架")
    private Boolean status;

    /**
     * 商品运费
     */
    @TableField(exist = false)
    @ApiModelProperty("商品运费")
    private BigDecimal shipping;

    /**
     * 商品满额减运费 需要一套模版表,地区减额，单独商品减额，还是商家总商品减额等。
     */
    @TableField(exist = false)
    @ApiModelProperty("商品满额减运费")
    private BigDecimal topShipping;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
