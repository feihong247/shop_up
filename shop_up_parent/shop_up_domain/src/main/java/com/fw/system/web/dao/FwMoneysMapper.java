package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwMoneys;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户钱包表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwMoneysMapper extends BaseMapper<FwMoneys> {

}
