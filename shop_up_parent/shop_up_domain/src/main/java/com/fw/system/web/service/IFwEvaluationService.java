package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwEvaluation;
import com.fw.system.web.model.form.v2.EvaluationQuery;
import com.fw.system.web.model.vo.EvaluationVo;
import com.fw.system.web.model.vo.v2.EvaluationCountVo;
import com.fw.system.web.model.vo.v2.EvaluationV2Vo;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 * 商品评价 服务类
 * </p>
 *
 * @author
 * @since 2021-05-24
 */
public interface IFwEvaluationService extends IService<FwEvaluation> {
    /**
     * 查询商品评价
     *
     * @param id 商品评价ID
     * @return 商品评价
     */
    public EvaluationVo selectFwEvaluationById(String id);

    /**
     * 查询商品评价列表
     *
     * @param fwEvaluation 商品评价
     * @return 商品评价集合
     */
    public List<EvaluationVo> selectFwEvaluationList(FwEvaluation fwEvaluation);

    /**
     * 新增商品评价
     *
     * @param fwEvaluation 商品评价
     * @return 结果
     */
    public int insertFwEvaluation(FwEvaluation fwEvaluation);

    /**
     * 修改商品评价
     *
     * @param fwEvaluation 商品评价
     * @return 结果
     */
    public int updateFwEvaluation(FwEvaluation fwEvaluation);

    /**
     * 批量删除商品评价
     *
     * @param ids 需要删除的商品评价ID
     * @return 结果
     */
    public int deleteFwEvaluationByIds(String[] ids);

    /**
     * 删除商品评价信息
     *
     * @param id 商品评价ID
     * @return 结果
     */
    public int deleteFwEvaluationById(String id);

    EvaluationCountVo countEva(String shopId);

    PageInfo<EvaluationV2Vo> pageList(EvaluationQuery evaluationQuery,String shopId);
}
