package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwSpucate;

/**
 * 商品类目Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwSpucateService extends IService<FwSpucate>
{
    /**
     * 查询商品类目
     * 
     * @param id 商品类目ID
     * @return 商品类目
     */
    public FwSpucate selectFwSpucateById(String id);

    /**
     * 查询商品类目列表
     * 
     * @param fwSpucate 商品类目
     * @return 商品类目集合
     */
    public List<FwSpucate> selectFwSpucateList(FwSpucate fwSpucate);

    /**
     * 新增商品类目
     * 
     * @param fwSpucate 商品类目
     * @return 结果
     */
    public int insertFwSpucate(FwSpucate fwSpucate);

    /**
     * 修改商品类目
     * 
     * @param fwSpucate 商品类目
     * @return 结果
     */
    public int updateFwSpucate(FwSpucate fwSpucate);

    /**
     * 批量删除商品类目
     * 
     * @param ids 需要删除的商品类目ID
     * @return 结果
     */
    public int deleteFwSpucateByIds(String[] ids);

    /**
     * 删除商品类目信息
     * 
     * @param id 商品类目ID
     * @return 结果
     */
    public int deleteFwSpucateById(String id);
}
