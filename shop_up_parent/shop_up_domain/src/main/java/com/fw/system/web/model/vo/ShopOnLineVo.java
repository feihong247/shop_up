package com.fw.system.web.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 商铺信息vo
 */
@Data
@Api(tags = "数据规则vo")
public class ShopOnLineVo  implements Serializable {

    @ApiModelProperty("经营类型")
    private OperatingVo operatingVo;

    /**
     * 主键
     */
    @ApiModelProperty("商铺主键")
    private String id;

    /**
     * 编号_平台管理的用户ID
     */
    @ApiModelProperty("平台管理的用户主键")
    private String userId;


    /**
     * 管理人名称
     */
    @ApiModelProperty("管理人名称")
    private String managerName;

    /**
     * 管理人电话，数据需要脱敏
     */
    @ApiModelProperty(" 管理人电话")
    private String managerPhone;

    /**
     * 系统商户编号
     */
    @ApiModelProperty("系统商户编号")
    private String shopHost;

    /**
     * 商铺名称
     */
    @ApiModelProperty("商铺名称")
    private String shopName;

    /**
     * 商铺简介
     */
    @ApiModelProperty("商铺简介")
    private String shopInfo;

    /**
     * 商铺营业执照图片
     */
    @ApiModelProperty("商铺营业执照图片")
    private String shopImage;



    /**
     * 商铺状态,0-待审核，1-审核通过，2-审核失败，驳回
     */
    @ApiModelProperty("商铺状态 0-待审核，1-审核通过，2-审核失败，驳回")
    private Integer status;

    /**
     * 商铺审核状态为2时，次字段生效
     */
    @ApiModelProperty("驳回信息 - 状态 2 有效")
    private String turndown;

    /**
     * 营业时间
     */
    @ApiModelProperty("营业时间")
    private  String operatingTime;


    /**
     * 商铺电话
     */
    @ApiModelProperty("商铺电话")
    private String shopMobile;

    /**
     * 商铺公告
     */
    @ApiModelProperty("商铺公告")
    private String shopNotice;

    /**
     * 商铺公告
     */
    @ApiModelProperty("商铺Logo")
    private String shopLogo;

    /**
     * 商家  线下支付图谱
     */
    @ApiModelProperty("线下支付图谱")
    private String shopPay;


    /**
     * 商户个性化设计 用户筛选 - 性别
     */
    @ApiModelProperty(hidden = true)
    private Integer dataSex;

    /**
     * 商户个性化设计 用户筛选 - 收入 - 收支
     */
    @ApiModelProperty(hidden = true)
    private String dataIncome;

    /**
     * 商铺 人均消费
     */
    @ApiModelProperty("人均消费")
    private String shopSvg;

    /**
     * 商铺评分
     */
    @ApiModelProperty("商铺评分")
    private String shopScore;

    /**
     * 商铺封面
     */
    @ApiModelProperty("商铺封面")
    private String shopCover;

    @ApiModelProperty("商铺的经纬度")
    private ShopAddVo shopAddVo;


    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime createTime;



    /**
     * 步行多少米
     */
    @ApiModelProperty("步行多少米")
    private String step;

    /**
     * 时间
     */
    @ApiModelProperty("时间")
    private String stepTime;

    /**
     * 时间
     */
    @ApiModelProperty("次店铺是否被收藏，0 = 未收藏，1 = 收藏")
    private Integer isLike = 0;

}
