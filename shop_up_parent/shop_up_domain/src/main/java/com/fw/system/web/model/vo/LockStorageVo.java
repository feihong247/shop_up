package com.fw.system.web.model.vo;

import com.fw.system.web.model.entity.FwLockStorage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("锁仓明细列表vo")
public class LockStorageVo extends FwLockStorage {

    /**
     * 技术锁仓数量
     */
    @ApiModelProperty("兑换剩余天数")
    private Long remDay;
}
