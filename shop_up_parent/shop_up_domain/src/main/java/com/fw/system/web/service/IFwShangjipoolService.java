package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwShangjipool;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商甲池表 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface IFwShangjipoolService extends IService<FwShangjipool> {
    void destoryShangjia(String userId);
}
