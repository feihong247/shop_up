package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 代理区域表
 * </p>
 *
 * @author
 * @since 2021-06-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwAgentRegion extends Model<FwAgentRegion> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    /**
     * 用户编号
     */
    @TableField("user_id")
    private String userId;

    /**
     * 省
     */
    @TableField("province")
    private String province;

    /**
     * 市
     */
    @TableField("city")
    private String city;

    /**
     * 区/县
     */
    @TableField("area")
    private String area;

    /**
     * 代理身份编号,5:区县代理,6:市级代理,7:省级代理
     */
    @TableField("agent")
    private String agent;


    /**
     * 是否正在使用该地址,1=是，0=否
     */
    @TableField("is_default")
    private Integer isDefault;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
