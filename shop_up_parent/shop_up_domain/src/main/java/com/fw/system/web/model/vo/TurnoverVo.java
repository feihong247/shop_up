package com.fw.system.web.model.vo;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


import java.math.BigDecimal;

/**
 * <p>
 * 恒定增长条件vo,以此表为依据增长商甲
 * </p>
 *
 * @author
 * @since 2021-06-07
 */
@Data
@Api(tags = "恒定增长条件vo")
public class TurnoverVo {


    @ApiModelProperty("id")
    private String id;

    /**
     * 阶段
     */
    @ApiModelProperty("阶段")
    private String phase;

    /**
     * 商甲价格
     */
    @ApiModelProperty("商甲价格")
    private BigDecimal money;

    /**
     * 消费额度
     */
    @ApiModelProperty("消费额度")
    private BigDecimal consumptionQuota;

    /**
     * 增长幅度
     */
    @ApiModelProperty("增长幅度")
    private BigDecimal growth;

    /**
     * 最终商甲价格
     */
    @ApiModelProperty("最终商甲价格")
    private BigDecimal endMoney;

    /**
     * 标记
     */
    @ApiModelProperty("标记")
    private Integer count;


}
