package com.fw.system.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

import com.fw.system.admin.domain.FwLockStorage;

/**
 * 锁仓池Service接口
 *
 * @author yanwei
 * @date 2021-06-28
 */
public interface IFwLockStorageService extends IService<FwLockStorage> {
    /**
     * 查询锁仓池
     *
     * @param id 锁仓池ID
     * @return 锁仓池
     */
    public FwLockStorage selectFwLockStorageById(String id);

    /**
     * 查询锁仓池列表
     *
     * @param fwLockStorage 锁仓池
     * @return 锁仓池集合
     */
    public List<FwLockStorage> selectFwLockStorageList(FwLockStorage fwLockStorage);

    /**
     * 新增锁仓池
     *
     * @param fwLockStorage 锁仓池
     * @return 结果
     */
    public int insertFwLockStorage(FwLockStorage fwLockStorage);

    /**
     * 修改锁仓池
     *
     * @param fwLockStorage 锁仓池
     * @return 结果
     */
    public int updateFwLockStorage(FwLockStorage fwLockStorage);

    /**
     * 批量删除锁仓池
     *
     * @param ids 需要删除的锁仓池ID
     * @return 结果
     */
    public int deleteFwLockStorageByIds(String[] ids);

    /**
     * 删除锁仓池信息
     *
     * @param id 锁仓池ID
     * @return 结果
     */
    public int deleteFwLockStorageById(String id);
}
