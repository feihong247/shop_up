package com.fw.system.web.dao;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwGuess;
import org.springframework.stereotype.Repository;

/**
 * 竞猜Mapper接口
 * 
 * @author yanwei
 * @date 2021-06-23
 */
@Repository
public interface FwGuessMapper extends BaseMapper<FwGuess> {
    /**
     * 查询竞猜
     * 
     * @param guessId 竞猜ID
     * @return 竞猜
     */
    public FwGuess selectFwGuessById(String guessId);

    /**
     * 查询竞猜列表
     * 
     * @param fwGuess 竞猜
     * @return 竞猜集合
     */
    public List<FwGuess> selectFwGuessList(FwGuess fwGuess);

    /**
     * 新增竞猜
     * 
     * @param fwGuess 竞猜
     * @return 结果
     */
    public int insertFwGuess(FwGuess fwGuess);

    /**
     * 修改竞猜
     * 
     * @param fwGuess 竞猜
     * @return 结果
     */
    public int updateFwGuess(FwGuess fwGuess);

    /**
     * 删除竞猜
     * 
     * @param guessId 竞猜ID
     * @return 结果
     */
    public int deleteFwGuessById(String guessId);

    /**
     * 批量删除竞猜
     * 
     * @param guessIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwGuessByIds(String[] guessIds);

    /**
     * @Effect 获取未满房间
     * @Author 姚
     * @Date 2021/7/2
     **/
    List<FwGuess> getNotFullRoom(FwGuess fwGuess);
}
