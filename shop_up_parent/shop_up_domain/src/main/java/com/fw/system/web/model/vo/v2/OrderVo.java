package com.fw.system.web.model.vo.v2;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fw.annotation.Excel;
import com.fw.system.web.model.entity.FwShop;
import com.fw.system.web.model.entity.FwSku;
import com.fw.system.web.model.entity.FwSpu;
import com.fw.system.web.model.vo.AddrsVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@ApiModel("订单返回VO")
public class OrderVo implements Serializable {

    /**
     * 主键
     */
    @ApiModelProperty("订单号")
    private String id;

    /**
     * 用户编号
     */
    @ApiModelProperty("用户编号")
    private String userId;

    /**
     * 是否是竞猜订单,0=否，1=是
     */
    @ApiModelProperty("是否是竞猜订单,0=否，1=是")
    private Integer isQuiz;

    /**
     * 订单总金额
     */
    @ApiModelProperty("订单总金额")
    private BigDecimal totalPrice;

    /**
     * 订单应付（不计算优惠）
     */
    @ApiModelProperty("订单应付（不计算优惠）")
    private BigDecimal totalFullPrice;

    /**
     * 订单状态,-1全部，0=待付款，1=待发货，2=待收货，3=待评价，4=待退货,5=退货/退款，6确认收货，7申请售后',
     */
    @ApiModelProperty("订单状态,-1全部，0=待付款，1=待发货，2=待收货，3=待评价，4=待退货,5=退货/退款，6确认收货，7申请售后',")
    private Integer status;

    /**
     * 订单运费
     */
    @ApiModelProperty("订单运费")
    private BigDecimal shopping;

    /**
     * 订单消证
     */
    @ApiModelProperty("订单消证")
    private BigDecimal disappear;

    /**
     * 商户编号 提交订单，多商户进行分裂即可
     */
    @ApiModelProperty("商户编号 提交订单，多商户进行分裂即可")
    private String shopId;



    /**
     * 运单公司名称
     */
    @ApiModelProperty("运单公司名称")
    private String courierName;

    /**
     * 运单编号
     */
    @ApiModelProperty("运单编号")
    private String courierNumber;

    /**
     * 订单系统编号
     */
    @ApiModelProperty("订单系统编号")
    private String outTradeNo;

    /**
     * 支付类型,0=支付宝，1=微信
     */
    @ApiModelProperty("支付类型,0=支付宝，1=微信")
    private Integer payType;



    /**
     * 创建时间
     */
    @TableField("create_time")
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;




    /** 客服二维码 */
    @TableField("server_qr")
    @ApiModelProperty("客服二维码")
    private String serverQr;



    @ApiModelProperty("商品对象")
    private FwSpu fwSpu;


    @ApiModelProperty("购买商品数量")
    private Integer spuNumber;



    @TableField(exist = false)
    @ApiModelProperty("地址对象")
    private AddrsVo fwAddr;

    @ApiModelProperty("地址字符串，&-fw-& 连接")
    private String addrString;

    /** 联系人姓名 */
    @ApiModelProperty("收货人姓名")
    private String contactName;

    /** 联系人电话 */
    @ApiModelProperty("收货人电话")
    private String contactPhone;

    /** 收货地址 */
    @ApiModelProperty("收货地址")
    private String addrInfo;

    /** 是否评价,0=否，1=是 */
    @ApiModelProperty(value = "是否评价,0=否，1=是")
    private Integer isAppraise;

    /** 售后状态（1申请，2同意，3拒绝） */
    @ApiModelProperty(value = "售后状态（1申请，2同意，3拒绝）")
    private Integer afterSaleState;

    /** 售后备注 */
    @ApiModelProperty(value = "售后备注")
    private String afterSaleRemark;

    /**
     * 订单满减
     */
    @ApiModelProperty("订单满减")
    private BigDecimal fullDiscount;



    @ApiModelProperty(value = "购买人用户名")
    private String userName;

    @ApiModelProperty(value = "购买人手机号")
    private String phone;

}
