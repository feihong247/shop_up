package com.fw.system.web.model.vo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.time.LocalDateTime;

/**
 * @Author:yanwei
 * @Date: 2020/8/5 - 15:39
 */
@ApiModel("示例Example VO_ 返回view")
@Data
public class ExampleVo {
    /**
     *    * 主键ID
     *
     *      */

    @ApiModelProperty("唯一标识")
    private String id;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String exampleName;

    /**
     * 类型
     */
    @ApiModelProperty("类型")
    private Boolean exampleType;

    /**
     * 创建日期
     */
    @ApiModelProperty("创建日期")
    private LocalDateTime createDate;
}
