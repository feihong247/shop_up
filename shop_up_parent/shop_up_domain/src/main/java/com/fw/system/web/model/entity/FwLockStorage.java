package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 锁仓池
 * </p>
 *
 * @author
 * @since 2021-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwLockStorage extends Model<FwLockStorage> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("id")
    private String id;

    /**
     * 用户编号
     */
    @TableField("user_id")
    private String userId;

    /**
     * 锁仓数量
     */
    @TableField("lock_count")
    private BigDecimal lockCount;
    /**
     * 技术锁仓数量
     */
    @TableField("tec_lock_count")
    private BigDecimal tecLockCount;
    /**
     * 管理锁仓数量
     */
    @TableField("man_lock_count")
    private BigDecimal manLockCount;

    /**
     * 锁仓时间
     */
    @TableField("lock_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lockTime;

    /**
     * 标记
     */
    @TableField("flag")
    private String flag;

    /**
     * 备用字段
     */
    @TableField("note")
    private String note;

    /** 上次兑换时间 */
    @ApiModelProperty(value = "上次兑换时间")
    @TableField("release_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime releaseTime;
    @TableField(exist = false)
    private String phone;
    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
