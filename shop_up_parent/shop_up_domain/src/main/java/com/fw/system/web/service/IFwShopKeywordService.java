package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwShopKeyword;

/**
 * <p>
 * 商铺环境展示 服务类
 * </p>
 *
 * @author  
 * @since 2021-07-16
 */
public interface IFwShopKeywordService extends IService<FwShopKeyword> {

}
