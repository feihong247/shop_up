package com.fw.system.admin.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 消证兑换对象 fw_disappear_freed
 *
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_disappear_freed")
@ApiModel(value="消证兑换", description="消证兑换表")
public class FwDisappearFreed extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 用户编号 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "用户编号")
    @TableField("user_id")
    private String userId;

    /** 兑换大小 */
    @ApiModelProperty(value = "用户编号")
    @Excel(name = "兑换大小")
    @TableField("disappear")
    private BigDecimal disappear;

    /** 兑换单位 */
    @ApiModelProperty(value = "兑换大小")
    @Excel(name = "兑换单位")
    @TableField("freed_type")
    private String freedType;

    /** 是否加速 0=未加速，1=已加速 */
    @ApiModelProperty(value = "兑换单位")
    @Excel(name = "是否加速 0=未加速，1=已加速")
    @TableField("is_accelerate")
    private Integer isAccelerate;




}
