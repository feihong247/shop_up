package com.fw.system.web.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 我的:页面统计,消证,积分,收藏
 */
@Data
public class myCountVo {
    @ApiModelProperty("消证数量")
    private BigDecimal disappear;
    @ApiModelProperty("积分数量")
    private BigDecimal integral;
    @ApiModelProperty("商品收藏数量")
    private Integer collectCount;
    @ApiModelProperty("店铺收藏数量")
    private Integer shopCollectCount;
}
