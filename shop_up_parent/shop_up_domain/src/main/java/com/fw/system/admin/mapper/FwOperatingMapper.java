package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwOperating;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 商家经营平台分类Mapper接口
 * 
 * @author yanwei
 * @date 2021-05-19
 */
public interface FwOperatingMapper extends BaseMapper<FwOperating>
{
    /**
     * 查询商家经营平台分类
     * 
     * @param id 商家经营平台分类ID
     * @return 商家经营平台分类
     */
    public FwOperating selectFwOperatingById(String id);

    /**
     * 查询商家经营平台分类列表
     * 
     * @param fwOperating 商家经营平台分类
     * @return 商家经营平台分类集合
     */
    public List<FwOperating> selectFwOperatingList(FwOperating fwOperating);

    /**
     * 新增商家经营平台分类
     * 
     * @param fwOperating 商家经营平台分类
     * @return 结果
     */
    public int insertFwOperating(FwOperating fwOperating);

    /**
     * 修改商家经营平台分类
     * 
     * @param fwOperating 商家经营平台分类
     * @return 结果
     */
    public int updateFwOperating(FwOperating fwOperating);

    /**
     * 删除商家经营平台分类
     * 
     * @param id 商家经营平台分类ID
     * @return 结果
     */
    public int deleteFwOperatingById(String id);

    /**
     * 批量删除商家经营平台分类
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwOperatingByIds(String[] ids);
}
