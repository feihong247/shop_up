package com.fw.system.admin.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 用户对象 fw_user
 *
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_user")
@ApiModel(value="用户", description="用户表")
public class FwUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 支付宝唯一标示 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "支付宝唯一标示")
    @TableField("app_auth_token")
    private String appAuthToken;

    /** 微信唯一标示 */
    @ApiModelProperty(value = "支付宝唯一标示")
    @Excel(name = "微信唯一标示")
    @TableField("open_id")
    private String openId;

    /** 用户名称 */
    @ApiModelProperty(value = "微信唯一标示")
    @Excel(name = "用户名称")
    @TableField("user_name")
    private String userName;

    /** 用户手机号 */
    @ApiModelProperty(value = "用户名称")
    @Excel(name = "用户手机号")
    @TableField("phone")
    private String phone;

    /** 用户性别 */
    @ApiModelProperty(value = "用户手机号")
    @Excel(name = "用户性别")
    @TableField("sex")
    private Integer sex;

    /** 用户月收入 */
    @ApiModelProperty(value = "用户性别")
    @Excel(name = "用户月收入")
    @TableField("range_money")
    private String rangeMoney;

    /** 用户标签,存标签名称即可，使用,号分隔开 */
    @ApiModelProperty(value = "用户月收入")
    @Excel(name = "用户标签,存标签名称即可，使用,号分隔开")
    @TableField("labels")
    private String labels;

    /** 用户生日 */
    @ApiModelProperty(value = "用户标签,存标签名称即可，使用,号分隔开")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "用户生日", width = 30, dateFormat = "yyyy-MM-dd")
    @TableField("birthday")
    private Date birthday;

    /** 用户编号_平台编号，可以充当邀请码来使用 */
    @ApiModelProperty(value = "用户生日")
    @Excel(name = "用户编号_平台编号，可以充当邀请码来使用")
    @TableField("system_host")
    private String systemHost;

    /** 用户头像 */
    @ApiModelProperty(value = "用户编号_平台编号，可以充当邀请码来使用")
    @Excel(name = "用户头像")
    @TableField("head_image")
    private String headImage;

    /** 我的上级编号 */
    @ApiModelProperty(value = "用户头像")
    @Excel(name = "我的上级编号")
    @TableField("parent_id")
    private String parentId;

    /** 用户密码 */
    @ApiModelProperty(value = "我的上级编号")
    @Excel(name = "用户密码")
    @TableField("pass_word")
    private String passWord;

    /** 设备号 */
    @ApiModelProperty(value = "用户密码")
    @Excel(name = "设备号")
    @TableField("imei")
    private String imei;

    /** 支付密码 */
    @ApiModelProperty(value = "设备号")
    @Excel(name = "支付密码")
    @TableField("pay_pass_word")
    private String payPassWord;


    /** 用户是否可多级得消证,0:不可,1可 */
    @ApiModelProperty(value = "支付密码")
    @Excel(name = "用户是否可多级得消证,0:不可,1可")
    @TableField("disappear_is_more")
    private Integer disappearIsMore;

    /** 用户是否可提现,0不可,1可 */
    @ApiModelProperty(value = "用户是否可多级得消证,0:不可,1可")
    @Excel(name = "用户是否可提现,0不可,1可")
    @TableField("is_withdraw")
    private Integer isWithdraw;

    /** 支付宝真实姓名 */
    @ApiModelProperty(value = "用户是否可提现,0不可,1可")
    @Excel(name = "支付宝真实姓名")
    @TableField("ali_user_name")
    private String aliUserName;

    /** 支付宝绑定手机号 */
    @ApiModelProperty(value = "支付宝真实姓名")
    @Excel(name = "支付宝绑定手机号")
    @TableField("ali_phone")
    private String aliPhone;

    /** 积分兑换比例 */
    @ApiModelProperty(value = "支付宝绑定手机号")
    @Excel(name = "积分兑换比例")
    @TableField("to_release")
    private BigDecimal toRelease;

    /**
     * 管理标识
     * @return
     */
    @TableField("is_account")
    @ApiModelProperty(value = "管理标识 0 = 普通用户，1= 技术，2= 管理，3 原始")
    private Integer isAccount;

    /**
     * 身份中间表编号
     */
    @TableField(exist = false)
    @ApiModelProperty("身份中间表编号")
    private List<String> uiJoinIds;

}
