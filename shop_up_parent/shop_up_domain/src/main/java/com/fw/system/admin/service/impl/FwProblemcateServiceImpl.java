package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwProblemcateMapper;
import com.fw.system.admin.domain.FwProblemcate;
import com.fw.system.admin.service.IFwProblemcateService;

/**
 * 常见问题类目Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwProblemcateServiceImpl extends ServiceImpl<FwProblemcateMapper, FwProblemcate> implements IFwProblemcateService
{
    @Autowired
    private FwProblemcateMapper fwProblemcateMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询常见问题类目
     * 
     * @param id 常见问题类目ID
     * @return 常见问题类目
     */
    @Override
    public FwProblemcate selectFwProblemcateById(String id)
    {
        return fwProblemcateMapper.selectFwProblemcateById(id);
    }

    /**
     * 查询常见问题类目列表
     * 
     * @param fwProblemcate 常见问题类目
     * @return 常见问题类目
     */
    @Override
    public List<FwProblemcate> selectFwProblemcateList(FwProblemcate fwProblemcate)
    {
        return fwProblemcateMapper.selectFwProblemcateList(fwProblemcate);
    }

    /**
     * 新增常见问题类目
     * 
     * @param fwProblemcate 常见问题类目
     * @return 结果
     */
    @Override
    public int insertFwProblemcate(FwProblemcate fwProblemcate)
    {
        fwProblemcate.setCreateTime(DateUtils.getNowDate());
        fwProblemcate.setId(idXD.nextId());
        return fwProblemcateMapper.insertFwProblemcate(fwProblemcate);
    }

    /**
     * 修改常见问题类目
     * 
     * @param fwProblemcate 常见问题类目
     * @return 结果
     */
    @Override
    public int updateFwProblemcate(FwProblemcate fwProblemcate)
    {
        fwProblemcate.setUpdateTime(DateUtils.getNowDate());
        return fwProblemcateMapper.updateFwProblemcate(fwProblemcate);
    }

    /**
     * 批量删除常见问题类目
     * 
     * @param ids 需要删除的常见问题类目ID
     * @return 结果
     */
    @Override
    public int deleteFwProblemcateByIds(String[] ids)
    {
        return fwProblemcateMapper.deleteFwProblemcateByIds(ids);
    }

    /**
     * 删除常见问题类目信息
     * 
     * @param id 常见问题类目ID
     * @return 结果
     */
    @Override
    public int deleteFwProblemcateById(String id)
    {
        return fwProblemcateMapper.deleteFwProblemcateById(id);
    }
}
