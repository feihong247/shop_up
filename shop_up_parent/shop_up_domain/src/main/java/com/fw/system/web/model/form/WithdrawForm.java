package com.fw.system.web.model.form;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

@Api(tags = "积分提现")
@Data
public class WithdrawForm {
    @ApiModelProperty("支付宝账号")
    private String aliPhone;
    @ApiModelProperty("账号姓名")
    private String aliUserName;
    @ApiModelProperty("提现金额")
    @DecimalMin(value = "1", message = "必须大于等于1")
    private BigDecimal money;
    @ApiModelProperty("支付密码")
    private String payPassWord;
}
