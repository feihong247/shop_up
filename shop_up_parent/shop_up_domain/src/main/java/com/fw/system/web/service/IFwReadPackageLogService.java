package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwReadPackageLog;

/**
 * <p>
 * 红包抽取记录表 服务类
 * </p>
 *
 * @author  
 * @since 2021-09-03
 */
public interface IFwReadPackageLogService extends IService<FwReadPackageLog> {

}
