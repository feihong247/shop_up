package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 用户收货地址对象 fw_addrs
 *
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_addrs")
@ApiModel(value="用户收货地址", description="用户收货地址表")
public class FwAddrs extends BaseEntity
{

    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 用户编号 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "用户编号")
    @TableField("user_id")
    private String userId;

    /** 是否默认地址,1=是，0=否 */
    @ApiModelProperty(value = "用户编号")
    @Excel(name = "是否默认地址,1=是，0=否")
    @TableField("is_default")
    private Integer isDefault;

    /** 联系人姓名 */
    @ApiModelProperty(value = "是否默认地址,1=是，0=否")
    @Excel(name = "联系人姓名")
    @TableField("contact_name")
    private String contactName;

    /** 联系人电话 */
    @ApiModelProperty(value = "联系人姓名")
    @Excel(name = "联系人电话")
    @TableField("contact_phone")
    private String contactPhone;

    /** 省 */
    @ApiModelProperty(value = "联系人电话")
    @Excel(name = "省")
    @TableField("province")
    private String province;

    /** 市 */
    @ApiModelProperty(value = "省")
    @Excel(name = "市")
    @TableField("city")
    private String city;

    /** 区/县 */
    @ApiModelProperty(value = "市")
    @Excel(name = "区/县")
    @TableField("area")
    private String area;

    /** 详细地址 */
    @ApiModelProperty(value = "区/县")
    @Excel(name = "详细地址")
    @TableField("addr_info")
    private String addrInfo;


    /**
     * 用户名称
     */
    @TableField(exist = false)
    private String userName;

    /**
     * 用户手机号
     */
    @TableField(exist = false)
    private String phone;

    /**
     * 用户头像
     */
    @TableField(exist = false)
    private String headImage;

}
