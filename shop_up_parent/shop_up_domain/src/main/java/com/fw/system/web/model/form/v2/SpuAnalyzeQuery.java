package com.fw.system.web.model.form.v2;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel("智能化分析查询参数")
public class SpuAnalyzeQuery implements Serializable {


    @ApiModelProperty(value = "年度查询  yyyy-MM-dd",required = true)
    @NotBlank(message = "年度查询字段不可为空")
    private String yearQuery;


    @ApiModelProperty(value = "1 = 收藏量，2 = 购买量",required = true)
    @NotNull(message = "标识不可为空")
    @Min(value = 1,message = "最小不可1以下")
    @Max(value = 2,message = "最大不不可2以上")
    private Integer queryFlag = 1;
}
