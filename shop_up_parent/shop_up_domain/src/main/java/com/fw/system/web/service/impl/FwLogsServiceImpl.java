package com.fw.system.web.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.common.Builder;
import com.fw.common.IdXD;
import com.fw.enums.LogsModelEnum;
import com.fw.enums.LogsTypeEnum;
import com.fw.system.web.model.entity.FwLogs;
import com.fw.system.web.dao.FwLogsMapper;
import com.fw.system.web.service.IFwLogsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 业务日志 服务实现类
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Service
public class FwLogsServiceImpl extends ServiceImpl<FwLogsMapper, FwLogs> implements IFwLogsService {
    @Autowired
    private IdXD idXD;
    @Autowired
    private FwLogsMapper logsMapper;
    /**
     * 新增日志
     */
    @Override
    public void saveLogs(String createId,String buiId,String isType,String modelName,String jsonTxt,String formInfo){
        FwLogs fwLogs = Builder.<FwLogs>of(FwLogs::new).build();
        fwLogs.setId(idXD.nextId());
        fwLogs.setBuiId(buiId);
        fwLogs.setIsType(isType);
        fwLogs.setModelName(modelName);
        fwLogs.setJsonTxt(jsonTxt);
        fwLogs.setFormInfo(formInfo);
        fwLogs.setCreateBy(createId);
        fwLogs.setCreateTime(LocalDateTime.now());
        save(fwLogs);
    }

    /**
     * 返销证时增加父级的日志存储
     * @param createId
     * @param parentId
     * @param buiId
     * @param isType
     * @param modelName
     * @param jsonTxt
     * @param formInfo
     */
    @Override
    public void saveParentLogs(String createId,String parentId,String buiId,String isType,String modelName,String jsonTxt,String formInfo){
        FwLogs fwLogs = Builder.<FwLogs>of(FwLogs::new).build();
        fwLogs.setId(idXD.nextId());
        fwLogs.setBuiId(parentId);
        fwLogs.setIsType(isType);
        fwLogs.setModelName(modelName);
        fwLogs.setJsonTxt(jsonTxt);
        fwLogs.setFormInfo(formInfo);
        fwLogs.setCreateBy(createId);
        fwLogs.setCreateTime(LocalDateTime.now());
        save(fwLogs);
    }

    @Override
    public List<FwLogs> getShangJiaTotal(String buiId) {
        return logsMapper.getShangJiaTotal(buiId);
    }

    @Override
    public List<FwLogs> getShangJiaYearTotal(String buiId) {
        return logsMapper.getShangJiaYearTotal(buiId);
    }

    @Override
    public List<FwLogs> getShangJiaMonthTotal(String buiId) {
        return logsMapper.getShangJiaMonthTotal(buiId);
    }

    @Override
    public List<FwLogs> getShangJiaDayTotal(String buiId) {
        return logsMapper.getShangJiaDayTotal(buiId);
    }

    @Override
    public List<FwLogs> getDisappearTotal(String buiId) {
        return logsMapper.getDisappearTotal(buiId);
    }

    @Override
    public List<Map> getDisappearUpTotal(String buiId) {
        return logsMapper.getDisappearUpTotal(buiId);
    }

    @Override
    public List<Map> getDisappearDownTotal(String buiId) {
        return logsMapper.getDisappearDownTotal(buiId);
    }

    @Override
    public List<Map> getDisappearUpDetail(String buiId,String createTime) {
        return logsMapper.getDisappearUpDetail(buiId,createTime);
    }

    @Override
    public List<Map> getDisappearDownDetail(String buiId,String createTime) {
        return logsMapper.getDisappearDownDetail(buiId,createTime);
    }

    /**
     * 枚举新增日志
     */
    @Override
    public void saveLogs(String createId, String buiId, LogsTypeEnum logsTypeEnum, LogsModelEnum logsModel, String jsonTxt, String formInfo){
        FwLogs fwLogs = Builder.<FwLogs>of(FwLogs::new).build();
        fwLogs.setId(idXD.nextId());
        fwLogs.setCreateBy(createId);
        fwLogs.setBuiId(buiId);
        fwLogs.setIsType(logsTypeEnum.getTypeName());
        fwLogs.setJsonTxt(jsonTxt);
        fwLogs.setFormInfo(formInfo);
        fwLogs.setModelName(logsModel.getModelName());
        fwLogs.setCreateTime(LocalDateTime.now());
        save(fwLogs);
    }

    @Override
    public FwLogs getLogs(String buiId,  LogsModelEnum logsModel,String ...logsTypeEnum) {
        return getOne(Wrappers.<FwLogs>lambdaQuery().eq(FwLogs::getBuiId,buiId).in(FwLogs::getIsType,logsTypeEnum).eq(FwLogs::getModelName,logsModel.getModelName()));
    }

    @Override
    public List<FwLogs> getBonusDetail(String buiId){
        return logsMapper.getBonusDetail(buiId);
    }

    @Override
    public List<FwLogs> getLastBonusDetail(String buiId) {
        return logsMapper.getLastBonusDetail(buiId);
    }

    @Override
    public List<FwLogs> getShangJiaOutDetail() {
        return logsMapper.getShangJiaOutDetail();
    }

    @Override
    public List<FwLogs> getShangJiaLtOneDay(String buiId) {
        return logsMapper.getShangJiaLtOneDay(buiId);
    }

    @Override
    public List<FwLogs> getShangJiaUseList(String typeModel, String modelName) {
        return logsMapper.getShangJiaUseList(typeModel,modelName);
    }

    @Override
    public List<FwLogs> getShangJiaDestoryList(String typeName, String modelName, String json) {
        return logsMapper.getShangJiaDestoryList(typeName,modelName,json);
    }

    ;
}
