package com.fw.system.web.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 商甲发行表
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
@Api(tags = "商甲发行Vo")
public class ShangjiaVo {

    /**
     * 主键
     */
    @TableId("id")
    private String id;

    /**
     * 商甲名称
     */
    @ApiModelProperty(value = "商甲名称")
    private String shangjiaName;

    /**
     * 商甲额度，厘为单位
     */
    @ApiModelProperty(value = "商甲额度,发行量,厘为单位")
    private BigDecimal shangjiaQuota;

    /**
     * 发行时间
     */
    @ApiModelProperty(value = "发行时间")
    private LocalDate shangjiaPublishData;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime updateTime;

    /** 商甲实际剩余额度,发放商甲时,更改此数据 */
    @ApiModelProperty(value = "商甲实际剩余额度")
    private BigDecimal shopIssue;

    /** 商甲现价 */
    @ApiModelProperty(value = "商甲现价")
    private BigDecimal presentPrice;

}
