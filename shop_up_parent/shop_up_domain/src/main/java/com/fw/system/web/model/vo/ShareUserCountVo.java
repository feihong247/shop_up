package com.fw.system.web.model.vo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("区县模块我分享的列表和统计vo")
public class ShareUserCountVo {


    @ApiModelProperty("分享人数")
    private Integer countNum;

    @ApiModelProperty("分享的下级列表")
    private List<ShareUserListVo> userList;

}
