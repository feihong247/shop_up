package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwIdentity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwUser;

import java.util.List;

/**
 * <p>
 * 身份表 服务类
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
public interface IFwIdentityService extends IService<FwIdentity> {
    /**
     * 查询身份
     *
     * @param id 身份ID
     * @return 身份
     */
    public FwIdentity selectFwIdentityById(String id);

    /**
     * 查询身份列表
     *
     * @param fwIdentity 身份
     * @return 身份集合
     */
    public List<FwIdentity> selectFwIdentityList(FwIdentity fwIdentity);

    /**
     * 新增身份
     *
     * @param fwIdentity 身份
     * @return 结果
     */
    public int insertFwIdentity(FwIdentity fwIdentity);

    /**
     * 修改身份
     *
     * @param fwIdentity 身份
     * @return 结果
     */
    public int updateFwIdentity(FwIdentity fwIdentity);

    /**
     * 批量删除身份
     *
     * @param ids 需要删除的身份ID
     * @return 结果
     */
    public int deleteFwIdentityByIds(String[] ids);

    /**
     * 删除身份信息
     *
     * @param id 身份ID
     * @return 结果
     */
    public int deleteFwIdentityById(String id);

    /**
     * @Effect 获取用户累计消费记录,查询条件需要加上收支类型,消费模板    每当用户产生消费,调用此方法
     **/
    void addIdentity(FwUser fwUser);
}
