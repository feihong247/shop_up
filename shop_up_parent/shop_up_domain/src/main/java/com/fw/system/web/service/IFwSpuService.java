package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwSpu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface IFwSpuService extends IService<FwSpu> {

    /**
     * 减销量
     *
     * @param spuId  商品ID
     * @param number 数量
     * @return 是否减成功
     * @Author 姚
     * @Date 2021/7/6
     **/
    boolean subtractTops(String spuId,Integer number);

    /**
     * 加销量
     *
     * @param spuId  商品ID
     * @param number 数量
     * @return 是否加成功
     * @Author 姚
     * @Date 2021/7/6
     **/
    boolean addTops(String spuId,Integer number);
}
