package com.fw.system.web.model.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * @Effect 购物车
 * @author 姚
 * @since 2021/6/8
 **/

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel("购物车")
public class FwCart extends Model<FwCart> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键 主键
     */
    @TableId("id")
    private String id;

    /**
     * 用户编号
     */
    @TableField("user_id")
    private String userId;

    /**
     * 商品编号
     */
    @TableField("spu_id")
    private String spuId;

    @ApiModelProperty("商品对象")
    @TableField(exist = false)
    private FwSpu fwSpu;

    /**
     * 商品sku编号,以sku为主
     */
    @TableField("sku_id")
    private String skuId;

    @ApiModelProperty("规格对象")
    @TableField(exist = false)
    private FwSku fwSku;

    /** 商品标题 */
    @ApiModelProperty("商品标题")
    @TableField("shop_id")
    private String shopId;

    /** 商品标题 */
    @ApiModelProperty("商品标题")
    @TableField("spu_title")
    private String spuTitle;

    /** 商品价格 元为单位 */
    @TableField("sku_moneys")
    private BigDecimal skuMoneys;

    /** 商品数量 */
    @TableField("spu_number")
    private Integer spuNumber;


    /** 创建人 创建人 */
    @TableField("create_by")
    private String createBy;

    /**
     * 创建时间 创建时间
     */
    @TableField("create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新人 更新人
     */
    @TableField("update_by")
    private String updateBy;

    /**
     * 更新时间 更新时间
     */
    @TableField("update_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
