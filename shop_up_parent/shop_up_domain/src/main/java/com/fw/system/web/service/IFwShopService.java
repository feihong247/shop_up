package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwShop;
import com.fw.system.web.model.entity.FwUser;
import com.fw.system.web.model.form.ShopQuery;
import com.fw.system.web.model.form.ShopRegisteredForm;
import com.fw.system.web.model.form.v2.ShopLoginForm;
import com.fw.system.web.model.vo.ShopKeywordVo;
import com.fw.system.web.model.vo.ShopOnLineVo;
import com.fw.system.web.model.vo.ShopVo;
import com.fw.system.web.model.vo.v2.ShopIsOffVo;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 商铺表 服务类
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
public interface IFwShopService extends IService<FwShop> {

    void shopMerchant(FwUser user, ShopRegisteredForm registeredForm);

    List<ShopOnLineVo> shopOnlineQuery(ShopQuery shopQuery, FwUser user);

    ShopVo shopDistance(String shopId, BigDecimal latitude, BigDecimal longitude, FwUser user);

    String offlineShop(String shopId, BigDecimal money, Integer payType, String userId);

    void offlinePaySuccess(String userId, String shopId, BigDecimal money);

    ShopVo selectByUserId(String userId);

    List<ShopKeywordVo> queryKeyWord(String shopId);

    void  svgStore(String itemId,Integer star);

    void shopPublishSpu(String shopId, String flag);

    void shopShangjiaCustomer(FwShop oldShop, FwShop newShop, String flag);

    Object shopLogin(ShopLoginForm shopLoginForm);

    ShopVo isShop(String userId);

    Object edit(ShopVo shopVo);

    ShopIsOffVo isOff(String shopId);

    // 判断是否是线上商户  TRUR 是 FALSE 不是
    Boolean shopOnline(String shopId);

    // 判断是否是线下商户  TRUR 是 FALSE 不是
    Boolean shopOff(String shopId);
}
