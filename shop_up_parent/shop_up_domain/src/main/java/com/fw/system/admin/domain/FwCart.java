package com.fw.system.admin.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 购物车对象 fw_cart
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_cart")
@ApiModel(value="购物车", description="购物车")
public class FwCart extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 用户编号 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "用户编号")
    @TableField("user_id")
    private String userId;

    /** 商品编号 */
    @ApiModelProperty(value = "用户编号")
    @Excel(name = "商品编号")
    @TableField("spu_id")
    private String spuId;

    /** 商品sku编号,以sku为主 */
    @ApiModelProperty(value = "商品编号")
    @Excel(name = "商品sku编号,以sku为主")
    @TableField("sku_id")
    private String skuId;

    /** 商品标题 */
    @ApiModelProperty(value = "商品sku编号,以sku为主")
    @Excel(name = "商品标题")
    @TableField("spu_title")
    private String spuTitle;

    /** 商品价格 元为单位 */
    @ApiModelProperty(value = "商品标题")
    @Excel(name = "商品价格 元为单位")
    @TableField("sku_moneys")
    private BigDecimal skuMoneys;




}
