package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwLogShopingAgency;

/**
 * <p>
 * 用户购买代理日志 服务类
 * </p>
 *
 * @author
 * @since 2021-06-07
 */
public interface IFwLogShopingAgencyService extends IService<FwLogShopingAgency> {

}
