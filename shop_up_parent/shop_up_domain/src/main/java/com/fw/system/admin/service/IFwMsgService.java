package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwMsg;

/**
 * 用户消息Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwMsgService extends IService<FwMsg>
{
    /**
     * 查询用户消息
     * 
     * @param id 用户消息ID
     * @return 用户消息
     */
    public FwMsg selectFwMsgById(String id);

    /**
     * 查询用户消息列表
     * 
     * @param fwMsg 用户消息
     * @return 用户消息集合
     */
    public List<FwMsg> selectFwMsgList(FwMsg fwMsg);

    /**
     * 新增用户消息
     * 
     * @param fwMsg 用户消息
     * @return 结果
     */
    public int insertFwMsg(FwMsg fwMsg);

    /**
     * 修改用户消息
     * 
     * @param fwMsg 用户消息
     * @return 结果
     */
    public int updateFwMsg(FwMsg fwMsg);

    /**
     * 批量删除用户消息
     * 
     * @param ids 需要删除的用户消息ID
     * @return 结果
     */
    public int deleteFwMsgByIds(String[] ids);

    /**
     * 删除用户消息信息
     * 
     * @param id 用户消息ID
     * @return 结果
     */
    public int deleteFwMsgById(String id);
}
