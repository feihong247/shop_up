package com.fw.system.web.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwGuess;

import java.util.List;

/**
 * 竞猜Service接口
 * 
 * @author yanwei
 * @date 2021-06-23
 */
public interface IFwGuessService extends IService<FwGuess>
{
    /**
     * 查询竞猜
     * 
     * @param guessId 竞猜ID
     * @return 竞猜
     */
    public FwGuess selectFwGuessById(String guessId);

    /**
     * 查询竞猜列表
     * 
     * @param fwGuess 竞猜
     * @return 竞猜集合
     */
    public List<FwGuess> selectFwGuessList(FwGuess fwGuess);

    /**
     * 新增竞猜
     * 
     * @param fwGuess 竞猜
     * @return 结果
     */
    public int insertFwGuess(FwGuess fwGuess);

    /**
     * 修改竞猜
     * 
     * @param fwGuess 竞猜
     * @return 结果
     */
    public int updateFwGuess(FwGuess fwGuess);

    /**
     * 批量删除竞猜
     * 
     * @param guessIds 需要删除的竞猜ID
     * @return 结果
     */
    public int deleteFwGuessByIds(String[] guessIds);

    /**
     * 删除竞猜信息
     * 
     * @param guessId 竞猜ID
     * @return 结果
     */
    public int deleteFwGuessById(String guessId);
}
