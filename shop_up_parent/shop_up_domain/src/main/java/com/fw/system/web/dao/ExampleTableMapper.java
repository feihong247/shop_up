package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.ExampleTable;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yanwei
 * @since 2020-08-05
 */
public interface ExampleTableMapper extends BaseMapper<ExampleTable> {

}
