package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwUljoinMapper;
import com.fw.system.admin.domain.FwUljoin;
import com.fw.system.admin.service.IFwUljoinService;

/**
 * 用户和身份中间Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwUljoinServiceImpl extends ServiceImpl<FwUljoinMapper, FwUljoin> implements IFwUljoinService
{
    @Autowired
    private FwUljoinMapper fwUljoinMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询用户和身份中间
     * 
     * @param id 用户和身份中间ID
     * @return 用户和身份中间
     */
    @Override
    public FwUljoin selectFwUljoinById(String id)
    {
        return fwUljoinMapper.selectFwUljoinById(id);
    }

    /**
     * 查询用户和身份中间列表
     * 
     * @param fwUljoin 用户和身份中间
     * @return 用户和身份中间
     */
    @Override
    public List<FwUljoin> selectFwUljoinList(FwUljoin fwUljoin)
    {
        return fwUljoinMapper.selectFwUljoinList(fwUljoin);
    }

    /**
     * 新增用户和身份中间
     * 
     * @param fwUljoin 用户和身份中间
     * @return 结果
     */
    @Override
    public int insertFwUljoin(FwUljoin fwUljoin)
    {
        fwUljoin.setCreateTime(DateUtils.getNowDate());
        fwUljoin.setId(idXD.nextId());
        fwUljoin.setSysCode(fwUljoin.getIdentityId());
        return fwUljoinMapper.insertFwUljoin(fwUljoin);
    }

    /**
     * 修改用户和身份中间
     * 
     * @param fwUljoin 用户和身份中间
     * @return 结果
     */
    @Override
    public int updateFwUljoin(FwUljoin fwUljoin)
    {
        fwUljoin.setUpdateTime(DateUtils.getNowDate());
        fwUljoin.setSysCode(fwUljoin.getIdentityId());
        return fwUljoinMapper.updateFwUljoin(fwUljoin);
    }

    /**
     * 批量删除用户和身份中间
     * 
     * @param ids 需要删除的用户和身份中间ID
     * @return 结果
     */
    @Override
    public int deleteFwUljoinByIds(String[] ids)
    {
        return fwUljoinMapper.deleteFwUljoinByIds(ids);
    }

    /**
     * 删除用户和身份中间信息
     * 
     * @param id 用户和身份中间ID
     * @return 结果
     */
    @Override
    public int deleteFwUljoinById(String id)
    {
        return fwUljoinMapper.deleteFwUljoinById(id);
    }
}
