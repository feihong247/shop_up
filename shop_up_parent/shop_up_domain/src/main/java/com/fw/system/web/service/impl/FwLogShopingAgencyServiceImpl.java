package com.fw.system.web.service.impl;

import com.fw.system.web.dao.FwLogShopingAgencyMapper;
import com.fw.system.web.model.entity.FwLogShopingAgency;
import com.fw.system.web.service.IFwLogShopingAgencyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户购买代理日志 服务实现类
 * </p>
 *
 * @author
 * @since 2021-06-07
 */
@Service
public class FwLogShopingAgencyServiceImpl extends ServiceImpl<FwLogShopingAgencyMapper, FwLogShopingAgency> implements IFwLogShopingAgencyService {

}
