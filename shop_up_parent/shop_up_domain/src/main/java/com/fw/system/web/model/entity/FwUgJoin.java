package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 用户竞猜中间对象 fw_ug_join
 * 
 * @author yanwei
 * @date 2021-06-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_ug_join")
@ApiModel(value="用户竞猜中间", description="用户竞猜中间表")
public class FwUgJoin extends Model<FwUgJoin> {
    private static final long serialVersionUID = 1L;

    /** 用户竞猜关联ID */
    @ApiModelProperty(value = "${comment}")
    @TableId("ug_id")
    private String ugId;

    /** 用户ID */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "用户ID")
    @TableField("user_id")
    private String userId;

    @ApiModelProperty(value = "用户")
    @TableField(exist = false)
    private FwUser user;

    /** 竞猜Id */
    @ApiModelProperty(value = "用户ID")
    @Excel(name = "竞猜Id")
    @TableField("guess_id")
    private String guessId;

    /** 猜拳结果（1剪刀，2石头，3布） */
    @ApiModelProperty(value = "猜拳结果（1剪刀，2石头，3布）")
    @Excel(name = "猜拳结果", readConverterExp = "1=剪刀,2=石头,3=布")
    @TableField("result")
    private Integer result;

    /** 上次猜拳结果（1剪刀，2石头，3布） */
    @ApiModelProperty(value = "上次猜拳结果（1剪刀，2石头，3布）")
    @Excel(name = "上次猜拳结果", readConverterExp = "1=剪刀,2=石头,3=布")
    @TableField("old_result")
    private Integer oldResult;


    /** 订单号 */
    @ApiModelProperty(value = "订单号")
    @Excel(name = "订单号")
    @TableField("order_id")
    private String orderId;

    /** 是否机器人（0否1是） */
    @ApiModelProperty(value = "是否机器人（0否1是）")
    @TableField("is_auto")
    private Integer isAuto;

    /** 创建人 */
    @TableField("create_by")
    private String createBy;

    /** 创建时间 */
    @TableField("create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /** 更新人 */
    @TableField("update_by")
    private String updateBy;

    /** 更新时间 */
    @TableField("update_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /** 是否有效（0否1是） */
    @ApiModelProperty(value = "是否有效（0否1是）")
    @TableField("is_valid")
    private Integer isValid;

    /** 是否覆盖（0否1是） */
    @ApiModelProperty(value = "是否覆盖（0否1是）")
    @TableField("is_cover")
    private Integer isCover;

    /** 红包金额 */
    @ApiModelProperty(value = "红包金额")
    @TableField("red_bag")
    private BigDecimal redBag;

    @ApiModelProperty(value = "商品图片地址")
    @TableField(exist = false)
    private String spuPhoto;

    @ApiModelProperty(value = "商品名")
    @TableField(exist = false)
    private String spuName;

    @ApiModelProperty(value = "商品消证")
    @TableField(exist = false)
    private BigDecimal spuDisappear;

    /** 胜利者 */
    @ApiModelProperty(value = "是否胜利(0未结束，1赢，2输)")
    @TableField(exist = false)
    private String isVictory;




}
