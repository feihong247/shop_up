package com.fw.system.web.model.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AliLoginVo {
    @ApiModelProperty("是否新用户,0否1是")
    private String isNew;
    @ApiModelProperty("token")
    private String token;
    @ApiModelProperty("支付宝授权码")
    private String aliCode;
    @ApiModelProperty("手机号")
    private String phone;
}
