package com.fw.system.admin.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 消证获取对象 fw_disappear_get
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_disappear_get")
@ApiModel(value="消证获取", description="消证获取表")
public class FwDisappearGet extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 用户编号 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "用户编号")
    @TableField("user_id")
    private String userId;

    /** 消证大小 */
    @ApiModelProperty(value = "用户编号")
    @Excel(name = "消证大小")
    @TableField("disappear")
    private BigDecimal disappear;

    /** 消证来源 */
    @ApiModelProperty(value = "消证大小")
    @Excel(name = "消证来源")
    @TableField("disappear_source")
    private String disappearSource;




}
