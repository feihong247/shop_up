package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 代理区域对象 fw_agent_region
 * 
 * @author yanwei
 * @date 2021-07-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_agent_region")
@ApiModel(value="代理区域", description="代理区域表")
public class FwAgentRegion extends BaseEntity
{

    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 用户编号 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "用户编号")
    @TableField("user_id")
    private String userId;

    /** 代理身份编号,5:区县代理,6:市级代理,7:省级代理 */
    @ApiModelProperty(value = "用户编号")
    @Excel(name = "代理身份编号,5:区县代理,6:市级代理,7:省级代理")
    @TableField("agent")
    private String agent;

    /** 省 */
    @ApiModelProperty(value = "代理身份编号,5:区县代理,6:市级代理,7:省级代理")
    @Excel(name = "省")
    @TableField("province")
    private String province;

    /** 市 */
    @ApiModelProperty(value = "省")
    @Excel(name = "市")
    @TableField("city")
    private String city;

    /** 区/县 */
    @ApiModelProperty(value = "市")
    @Excel(name = "区/县")
    @TableField("area")
    private String area;

    /** 是否正在使用该地址,1=是，0=否 */
    @ApiModelProperty(value = "区/县")
    @Excel(name = "是否正在使用该地址,1=是，0=否")
    @TableField("is_default")
    private Integer isDefault;




}
