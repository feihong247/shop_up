package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页轮播 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwBannerMapper extends BaseMapper<FwBanner> {

}
