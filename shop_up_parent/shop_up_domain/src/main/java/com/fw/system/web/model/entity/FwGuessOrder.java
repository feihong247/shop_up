package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 竞猜订单对象 fw_guess_order
 *
 * @author yanwei
 * @date 2021-06-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_guess_order")
@ApiModel(value = "竞猜订单", description = "竞猜订单")
public class FwGuessOrder extends Model<FwOrder> {
    private static final long serialVersionUID = 1L;

    /**
     * 竞猜订单主键
     */
    @ApiModelProperty(value = "${comment}")
    @TableId("guess_order_id")
    private String guessOrderId;

    /**
     * 订单Id
     */
    @ApiModelProperty(value = "${comment}")
    @TableField("order_id")
    private String orderId;

    /**
     * 参与者1Id
     */
    @ApiModelProperty(value = "订单Id")
    @TableField("user_one_id")
    private String userOneId;

    /** 用户1的猜拳结果（1剪刀，2石头，3布） */
    @ApiModelProperty(value = "用户1的猜拳结果（1剪刀，2石头，3布）")
    @TableField("one_result")
    private Integer oneResult;

    /** 用户1的订单号 */
    @ApiModelProperty(value = "用户1的订单号")
    @TableField("one_order_id")
    private String oneOrderId;

    /**
     * 参与者2Id
     */
    @ApiModelProperty(value = "参与者1Id")
    @Excel(name = "参与者2Id")
    @TableField("user_two_id")
    private String userTwoId;

    /** 用户2的猜拳结果（1剪刀，2石头，3布） */
    @ApiModelProperty(value = "用户2的猜拳结果（1剪刀，2石头，3布）")
    @TableField("two_result")
    private Integer twoResult;

    /** 用户2是否机器人（0否1是） */
    @ApiModelProperty(value = "用户2是否机器人（0否1是）")
    @TableField("two_is_auto")
    private Integer twoIsAuto;

    /** 用户2的订单号 */
    @ApiModelProperty(value = "用户2的订单号")
    @TableField("two_order_id")
    private String twoOrderId;

    /**
     * 参与者3Id
     */
    @ApiModelProperty(value = "参与者2Id")
    @Excel(name = "参与者3Id")
    @TableField("user_three_id")
    private String userThreeId;

    /** 用户3的猜拳结果（1剪刀，2石头，3布） */
    @ApiModelProperty(value = "用户3的猜拳结果（1剪刀，2石头，3布）")
    @TableField("three_result")
    private Integer threeResult;

    /** 用户3是否机器人（0否1是） */
    @ApiModelProperty(value = "用户3是否机器人（0否1是）")
    @TableField("three_is_auto")
    private Integer threeIsAuto;

    /** 用户3的订单号 */
    @ApiModelProperty(value = "用户3的订单号")
    @TableField("three_order_id")
    private String threeOrderId;

    /** 胜利者Id */
    @ApiModelProperty(value = "参与者3Id")
    @Excel(name = "胜利者Id")
    @TableField("victory_user_id")
    private String victoryUserId;

    /** 房间人数 */
    @ApiModelProperty(value = "房间人数")
    @Excel(name = "房间人数")
    @TableField("people_number")
    private Integer peopleNumber;

    /**
     * 创建人
     */
    @TableField("create_by")
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @TableField("update_by")
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;


}
