package com.fw.system.web.service.impl;

import com.fw.system.web.model.entity.FwMsg;
import com.fw.system.web.dao.FwMsgMapper;
import com.fw.system.web.service.IFwMsgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户消息 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Service
public class FwMsgServiceImpl extends ServiceImpl<FwMsgMapper, FwMsg> implements IFwMsgService {

}
