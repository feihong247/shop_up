package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 足迹对象 fw_footprint
 *
 * @author yanwei
 * @date 2021-06-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_footprint")
@ApiModel(value = "足迹", description = "足迹表")
public class FwFootprint extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /**
     * 用户编号
     */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "用户编号")
    @TableField("user_id")
    private String userId;

    /**
     * 商品编号
     */
    @ApiModelProperty(value = "用户编号")
    @Excel(name = "商品编号")
    @TableField("item_id")
    private String itemId;


}
