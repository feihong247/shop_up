package com.fw.system.web.model.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户钱包表
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
public class MoneysVo {
    /**
     * 主键
     */
    @ApiModelProperty("主键")
    private String id;

    /**
     * 用户编号
     */
    @ApiModelProperty("用户编号")
    private String userId;

    /**
     * 消证_厘为单位
     */
    @ApiModelProperty("消证_厘为单位")
    private BigDecimal disappear;

    /**
     * 积分_厘为单位
     */
    @ApiModelProperty("积分_厘为单位")
    private BigDecimal integral;

    /**
     * 商甲_厘为单位
     */
    @ApiModelProperty("商甲_厘为单位")
    private BigDecimal shangJia;

    /**
     * 创建人
     */
    @ApiModelProperty("create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty("create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;


    /**
     * 更新人
     */
    @ApiModelProperty("update_by")
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty("update_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;


}
