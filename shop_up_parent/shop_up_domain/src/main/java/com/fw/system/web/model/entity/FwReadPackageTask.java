package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 红包任务类型列表
 * </p>
 *
 * @author  
 * @since 2021-09-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwReadPackageTask extends Model<FwReadPackageTask> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    /**
     * 任务类型名称
     */
    @TableField("task_name")
    private String taskName;

    /**
     * 任务可以完成的次数
     */
    @TableField("task_num")
    private Integer taskNum;

    /**
     * 任务累加红包的额度
     */
    @TableField("task_amount")
    private BigDecimal taskAmount;

    /**
     * 关联的红包ID
     */
    @TableField("read_id")
    private String readId;

    @TableField("create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
