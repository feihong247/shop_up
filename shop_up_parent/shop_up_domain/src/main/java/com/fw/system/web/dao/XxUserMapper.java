package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.XxUser;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-07-31
 */
public interface XxUserMapper extends BaseMapper<XxUser> {

}
