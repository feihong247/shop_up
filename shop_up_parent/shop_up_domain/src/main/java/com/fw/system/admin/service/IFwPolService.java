package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwPol;

/**
 * 地图Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwPolService extends IService<FwPol>
{
    /**
     * 查询地图
     * 
     * @param id 地图ID
     * @return 地图
     */
    public FwPol selectFwPolById(String id);

    /**
     * 查询地图列表
     * 
     * @param fwPol 地图
     * @return 地图集合
     */
    public List<FwPol> selectFwPolList(FwPol fwPol);

    /**
     * 新增地图
     * 
     * @param fwPol 地图
     * @return 结果
     */
    public int insertFwPol(FwPol fwPol);

    /**
     * 修改地图
     * 
     * @param fwPol 地图
     * @return 结果
     */
    public int updateFwPol(FwPol fwPol);

    /**
     * 批量删除地图
     * 
     * @param ids 需要删除的地图ID
     * @return 结果
     */
    public int deleteFwPolByIds(String[] ids);

    /**
     * 删除地图信息
     * 
     * @param id 地图ID
     * @return 结果
     */
    public int deleteFwPolById(String id);
}
