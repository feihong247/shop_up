package com.fw.system.web.model.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel("区县模块我分享的列表和统计vo")
public class ShareUserListVo {

    @ApiModelProperty("用户头像")
    private String headImage;

    @ApiModelProperty("用户名称")
    private String userName;

    @ApiModelProperty("用户手机号")
    private String phone;

    @ApiModelProperty("用户身份名称")
    private String identity;


    @ApiModelProperty("分享人数")
    private Integer countNum;

    @ApiModelProperty("分享时间取用户注册时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

}
