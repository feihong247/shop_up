package com.fw.system.web.service.impl;

import com.fw.common.IdXD;
import com.fw.core.text.StrFormatter;
import com.fw.enums.PayTypeEnum;
import com.fw.system.web.dao.FwUserMapper;
import com.fw.system.web.model.entity.FwOffline;
import com.fw.system.web.dao.FwOfflineMapper;
import com.fw.system.web.model.entity.FwUser;
import com.fw.system.web.model.form.v2.OfflineQuery;
import com.fw.system.web.model.vo.v2.OfflineVo;
import com.fw.system.web.model.vo.v2.OrderMoneyVo;
import com.fw.system.web.service.IFwOfflineService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 线下消费记录表 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Service
public class FwOfflineServiceImpl extends ServiceImpl<FwOfflineMapper, FwOffline> implements IFwOfflineService {

    @Autowired
    private IdXD idXD;

    @Autowired
    private FwUserMapper userMapper;

    @Autowired
    private FwOfflineMapper offlineMapper;

    @Override
    public void shopSave(String userId, String shopId, BigDecimal money, BigDecimal realAmount, PayTypeEnum payTypeEnum, String message) {
        FwUser fwUser = userMapper.selectById(userId);
        FwOffline fwOffline = new FwOffline();
        fwOffline.setId(idXD.nextId());
        fwOffline.setShopId(shopId);
        fwOffline.setUserId(userId);
        fwOffline.setPrice(money);
        fwOffline.setPayType(payTypeEnum.getType());
        fwOffline.setInfos(StrFormatter.format("用户编号为:{},手机号为:{},线下支付消费实际到账:{},支付状态:{}",userId,fwUser.getPhone(),realAmount,message));
        fwOffline.createD(userId);
        save(fwOffline);
    }

    @Override
    public PageInfo<OfflineVo> pageList(OfflineQuery offlineQuery, String shopId) {
        PageHelper.startPage(offlineQuery.getPageNum(),offlineQuery.getPageSize());
         List<OfflineVo> offlineVos =  offlineMapper.pageList(shopId);
         PageInfo<OfflineVo> pageInfo = new PageInfo<>(offlineVos);
        return pageInfo;
    }

    @Override
    public List<OrderMoneyVo> manageOffOrder(String shopId, Integer queryRange, Integer limit) {
        return offlineMapper.manageOffOrder(shopId,queryRange,limit);
    }
}
