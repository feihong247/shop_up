package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;
import java.util.List;
import com.fw.system.admin.domain.FwMoneys;

/**
 * 用户钱包Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwMoneysService extends IService<FwMoneys>
{
    /**
     * 查询用户钱包
     * 
     * @param id 用户钱包ID
     * @return 用户钱包
     */
    public FwMoneys selectFwMoneysById(String id);

    /**
     * 查询用户钱包列表
     * 
     * @param fwMoneys 用户钱包
     * @return 用户钱包集合
     */
    public List<FwMoneys> selectFwMoneysList(FwMoneys fwMoneys);

    /**
     * 新增用户钱包
     * 
     * @param fwMoneys 用户钱包
     * @return 结果
     */
    public int insertFwMoneys(FwMoneys fwMoneys);

    /**
     * 修改用户钱包
     * 
     * @param fwMoneys 用户钱包
     * @return 结果
     */
    public int updateFwMoneys(FwMoneys fwMoneys);

    /**
     * 批量删除用户钱包
     * 
     * @param ids 需要删除的用户钱包ID
     * @return 结果
     */
    public int deleteFwMoneysByIds(String[] ids);

    /**
     * 删除用户钱包信息
     * 
     * @param id 用户钱包ID
     * @return 结果
     */
    public int deleteFwMoneysById(String id);

    /**
     * 分配原始商甲
     * @param shangjia
     * @param userId
     * @return
     */
    public int updateMoneysShangJia(BigDecimal shangjia, String userId,Integer isAccount);
    /**
     * 分配技术商甲
     * @param shangjia
     * @param userId
     * @return
     */
    public int updateMoneysTecShangJia(BigDecimal shangjia, String userId,Integer isAccount);
    /**
     * 分配管理商甲
     * @param shangjia
     * @param userId
     * @return
     */
    public int updateMoneysManShangJia(BigDecimal shangjia, String userId,Integer isAccount);

    /**
     * 回收管理商甲
     * @param shangjia
     * @param userId
     * @return
     */
    public int recycleManShangJia(BigDecimal shangjia, String userId,Integer isAccount);
    /**
     * 回收技术商甲
     * @param shangjia
     * @param userId
     * @return
     */
    public int recycleTecShangJia(BigDecimal shangjia, String userId,Integer isAccount);
}
