package com.fw.system.web.model.form.v2;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
@Data
@ApiModel("sku表单")
public class SkuForm implements Serializable {
    /**
     * sku价格
     */
    @ApiModelProperty("sku价格")
    @NotNull(message = "sku价格 不可为空")
    private BigDecimal skuMoney;

    /**
     * sku规格名称
     */
    @ApiModelProperty("sku规格名称")
    private String skuName;

    /**
     * sku余量
     */
    @ApiModelProperty("sku余量")
    @NotNull(message = "sku余量 不可为空")
    @Size(min = 60,message = "规格余量不可为空")
    private Integer skuBalance;
}
