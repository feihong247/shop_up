package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwReadPackage;

/**
 * <p>
 * 红包类型表 服务类
 * </p>
 *
 * @author  
 * @since 2021-09-03
 */
public interface IFwReadPackageService extends IService<FwReadPackage> {

}
