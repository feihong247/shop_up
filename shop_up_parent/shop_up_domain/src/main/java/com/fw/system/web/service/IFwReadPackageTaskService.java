package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwReadPackageTask;

/**
 * <p>
 * 红包任务类型列表 服务类
 * </p>
 *
 * @author  
 * @since 2021-09-03
 */
public interface IFwReadPackageTaskService extends IService<FwReadPackageTask> {

}
