package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 我的商铺收藏中间对象 fw_usjoin
 *
 * @author yanwei
 * @date 2021-05-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_usjoin")
@ApiModel(value = "我的商铺收藏中间", description = "我的商铺收藏中间表")
public class FwUsjoin extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键 主键
     */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /**
     * 用户编号
     */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "用户编号")
    @TableField("user_id")
    private String userId;

    /**
     * 商铺编号
     */
    @ApiModelProperty(value = "用户编号")
    @Excel(name = "商铺编号")
    @TableField("shop_id")
    private String shopId;


}
