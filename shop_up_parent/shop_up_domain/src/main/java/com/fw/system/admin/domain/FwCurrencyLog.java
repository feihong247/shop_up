package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 通用日志记录表
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwCurrencyLog extends Model<FwCurrencyLog> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    /**
     * 日志记录名称
     */
    @TableField("log_name")
    private String logName;

    /**
     * 发生日志的数量
     */
    @TableField("log_count")
    private BigDecimal logCount;

    /**
     * 1商甲 2积分 3消证 ...可变type
     */
    @TableField("log_type")
    private Integer logType;

    /**
     * 用户编号 需要就记录
     */
    @TableField("user_id")
    private String userId;

    /**
     * 1 收入 2支出 ...可变from参数
     */
    @TableField("info_from")
    private Integer infoFrom;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
