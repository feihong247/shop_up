package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwSpu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 商品Mapper接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface FwSpuMapper extends BaseMapper<FwSpu>
{
    /**
     * 查询商品
     * 
     * @param id 商品ID
     * @return 商品
     */
    public FwSpu selectFwSpuById(String id);

    /**
     * 查询商品列表
     * 
     * @param fwSpu 商品
     * @return 商品集合
     */
    public List<FwSpu> selectFwSpuList(FwSpu fwSpu);

    /**
     * 新增商品
     * 
     * @param fwSpu 商品
     * @return 结果
     */
    public int insertFwSpu(FwSpu fwSpu);

    /**
     * 修改商品
     * 
     * @param fwSpu 商品
     * @return 结果
     */
    public int updateFwSpu(FwSpu fwSpu);

    /**
     * 删除商品
     * 
     * @param id 商品ID
     * @return 结果
     */
    public int deleteFwSpuById(String id);

    /**
     * 批量删除商品
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwSpuByIds(String[] ids);
}
