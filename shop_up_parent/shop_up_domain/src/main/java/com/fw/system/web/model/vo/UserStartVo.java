package com.fw.system.web.model.vo;

import com.fw.system.web.model.entity.FwUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("用户星级及用户信息封装")
public class UserStartVo extends FwUser {


    /**
     * 微信唯一标示
     */
    @ApiModelProperty("用户星级名称")
    private String userStart;

    @ApiModelProperty("那些菜单是否展示 true 展示 false 不展示")
    private boolean isShow = false;



}
