package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwImages;

/**
 * 商品图片列Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwImagesService extends IService<FwImages>
{
    /**
     * 查询商品图片列
     * 
     * @param id 商品图片列ID
     * @return 商品图片列
     */
    public FwImages selectFwImagesById(String id);

    /**
     * 查询商品图片列列表
     * 
     * @param fwImages 商品图片列
     * @return 商品图片列集合
     */
    public List<FwImages> selectFwImagesList(FwImages fwImages);

    /**
     * 新增商品图片列
     * 
     * @param fwImages 商品图片列
     * @return 结果
     */
    public int insertFwImages(FwImages fwImages);

    /**
     * 修改商品图片列
     * 
     * @param fwImages 商品图片列
     * @return 结果
     */
    public int updateFwImages(FwImages fwImages);

    /**
     * 批量删除商品图片列
     * 
     * @param ids 需要删除的商品图片列ID
     * @return 结果
     */
    public int deleteFwImagesByIds(String[] ids);

    /**
     * 删除商品图片列信息
     * 
     * @param id 商品图片列ID
     * @return 结果
     */
    public int deleteFwImagesById(String id);
}
