package com.fw.system.web.model.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel("商品")
public class FwSpu extends Model<FwSpu> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("id")
    @ApiModelProperty("商品编号")
    private String id;

    /**
     * 商铺编号,自营另外区分
     */
    @TableField("shop_id")
    @ApiModelProperty("商铺编号")
    private String shopId;

    /**
     * 商品标题
     */
    @TableField("title")
    @ApiModelProperty("商品标题")
    private String title;

    /**
     * 商品logo
     */
    @TableField("logo")
    @ApiModelProperty("商品logo")
    private String logo;

    /**
     * 商品销量
     */
    @TableField("tops")
    @ApiModelProperty("商品销量")
    private Integer tops;

    /**
     * 商品原价格
     */
    @TableField("virtual_price")
    @ApiModelProperty("商品原价格")
    private BigDecimal virtualPrice;

    /**
     * 商品现价格
     */
    @TableField("real_price")
    @ApiModelProperty("商品现价格")
    private BigDecimal realPrice;

    /**
     * 商品利润值,后管看的
     */
    @TableField("price")
    @ApiModelProperty("商品利润值")
    private BigDecimal price;

    /**
     * 商品消证,后管自营人员填写
     */
    @TableField("disappear")
    @ApiModelProperty("商品消证")
    private BigDecimal disappear;

    /**
     * 商品总库存
     */
    @TableField("balance")
    @ApiModelProperty("商品总库存")
    private Integer balance;

    /**
     * 商品详情-富文本形式
     */
    @TableField("infos")
    @ApiModelProperty("商品详情")
    private String infos;

    /**
     * 商品状态 0=正常，1=下架
     */
    @TableField("status")
    @ApiModelProperty("商品状态 0=正常，1=下架")
    private Object status;

    /** 商品运费 */
    @TableField("shipping")
    @ApiModelProperty("商品运费")
    private BigDecimal shipping;

    /**
     * 商品满额减运费 需要一套模版表,地区减额，单独商品减额，还是商家总商品减额等。
     */
    @TableField("top_shipping")
    @ApiModelProperty("商品满额减运费")
    private BigDecimal topShipping;

    /** 是否满减运费0否1是 */
    @TableField("is_top_shipping")
    @ApiModelProperty("是否满减运费0否1是")
    private Integer isTopShipping;


    /**
     * 创建人
     */
    @TableField("create_by")
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @TableField("update_by")
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;

    /** 是否热卖 */
    @TableField("hot_sale")
    @ApiModelProperty("是否热卖")
    private Integer hotSale;

    /** 是否推荐 */
    @TableField("recommend")
    @ApiModelProperty("是否推荐")
    private Integer recommend;

    /** 类目 */
    @TableField("category")
    @ApiModelProperty("类目")
    private String category;

    @ApiModelProperty("类目名")
    @TableField(exist = false)
    private String categoryName;

    @ApiModelProperty("商铺对象")
    @TableField(exist = false)
    private FwShop fwShop;

    /** 是否特卖（0否1是） */
    @TableField("is_sale")
    @ApiModelProperty("是否特卖（0否1是）")
    private Integer isSale;

    /** 审批结果(0待审批1通过2驳回) */
    @TableField("approval_result")
    @ApiModelProperty("审批结果(0待审批1通过2驳回)")
    private Integer approvalResult;

    /** 驳回原因 */
    @TableField("result_info")
    @ApiModelProperty("驳回原因")
    private String resultInfo;


    /** 是否线下(0线上1线下) */
    @TableField("is_offline")
    @ApiModelProperty("是否线下(0否1是)")
    private String isOffline;

    /** 是否竞猜(0线上1线下) */
    @TableField("is_quiz")
    @ApiModelProperty("是否竞猜(0否1是)")
    private Integer isQuiz;

    /** 满减标准 */
    @TableField("full_standard")
    @ApiModelProperty("满减标准")
    private BigDecimal fullStandard;

    /** 满减金额 */
    @TableField("full_amount")
    @ApiModelProperty("满减金额")
    private BigDecimal fullAmount;

    @TableField("province")
    @ApiModelProperty("省")
    private String province;

    @TableField("city")
    @ApiModelProperty("市")
    private String city;

    @TableField("area")
    @ApiModelProperty("县")
    private String area;

    @ApiModelProperty("规格列表")
    @TableField(exist = false)
    private List<FwSku> fwSkuList;

    @ApiModelProperty("图片列表")
    @TableField(exist = false)
    private List<FwImages> fwImages;

    @ApiModelProperty("是否收藏0否1是")
    @TableField(exist = false)
    private Integer isKeep;

    /** 规格列表 */
    @TableField(exist = false)
    @ApiModelProperty("规格列表")
    private List<FwSku> skuList = new ArrayList<>();

    /** 图片列表 */
    @TableField(exist = false)
    @ApiModelProperty("图片列表")
    private List<FwImages> images = new ArrayList<>();


    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    public void createD(String createId){
        this.createBy = createId;
        this.createTime = LocalDateTime.now();
        if (this.createTime != null)
            this.updateTime = this.createTime;
        this.updateBy = this.createBy;
    }

    public void updateD(String updateId){
        this.updateBy = updateId;
        this.updateTime = LocalDateTime.now();
    }
}
