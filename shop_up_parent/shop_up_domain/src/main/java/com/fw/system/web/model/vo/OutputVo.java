package com.fw.system.web.model.vo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Api(tags = "商甲产出Vo")
public class OutputVo implements Serializable {

    @ApiModelProperty("VIP总数")
    private Integer vipCount;
    @ApiModelProperty("VIP年增长数量")
    private Integer vipYearsCount;
    @ApiModelProperty("VIP月增长数量")
    private Integer vipMonthCount;
    @ApiModelProperty("VIP日增长数量")
    private Integer vipDayCount;
    @ApiModelProperty("商甲总产出数量")
    private BigDecimal shangJiaCount;
    @ApiModelProperty("恒定增长条件")
    private String growthConditions;
    @ApiModelProperty("当前商甲价格")
    private BigDecimal presentPrice;

}
