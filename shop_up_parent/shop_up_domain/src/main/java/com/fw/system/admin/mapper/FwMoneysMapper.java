package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwMoneys;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户钱包Mapper接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface FwMoneysMapper extends BaseMapper<FwMoneys>
{
    /**
     * 查询用户钱包
     * 
     * @param id 用户钱包ID
     * @return 用户钱包
     */
    public FwMoneys selectFwMoneysById(String id);

    /**
     * 查询用户钱包列表
     * 
     * @param fwMoneys 用户钱包
     * @return 用户钱包集合
     */
    public List<FwMoneys> selectFwMoneysList(FwMoneys fwMoneys);

    /**
     * 新增用户钱包
     * 
     * @param fwMoneys 用户钱包
     * @return 结果
     */
    public int insertFwMoneys(FwMoneys fwMoneys);

    /**
     * 修改用户钱包
     * 
     * @param fwMoneys 用户钱包
     * @return 结果
     */
    public int updateFwMoneys(FwMoneys fwMoneys);

    /**
     * 删除用户钱包
     * 
     * @param id 用户钱包ID
     * @return 结果
     */
    public int deleteFwMoneysById(String id);

    /**
     * 批量删除用户钱包
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwMoneysByIds(String[] ids);
}
