package com.fw.system.web.service.impl;

import com.fw.system.web.model.entity.FwPol;
import com.fw.system.web.dao.FwPolMapper;
import com.fw.system.web.service.IFwPolService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地图表 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Service
public class FwPolServiceImpl extends ServiceImpl<FwPolMapper, FwPol> implements IFwPolService {

}
