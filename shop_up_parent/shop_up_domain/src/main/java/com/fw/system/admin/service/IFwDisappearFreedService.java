package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwDisappearFreed;

/**
 * 消证兑换Service接口
 *
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwDisappearFreedService extends IService<FwDisappearFreed>
{
    /**
     * 查询消证兑换
     *
     * @param id 消证兑换ID
     * @return 消证兑换
     */
    public FwDisappearFreed selectFwDisappearFreedById(String id);

    /**
     * 查询消证兑换列表
     *
     * @param fwDisappearFreed 消证兑换
     * @return 消证兑换集合
     */
    public List<FwDisappearFreed> selectFwDisappearFreedList(FwDisappearFreed fwDisappearFreed);

    /**
     * 新增消证兑换
     *
     * @param fwDisappearFreed 消证兑换
     * @return 结果
     */
    public int insertFwDisappearFreed(FwDisappearFreed fwDisappearFreed);

    /**
     * 修改消证兑换
     *
     * @param fwDisappearFreed 消证兑换
     * @return 结果
     */
    public int updateFwDisappearFreed(FwDisappearFreed fwDisappearFreed);

    /**
     * 批量删除消证兑换
     *
     * @param ids 需要删除的消证兑换ID
     * @return 结果
     */
    public int deleteFwDisappearFreedByIds(String[] ids);

    /**
     * 删除消证兑换信息
     *
     * @param id 消证兑换ID
     * @return 结果
     */
    public int deleteFwDisappearFreedById(String id);
}
