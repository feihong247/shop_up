package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwUser;
import com.fw.system.admin.domain.vo.UserAgentVo;
import com.fw.system.admin.domain.vo.UserLevelVo;
import com.fw.system.admin.domain.vo.UserVo;

/**
 * 用户Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwUserService extends IService<FwUser>
{
    /**
     * 查询用户
     * 
     * @param id 用户ID
     * @return 用户
     */
    public FwUser selectFwUserById(String id);

    /**
     * 查询用户列表
     * 
     * @param fwUser 用户
     * @return 用户集合
     */
    public List<UserVo> selectFwUserList(FwUser fwUser);

    /**
     * 新增用户
     * 
     * @param fwUser 用户
     * @return 结果
     */
    public int insertFwUser(FwUser fwUser);

    /**
     * 修改用户
     * 
     * @param fwUser 用户
     * @return 结果
     */
    public int updateFwUser(FwUser fwUser);

    /**
     * 批量删除用户
     * 
     * @param ids 需要删除的用户ID
     * @return 结果
     */
    public int deleteFwUserByIds(String[] ids);

    /**
     * 删除用户信息
     * 
     * @param id 用户ID
     * @return 结果
     */
    public int deleteFwUserById(String id);

    List<UserLevelVo> userLevel(String parentId);

    UserAgentVo getUserAgentVo(String userId);

}
