package com.fw.system.web.model.form.v2;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@ApiModel("智能化分析查询参数")
public class ShopAnalyzeQuery implements Serializable {


    @ApiModelProperty(value = "年度查询  yyyy-MM-dd",required = true)
    @NotBlank(message = "年度查询字段不可为空")
    private String yearQuery;
}
