package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwReadPackageTaskContentMapper;
import com.fw.system.admin.domain.FwReadPackageTaskContent;
import com.fw.system.admin.service.IFwReadPackageTaskContentService;

/**
 * 红包关联任务用户完成情况Service业务层处理
 * 
 * @author yanwei
 * @date 2021-10-12
 */
@Service
public class FwReadPackageTaskContentServiceImpl extends ServiceImpl<FwReadPackageTaskContentMapper, FwReadPackageTaskContent> implements IFwReadPackageTaskContentService
{
    @Autowired
    private FwReadPackageTaskContentMapper fwReadPackageTaskContentMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询红包关联任务用户完成情况
     * 
     * @param id 红包关联任务用户完成情况ID
     * @return 红包关联任务用户完成情况
     */
    @Override
    public FwReadPackageTaskContent selectFwReadPackageTaskContentById(String id)
    {
        return fwReadPackageTaskContentMapper.selectFwReadPackageTaskContentById(id);
    }

    /**
     * 查询红包关联任务用户完成情况列表
     * 
     * @param fwReadPackageTaskContent 红包关联任务用户完成情况
     * @return 红包关联任务用户完成情况
     */
    @Override
    public List<FwReadPackageTaskContent> selectFwReadPackageTaskContentList(FwReadPackageTaskContent fwReadPackageTaskContent)
    {
        return fwReadPackageTaskContentMapper.selectFwReadPackageTaskContentList(fwReadPackageTaskContent);
    }

    /**
     * 新增红包关联任务用户完成情况
     * 
     * @param fwReadPackageTaskContent 红包关联任务用户完成情况
     * @return 结果
     */
    @Override
    public int insertFwReadPackageTaskContent(FwReadPackageTaskContent fwReadPackageTaskContent)
    {
        fwReadPackageTaskContent.setCreateTime(DateUtils.getNowDate());
        fwReadPackageTaskContent.setId(idXD.nextId());
        return fwReadPackageTaskContentMapper.insertFwReadPackageTaskContent(fwReadPackageTaskContent);
    }

    /**
     * 修改红包关联任务用户完成情况
     * 
     * @param fwReadPackageTaskContent 红包关联任务用户完成情况
     * @return 结果
     */
    @Override
    public int updateFwReadPackageTaskContent(FwReadPackageTaskContent fwReadPackageTaskContent)
    {
        return fwReadPackageTaskContentMapper.updateFwReadPackageTaskContent(fwReadPackageTaskContent);
    }

    /**
     * 批量删除红包关联任务用户完成情况
     * 
     * @param ids 需要删除的红包关联任务用户完成情况ID
     * @return 结果
     */
    @Override
    public int deleteFwReadPackageTaskContentByIds(String[] ids)
    {
        return fwReadPackageTaskContentMapper.deleteFwReadPackageTaskContentByIds(ids);
    }

    /**
     * 删除红包关联任务用户完成情况信息
     * 
     * @param id 红包关联任务用户完成情况ID
     * @return 结果
     */
    @Override
    public int deleteFwReadPackageTaskContentById(String id)
    {
        return fwReadPackageTaskContentMapper.deleteFwReadPackageTaskContentById(id);
    }
}
