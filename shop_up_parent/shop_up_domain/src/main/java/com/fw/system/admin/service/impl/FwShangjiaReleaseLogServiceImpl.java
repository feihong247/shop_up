package com.fw.system.admin.service.impl;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.thread.ThreadUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.common.Builder;
import com.fw.common.IdXD;
import com.fw.enums.ReleaseLogEnum;
import com.fw.system.admin.domain.FwUser;
import com.fw.system.admin.mapper.FwShangjiaReleaseLogMapper;
import com.fw.system.admin.domain.FwShangjiaReleaseLog;
import com.fw.system.admin.service.IFwShangjiaReleaseLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * <p>
 * 技术商甲管理商甲推广商甲释放明细表 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
@Service
public class FwShangjiaReleaseLogServiceImpl extends ServiceImpl<FwShangjiaReleaseLogMapper, FwShangjiaReleaseLog> implements IFwShangjiaReleaseLogService {

    @Autowired
    private IdXD idXD;
    @Override
    public void createReleaseLog(BigDecimal logCount, FwUser user, ReleaseLogEnum releaseLogEnum) {
        ThreadUtil.execute(()->{
            save(Builder.of(FwShangjiaReleaseLog::new)
                      .with(FwShangjiaReleaseLog::setId,idXD.nextId())
                        .with(FwShangjiaReleaseLog::setCreateTime, LocalDateTimeUtil.now())
                          .with(FwShangjiaReleaseLog::setLogCount,logCount)
                            .with(FwShangjiaReleaseLog::setLogType,releaseLogEnum.getLogType())
                             .with(FwShangjiaReleaseLog::setLogName,releaseLogEnum.getLogName())
                               .with(FwShangjiaReleaseLog::setUserId,user.getId())
                                .with(FwShangjiaReleaseLog::setUserName,user.getUserName())
                                 .with(FwShangjiaReleaseLog::setPhone,user.getPhone())
                           .build());
        });
    }
}
