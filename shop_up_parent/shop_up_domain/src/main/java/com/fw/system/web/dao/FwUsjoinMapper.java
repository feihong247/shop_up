package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwUsjoin;
import com.fw.system.web.model.vo.UsjoinVo;

import java.util.List;

/**
 * <p>
 * 我的商铺收藏中间表 Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-05-24
 */
public interface FwUsjoinMapper extends BaseMapper<FwUsjoin> {
    /**
     * 查询我的商铺收藏中间
     *
     * @param id 我的商铺收藏中间ID
     * @return 我的商铺收藏中间
     */
    public FwUsjoin selectFwUsjoinById(String id);

    /**
     * 查询我的商铺收藏中间列表
     *
     * @param fwUsjoin 我的商铺收藏中间
     * @return 我的商铺收藏中间集合
     */
    public List<UsjoinVo> selectFwUsjoinList(FwUsjoin fwUsjoin);

    /**
     * 新增我的商铺收藏中间
     *
     * @param fwUsjoin 我的商铺收藏中间
     * @return 结果
     */
    public int insertFwUsjoin(FwUsjoin fwUsjoin);

    /**
     * 修改我的商铺收藏中间
     *
     * @param fwUsjoin 我的商铺收藏中间
     * @return 结果
     */
    public int updateFwUsjoin(FwUsjoin fwUsjoin);

    /**
     * 删除我的商铺收藏中间
     *
     * @param id 我的商铺收藏中间ID
     * @return 结果
     */
    public int deleteFwUsjoinById(String id);

    /**
     * 批量删除我的商铺收藏中间
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwUsjoinByIds(String[] ids);
}

