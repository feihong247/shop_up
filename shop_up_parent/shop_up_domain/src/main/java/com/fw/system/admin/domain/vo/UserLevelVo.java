package com.fw.system.admin.domain.vo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel("用户级别返回VO")
public class UserLevelVo implements Serializable {


    @ApiModelProperty("用户编号")
    private String id;

    @ApiModelProperty("用户姓名")
    private String userName;

    @ApiModelProperty("用户手机号")
    private String phone;

    @ApiModelProperty("是否商户 ,1 是，2否")
    private Integer isShop;

    @ApiModelProperty("直推人数")
    private Integer pushCount;

    @ApiModelProperty("等级名称")
    private String levelName;

    @ApiModelProperty("锁仓数")
    private Long lockShangjia;

    @ApiModelProperty(value = "消证")
    private BigDecimal disappear;

    /** 积分_厘为单位 */
    @ApiModelProperty(value = "积分_厘为单位")
    private BigDecimal integral;

    /** 商甲_厘为单位 */
    @ApiModelProperty(value = "shangjia_厘为单位")
    private BigDecimal shangJia;

    /**
     * 邀请码
     */
    @ApiModelProperty("邀请码")
    private String systemHost;

    /** 用户头像 */
    @ApiModelProperty(value = "用户头像")
    private String headImage;


    /** 积分兑换比例 */
    @ApiModelProperty(value = "积分兑换比例")
    private BigDecimal toRelease;


}
