package com.fw.system.web.model.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 营业额表,以此表为依据增长商甲
 * </p>
 *
 * @author
 * @since 2021-06-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwTurnover extends Model<FwTurnover> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    /**
     * 阶段
     */
    @TableField("phase")
    private String phase;

    /**
     * 商甲价格
     */
    @TableField("money")
    private BigDecimal money;

    /**
     * 消费额度
     */
    @TableField("consumption_quota")
    private BigDecimal consumptionQuota;

    /**
     * 增长幅度
     */
    @TableField("growth")
    private BigDecimal growth;

    /**
     * 最终商甲价格
     */
    @TableField("end_money")
    private BigDecimal endMoney;

    /**
     * 标记
     */
    @TableField("count")
    private Integer count;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
