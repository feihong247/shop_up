package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwSku;

/**
 * 商品sku规格Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwSkuService extends IService<FwSku>
{
    /**
     * 查询商品sku规格
     * 
     * @param id 商品sku规格ID
     * @return 商品sku规格
     */
    public FwSku selectFwSkuById(String id);

    /**
     * 查询商品sku规格列表
     * 
     * @param fwSku 商品sku规格
     * @return 商品sku规格集合
     */
    public List<FwSku> selectFwSkuList(FwSku fwSku);

    /**
     * 新增商品sku规格
     * 
     * @param fwSku 商品sku规格
     * @return 结果
     */
    public int insertFwSku(FwSku fwSku);

    /**
     * 修改商品sku规格
     * 
     * @param fwSku 商品sku规格
     * @return 结果
     */
    public int updateFwSku(FwSku fwSku);

    /**
     * 批量删除商品sku规格
     * 
     * @param ids 需要删除的商品sku规格ID
     * @return 结果
     */
    public int deleteFwSkuByIds(String[] ids);

    /**
     * 删除商品sku规格信息
     * 
     * @param id 商品sku规格ID
     * @return 结果
     */
    public int deleteFwSkuById(String id);
}
