package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwFreed;

/**
 * 意见反馈Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwFreedService extends IService<FwFreed>
{
    /**
     * 查询意见反馈
     * 
     * @param id 意见反馈ID
     * @return 意见反馈
     */
    public FwFreed selectFwFreedById(String id);

    /**
     * 查询意见反馈列表
     * 
     * @param fwFreed 意见反馈
     * @return 意见反馈集合
     */
    public List<FwFreed> selectFwFreedList(FwFreed fwFreed);

    /**
     * 新增意见反馈
     * 
     * @param fwFreed 意见反馈
     * @return 结果
     */
    public int insertFwFreed(FwFreed fwFreed);

    /**
     * 修改意见反馈
     * 
     * @param fwFreed 意见反馈
     * @return 结果
     */
    public int updateFwFreed(FwFreed fwFreed);

    /**
     * 批量删除意见反馈
     * 
     * @param ids 需要删除的意见反馈ID
     * @return 结果
     */
    public int deleteFwFreedByIds(String[] ids);

    /**
     * 删除意见反馈信息
     * 
     * @param id 意见反馈ID
     * @return 结果
     */
    public int deleteFwFreedById(String id);
}
