package com.fw.system.admin.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 商甲池对象 fw_shangjipool
 *
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_shangjipool")
@ApiModel(value="商甲池", description="商甲池表")
public class FwShangjipool extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 兑换来源 0=用户，1=系统，3=购买 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "兑换来源 0=用户，1=系统，3=购买")
    @TableField("source")
    private Integer source;

    /** 用户编号 */
    @ApiModelProperty(value = "兑换来源 0=用户，1=系统，3=购买")
    @Excel(name = "用户编号")
    @TableField("user_id")
    private String userId;

    /** 商甲大小 */
    @ApiModelProperty(value = "用户编号")
    @Excel(name = "商甲大小")
    @TableField("freed_shangjia")
    private BigDecimal freedShangjia;

    /** 商甲入池说明 */
    @ApiModelProperty(value = "商甲大小")
    @Excel(name = "商甲入池说明")
    @TableField("pool_remind")
    private String poolRemind;




}
