package com.fw.system.web.service.impl;

import com.fw.system.web.dao.FwAgentRegionMapper;
import com.fw.system.web.model.entity.FwAgentRegion;
import com.fw.system.web.service.IFwAgentRegionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 代理区域表 服务实现类
 * </p>
 *
 * @author
 * @since 2021-06-08
 */
@Service
public class FwAgentRegionServiceImpl extends ServiceImpl<FwAgentRegionMapper, FwAgentRegion> implements IFwAgentRegionService {

}
