package com.fw.system.web.service.impl.v2;
import com.fw.system.web.dao.FwOneAreaItemMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.system.web.model.entity.FwOneAreaItem;
import com.fw.system.web.service.v2.IFwOneAreaItemService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 一县一品表 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-09-29
 */
@Service
public class FwOneAreaItemServiceImpl extends ServiceImpl<FwOneAreaItemMapper, FwOneAreaItem> implements IFwOneAreaItemService {

}
