package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 身份对象 fw_Identity
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_Identity")
@ApiModel(value="身份", description="身份表")
public class FwIdentity extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 身份名称 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "身份名称")
    @TableField("Identity_name")
    private String identityName;

    /** 条件 条件比较复杂，满额升级的，分享升级的，充值升级的 */
    @ApiModelProperty(value = "身份名称")
    @Excel(name = "条件 条件比较复杂，满额升级的，分享升级的，充值升级的")
    @TableField("condition_txt")
    private String conditionTxt;

    /** 条件说明 */
    @ApiModelProperty(value = "条件 条件比较复杂，满额升级的，分享升级的，充值升级的")
    @Excel(name = "条件说明")
    @TableField("condition_info")
    private String conditionInfo;

    /** 身份说明 */
    @ApiModelProperty(value = "条件说明")
    @Excel(name = "身份说明")
    @TableField("Identity_info")
    private String identityInfo;

    /** 身份标识 */
    @ApiModelProperty(value = "身份说明")
    @Excel(name = "身份标识")
    @TableField("sys_code")
    private String sysCode;




}
