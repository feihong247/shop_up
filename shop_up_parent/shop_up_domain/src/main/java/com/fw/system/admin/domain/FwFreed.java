package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 意见反馈对象 fw_freed
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_freed")
@ApiModel(value="意见反馈", description="意见反馈")
public class FwFreed extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 用户编号 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "用户编号")
    @TableField("user_id")
    private String userId;

    /** 反馈类型 */
    @ApiModelProperty(value = "用户编号")
    @Excel(name = "反馈类型")
    @TableField("freed_type")
    private String freedType;

    /** 反馈意见 */
    @ApiModelProperty(value = "反馈类型")
    @Excel(name = "反馈意见")
    @TableField("freed_info")
    private String freedInfo;




}
