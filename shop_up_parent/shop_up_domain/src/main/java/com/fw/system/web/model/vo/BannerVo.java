package com.fw.system.web.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 首页轮播
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BannerVo extends Model<BannerVo> {

    @ApiModelProperty("id")
    private String id;

    /**
     * 轮播标题
     */
    @ApiModelProperty("轮播标题")
    private String bannerTitle;

    /**
     * 轮播封面 图片封面
     */
    @ApiModelProperty("轮播封面")
    private String bannerShow;

    /**
     * 轮播详情
     */
    @ApiModelProperty("轮播详情")
    private String bannerInfos;

    /**
     * 排序 小的优先级高
     */
    @ApiModelProperty("排序")
    private Integer bannerOrder;

    /**
     * 创建人 创建人
     */
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新人 更新人
     */
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;


}
