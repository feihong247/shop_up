package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwTurnover;

/**
 * <p>
 * 营业额表,以此表为依据增长商甲 Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-06-07
 */
public interface FwTurnoverMapper extends BaseMapper<FwTurnover> {

}
