package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 技术商甲管理商甲推广商甲释放明细表
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwShangjiaReleaseLog extends Model<FwShangjiaReleaseLog> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;
    /**
     * 释放的商甲数量
     */
    @TableField("log_count")
    private BigDecimal logCount;

    /**
     * 释放给的人id
     */
    @TableField("user_id")
    private String userId;

    /**
     * 释放给的人姓名
     */
    @TableField("user_name")
    private String userName;

    /**
     * 电话
     */
    @TableField("phone")
    private String phone;

    /**
     * 产出时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 日志名称
     */
    @TableField("log_name")
    private String logName;

    /**
     * 可变type ....
     */
    @TableField("log_type")
    private Integer logType;



    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
