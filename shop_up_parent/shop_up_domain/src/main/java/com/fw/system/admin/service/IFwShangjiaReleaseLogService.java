package com.fw.system.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.enums.ReleaseLogEnum;
import com.fw.system.admin.domain.FwShangjiaReleaseLog;
import com.fw.system.admin.domain.FwUser;

import java.math.BigDecimal;

/**
 * <p>
 * 技术商甲管理商甲推广商甲释放明细表 服务类
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
public interface IFwShangjiaReleaseLogService extends IService<FwShangjiaReleaseLog> {

    void createReleaseLog(BigDecimal logCount, FwUser user, ReleaseLogEnum releaseLogEnum);

}
