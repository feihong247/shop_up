package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 首页轮播Mapper接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface FwBannerMapper extends BaseMapper<FwBanner>
{
    /**
     * 查询首页轮播
     * 
     * @param id 首页轮播ID
     * @return 首页轮播
     */
    public FwBanner selectFwBannerById(String id);

    /**
     * 查询首页轮播列表
     * 
     * @param fwBanner 首页轮播
     * @return 首页轮播集合
     */
    public List<FwBanner> selectFwBannerList(FwBanner fwBanner);

    /**
     * 新增首页轮播
     * 
     * @param fwBanner 首页轮播
     * @return 结果
     */
    public int insertFwBanner(FwBanner fwBanner);

    /**
     * 修改首页轮播
     * 
     * @param fwBanner 首页轮播
     * @return 结果
     */
    public int updateFwBanner(FwBanner fwBanner);

    /**
     * 删除首页轮播
     * 
     * @param id 首页轮播ID
     * @return 结果
     */
    public int deleteFwBannerById(String id);

    /**
     * 批量删除首页轮播
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwBannerByIds(String[] ids);
}
