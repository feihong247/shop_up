package com.fw.system.web.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 我的店铺收藏中间表
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UsjoinVo extends Model<UsjoinVo> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键 主键
     */
    @TableField(exist = false)
    @ApiModelProperty("主键")
    private String id;

    /**
     * 用户编号
     */
    @TableField(exist = false)
    @ApiModelProperty("用户编号")
    private String userId;

    /**
     * 店铺编号
     */
    @TableField(exist = false)
    @ApiModelProperty("店铺编号")
    private String shopId;

    /**
     * 创建人 创建人
     */
    @TableField(exist = false)
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间 创建时间
     */
    @TableField(exist = false)
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 更新人 更新人
     */
    @TableField(exist = false)
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间 更新时间
     */
    @TableField(exist = false)
    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;


    /** 商铺质押多少商甲 */
    @ApiModelProperty(value = "商铺质押多少商甲")
    @TableField("shangjia")
    private BigDecimal shangjia;

    /** 经营类型,存储类型名称 */
    @ApiModelProperty(value = "经营类型,存储类型名称")
    @TableField("operating_name")
    private String operatingName;

    /** 管理人名称 */
    @ApiModelProperty(value = "管理人名称")
    @TableField("manager_name")
    private String managerName;

    /** 管理人电话，数据需要脱敏 */
    @ApiModelProperty(value = "管理人电话")
    @TableField("manager_phone")
    private String managerPhone;

    /** 系统商户编号 */
    @ApiModelProperty(value = "系统商户编号")
    @TableField("shop_host")
    private String shopHost;

    /** 商铺名称 */
    @ApiModelProperty(value = "商铺名称")
    @TableField("shop_name")
    private String shopName;

    /** 商铺简介 */
    @ApiModelProperty(value = "商铺简介")
    @TableField("shop_info")
    private String shopInfo;

    /** 商铺营业执照图片 */
    @ApiModelProperty(value = "商铺营业执照图片")
    @TableField("shop_image")
    private String shopImage;

    /** 数据设定-范围m为单位 */
    @ApiModelProperty(value = "数据设定-范围m为单位")
    @TableField("data_range")
    private String dataRange;

    /** 数据设定-性别,0=女，1=男，2=不限 */
    @ApiModelProperty(value = "数据设定-性别,0=女，1=男，2=不限")
    @TableField("data_sex")
    private Integer dataSex;

    /** 数据设定-年龄设定 */
    @ApiModelProperty(value = "数据设定-年龄设定")
    @TableField("data_age")
    private String dataAge;

    /** 数据设定-收入设定 */
    @ApiModelProperty(value = "数据设定-收入设定")
    @TableField("data_income")
    private String dataIncome;

    /** 商铺状态,0-待审核，1-审核通过，2-审核失败，驳回 */
    @ApiModelProperty(value = "商铺状态,0-待审核，1-审核通过，2-审核失败，驳回")
    @TableField("status")
    private Integer status;

    /** 商铺审核状态为2时，次字段生效 */
    @ApiModelProperty(value = "商铺审核状态为2时，次字段生效")
    @TableField("turndown")
    private String turndown;

    /** 线下消费，比例返回消证给用户，此逻辑，全局的有，如果不再是0。00 证明总管理员对这个商户特殊关照。 */
    @ApiModelProperty(value = "线下消费，比例返回消证给用户，此逻辑，全局的有，如果不再是0。00 证明总管理员对这个商户特殊关照")
    @TableField("line_customer")
    private BigDecimal lineCustomer;

    /** 营业时间 */
    @ApiModelProperty(value = "营业时间")
    @TableField("operating_time")
    private String operatingTime;

    /** 商铺电话 */
    @ApiModelProperty(value = "商铺电话")
    @TableField("shop_mobile")
    private String shopMobile;

    /** 商铺公告 */
    @ApiModelProperty(value = "商铺公告")
    @TableField("shop_notice")
    private String shopNotice;

    /** 商铺logo */
    @ApiModelProperty(value = "商铺logo")
    @TableField("shop_logo")
    private String shopLogo;

    /** 商甲支付编码 */
    @ApiModelProperty(value = "商甲支付编码")
    @TableField("shop_pay")
    private String shopPay;

    /** 线下质押多少商甲 */
    @ApiModelProperty(value = "线下质押多少商甲")
    @TableField("offline_shangjia")
    private BigDecimal offlineShangjia;

    /** 商铺封面 */
    @ApiModelProperty(value = "商铺封面")
    @TableField("shop_cover")
    private String shopCover;

    /** 商铺评分 */
    @ApiModelProperty(value = "商铺评分")
    @TableField("shop_score")
    private Long shopScore;

    /** 商铺平均消费 */
    @ApiModelProperty(value = "商铺平均消费")
    @TableField("shop_svg")
    private String shopSvg;

    /**
     * 图标照片
     */
    @TableField("icon_url")
    private String iconUrl;
}
