package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwDisappearGet;

/**
 * 消证获取Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwDisappearGetService extends IService<FwDisappearGet>
{
    /**
     * 查询消证获取
     * 
     * @param id 消证获取ID
     * @return 消证获取
     */
    public FwDisappearGet selectFwDisappearGetById(String id);

    /**
     * 查询消证获取列表
     * 
     * @param fwDisappearGet 消证获取
     * @return 消证获取集合
     */
    public List<FwDisappearGet> selectFwDisappearGetList(FwDisappearGet fwDisappearGet);

    /**
     * 新增消证获取
     * 
     * @param fwDisappearGet 消证获取
     * @return 结果
     */
    public int insertFwDisappearGet(FwDisappearGet fwDisappearGet);

    /**
     * 修改消证获取
     * 
     * @param fwDisappearGet 消证获取
     * @return 结果
     */
    public int updateFwDisappearGet(FwDisappearGet fwDisappearGet);

    /**
     * 批量删除消证获取
     * 
     * @param ids 需要删除的消证获取ID
     * @return 结果
     */
    public int deleteFwDisappearGetByIds(String[] ids);

    /**
     * 删除消证获取信息
     * 
     * @param id 消证获取ID
     * @return 结果
     */
    public int deleteFwDisappearGetById(String id);
}
