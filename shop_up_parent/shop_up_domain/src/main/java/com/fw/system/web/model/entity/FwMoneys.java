package com.fw.system.web.model.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户钱包表
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwMoneys extends Model<FwMoneys> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("id")
    private String id;

    /**
     * 用户编号
     */
    @TableField("user_id")
    private String userId;

    /**
     * 消证_厘为单位
     */
    @TableField("disappear")
    private BigDecimal disappear;

    /**
     * 积分_厘为单位
     */
    @TableField("integral")
    private BigDecimal integral;

    /**
     * 商甲_厘为单位
     */
    @TableField("shang_jia")
    private BigDecimal shangJia;

    /**
     * 创建人
     */
    @TableField("create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @TableField("update_by")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 持有技术商甲
     */
    @TableField("tec_shangjia")
    private BigDecimal tecShangjia;

    /**
     * 持有管理商甲
     */
    @TableField("man_shangjia")
    private BigDecimal manShangjia;

    public FwMoneys createD(String createId){
        this.createBy = createId;
        this.createTime = LocalDateTime.now();
        if (this.createTime != null)
            this.updateTime = this.createTime;
        this.updateBy = this.createBy;
        return this;
    }

    public FwMoneys updateD(String updateId){
        this.updateBy = updateId;
        this.updateTime = LocalDateTime.now();
        return this;
    }


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
