package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwProblemcate;

/**
 * 常见问题类目Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwProblemcateService extends IService<FwProblemcate>
{
    /**
     * 查询常见问题类目
     * 
     * @param id 常见问题类目ID
     * @return 常见问题类目
     */
    public FwProblemcate selectFwProblemcateById(String id);

    /**
     * 查询常见问题类目列表
     * 
     * @param fwProblemcate 常见问题类目
     * @return 常见问题类目集合
     */
    public List<FwProblemcate> selectFwProblemcateList(FwProblemcate fwProblemcate);

    /**
     * 新增常见问题类目
     * 
     * @param fwProblemcate 常见问题类目
     * @return 结果
     */
    public int insertFwProblemcate(FwProblemcate fwProblemcate);

    /**
     * 修改常见问题类目
     * 
     * @param fwProblemcate 常见问题类目
     * @return 结果
     */
    public int updateFwProblemcate(FwProblemcate fwProblemcate);

    /**
     * 批量删除常见问题类目
     * 
     * @param ids 需要删除的常见问题类目ID
     * @return 结果
     */
    public int deleteFwProblemcateByIds(String[] ids);

    /**
     * 删除常见问题类目信息
     * 
     * @param id 常见问题类目ID
     * @return 结果
     */
    public int deleteFwProblemcateById(String id);
}
