package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户购买代理日志
 * </p>
 *
 * @author
 * @since 2021-06-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwLogShopingAgency extends Model<FwLogShopingAgency> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    /**
     * 用户编号
     */
    @TableField("user_id")
    private String userId;

    /**
     * 所购代理
     */
    @TableField("Identity_id")
    private String identityId;

    /**
     * 审核状态:0已购买待审核,1审核通过,赋予身份,2驳回
     */
    @TableField("is_type")
    private Integer isType;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
