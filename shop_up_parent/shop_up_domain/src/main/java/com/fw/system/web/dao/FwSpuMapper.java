package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwSpu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwSpuMapper extends BaseMapper<FwSpu> {

}
