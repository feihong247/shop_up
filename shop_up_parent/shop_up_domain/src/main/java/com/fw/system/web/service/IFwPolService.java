package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwPol;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 地图表 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface IFwPolService extends IService<FwPol> {

}
