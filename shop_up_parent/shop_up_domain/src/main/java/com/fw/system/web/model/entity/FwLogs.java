package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 业务日志
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwLogs extends Model<FwLogs> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键 主键
     */
    @TableId("id")
    private String id;

    /**
     * 业务编号
     */
    @TableField("bui_id")
    private String buiId;

    /**
     * 类型
     */
    @TableField("is_type")
    private String isType;

    /**
     * 模块编号
     */
    @TableField("model_name")
    private String modelName;

    /**
     * 大文本记录内容
     */
    @TableField("json_txt")
    private String jsonTxt;

    /**
     * 自定义数据
     */
    @TableField("form_info")
    private String formInfo;

    /**
     * 创建人 创建人
     */
    @TableField("create_by")
    private String createBy;

    /**
     * 创建时间 创建时间
     */
    @TableField("create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新人 更新人
     */
    @TableField("update_by")
    private String updateBy;

    /**
     * 更新时间 更新时间
     */
    @TableField("update_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 电话
     */
    @TableField(exist = false)
    @ApiModelProperty("电话")
    private String phone;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
