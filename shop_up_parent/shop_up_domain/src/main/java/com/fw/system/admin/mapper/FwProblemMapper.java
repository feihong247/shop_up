package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwProblem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 常见问题Mapper接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface FwProblemMapper extends BaseMapper<FwProblem>
{
    /**
     * 查询常见问题
     * 
     * @param id 常见问题ID
     * @return 常见问题
     */
    public FwProblem selectFwProblemById(String id);

    /**
     * 查询常见问题列表
     * 
     * @param fwProblem 常见问题
     * @return 常见问题集合
     */
    public List<FwProblem> selectFwProblemList(FwProblem fwProblem);

    /**
     * 新增常见问题
     * 
     * @param fwProblem 常见问题
     * @return 结果
     */
    public int insertFwProblem(FwProblem fwProblem);

    /**
     * 修改常见问题
     * 
     * @param fwProblem 常见问题
     * @return 结果
     */
    public int updateFwProblem(FwProblem fwProblem);

    /**
     * 删除常见问题
     * 
     * @param id 常见问题ID
     * @return 结果
     */
    public int deleteFwProblemById(String id);

    /**
     * 批量删除常见问题
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwProblemByIds(String[] ids);
}
