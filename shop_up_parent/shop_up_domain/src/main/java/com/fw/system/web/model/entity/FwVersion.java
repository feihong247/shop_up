package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 版本表
 * </p>
 *
 * @author  
 * @since 2021-09-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwVersion extends Model<FwVersion> {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    /**
     * 版本名称
     */
    @TableField("name")
    @ApiModelProperty("版本名称")
    private String name;

    /**
     * 版本号
     */
    @TableField("version_num")
    @ApiModelProperty("版本名称")
    private String versionNum;

    /**
     * 发布时间
     */
    @TableField("create_time")
    @ApiModelProperty("发布时间")
    private LocalDateTime createTime;

    /**
     * 版本机型 1安卓 2苹果 3鸿蒙
     */
    @TableField("is_type")
    @ApiModelProperty("版本机型 1安卓 2苹果 3鸿蒙")
    private Integer isType;

    /**
     * 更新说明
     */
    @TableField("ver_content")
    @ApiModelProperty("更新说明")
    private String verContent;

    @TableField("upload_uri")
    @ApiModelProperty("下载链接")
    private String uploadUri;


    /**
     * 0 需要强制更新 1不需要
     */
    @TableField("is_state")
    @ApiModelProperty("0 需要强制更新 1不需要")
    private Integer isState;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
