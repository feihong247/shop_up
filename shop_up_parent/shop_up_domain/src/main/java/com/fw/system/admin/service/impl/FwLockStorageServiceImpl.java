package com.fw.system.admin.service.impl;

import java.util.List;

import com.fw.common.IdXD;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwLockStorageMapper;
import com.fw.system.admin.domain.FwLockStorage;
import com.fw.system.admin.service.IFwLockStorageService;

/**
 * 锁仓池Service业务层处理
 *
 * @author yanwei
 * @date 2021-06-28
 */
@Service
public class FwLockStorageServiceImpl extends ServiceImpl<FwLockStorageMapper, FwLockStorage> implements IFwLockStorageService {
    @Autowired
    private FwLockStorageMapper fwLockStorageMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询锁仓池
     *
     * @param id 锁仓池ID
     * @return 锁仓池
     */
    @Override
    public FwLockStorage selectFwLockStorageById(String id) {
        return fwLockStorageMapper.selectFwLockStorageById(id);
    }

    /**
     * 查询锁仓池列表
     *
     * @param fwLockStorage 锁仓池
     * @return 锁仓池
     */
    @Override
    public List<FwLockStorage> selectFwLockStorageList(FwLockStorage fwLockStorage) {
        return fwLockStorageMapper.selectFwLockStorageList(fwLockStorage);
    }

    /**
     * 新增锁仓池
     *
     * @param fwLockStorage 锁仓池
     * @return 结果
     */
    @Override
    public int insertFwLockStorage(FwLockStorage fwLockStorage) {
        fwLockStorage.setId(idXD.nextId());
        return fwLockStorageMapper.insertFwLockStorage(fwLockStorage);
    }

    /**
     * 修改锁仓池
     *
     * @param fwLockStorage 锁仓池
     * @return 结果
     */
    @Override
    public int updateFwLockStorage(FwLockStorage fwLockStorage) {
        return fwLockStorageMapper.updateFwLockStorage(fwLockStorage);
    }

    /**
     * 批量删除锁仓池
     *
     * @param ids 需要删除的锁仓池ID
     * @return 结果
     */
    @Override
    public int deleteFwLockStorageByIds(String[] ids) {
        return fwLockStorageMapper.deleteFwLockStorageByIds(ids);
    }

    /**
     * 删除锁仓池信息
     *
     * @param id 锁仓池ID
     * @return 结果
     */
    @Override
    public int deleteFwLockStorageById(String id) {
        return fwLockStorageMapper.deleteFwLockStorageById(id);
    }
}
