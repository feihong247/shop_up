package com.fw.system.web.model.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * 系统公告&通知
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
@Api(tags = "系统公告&通知")
public class NoticeVo {

    /**
     * 主键
     */
    @ApiModelProperty("id")
    private String id;

    /**
     * 标题
     */
    @ApiModelProperty("标题")
    private String noticeTitle;

    /**
     * 类型，0=公告，1=通知
     */
    @ApiModelProperty("类型，0=公告，1=通知")
    private Boolean noticeType;

    /**
     * 内容
     */
    @ApiModelProperty("内容")
    private String content;

    /**
     * 封面
     */
    @ApiModelProperty("封面")
    private String urlImage;

    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;



}
