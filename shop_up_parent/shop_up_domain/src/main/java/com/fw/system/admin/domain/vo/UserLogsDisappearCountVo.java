package com.fw.system.admin.domain.vo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Api(tags = "首页统计项")
public class UserLogsDisappearCountVo {

    @ApiModelProperty("用户编号")
    private String id;
    @ApiModelProperty("用户姓名")
    private String userName;
    @ApiModelProperty("用户电话")
    private String phone;
    @ApiModelProperty("消证总额")
    private String formInfo;
    @ApiModelProperty("今日消证释放量")
    private double toDayDisappearCount;
    @ApiModelProperty("今日积分量")
    private double toDayIntegralCount;
}
