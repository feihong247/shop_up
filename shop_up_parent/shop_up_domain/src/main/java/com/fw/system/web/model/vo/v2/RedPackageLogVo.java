package com.fw.system.web.model.vo.v2;

import com.fw.system.web.model.entity.FwReadPackageLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("用户领取红包及领取情况封装")
public class RedPackageLogVo extends FwReadPackageLog {

    @ApiModelProperty("true 第一次领取 false =不是第一次领取 ")
    private boolean checkState = false ;
    @ApiModelProperty("红包差额")
    private BigDecimal difference;

    @ApiModelProperty("详情页面是否能拆")
    private boolean isOpen = false;
}
