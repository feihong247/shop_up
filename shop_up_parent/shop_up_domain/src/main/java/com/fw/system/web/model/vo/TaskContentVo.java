package com.fw.system.web.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("红包任务列表和用户完成情况封装")
public class TaskContentVo {


    @TableId("id")
    @ApiModelProperty("任务id")
    private String id;

    /**
     * 任务类型名称
     */
    @TableField("task_name")
    @ApiModelProperty("任务类型名称")
    private String taskName;

    /**
     * 任务可以完成的次数
     */
    @TableField("task_num")
    @ApiModelProperty("任务应该完成的次数")
    private Integer taskNum;

    /**
     * 用户已完成的次数
     */
    @TableField("user_num")
    @ApiModelProperty("用户已完成的次数")
    private Integer userNum;


    @ApiModelProperty("任务增加金额")
    private BigDecimal taskAmount;


}
