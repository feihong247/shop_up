package com.fw.system.web.model.vo.v2;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Api("评价查询返回VO")
public class EvaluationV2Vo implements Serializable {


    /**
     * 主键 主键
     */
    @ApiModelProperty("主键")
    private String id;

    /**
     * 用户编号
     */
    @ApiModelProperty("用户编号")
    private String userId;

    /**
     * 商品编号
     */
    @ApiModelProperty("商品编号")
    private String itemId;

    /**
     * 评价图片,多个以逗号分隔
     */
    @ApiModelProperty("评价图片")
    private String evaImg;

    /**
     * 评价内容
     */
    @ApiModelProperty("评价内容")
    private String infos;

    /**
     * 评价星级 0-5之间
     */
    @ApiModelProperty("评价星级 0-5之间")
    private Integer starNum;

    /**
     * 时间描述
     */
    @ApiModelProperty("时间描述,例如 3小时前")
    private String timeDesc;



    /**
     * 用户名称
     */
    @ApiModelProperty("用户名称")
    private String userName;


    /**
     * 用户头像
     */
    @ApiModelProperty("用户头像")
    private String headImage;



    /**
     * 创建时间 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 订单编号
     */
    @ApiModelProperty("订单编号")
    private String orderId;



}
