package com.fw.system.web.model.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MoneyQuery extends PageQuery {


    @ApiModelProperty("1:消证 2积分")
    private Integer type;
}
