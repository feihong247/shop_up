package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 商甲发行对象 fw_shangjia
 *
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_shangjia")
@ApiModel(value="商甲发行", description="商甲发行表")
public class FwShangjia extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 商甲名称 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "商甲名称")
    @TableField("shangjia_name")
    private String shangjiaName;

    /** 商甲额度，厘为单位 */
    @ApiModelProperty(value = "商甲名称")
    @Excel(name = "商甲额度，厘为单位")
    @TableField("shangjia_quota")
    private BigDecimal shangjiaQuota;

    /** 发行时间 */
    @ApiModelProperty(value = "商甲额度，厘为单位")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发行时间", width = 30, dateFormat = "yyyy-MM-dd")
    @TableField("shangjia_publish_data")
    private Date shangjiaPublishData;

    /** 商甲实际剩余额度,发放商甲时,更改此数据 */
    @ApiModelProperty(value = "发行时间")
    @Excel(name = "商甲实际剩余额度,发放商甲时,更改此数据")
    @TableField("shop_issue")
    private BigDecimal shopIssue;

    /** 商甲现价 */
    @ApiModelProperty(value = "商甲实际剩余额度,发放商甲时,更改此数据")
    @Excel(name = "商甲现价")
    @TableField("present_price")
    private BigDecimal presentPrice;

    @ApiModelProperty(value = "各个仓销毁数量")
    @TableField("delete_num")
    private BigDecimal deleteNum;






}
