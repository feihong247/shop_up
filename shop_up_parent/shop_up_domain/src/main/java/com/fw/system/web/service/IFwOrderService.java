package com.fw.system.web.service;

import com.fw.mes.Result;
import com.fw.system.web.model.entity.FwOrder;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwUser;
import com.fw.system.web.model.vo.v2.OrderMoneyVo;

import java.util.List;

/**
 * <p>
 * 订单 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface IFwOrderService extends IService<FwOrder> {
    /**
     * 完善订单
     *
     * @param order 订单
     * @return 订单信息
     * @Author 姚
     * @Date 2021/6/16
     **/
    FwOrder perfectOrder(FwOrder order);

    /**
     *  给商户钱
     *
     * @param
     * @return void
     * @author 姚自强
     * @date 2021/8/7
     */
    String payToShop(FwOrder order, String shopId);

    List<OrderMoneyVo> manageOnlineOrder(String shopId, Integer queryRange, Integer limit);

    void pubPackage(String id, FwUser user);

    Result confirmReceipt(String orderId);
}
