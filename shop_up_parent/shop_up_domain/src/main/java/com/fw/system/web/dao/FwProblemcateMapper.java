package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwProblemcate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 常见问题类目 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwProblemcateMapper extends BaseMapper<FwProblemcate> {

}
