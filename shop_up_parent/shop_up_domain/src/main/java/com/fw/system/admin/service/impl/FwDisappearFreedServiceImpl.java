package com.fw.system.admin.service.impl;

import java.util.List;

import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwDisappearFreedMapper;
import com.fw.system.admin.domain.FwDisappearFreed;
import com.fw.system.admin.service.IFwDisappearFreedService;

/**
 * 消证兑换Service业务层处理
 *
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwDisappearFreedServiceImpl extends ServiceImpl<FwDisappearFreedMapper, FwDisappearFreed> implements IFwDisappearFreedService {
    @Autowired
    private FwDisappearFreedMapper fwDisappearFreedMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询消证兑换
     *
     * @param id 消证兑换ID
     * @return 消证兑换
     */
    @Override
    public FwDisappearFreed selectFwDisappearFreedById(String id) {
        return fwDisappearFreedMapper.selectFwDisappearFreedById(id);
    }

    /**
     * 查询消证兑换列表
     *
     * @param fwDisappearFreed 消证兑换
     * @return 消证兑换
     */
    @Override
    public List<FwDisappearFreed> selectFwDisappearFreedList(FwDisappearFreed fwDisappearFreed) {
        return fwDisappearFreedMapper.selectFwDisappearFreedList(fwDisappearFreed);
    }

    /**
     * 新增消证兑换
     *
     * @param fwDisappearFreed 消证兑换
     * @return 结果
     */
    @Override
    public int insertFwDisappearFreed(FwDisappearFreed fwDisappearFreed) {
        fwDisappearFreed.setCreateTime(DateUtils.getNowDate());
        fwDisappearFreed.setId(idXD.nextId());
        return fwDisappearFreedMapper.insertFwDisappearFreed(fwDisappearFreed);
    }

    /**
     * 修改消证兑换
     *
     * @param fwDisappearFreed 消证兑换
     * @return 结果
     */
    @Override
    public int updateFwDisappearFreed(FwDisappearFreed fwDisappearFreed) {
        fwDisappearFreed.setUpdateTime(DateUtils.getNowDate());
        return fwDisappearFreedMapper.updateFwDisappearFreed(fwDisappearFreed);
    }

    /**
     * 批量删除消证兑换
     *
     * @param ids 需要删除的消证兑换ID
     * @return 结果
     */
    @Override
    public int deleteFwDisappearFreedByIds(String[] ids) {
        return fwDisappearFreedMapper.deleteFwDisappearFreedByIds(ids);
    }

    /**
     * 删除消证兑换信息
     *
     * @param id 消证兑换ID
     * @return 结果
     */
    @Override
    public int deleteFwDisappearFreedById(String id) {
        return fwDisappearFreedMapper.deleteFwDisappearFreedById(id);
    }
}
