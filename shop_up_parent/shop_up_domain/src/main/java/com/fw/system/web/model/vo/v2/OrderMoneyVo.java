package com.fw.system.web.model.vo.v2;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 线上分析
 */
@Data
@ApiModel("财务管理_订单 日 月 年 返回VO")
public class OrderMoneyVo implements Serializable {

    @ApiModelProperty("日期名 2021，08，12 等 ")
    private String lable;

    @ApiModelProperty("值")
    private BigDecimal moneyVal;


}
