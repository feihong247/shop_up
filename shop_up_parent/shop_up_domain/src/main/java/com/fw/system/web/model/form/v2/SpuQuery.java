package com.fw.system.web.model.form.v2;

import com.fw.system.web.model.form.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("商户 查询商品列表")
public class SpuQuery extends PageQuery {

    /** 是否竞猜(0线上1线下) */
    @ApiModelProperty("是否竞猜(0否1是) （-1全部）")
    private Integer isQuiz = -1;

    @ApiModelProperty("类目编号")
    private String category = "";

    @ApiModelProperty("是否热卖 （-1全部）")
    private Integer hotSale = -1;

    @ApiModelProperty("是否推荐 （-1全部）")
    private Integer recommend = -1;

    /**
     * 商品状态 0=正常，1=下架
     */
    @ApiModelProperty("商品状态 0=正常，1=下架  （-1 全部）")
    private Integer status;

    @ApiModelProperty("商品标题")
    private String title;

    @ApiModelProperty("是否线下(0否1是),必传 （-1全部）")
    private Integer isOffline = -1;
}
