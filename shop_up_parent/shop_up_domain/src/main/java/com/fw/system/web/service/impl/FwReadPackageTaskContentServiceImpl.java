package com.fw.system.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.system.web.dao.FwReadPackageTaskContentMapper;
import com.fw.system.web.model.entity.FwReadPackageTaskContent;
import com.fw.system.web.service.IFwReadPackageTaskContentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 红包关联任务用户完成情况表 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-09-03
 */
@Service
public class FwReadPackageTaskContentServiceImpl extends ServiceImpl<FwReadPackageTaskContentMapper, FwReadPackageTaskContent> implements IFwReadPackageTaskContentService {

}
