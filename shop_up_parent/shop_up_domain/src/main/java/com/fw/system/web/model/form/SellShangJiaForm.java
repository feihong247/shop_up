package com.fw.system.web.model.form;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 卖出商甲form
 */
@Data
@Api(tags = "卖出商甲form")
public class SellShangJiaForm {
    @ApiModelProperty("卖出单价")
    private BigDecimal money;

    @ApiModelProperty("卖出数量")
    private BigDecimal count;

}
