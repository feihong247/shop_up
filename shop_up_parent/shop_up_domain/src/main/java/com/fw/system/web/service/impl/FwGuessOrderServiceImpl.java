package com.fw.system.web.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import com.fw.common.IdXD;
import com.fw.system.web.dao.FwGuessOrderMapper;
import com.fw.system.web.model.entity.FwGuessOrder;
import com.fw.system.web.service.IFwGuessOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;


/**
 * 竞猜订单Service业务层处理
 *
 * @author yanwei
 * @date 2021-06-15
 */
@Service
public class FwGuessOrderServiceImpl extends ServiceImpl<FwGuessOrderMapper, FwGuessOrder> implements IFwGuessOrderService {
    @Autowired
    private FwGuessOrderMapper fwGuessOrderMapper;
    @Autowired
    private IdXD idXD;

    /**
     * 查询竞猜订单
     *
     * @param guessOrderId 竞猜订单ID
     * @return 竞猜订单
     */
    @Override
    public FwGuessOrder selectFwGuessOrderById(String guessOrderId) {
        return fwGuessOrderMapper.selectFwGuessOrderById(guessOrderId);
    }

    /**
     * 查询竞猜订单列表
     *
     * @param fwGuessOrder 竞猜订单
     * @return 竞猜订单
     */
    @Override
    public List<FwGuessOrder> selectFwGuessOrderList(FwGuessOrder fwGuessOrder) {
        return fwGuessOrderMapper.selectFwGuessOrderList(fwGuessOrder);
    }

    /**
     * 新增竞猜订单
     *
     * @param fwGuessOrder 竞猜订单
     * @return 结果
     */
    @Override
    public int insertFwGuessOrder(FwGuessOrder fwGuessOrder) {
        fwGuessOrder.setCreateTime(LocalDateTime.now());
        fwGuessOrder.setGuessOrderId(idXD.nextId());
        return fwGuessOrderMapper.insertFwGuessOrder(fwGuessOrder);
    }

    /**
     * 修改竞猜订单
     *
     * @param fwGuessOrder 竞猜订单
     * @return 结果
     */
    @Override
    public int updateFwGuessOrder(FwGuessOrder fwGuessOrder) {
        fwGuessOrder.setUpdateTime(LocalDateTime.now());
        return fwGuessOrderMapper.updateFwGuessOrder(fwGuessOrder);
    }

    /**
     * 批量删除竞猜订单
     *
     * @param guessOrderIds 需要删除的竞猜订单ID
     * @return 结果
     */
    @Override
    public int deleteFwGuessOrderByIds(String[] guessOrderIds) {
        return fwGuessOrderMapper.deleteFwGuessOrderByIds(guessOrderIds);
    }

    /**
     * 删除竞猜订单信息
     *
     * @param guessOrderId 竞猜订单ID
     * @return 结果
     */
    @Override
    public int deleteFwGuessOrderById(String guessOrderId) {
        return fwGuessOrderMapper.deleteFwGuessOrderById(guessOrderId);
    }
}
