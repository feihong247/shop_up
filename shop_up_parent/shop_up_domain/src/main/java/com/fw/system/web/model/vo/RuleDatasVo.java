package com.fw.system.web.model.vo;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 数据规则表
 * </p>
 *
 * @author
 * @since 2021-06-02
 */
@Data
@Api(tags = "数据规则vo")
public class RuleDatasVo {

    /**
     * 主键id
     */
    @ApiModelProperty("id")
    private String id;

    /**
     * 规则数据名称
     */
    @ApiModelProperty("规则数据名称")
    private String ruleName;

    /**
     * 数值
     */
    @ApiModelProperty("数值")
    private String ruleCount;

    /**
     * 规则类型
     */
    @ApiModelProperty("规则类型")
    private Integer ruleType;



}
