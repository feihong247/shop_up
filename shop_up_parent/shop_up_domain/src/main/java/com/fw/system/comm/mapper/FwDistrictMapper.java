package com.fw.system.comm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.comm.mode.entity.District;
import com.fw.system.comm.mode.vo.DistrictVo;
import com.fw.system.web.model.entity.FwAddrs;

import java.util.List;

/**
 * <p>
 * 用户收货地址表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwDistrictMapper extends BaseMapper<District> {

    List<DistrictVo> findAll();
}
