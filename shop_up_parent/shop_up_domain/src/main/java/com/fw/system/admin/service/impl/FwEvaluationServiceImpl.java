package com.fw.system.admin.service.impl;

import java.util.List;

import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwEvaluationMapper;
import com.fw.system.admin.domain.FwEvaluation;
import com.fw.system.admin.service.IFwEvaluationService;

/**
 * 商品评价Service业务层处理
 *
 * @author yanwei
 * @date 2021-05-24
 */
@Service
public class FwEvaluationServiceImpl extends ServiceImpl<FwEvaluationMapper, FwEvaluation> implements IFwEvaluationService {
    @Autowired
    private FwEvaluationMapper fwEvaluationMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询商品评价
     *
     * @param id 商品评价ID
     * @return 商品评价
     */
    @Override
    public FwEvaluation selectFwEvaluationById(String id) {
        return fwEvaluationMapper.selectFwEvaluationById(id);
    }

    /**
     * 查询商品评价列表
     *
     * @param fwEvaluation 商品评价
     * @return 商品评价
     */
    @Override
    public List<FwEvaluation> selectFwEvaluationList(FwEvaluation fwEvaluation) {
        return fwEvaluationMapper.selectFwEvaluationList(fwEvaluation);
    }

    /**
     * 新增商品评价
     *
     * @param fwEvaluation 商品评价
     * @return 结果
     */
    @Override
    public int insertFwEvaluation(FwEvaluation fwEvaluation) {
        fwEvaluation.setCreateTime(DateUtils.getNowDate());
        fwEvaluation.setId(idXD.nextId());
        return fwEvaluationMapper.insertFwEvaluation(fwEvaluation);
    }

    /**
     * 修改商品评价
     *
     * @param fwEvaluation 商品评价
     * @return 结果
     */
    @Override
    public int updateFwEvaluation(FwEvaluation fwEvaluation) {
        fwEvaluation.setUpdateTime(DateUtils.getNowDate());
        return fwEvaluationMapper.updateFwEvaluation(fwEvaluation);
    }

    /**
     * 批量删除商品评价
     *
     * @param ids 需要删除的商品评价ID
     * @return 结果
     */
    @Override
    public int deleteFwEvaluationByIds(String[] ids) {
        return fwEvaluationMapper.deleteFwEvaluationByIds(ids);
    }

    /**
     * 删除商品评价信息
     *
     * @param id 商品评价ID
     * @return 结果
     */
    @Override
    public int deleteFwEvaluationById(String id) {
        return fwEvaluationMapper.deleteFwEvaluationById(id);
    }
}
