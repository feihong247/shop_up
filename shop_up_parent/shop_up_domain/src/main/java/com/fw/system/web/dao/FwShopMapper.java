package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwShop;
import com.fw.system.web.model.form.ShopQuery;
import com.fw.system.web.model.vo.ShopOnLineVo;

import java.util.List;

/**
 * <p>
 * 商铺表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwShopMapper extends BaseMapper<FwShop> {

    List<ShopOnLineVo> shopOnlineQuery(ShopQuery shopQuery);

}
