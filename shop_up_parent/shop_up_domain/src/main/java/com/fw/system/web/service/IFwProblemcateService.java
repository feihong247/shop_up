package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwProblemcate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 常见问题类目 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface IFwProblemcateService extends IService<FwProblemcate> {

}
