package com.fw.system.admin.mapper;

import java.util.List;
import com.fw.system.admin.domain.FwDisappearGet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 消证获取Mapper接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface FwDisappearGetMapper extends BaseMapper<FwDisappearGet>
{
    /**
     * 查询消证获取
     * 
     * @param id 消证获取ID
     * @return 消证获取
     */
    public FwDisappearGet selectFwDisappearGetById(String id);

    /**
     * 查询消证获取列表
     * 
     * @param fwDisappearGet 消证获取
     * @return 消证获取集合
     */
    public List<FwDisappearGet> selectFwDisappearGetList(FwDisappearGet fwDisappearGet);

    /**
     * 新增消证获取
     * 
     * @param fwDisappearGet 消证获取
     * @return 结果
     */
    public int insertFwDisappearGet(FwDisappearGet fwDisappearGet);

    /**
     * 修改消证获取
     * 
     * @param fwDisappearGet 消证获取
     * @return 结果
     */
    public int updateFwDisappearGet(FwDisappearGet fwDisappearGet);

    /**
     * 删除消证获取
     * 
     * @param id 消证获取ID
     * @return 结果
     */
    public int deleteFwDisappearGetById(String id);

    /**
     * 批量删除消证获取
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwDisappearGetByIds(String[] ids);
}
