package com.fw.system.web.service.impl;

import com.fw.system.web.model.entity.FwProblemcate;
import com.fw.system.web.dao.FwProblemcateMapper;
import com.fw.system.web.service.IFwProblemcateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 常见问题类目 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Service
public class FwProblemcateServiceImpl extends ServiceImpl<FwProblemcateMapper, FwProblemcate> implements IFwProblemcateService {

}
