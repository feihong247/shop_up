package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwOsjoin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单与spu中间表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwOsjoinMapper extends BaseMapper<FwOsjoin> {

}
