package com.fw.system.web.model.vo;

import com.fw.system.web.model.entity.FwLogs;
import com.fw.system.web.model.entity.FwShangjiaDeal;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@Api(tags = "商甲-我要卖vo")
public class SellVo {
    @ApiModelProperty("卖出记录")
    private List<FwShangjiaDeal> logsList;
    @ApiModelProperty("我的商甲余额")
    private BigDecimal moneyShangJia;
    @ApiModelProperty("市场价")
    private BigDecimal presentPrice;
}
