package com.fw.system.web.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.common.Builder;
import com.fw.common.IdXD;
import com.fw.system.web.model.entity.FwDisappearFreed;
import com.fw.system.web.dao.FwDisappearFreedMapper;
import com.fw.system.web.service.IFwDisappearFreedService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 消证兑换表 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Service
public class FwDisappearFreedServiceImpl extends ServiceImpl<FwDisappearFreedMapper, FwDisappearFreed> implements IFwDisappearFreedService {

    @Autowired
    private IdXD idXD;

    @Override
    public void createFreed(BigDecimal disappear,Integer isAccele,String userId,String info) {
        save(Builder.of(FwDisappearFreed::new)
                 .with(FwDisappearFreed::setId,idXD.nextId())
                  .with(FwDisappearFreed::setDisappear,disappear)
                    .with(FwDisappearFreed::setIsAccelerate,isAccele)
                     .with(FwDisappearFreed::setFreedType,info)
                      .with(FwDisappearFreed::setUserId,userId)
                       .with(FwDisappearFreed::createD,userId)
                 .build());
    }

    /**
     * 今日是否参与普通分红？
     * @param userId
     * @return
     */
    @Override
    public boolean isFlag(String userId){
        List<FwDisappearFreed> fwDisappearFreeds = list(Wrappers.<FwDisappearFreed>lambdaQuery().eq(FwDisappearFreed::getUserId, userId).eq(FwDisappearFreed::getIsAccelerate, 0).apply(" date_format(create_time,'%Y-%m-%d') = {0}", DateUtils.getDate()));
        return fwDisappearFreeds.isEmpty();

    }
}
