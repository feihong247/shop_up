package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwParentOrder;

public interface FwParentOrderMapper extends BaseMapper<FwParentOrder> {
}
