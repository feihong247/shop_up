package com.fw.system.web.model.form;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Effect 预订单Form
 * @Author 姚自强
 * @Date 2021/6/9
 **/
@Data
@ApiModel("预订单Form")
public class ReadyOrderForm {


    private String skuId;

    @ApiModelProperty("购买数量")
    private Integer number;

    /** 地址Id */
    @ApiModelProperty("地址Id")
    private String addrId;
}
