package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwSpucate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品类目 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface IFwSpucateService extends IService<FwSpucate> {

}
