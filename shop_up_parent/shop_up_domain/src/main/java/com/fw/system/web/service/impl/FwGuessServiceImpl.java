package com.fw.system.web.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import com.fw.common.IdXD;
import com.fw.system.web.dao.FwGuessMapper;
import com.fw.system.web.model.entity.FwGuess;
import com.fw.system.web.service.IFwGuessService;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 竞猜Service业务层处理
 * 
 * @author yanwei
 * @date 2021-06-23
 */
@Service
public class FwGuessServiceImpl extends ServiceImpl<FwGuessMapper, FwGuess> implements IFwGuessService {
    @Autowired
    private FwGuessMapper fwGuessMapper;
    @Autowired
    private IdXD idXD;

    /**
     * 查询竞猜
     * 
     * @param guessId 竞猜ID
     * @return 竞猜
     */
    @Override
    public FwGuess selectFwGuessById(String guessId)
    {
        return fwGuessMapper.selectFwGuessById(guessId);
    }

    /**
     * 查询竞猜列表
     * 
     * @param fwGuess 竞猜
     * @return 竞猜
     */
    @Override
    public List<FwGuess> selectFwGuessList(FwGuess fwGuess)
    {
        return fwGuessMapper.selectFwGuessList(fwGuess);
    }

    /**
     * 新增竞猜
     * 
     * @param fwGuess 竞猜
     * @return 结果
     */
    @Override
    public int insertFwGuess(FwGuess fwGuess) {
        fwGuess.setCreateTime(LocalDateTime.now());
        fwGuess.setGuessId(idXD.nextId());
        return fwGuessMapper.insertFwGuess(fwGuess);
    }

    /**
     * 修改竞猜
     * 
     * @param fwGuess 竞猜
     * @return 结果
     */
    @Override
    public int updateFwGuess(FwGuess fwGuess)
    {
        fwGuess.setUpdateTime(LocalDateTime.now());
        return fwGuessMapper.updateFwGuess(fwGuess);
    }

    /**
     * 批量删除竞猜
     * 
     * @param guessIds 需要删除的竞猜ID
     * @return 结果
     */
    @Override
    public int deleteFwGuessByIds(String[] guessIds)
    {
        return fwGuessMapper.deleteFwGuessByIds(guessIds);
    }

    /**
     * 删除竞猜信息
     * 
     * @param guessId 竞猜ID
     * @return 结果
     */
    @Override
    public int deleteFwGuessById(String guessId)
    {
        return fwGuessMapper.deleteFwGuessById(guessId);
    }
}
