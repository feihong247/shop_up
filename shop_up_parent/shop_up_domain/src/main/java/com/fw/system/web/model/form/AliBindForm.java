package com.fw.system.web.model.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@ApiModel("支付宝绑定表单")
public class AliBindForm implements Serializable {

    @ApiModelProperty(value = "真实姓名",required = true)
    @NotBlank(message = "真实姓名 不可为空")
    private String userName;

    @ApiModelProperty(value = "身份证号",required = true)
    @NotBlank(message = "身份证号 不可为空")
    private String certNo;

    @ApiModelProperty(value = "支付宝号",required = true)
    @NotBlank(message = "支付宝号 不可为空")
    private String logonId;

}
