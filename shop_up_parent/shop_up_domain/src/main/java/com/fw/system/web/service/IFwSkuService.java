package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwSku;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品sku规格表 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface IFwSkuService extends IService<FwSku> {

    /**
     * 获取剩余库存
     * @Author 姚
     * @Date 2021/7/6
     * @return 剩余库存
     * @param skuId 规格ID
     **/
    Integer getBalance(String skuId);

    /**
     * 减库存
     * @Author 姚
     * @Date 2021/7/6
     * @param skuId guigeID
     * @param buyNum 购买数量
     * @return 是否减成功
     **/
    boolean subtractBalance(String skuId, Integer buyNum);

    /**
     * 加库存（取消订单等）
     * @Effect
     * @Author 姚
     * @Date 2021/7/7
     * @param skuId guigeID
     * @param buyNum 购买数量
     * @return 是否加成功
     **/
    boolean addBalance(String skuId, Integer buyNum);

}
