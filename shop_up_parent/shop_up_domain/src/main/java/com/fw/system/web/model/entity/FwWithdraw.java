package com.fw.system.web.model.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户提现表
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class FwWithdraw extends Model<FwWithdraw> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("id")
    private String id;

    /**
     * 用户编号
     */
    @TableField("user_id")
    private String userId;

    /**
     * 提现积分
     */
    @TableField("integral")
    private BigDecimal integral;

    /**
     * 实际提现金额
     */
    @TableField("money")
    private BigDecimal money;

    /**
     * 审核状态,0=待审核，1=审核成功，2=审核失败
     */
    @TableField("status")
    private Integer status;

    /**
     * 提现备注_后管填写
     */
    @TableField("remark")
    private String remark;

    /**
     * 创建人
     */
    @TableField("create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @TableField("update_by")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
