package com.fw.system.comm.controller;

import com.fw.mes.Result;
import com.fw.system.comm.mode.vo.DistrictVo;
import com.fw.system.comm.service.DistrictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.fw.mes.ResultUtils.success;

@RestController()
@Api(tags = "省市区街道相关api")
@RequiredArgsConstructor(onConstructor_= {@Autowired})
@RequestMapping("/district")
public class DistrictController {
    private final DistrictService districtService;

    @GetMapping("/findAll")
    @ApiOperation("查询所有数据")
    public Result<List<DistrictVo>> findAll(){
        return success(districtService.findAll());
    }

    @GetMapping("asyncDistrict")
    @Async
    @ApiOperation("异步执行redis同步城市信息")
    public void asyncDistrict(){
        districtService.synchronizeDistrict();
    }


    @GetMapping("/findPid/{pid}")
    @ApiOperation("查询省｜市｜区｜街道 联动接口,0是总节点数据")
    public Result<List<DistrictVo>> findPid(@PathVariable Integer pid){
        List<DistrictVo> districtVos =   districtService.findPid(pid);
        return success(districtVos);
    }

    @GetMapping("/codeConversionName/{code}")
    @ApiOperation("根据code转移名称")
    public Result<String> codeConversionName(@PathVariable String code){
        return success(districtService.codeConversionName(code));
    }






}
