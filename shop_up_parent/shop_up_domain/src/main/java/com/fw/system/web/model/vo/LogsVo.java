package com.fw.system.web.model.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * 业务日志
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
@Data
public class LogsVo {

    /**
     * 主键
     */
    @ApiModelProperty("主键")
    private String id;

    /**
     * 业务编号
     */
    @ApiModelProperty("业务编号")
    private String buiId;

    /**
     * 类型
     */
    @ApiModelProperty("类型")
    private String isType;

    /**
     * 模块编号
     */
    @ApiModelProperty("模块编号")
    private String modelName;

    /**
     * 大文本记录内容
     */
    @ApiModelProperty("大文本记录内容")
    private String jsonTxt;

    /**
     * 自定义数据
     */
    @ApiModelProperty("自定义数据")
    private String formInfo;

    /**
     * 创建人 创建人
     */
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新人 更新人
     */
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;



}
