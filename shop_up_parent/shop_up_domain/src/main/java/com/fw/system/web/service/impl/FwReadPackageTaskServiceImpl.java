package com.fw.system.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fw.system.web.dao.FwReadPackageTaskMapper;
import com.fw.system.web.model.entity.FwReadPackageTask;
import com.fw.system.web.service.IFwReadPackageTaskService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 红包任务类型列表 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-09-03
 */
@Service
public class FwReadPackageTaskServiceImpl extends ServiceImpl<FwReadPackageTaskMapper, FwReadPackageTask> implements IFwReadPackageTaskService {

}
