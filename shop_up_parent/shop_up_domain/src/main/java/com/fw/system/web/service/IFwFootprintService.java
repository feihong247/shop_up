package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwFootprint;
import com.fw.system.web.model.vo.FootprintVo;

import java.util.List;

/**
 * <p>
 * 足迹表 服务类
 * </p>
 *
 * @author
 * @since 2021-06-23
 */
public interface IFwFootprintService extends IService<FwFootprint> {
    /**
     * 查询足迹
     *
     * @param id 足迹ID
     * @return 足迹
     */
    public FootprintVo selectFwFootprintById(String id);

    /**
     * 查询足迹列表
     *
     * @return 足迹集合
     */
    public List<FootprintVo> selectFwFootprintList(String userId);

    /**
     * 新增足迹
     *
     * @param itemId 足迹
     * @return 结果
     */
    public int insertFwFootprint(String itemId,String userId);

    /**
     * 修改足迹
     *
     * @param fwFootprint 足迹
     * @return 结果
     */
    public int updateFwFootprint(FwFootprint fwFootprint);

    /**
     * 批量删除足迹
     *
     * @param ids 需要删除的足迹ID
     * @return 结果
     */
    public int deleteFwFootprintByIds(String[] ids);

    /**
     * 删除足迹信息
     *
     * @param id 足迹ID
     * @return 结果
     */
    public int deleteFwFootprintById(String id);
}
