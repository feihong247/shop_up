package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwSku;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品sku规格表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwSkuMapper extends BaseMapper<FwSku> {

}
