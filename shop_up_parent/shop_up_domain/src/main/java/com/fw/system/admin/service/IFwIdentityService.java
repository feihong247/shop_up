package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwIdentity;

/**
 * 身份Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwIdentityService extends IService<FwIdentity>
{
    /**
     * 查询身份
     * 
     * @param id 身份ID
     * @return 身份
     */
    public FwIdentity selectFwIdentityById(String id);

    /**
     * 查询身份列表
     * 
     * @param fwIdentity 身份
     * @return 身份集合
     */
    public List<FwIdentity> selectFwIdentityList(FwIdentity fwIdentity);

    /**
     * 新增身份
     * 
     * @param fwIdentity 身份
     * @return 结果
     */
    public int insertFwIdentity(FwIdentity fwIdentity);

    /**
     * 修改身份
     * 
     * @param fwIdentity 身份
     * @return 结果
     */
    public int updateFwIdentity(FwIdentity fwIdentity);

    /**
     * 批量删除身份
     * 
     * @param ids 需要删除的身份ID
     * @return 结果
     */
    public int deleteFwIdentityByIds(String[] ids);

    /**
     * 删除身份信息
     * 
     * @param id 身份ID
     * @return 结果
     */
    public int deleteFwIdentityById(String id);
}
