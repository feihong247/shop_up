package com.fw.system.web.service.impl;

import com.fw.system.web.model.entity.FwService;
import com.fw.system.web.dao.FwServiceMapper;
import com.fw.system.web.service.IFwServiceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 在线客服 服务实现类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
@Service
public class FwServiceServiceImpl extends ServiceImpl<FwServiceMapper, FwService> implements IFwServiceService {

}
