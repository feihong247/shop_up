package com.fw.system.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.admin.domain.FwCurrencyLog;

/**
 * <p>
 * 通用日志记录表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-08-01
 */
public interface FwCurrencyLogMapper extends BaseMapper<FwCurrencyLog> {

}
