package com.fw.system.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fw.system.web.model.entity.FwLockStorage;

import java.util.List;


/**
 * <p>
 * 锁仓池 服务类
 * </p>
 *
 * @author
 * @since 2021-06-28
 */
public interface IFwLockStorageService extends IService<FwLockStorage> {

    //查询参与锁仓的分数不包含当前月
    List<FwLockStorage> listStorages();
}
