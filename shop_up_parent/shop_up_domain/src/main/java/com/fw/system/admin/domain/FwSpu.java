package com.fw.system.admin.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import com.fw.utils.RandomUtils;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 商品对象 fw_spu
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_spu")
@ApiModel(value="商品", description="商品表")
public class FwSpu extends Model<FwSpu>
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 商铺编号,自营另外区分 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "商铺编号")
    @TableField("shop_id")
    private String shopId;

    /** 商品标题 */
    @ApiModelProperty(value = "商品标题")
    @Excel(name = "商品标题")
    @TableField("title")
    private String title;

    /** 商品logo */
    @ApiModelProperty(value = "商品logo")
    @TableField("logo")
    private String logo;

    /** 商品销量 */
    @ApiModelProperty(value = "商品logo")
    @Excel(name = "商品销量")
    @TableField("tops")
    private Long tops;

    /** 商品原价格 */
    @ApiModelProperty(value = "商品原价格")
    @Excel(name = "商品原价格")
    @TableField("virtual_price")
    private BigDecimal virtualPrice;

    /** 商品现价格 */
    @ApiModelProperty(value = "商品现价格")
    @Excel(name = "商品现价格")
    @TableField("real_price")
    private BigDecimal realPrice = BigDecimal.ZERO;

    /** 商品利润值,后管看的 */
    @ApiModelProperty(value = "商品现价格")
    @Excel(name = "商品利润值")
    @TableField("price")
    private BigDecimal price = BigDecimal.ZERO;

    /** 商品消证,后管自营人员填写 */
    @ApiModelProperty(value = "商品利润值,后管看的")
    @Excel(name = "商品消证")
    @TableField("disappear")
    private BigDecimal disappear;

    /** 商品总库存 */
    @ApiModelProperty(value = "商品消证")
    @Excel(name = "商品总库存")
    @TableField("balance")
    private Long balance;

    /** 商品详情-富文本形式 */
    @ApiModelProperty(value = "商品总库存")
    @Excel(name = "商品详情")
    @TableField("infos")
    private String infos;

    /** 商品状态 0=启用，1=下架 */
    @ApiModelProperty(value = "商品状态 0=启用，1=下架")
    @Excel(name = "商品状态",readConverterExp = "0=启用,1=下架")
    @TableField("status")
    private Integer status;

    /** 商品运费 */
    @ApiModelProperty(value = "商品运费")
//    @Excel(name = "商品运费")
    @TableField("shipping")
    private BigDecimal shipping;

    /** 商品满额减运费 需要一套模版表,地区减额，单独商品减额，还是商家总商品减额等。 */
    @ApiModelProperty(value = "商品满额减运费")
//    @Excel(name = "商品满额减运费")
    @TableField("top_shipping")
    private BigDecimal topShipping;

    /** 是否热卖 */
    @TableField("hot_sale")
    @ApiModelProperty(value = "是否热卖")
    @Excel(name = "是否热卖",readConverterExp = "0=否,1=是")
    private Integer hotSale;

    /** 是否推荐 */
    @TableField("recommend")
    @ApiModelProperty(value = "是否推荐")
    @Excel(name = "是否推荐",readConverterExp = "0=否,1=是")
    private Integer recommend;

    /** 类目 */
    @TableField("category")
    @ApiModelProperty(value = "类目")
    @Excel(name = "类目")
    private String category;

    /** 是否特卖（0否1是） */
    @TableField("is_sale")
    @ApiModelProperty("是否特卖（0否1是）")
    @Excel(name = "是否特卖",readConverterExp = "0=否,1=是")
    private Integer isSale;

    /** 审批结果(0待审批1通过2驳回) */
    @TableField("approval_result")
    @ApiModelProperty("审批结果(0待审批1通过2驳回)")
    @Excel(name = "审批结果",readConverterExp = "0=待审批,1=通过,2=驳回")
    private Integer approvalResult;

    /** 驳回原因 */
    @TableField("result_info")
    @ApiModelProperty("驳回原因")
    @Excel(name = "驳回原因")
    private String resultInfo;


    /** 是否线下(0线上1线下) */
    @TableField("is_offline")
    @ApiModelProperty("是否线下(0否1是)")
    @Excel(name = "是否线下",readConverterExp = "0=否,1=是")
    private Integer isOffline;

    /** 是否竞猜(0线上1线下) */
    @TableField("is_quiz")
    @ApiModelProperty("是否竞猜(0否1是)")
    private Integer isQuiz = 0;

    /** 满减标准 */
    @TableField("full_standard")
    @ApiModelProperty("满减标准")
    private BigDecimal fullStandard;

    /** 满减金额 */
    @TableField("full_amount")
    @ApiModelProperty("满减金额")
    private BigDecimal fullAmount;

    /**
     * 创建人
     */
    @TableField("create_by")
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @TableField("update_by")
    @ApiModelProperty("更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    @ApiModelProperty("更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    @TableField("province")
    @ApiModelProperty("省")
    private String province;

    @TableField("city")
    @ApiModelProperty("市")
    private String city;

    @TableField("area")
    @ApiModelProperty("县")
    private String area;

    /** 规格列表 */
    @TableField(exist = false)
    @ApiModelProperty("规格列表")
    private List<FwSku> skuList = new ArrayList<>();

    /** 图片列表 */
    @TableField(exist = false)
    @ApiModelProperty("图片列表")
    private List<FwImages> images = new ArrayList<>();


}
