package com.fw.system.admin.mapper;

import java.util.List;

import com.fw.system.admin.domain.FwGuess;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 竞猜Mapper接口
 *
 * @author yanwei
 * @date 2021-06-23
 */
@Repository
public interface FwGuessMapper extends BaseMapper<FwGuess> {
    /**
     * 查询竞猜
     *
     * @param guessId 竞猜ID
     * @return 竞猜
     */
    FwGuess selectFwGuessById(String guessId);

    /**
     * 查询竞猜列表
     *
     * @param fwGuess 竞猜
     * @return 竞猜集合
     */
    List<FwGuess> selectFwGuessList(FwGuess fwGuess);

    /**
     * 新增竞猜
     *
     * @param fwGuess 竞猜
     * @return 结果
     */
    int insertFwGuess(FwGuess fwGuess);

    /**
     * 修改竞猜
     *
     * @param fwGuess 竞猜
     * @return 结果
     */
    int updateFwGuess(FwGuess fwGuess);

    /**
     * 删除竞猜
     *
     * @param guessId 竞猜ID
     * @return 结果
     */
    int deleteFwGuessById(String guessId);

    /**
     * 批量删除竞猜
     *
     * @param guessIds 需要删除的数据ID
     * @return 结果
     */
    int deleteFwGuessByIds(String[] guessIds);
}
