package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwReadPackageLog;

/**
 * 红包抽取记录Service接口
 * 
 * @author yanwei
 * @date 2021-10-12
 */
public interface IFwReadPackageLogService extends IService<FwReadPackageLog>
{
    /**
     * 查询红包抽取记录
     * 
     * @param id 红包抽取记录ID
     * @return 红包抽取记录
     */
    public FwReadPackageLog selectFwReadPackageLogById(String id);

    /**
     * 查询红包抽取记录列表
     * 
     * @param fwReadPackageLog 红包抽取记录
     * @return 红包抽取记录集合
     */
    public List<FwReadPackageLog> selectFwReadPackageLogList(FwReadPackageLog fwReadPackageLog);

    /**
     * 新增红包抽取记录
     * 
     * @param fwReadPackageLog 红包抽取记录
     * @return 结果
     */
    public int insertFwReadPackageLog(FwReadPackageLog fwReadPackageLog);

    /**
     * 修改红包抽取记录
     * 
     * @param fwReadPackageLog 红包抽取记录
     * @return 结果
     */
    public int updateFwReadPackageLog(FwReadPackageLog fwReadPackageLog);

    /**
     * 批量删除红包抽取记录
     * 
     * @param ids 需要删除的红包抽取记录ID
     * @return 结果
     */
    public int deleteFwReadPackageLogByIds(String[] ids);

    /**
     * 删除红包抽取记录信息
     * 
     * @param id 红包抽取记录ID
     * @return 结果
     */
    public int deleteFwReadPackageLogById(String id);
}
