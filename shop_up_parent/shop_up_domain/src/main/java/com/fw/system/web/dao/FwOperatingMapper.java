package com.fw.system.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwOperating;
import com.fw.system.web.model.vo.OperatingVo;

import java.util.List;

/**
 * <p>
 * 商家经营平台分类 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-19
 */
public interface FwOperatingMapper extends BaseMapper<FwOperating> {

    List<OperatingVo> findAll();
}
