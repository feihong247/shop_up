package com.fw.system.web.service.impl;

import com.fw.system.web.dao.FwTurnoverMapper;
import com.fw.system.web.model.entity.FwTurnover;
import com.fw.system.web.service.IFwTurnoverService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 营业额表,以此表为依据增长商甲 服务实现类
 * </p>
 *
 * @author
 * @since 2021-06-07
 */
@Service
public class FwTurnoverServiceImpl extends ServiceImpl<FwTurnoverMapper, FwTurnover> implements IFwTurnoverService {

}
