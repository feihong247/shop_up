package com.fw.system.web.dao;

import com.fw.system.web.model.entity.FwDisappearFreed;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 消证兑换表 Mapper 接口
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface FwDisappearFreedMapper extends BaseMapper<FwDisappearFreed> {

}
