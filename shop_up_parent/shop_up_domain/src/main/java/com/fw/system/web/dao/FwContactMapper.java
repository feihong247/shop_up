package com.fw.system.web.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fw.system.web.model.entity.FwContact;

import java.util.List;

/**
 * <p>
 * 关于我们 Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-05-24
 */
public interface FwContactMapper extends BaseMapper<FwContact> {
    /**
     * 查询关于我们
     *
     * @param id 关于我们ID
     * @return 关于我们
     */
    public FwContact selectFwContactById(String id);

    /**
     * 查询关于我们列表
     *
     * @param fwContact 关于我们
     * @return 关于我们集合
     */
    public List<FwContact> selectFwContactList(FwContact fwContact);

}
