package com.fw.system.web.model.form;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 购买商甲form
 */
@Data
@Api(tags = "购买商甲form")
public class ShopIngShangJiaForm {
    @ApiModelProperty("商甲交易记录id")
    private String id;
    @ApiModelProperty("购买单价")
    private BigDecimal money;

    @ApiModelProperty("购买数量")
    private BigDecimal count;

    @ApiModelProperty("用户编号(前端无需传)")
    private String userId;

}
