package com.fw.system.web.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 竞猜对象 fw_guess
 * 
 * @author yanwei
 * @date 2021-06-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_guess")
@ApiModel(value="竞猜", description="竞猜表")
public class FwGuess extends Model<FwGuess> {
    private static final long serialVersionUID = 1L;

    /** 竞猜id（房间号） */
    @ApiModelProperty(value = "竞猜id（房间号）")
    @TableId("guess_id")
    private String guessId;

    /** 房间人数 */
    @ApiModelProperty(value = "房间人数")
    @Excel(name = "房间人数")
    @TableField("people_number")
    private Integer peopleNumber;

    /** 商品Id */
    @ApiModelProperty(value = "商品Id")
    @Excel(name = "商品Id")
    @TableField("spu_id")
    private String spuId;

    /** 规格ID */
    @ApiModelProperty(value = "规格ID")
    @Excel(name = "规格ID")
    @TableField("sku_id")
    private String skuId;

    /** 购买数量 */
    @ApiModelProperty(value = "购买数量")
    @Excel(name = "购买数量")
    @TableField("buy_number")
    private Integer buyNumber;

    /** 胜利者 */
    @ApiModelProperty(value = "胜利者")
    @Excel(name = "胜利者")
    @TableField("victory_user_id")
    private String victoryUserId;

    /** 创建人 */
    @TableField("create_by")
    @ApiModelProperty(value = "创建人")
    private String createBy;

    /** 创建时间 */
    @TableField("create_time")
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /** 更新人 */
    @TableField("update_by")
    @ApiModelProperty(value = "更新人")
    private String updateBy;

    /** 更新时间 */
    @TableField("update_time")
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /** 竞猜id（房间号） */
    @ApiModelProperty(value = "新的房间号")
    @TableId("new_guess_id")
    private String newGuessId;

    /** 是否匹配（0否1是） */
    @ApiModelProperty(value = "是否匹配（0否1是）")
    @TableField("is_match")
    private Integer isMatch;

    /** PK时间（人齐了，或平局的时间） */
    @TableField("pk_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime pkTime;



    @ApiModelProperty(value = "起始页码")
    @TableField(exist = false)
    private  Integer pageNum = 0;

    @ApiModelProperty(value = "查询每页多少条")
    @TableField(exist = false)
    private  Integer pageSize = 10;

    @ApiModelProperty(value = "分享地址")
    @TableField(exist = false)
    private String shareUrl;

    @ApiModelProperty(value = "房间中的人")
    @TableField(exist = false)
    private List<FwUgJoin> ugList;

    @ApiModelProperty(value = "商品对象")
    @TableField(exist = false)
    private FwSpu fwSpu;

    @ApiModelProperty(value = "规格对象")
    @TableField(exist = false)
    private FwSku fwSku;

    @ApiModelProperty(value = "是否房主")
    @TableField(exist = false)
    private Integer isHomeowner;

    @ApiModelProperty(value = "是否结束")
    @TableField(exist = false)
    private Integer isOver;

    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime overTime;

    @ApiModelProperty(value = "调用者的用户id")
    @TableField(exist = false)
    private String userId;
}
