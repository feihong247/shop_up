package com.fw.system.admin.service.impl;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fw.common.IdXD;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwRuleDatasMapper;
import com.fw.system.admin.domain.FwRuleDatas;
import com.fw.system.admin.service.IFwRuleDatasService;

/**
 * 数据规则Service业务层处理
 *
 * @author yanwei
 * @date 2021-06-02
 */
@Service
public class FwRuleDatasServiceImpl extends ServiceImpl<FwRuleDatasMapper, FwRuleDatas> implements IFwRuleDatasService {
    @Autowired
    private FwRuleDatasMapper fwRuleDatasMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询数据规则
     *
     * @param id 数据规则ID
     * @return 数据规则
     */
    @Override
    public FwRuleDatas selectFwRuleDatasById(String id) {
        return fwRuleDatasMapper.selectFwRuleDatasById(id);
    }

    /**
     * 查询数据规则列表
     *
     * @param fwRuleDatas 数据规则
     * @return 数据规则
     */
    @Override
    public List<FwRuleDatas> selectFwRuleDatasList(FwRuleDatas fwRuleDatas) {
        return fwRuleDatasMapper.selectFwRuleDatasList(fwRuleDatas);
    }

    /**
     * 新增数据规则
     *
     * @param fwRuleDatas 数据规则
     * @return 结果
     */
    @Override
    public int insertFwRuleDatas(FwRuleDatas fwRuleDatas) {
        fwRuleDatas.setId(idXD.nextId());
        return fwRuleDatasMapper.insertFwRuleDatas(fwRuleDatas);
    }

    /**
     * 修改数据规则
     *
     * @param fwRuleDatas 数据规则
     * @return 结果
     */
    @Override
    public int updateFwRuleDatas(FwRuleDatas fwRuleDatas) {
        return fwRuleDatasMapper.updateFwRuleDatas(fwRuleDatas);
    }

    /**
     * 批量删除数据规则
     *
     * @param ids 需要删除的数据规则ID
     * @return 结果
     */
    @Override
    public int deleteFwRuleDatasByIds(String[] ids) {
        return fwRuleDatasMapper.deleteFwRuleDatasByIds(ids);
    }

    /**
     * 删除数据规则信息
     *
     * @param id 数据规则ID
     * @return 结果
     */
    @Override
    public int deleteFwRuleDatasById(String id) {
        return fwRuleDatasMapper.deleteFwRuleDatasById(id);
    }

    @Override
    public BigDecimal getBigDecimal(String ruleType) {
        return getOne(Wrappers.<FwRuleDatas>lambdaQuery().eq(FwRuleDatas::getRuleType, ruleType)).getRuleCount();
    }

    @Override
    public Integer getInteger(String ruleType) {
        return getBigDecimal(ruleType).intValue();
    }
}
