package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 商品图片列对象 fw_images
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_images")
@ApiModel(value="商品图片列", description="商品图片列")
public class FwImages extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 图片链接 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "图片链接")
    @TableField("link_url")
    private String linkUrl;

    /** 业务编号 */
    @ApiModelProperty(value = "图片链接")
    @Excel(name = "业务编号")
    @TableField("business_id")
    private String businessId;

    /** 列状态 0=禁用，1=启用 */
    @ApiModelProperty(value = "业务编号")
    @Excel(name = "列状态 0=禁用，1=启用")
    @TableField("status")
    private Integer status;




}
