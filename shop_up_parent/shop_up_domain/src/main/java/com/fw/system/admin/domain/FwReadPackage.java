package com.fw.system.admin.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 红包类型对象 fw_read_package
 * 
 * @author yanwei
 * @date 2021-10-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_read_package")
@ApiModel(value="红包类型", description="红包类型表")
public class FwReadPackage implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 红包名称 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "红包名称")
    @TableField("read_name")
    private String readName;

    /** 红包大小 */
    @ApiModelProperty(value = "红包名称")
    @Excel(name = "红包大小")
    @TableField("amount_num")
    private BigDecimal amountNum;

    /** 起始累加金额 */
    @ApiModelProperty(value = "红包大小")
    @Excel(name = "起始累加金额")
    @TableField("start_num")
    private BigDecimal startNum;

    /** 红包截止日期 */
    @ApiModelProperty(value = "起始累加金额")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "红包截止日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @TableField("end_time")
    private Date endTime;

    /** 0 不发布 1发布 2删除 */
    @ApiModelProperty(value = "红包截止日期")
    @Excel(name = "0 不发布 1发布 2删除")
    @TableField("is_state")
    private Long isState;

    /** 红包完成难易程度 1简单(完成一个任务加一元) 2中等(加一毛) 3困难(加一分) */
    @ApiModelProperty(value = "0 不发布 1发布 2删除")
    @Excel(name = "红包完成难易程度 1简单(完成一个任务加一元) 2中等(加一毛) 3困难(加一分)")
    @TableField("is_type")
    private Long isType;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time",exist = false)
    private Date createTime;



}
