package com.fw.system.admin.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 锁仓池对象 fw_lock_storage
 *
 * @author yanwei
 * @date 2021-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_lock_storage")
@ApiModel(value = "锁仓池", description = "锁仓池")
public class FwLockStorage extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /**
     * 用户编号
     */
    @ApiModelProperty(value = "用户编号")
    @Excel(name = "用户编号")
    @TableField("user_id")
    private String userId;

    /**
     * 锁仓数量
     */
    @ApiModelProperty(value = "锁仓数量")
    @Excel(name = "锁仓数量")
    @TableField("lock_count")
    private Long lockCount;
    /**
     * 锁仓数量
     */
    @ApiModelProperty(value = "技术锁仓数量")
    @Excel(name = "技术锁仓数量")
    @TableField("tec_lock_count")
    private Long tecLockCount;
    /**
     * 锁仓数量
     */
    @ApiModelProperty(value = "管理锁仓数量")
    @Excel(name = "管理锁仓数量")
    @TableField("man_lock_count")
    private Long manLockCount;

    /**
     * 锁仓时间
     */
    @ApiModelProperty(value = "锁仓时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "锁仓时间", width = 30, dateFormat = "yyyy-MM-dd")
    @TableField("lock_time")
    private Date lockTime;

    /**
     * 标记
     */
    @ApiModelProperty(value = "标记")
    @Excel(name = "标记")
    @TableField("flag")
    private String flag;

    /**
     * 备用字段
     */
    @ApiModelProperty(value = "备用字段")
    @Excel(name = "备用字段")
    @TableField("note")
    private String note;


    /** 上次兑换时间 */
    @ApiModelProperty(value = "上次兑换时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "上次兑换时间", width = 30, dateFormat = "yyyy-MM-dd")
    @TableField("release_time")
    private Date releaseTime;
}
