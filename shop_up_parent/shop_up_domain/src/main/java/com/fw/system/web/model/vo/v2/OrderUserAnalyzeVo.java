package com.fw.system.web.model.vo.v2;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel("订单分析大客户统计")
public class OrderUserAnalyzeVo implements Serializable {


    @ApiModelProperty("用户头像")
    private String userHeadImgUrl;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("用户手机号")
    private String phone;

     @ApiModelProperty("用户消费额")
    private BigDecimal customerVal;

}
