package com.fw.system.web.service;

import com.fw.enums.LogsModelEnum;
import com.fw.enums.LogsTypeEnum;
import com.fw.system.web.model.entity.FwLogs;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 业务日志 服务类
 * </p>
 *
 * @author
 * @since 2021-05-10
 */
public interface IFwLogsService extends IService<FwLogs> {

    void saveLogs(String createId, String buiId, String isType, String modelName, String jsonTxt, String formInfo);

    void saveLogs(String createId, String buiId, LogsTypeEnum logsTypeEnum, LogsModelEnum logsModel, String jsonTxt, String formInfo);

    FwLogs getLogs(String buiId,  LogsModelEnum logsModel,String ...logsTypeEnum);
    public void saveParentLogs(String createId,String parentId,String buiId,String isType,String modelName,String jsonTxt,String formInfo);
    /**
     * 年商甲产出
     * @return
     */
    public List<FwLogs> getShangJiaTotal(String buiId);
    /**
     * 年商甲产出
     * @return
     */
    public List<FwLogs> getShangJiaYearTotal(String buiId);

    /**
     * 月商甲产出
     * @return
     */
    public List<FwLogs> getShangJiaMonthTotal(String buiId);
    /**
     * 日商甲产出
     * @return
     */
    public List<FwLogs> getShangJiaDayTotal(String buiId);
    /**
     * 消费获取销证总数
     * @return
     */
    public List<FwLogs> getDisappearTotal(String buiId);

    /**
     * 消费获取线上销证总数
     * @return
     */
    public List<Map> getDisappearUpTotal(String buiId);
    /**
     *消费获取线下销证总数
     * @return
     */
    public List<Map> getDisappearDownTotal(String buiId);
    /**
     *消费获取线上销证明细
     * @return
     */
    public List<Map> getDisappearUpDetail(String buiId,String createTime);
    /**
     *消费获取线下销证明细
     * @return
     */
    public List<Map> getDisappearDownDetail(String buiId,String createTime);
    /**
     *分红
     * @return
     */
    public List<FwLogs> getBonusDetail(String buiId);
    /**
     * 获取上一次分红
     * @return
     */
    public List<FwLogs> getLastBonusDetail(String buiId);
    /**
     * 获取商甲总产出包括产出兑换
     * @return
     */
    public List<FwLogs> getShangJiaOutDetail();
    /**
     * 获取兑换未满24h的商甲数
     * @return
     */
    public List<FwLogs> getShangJiaLtOneDay(String buiId);

    /**
     * 获取已用商甲列表商甲数
     * @return
     */
    public List<FwLogs> getShangJiaUseList(String typeModel, String modelName);

    /**
     * 获取销毁商甲列表商甲数
     * @return
     */
    public List<FwLogs> getShangJiaDestoryList(String typeName, String modelName, String json);
}
