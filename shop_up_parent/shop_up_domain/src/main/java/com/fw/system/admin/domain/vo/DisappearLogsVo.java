package com.fw.system.admin.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel("消证日志")
public class DisappearLogsVo implements Serializable {
    @ApiModelProperty("用户编号")
    private String userId;

    @ApiModelProperty("用户手机号")
    private String userPhone;

    @ApiModelProperty("用户昵称")
    private String nickName;

    @ApiModelProperty("支付宝账号")
    private String aliPhone;

    @ApiModelProperty("支付宝姓名")
    private String aliName;

    @ApiModelProperty("今日释放量")
    private BigDecimal toDayOpen;

    @ApiModelProperty("今日获取量")
    private BigDecimal toDayGet;

    @ApiModelProperty("释放总量")
    private BigDecimal openCount;

    @ApiModelProperty("获取总量")
    private BigDecimal getCount;


}
