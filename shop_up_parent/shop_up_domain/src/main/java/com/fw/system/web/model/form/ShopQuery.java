package com.fw.system.web.model.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@ApiModel("商铺查询条件")
public class ShopQuery  implements Serializable {

    @ApiModelProperty("范围查询,m 为单位")
    private Integer dataRange;

    @ApiModelProperty("经营类目查询,给定类目编号")
    private  String[]  operatingIds;

    @ApiModelProperty("商铺标题查询")
    private String shopName;

    @ApiModelProperty("评分查询,1 - 5 星")
    private Integer rangeStart;

    @ApiModelProperty("按 人均消费 ， 100,150 or 0,50 or 300,10000")
    private String customSvgRange;

    /**
     * 纬度
     */
    @ApiModelProperty(value = "当前用户的纬度",required = true)
    @NotNull(message = "纬度 不可为空" )
    private BigDecimal latitude;

    /**
     * 经度
     */
    @ApiModelProperty(value = "当前用户的经度",required = true)
    @NotNull(message = "经度 不可为空")
    private BigDecimal longitude;





}
