package com.fw.system.admin.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fw.annotation.Excel;
import com.fw.core.domain.BaseEntity;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 首页轮播对象 fw_banner
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fw_banner")
@ApiModel(value="首页轮播", description="首页轮播")
public class FwBanner extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 轮播标题 */
    @ApiModelProperty(value = "${comment}")
    @Excel(name = "轮播标题")
    @TableField("banner_title")
    private String bannerTitle;

    /** 轮播封面 图片封面 */
    @ApiModelProperty(value = "轮播标题")
    @Excel(name = "轮播封面 图片封面")
    @TableField("banner_show")
    private String bannerShow;

    /** 轮播详情 */
    @ApiModelProperty(value = "轮播封面 图片封面")
    @Excel(name = "轮播详情")
    @TableField("banner_infos")
    private String bannerInfos;

    /** 排序 小的优先级高 */
    @ApiModelProperty(value = "轮播详情")
    @Excel(name = "排序 小的优先级高")
    @TableField("banner_order")
    private Long bannerOrder;




}
