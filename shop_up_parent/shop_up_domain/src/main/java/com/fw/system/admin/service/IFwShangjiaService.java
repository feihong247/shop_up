package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwShangjia;

/**
 * 商甲发行Service接口
 *
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwShangjiaService extends IService<FwShangjia>
{
    /**
     * 查询商甲发行
     *
     * @param id 商甲发行ID
     * @return 商甲发行
     */
    public FwShangjia selectFwShangjiaById(String id);

    /**
     * 查询商甲发行列表
     *
     * @param fwShangjia 商甲发行
     * @return 商甲发行集合
     */
    public List<FwShangjia> selectFwShangjiaList(FwShangjia fwShangjia);

    /**
     * 新增商甲发行
     *
     * @param fwShangjia 商甲发行
     * @return 结果
     */
    public int insertFwShangjia(FwShangjia fwShangjia);

    /**
     * 修改商甲发行
     *
     * @param fwShangjia 商甲发行
     * @return 结果
     */
    public int updateFwShangjia(FwShangjia fwShangjia);

    /**
     * 批量删除商甲发行
     *
     * @param ids 需要删除的商甲发行ID
     * @return 结果
     */
    public int deleteFwShangjiaByIds(String[] ids);

    /**
     * 删除商甲发行信息
     *
     * @param id 商甲发行ID
     * @return 结果
     */
    public int deleteFwShangjiaById(String id);

    /**
     * 公众大屏数据:大屏商户
     */
    public void shopScreenData();

    /**
     * 公众大屏数据:系统管理员大屏
     */
    public void ScreenData();
}
