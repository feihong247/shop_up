package com.fw.system.admin.service;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.fw.system.admin.domain.FwUljoin;

/**
 * 用户和身份中间Service接口
 * 
 * @author yanwei
 * @date 2021-05-10
 */
public interface IFwUljoinService extends IService<FwUljoin>
{
    /**
     * 查询用户和身份中间
     * 
     * @param id 用户和身份中间ID
     * @return 用户和身份中间
     */
    public FwUljoin selectFwUljoinById(String id);

    /**
     * 查询用户和身份中间列表
     * 
     * @param fwUljoin 用户和身份中间
     * @return 用户和身份中间集合
     */
    public List<FwUljoin> selectFwUljoinList(FwUljoin fwUljoin);

    /**
     * 新增用户和身份中间
     * 
     * @param fwUljoin 用户和身份中间
     * @return 结果
     */
    public int insertFwUljoin(FwUljoin fwUljoin);

    /**
     * 修改用户和身份中间
     * 
     * @param fwUljoin 用户和身份中间
     * @return 结果
     */
    public int updateFwUljoin(FwUljoin fwUljoin);

    /**
     * 批量删除用户和身份中间
     * 
     * @param ids 需要删除的用户和身份中间ID
     * @return 结果
     */
    public int deleteFwUljoinByIds(String[] ids);

    /**
     * 删除用户和身份中间信息
     * 
     * @param id 用户和身份中间ID
     * @return 结果
     */
    public int deleteFwUljoinById(String id);
}
