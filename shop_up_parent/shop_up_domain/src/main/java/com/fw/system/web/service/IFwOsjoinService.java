package com.fw.system.web.service;

import com.fw.system.web.model.entity.FwOsjoin;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单与spu中间表 服务类
 * </p>
 *
 * @author  
 * @since 2021-05-10
 */
public interface IFwOsjoinService extends IService<FwOsjoin> {

}
