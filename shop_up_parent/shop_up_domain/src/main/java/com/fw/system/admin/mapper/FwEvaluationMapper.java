package com.fw.system.admin.mapper;

import java.util.List;

import com.fw.system.admin.domain.FwEvaluation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 商品评价Mapper接口
 *
 * @author yanwei
 * @date 2021-05-24
 */
public interface FwEvaluationMapper extends BaseMapper<FwEvaluation> {
    /**
     * 查询商品评价
     *
     * @param id 商品评价ID
     * @return 商品评价
     */
    public FwEvaluation selectFwEvaluationById(String id);

    /**
     * 查询商品评价列表
     *
     * @param fwEvaluation 商品评价
     * @return 商品评价集合
     */
    public List<FwEvaluation> selectFwEvaluationList(FwEvaluation fwEvaluation);

    /**
     * 新增商品评价
     *
     * @param fwEvaluation 商品评价
     * @return 结果
     */
    public int insertFwEvaluation(FwEvaluation fwEvaluation);

    /**
     * 修改商品评价
     *
     * @param fwEvaluation 商品评价
     * @return 结果
     */
    public int updateFwEvaluation(FwEvaluation fwEvaluation);

    /**
     * 删除商品评价
     *
     * @param id 商品评价ID
     * @return 结果
     */
    public int deleteFwEvaluationById(String id);

    /**
     * 批量删除商品评价
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFwEvaluationByIds(String[] ids);
}
