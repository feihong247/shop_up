package com.fw.system.admin.service.impl;

import java.util.List;
import com.fw.common.IdXD;
import com.fw.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fw.system.admin.mapper.FwMsgMapper;
import com.fw.system.admin.domain.FwMsg;
import com.fw.system.admin.service.IFwMsgService;

/**
 * 用户消息Service业务层处理
 * 
 * @author yanwei
 * @date 2021-05-10
 */
@Service
public class FwMsgServiceImpl extends ServiceImpl<FwMsgMapper, FwMsg> implements IFwMsgService
{
    @Autowired
    private FwMsgMapper fwMsgMapper;

    @Autowired
    private IdXD idXD;

    /**
     * 查询用户消息
     * 
     * @param id 用户消息ID
     * @return 用户消息
     */
    @Override
    public FwMsg selectFwMsgById(String id)
    {
        return fwMsgMapper.selectFwMsgById(id);
    }

    /**
     * 查询用户消息列表
     * 
     * @param fwMsg 用户消息
     * @return 用户消息
     */
    @Override
    public List<FwMsg> selectFwMsgList(FwMsg fwMsg)
    {
        return fwMsgMapper.selectFwMsgList(fwMsg);
    }

    /**
     * 新增用户消息
     * 
     * @param fwMsg 用户消息
     * @return 结果
     */
    @Override
    public int insertFwMsg(FwMsg fwMsg)
    {
        fwMsg.setCreateTime(DateUtils.getNowDate());
        fwMsg.setId(idXD.nextId());
        return fwMsgMapper.insertFwMsg(fwMsg);
    }

    /**
     * 修改用户消息
     * 
     * @param fwMsg 用户消息
     * @return 结果
     */
    @Override
    public int updateFwMsg(FwMsg fwMsg)
    {
        fwMsg.setUpdateTime(DateUtils.getNowDate());
        return fwMsgMapper.updateFwMsg(fwMsg);
    }

    /**
     * 批量删除用户消息
     * 
     * @param ids 需要删除的用户消息ID
     * @return 结果
     */
    @Override
    public int deleteFwMsgByIds(String[] ids)
    {
        return fwMsgMapper.deleteFwMsgByIds(ids);
    }

    /**
     * 删除用户消息信息
     * 
     * @param id 用户消息ID
     * @return 结果
     */
    @Override
    public int deleteFwMsgById(String id)
    {
        return fwMsgMapper.deleteFwMsgById(id);
    }
}
